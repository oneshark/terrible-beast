﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBreakable : MonoBehaviour {
	public string spawnString;
	public List<ItemDrops> itemDrops = new List<ItemDrops> ();
	public float hp;

	void OnEnable ()
	{
		EventManager.OnGuillotineDropped += Spawn;
	}

	void OnDisable ()
	{
		EventManager.OnGuillotineDropped -= Spawn;
	}


	public void Spawn(){
		if (!PhotonNetwork.isMasterClient)
			return;
		Debug.Log ("Spawn Box");
		object[] args = new object[1]; // Put our data in an object array, to send

		GameObject newObj = PhotonNetwork.InstantiateSceneObject (spawnString, transform.position, Quaternion.identity, 0, args);
		EnemyNetwork en = newObj.GetComponent<EnemyNetwork> ();
		en.itemDrops = itemDrops;//replace itemdrops
		en.baseEntityStat.hpMax = hp;
		en.baseEntityStat.hp = hp;
		en.Initialize ();
	}
}
