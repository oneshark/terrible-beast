﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorPopUp : MonoBehaviour
{

	void OnCollisionEnter (Collision collision)
	{
	Debug.Log("Fence - On Collision Enter");
		if (!isCool)
			return;

		StartCoroutine ("CollideCooldown");

		//Do the pop up here if it's your player
		//Debug.Log ("TAG:" + collision.gameObject.tag);
//		GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
//		PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
//		puc.SetPopup ("Need a", "key!");
//		puc.SetRejectIcon ();
//		puc.SetTextColor ("white");
//		nextObj.SetActive (true);
		AudioManager.Instance.PlayClip ("rejection", true, false);

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Need a", "key!");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}

	bool isCool = true;

	IEnumerator CollideCooldown ()
	{
		isCool = false;
		yield return new WaitForSeconds (4);
		isCool = true;
	}
}
