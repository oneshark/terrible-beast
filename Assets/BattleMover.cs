﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class BattleMover : MonoBehaviour
{
    //	[HideInInspector]
    //	public bool hasFocus = true;
    public Image filledRollCircle;

    public float walkSpeedHorizontal = 1;
    public float walkSpeedVertical = 1;
    public float flySpeedVertical = 0;

    AdventurerNetwork atn;
    Rigidbody2D m_Body;
    PhotonView m_PhotonView;

    void Start()
    {
        atn = GetComponent<AdventurerNetwork>();
        Invoke("SetWalkSpeeds", 2);
    }

    void Awake()
    {
        m_Body = GetComponent<Rigidbody2D>();
        m_PhotonView = GetComponent<PhotonView>();
    }

    void Update()
    {
        
        if (!m_PhotonView.isMine) return;
        
      

        //CHOOSE ANIMATION
        ChooseAnimation();
        //CHOOSE FACING
            currentFill += Time.deltaTime;
            FillImage();
    }

    float currentFill = 0;
    public void FillImage()
    {
        //turn off the fill if this is not our character
        if (!m_PhotonView.isMine)
        {
            filledRollCircle.fillAmount = 0;
        }

        float fillAmount = currentFill / RollCoolTime;
        if (currentFill >= 2.5f) fillAmount = 0;
        filledRollCircle.fillAmount = fillAmount;
    }

    void SetWalkSpeeds()
    {
        Debug.Log("equipped weapon walk speed:" + atn.equippedWeapon.stat.walkSpeed);
        Item thisWeap = InventoryManager.Instance.GetItem(ItemType.weapon, GameManager.Instance.equippedWeapon.ID);
		float currentWalkSpeed = atn.baseEntityStat.walkSpeed;
		currentWalkSpeed += thisWeap.stat.walkSpeed;

		//ADD IN SKILL TREE SPEEDS HERE ALSO

		walkSpeedVertical = currentWalkSpeed;
		walkSpeedHorizontal = currentWalkSpeed*1.25f;
    }

    void FixedUpdate()
    {
        if (m_PhotonView.isMine == false)
        {
            return;
        }

        bool canMove = true;
        if (atn == null) atn = GetComponent<AdventurerNetwork>();
        if (atn.charState == CharacterState.dead) canMove = false;
        if (Chat.Instance.chatInputCustom.isFocused) canMove = false;
        if (!GameManager.Instance.InputEnabled) canMove = false;
        if (GameManager.Instance.waitingOnGuillotine) canMove = false;

        if (canMove)
        {
            UpdateMovement();
            ListenKeys();
            CallFacing();
        }
    }

    void UpdateMovement()
    {
        if (atn.charState == CharacterState.dead)
        {
            m_Body.velocity = new Vector3(0, 0, 0);
            return;
        }

        Vector3 movementVelocity = m_Body.velocity;

        if (Input.GetAxisRaw("Horizontal") > 0.5f)
        {
            movementVelocity.x = walkSpeedHorizontal;
        }
        else if (Input.GetAxisRaw("Horizontal") < -0.5f)
        {
            movementVelocity.x = -walkSpeedHorizontal;

        }
        else
        {
            movementVelocity.x = 0;
        }

        if (Input.GetAxisRaw("Vertical") > 0.5f)
        {
            movementVelocity.y = walkSpeedVertical;
        }
        else if (Input.GetAxisRaw("Vertical") < -0.5f)
        {
            movementVelocity.y = -walkSpeedVertical;
        }
        else
        {
            movementVelocity.y = 0;
        }

        //PAUSE DURING ATTACK
        if (atn.adventurerAnimation.currentAnimation.Contains("Attack"))
        {
            movementVelocity.x = 0;
            movementVelocity.y = 0;
        }

        //IsRolling dont allow change of direction
        if (isRolling)
        {
            if (atn.facing == 0 & movementVelocity.x > 0)
                movementVelocity.x = 0;
            if (atn.facing == 1 & movementVelocity.x < 0)
                movementVelocity.x = 0;
        }


        float xPos = transform.position.x + (movementVelocity.x * walkSpeedHorizontal * .15f);
        float yPos = transform.position.y + (movementVelocity.y * walkSpeedVertical * .15f);

        transform.position = new Vector3(xPos, yPos, transform.position.z);
        m_Body.velocity = movementVelocity;
    }

    void ListenKeys()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Input.GetAxisRaw("Horizontal") == 0)
                return;
            StartRoll();
        }
    }

    public float rollForce = 200;
    bool isRollCool = true;
    public bool isRolling = false;
    public float RollCoolTime = 2;
    void StartRoll()
    {
        //COOLDOWN SO WE CANT REPEAT CALL ROLL
        if (!isRollCool)
            return;
        isRollCool = false;
        Invoke("RollCooldown", RollCoolTime);

        isRolling = true;
        currentFill = 0;

        //add a force to it, then lock motion
        if (atn.facing == 0)
        {
            atn.rb.AddForce(Vector2.left * rollForce, ForceMode2D.Force);
        }
        else
        {
            atn.rb.AddForce(Vector2.right * rollForce, ForceMode2D.Force);
        }
        AudioManager.Instance.PlayClip("roll", true, false);

        atn.adventurerAnimation.SetAnimation(atn.stance + "-Roll", true, false);
        //	walkSpeedHorizontal = walkSpeedHorizontal * 2;
    }

    void RollCooldown()
    {
        isRollCool = true;
    }


    public void ChooseAnimation()
    //Walk or run
    {
        if (atn.charState == CharacterState.dead) return;
        if (isRolling)
            return;

        if (!InventoryCanvas.Instance.equipmentOpen & !GameManager.Instance.InputEnabled)
        {
            return;
        }
        if (Mathf.Abs(m_Body.velocity.x) < .001f & Mathf.Abs(m_Body.velocity.y) < 0.001f)
        {
            atn.adventurerAnimation.SetAnimation(atn.stance + "-Idle", true, false);
        }
        else
        {
            atn.adventurerAnimation.SetAnimation(atn.stance + "-Run1", true, false);
        }
        //}
    }

    public void CallFacing()
    {
        //	Debug.Log("FACING:"+m_Body.velocity.x);
        if (m_Body.velocity.x < -0.01f)
        {
            atn.SetFacing(0);
        }
        if (m_Body.velocity.x > 0.01f)
        {
            atn.SetFacing(1);
        }
    }
}


