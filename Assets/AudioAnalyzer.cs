﻿using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class AudioAnalyzer : MonoBehaviour
{
	public float highestVal=0;
	public float speakThreshhold=0.1f;
	public bool isSPeaking=false;
	//public Transform CubeLoc;

	void Update()
	{
		float[] spectrum = new float[256];

		AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);

		float highestValueInFrame = 0;

		for (int i = 1; i < spectrum.Length - 1; i++)
		{
			//Debug.Log ("data:" + spectrum [i]);
			if (spectrum[i]>highestValueInFrame)highestValueInFrame=spectrum[i];
		}
		Debug.Log ("HighestValInFrame:" + highestValueInFrame);
		//CubeLoc.transform.position=new Vector3(0,highestValueInFrame,0);

		if (highestValueInFrame > speakThreshhold) {
			Debug.Log ("IsSPeaking=false");
			isSPeaking = false;
		} else {
			isSPeaking = true;
		}
	}
}
