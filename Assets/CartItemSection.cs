﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CartItemSection : MonoBehaviour {
	public Item associatedItem;
	public Text itemName, crownsCost;
	public Image icon;

	public void SetItem(Item item){
		associatedItem = item;
		itemName.text = associatedItem.name;
		crownsCost.text = associatedItem.crownCost.ToString();
		icon.sprite = associatedItem.icon;
	}

	public void Buy(){
		if (GameManager.Instance.crowns < associatedItem.crownCost) {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Not Enough", "Crowns");
			puc.SetSoulIcon ();
			puc.SetTextColor ("blue");
			nextObj.SetActive (true);
			return;
		}


			InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
			InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);


		GameManager.Instance.crowns -= associatedItem.crownCost ;
		CrownBuyItemsCanvas.Instance.SetCrownsText ();

		AudioManager.Instance.PlayClip ("invasion_unlock", true, false);
		GameObject feathers = ObjectPool.Instance.NextObject ("CrownFeathers");
		feathers.transform.position = CrownBuyItemsCanvas.Instance.mannequin.transform.position;
		feathers.SetActive (true);

		CrownBuyItemsCanvas.Instance.shoppingCartItems.Remove (associatedItem);
		CrownBuyItemsCanvas.Instance.crownsTotalText.text = CrownBuyItemsCanvas.Instance.CalcCrownTotal ().ToString ();

				gameObject.SetActive (false);
	}

}
