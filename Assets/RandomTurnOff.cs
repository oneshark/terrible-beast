﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTurnOff : MonoBehaviour {
	public float percChanceToShow = .1f;
	// Use this for initialization
	void Start () {
		float chance=Random.Range (0, 1f);	
		if (chance < percChanceToShow) {
		} else {
			gameObject.SetActive (false);
		}
	}

}
