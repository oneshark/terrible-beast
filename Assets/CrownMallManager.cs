﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CrownMallManager : MonoBehaviour
{
		private static CrownMallManager _instance;
	public static CrownMallManager Instance
	{
		get
		{
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = FindObjectOfType<CrownMallManager>();
			return _instance;
		}
	}
	public GameObject CrownMallGraphics;

	public Text crownsText;
	public List<NPCShop> npcShops = new List<NPCShop>();
	public bool isOpen = false;
	public Animator anim;
	CanvasTools ct;

	void Awake()
	{
		ct = GetComponent<CanvasTools> ();
	}

	void Start(){
		InvokeRepeating ("CheckClaim", 2, 2);
	}

	public void SetCrownsText(){
		crownsText.text = GameManager.Instance.crowns.ToString ();
	}

	public void Close(){
		AudioManager.Instance.PlayClip ("click", true, false);

		ToggleMe (false);

	}

	public void BuyItems(){
		//Debug.Log ("BuyItems");
		AudioManager.Instance.PlayClip ("click", true, false);
		ToggleMe (false);
		CrownBuyItemsCanvas.Instance.ToggleMe (true);
	}

	public void BuyCrowns(){
		//Debug.Log ("BuyCrowns");
		AudioManager.Instance.PlayClip ("click", true, false);
		Application.OpenURL ("http://tod.dx.am/Main/TerribleBeastStore/index.php?PlayerName="+GameManager.Instance.PlayerName);
	}

	public void ToggleMe(bool b)
	{
		if (b == isOpen) return;
		if (b)
		{
			SetCrownsText ();
			ct.CallFadeCanvas (1);
			GameManager.Instance.InputEnabled = false;
			NPCManager.Instance.ToggleNPCCanvas(false);
			CrownMallGraphics.SetActive (true);
			TownCanvas.Instance.ToggleSoulText(false);
			TownCanvas.Instance.ToggleMe (false);
		}
		else
		{
			ct.CallFadeCanvas (0);
			GameManager.Instance.InputEnabled = true;

			CrownMallGraphics.SetActive (false);
			TownCanvas.Instance.ToggleSoulText(true);
			TownCanvas.Instance.ToggleMe (true);
			MenuCanvas.Instance.ToggleMe (true);
		}
		isOpen = b;
	}


	void CheckClaim(){
		if (!isOpen & !CrownBuyItemsCanvas.Instance.isOpen){
			//Debug.Log ("Check Claim but cancel");
			return;//dont do this if menu closed
		}
		Debug.Log ("Check Claim!");
		Dictionary<string, string> formPost = new Dictionary<string, string> ();
		formPost.Add ("playername", GameManager.Instance.PlayerName);

		StartCoroutine (HandleWWWClaimItems ("http://tod.dx.am/Main/TerribleBeastStore/claim_items.php", formPost));

	}

	/// <summary>
	/// CLAIM MALL CROWNS AND WHATEVER ELSE THEY BUY
	/// </summary>
	public List<string> itemsClaimed ;
	IEnumerator HandleWWWClaimItems (string url, Dictionary<string, string> post)
	{
		//Debug.Log("HandleWWWPOSTItems");
		WWWForm form = new WWWForm ();
		//Debug.Log ("ClaimItems");
		foreach (KeyValuePair<String, String> post_arg in post) {
			form.AddField (post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW (url, form);
		yield return www;
		//Debug.Log ("www return:" + www.text);
		itemsClaimed = www.text.Split(';').ToList<string>();

//		for (int inc = 0; inc < itemsClaimed.Count; inc++) {
//			Debug.Log ("loop:"+inc+" item:"+itemsClaimed [inc]);
//			if (itemsClaimed [inc] == "")
		itemsClaimed.Remove("");
//			}

		//FIRST RESULT IS ALWAYS EMPTY 
		if (itemsClaimed.Count>0)InventoryManager.Instance.AddClaimedItems (itemsClaimed);

		//REWARD PARTICLES

	}
}
