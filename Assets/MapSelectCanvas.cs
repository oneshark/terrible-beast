﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapSelectCanvas : MonoBehaviour {
    public GameObject[] mapPanels;
    public Text mapTitle;
	CanvasTools ct;
	private static MapSelectCanvas _instance;
	public static MapSelectCanvas Instance
	{
		get
		{
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = FindObjectOfType<MapSelectCanvas>();
			return _instance;
		}
	}

	void Awake(){
		ct = GetComponent<CanvasTools> ();
	}

	public void Close(){
		AudioManager.Instance.PlayClip ("click", true, false);

		ToggleMe (false);
	}

    public void SetPanel(int i)
    {
        Debug.Log("MapSelectCanvas.SetPanel:" + i);
        switch (i)
        {
            case 0:
                mapTitle.text = "When All Hope Is Lost";
                break;
            case 1:
                mapTitle.text = "The Blasphemous Moonlight Ruins";
                break;
		case 2:
			mapTitle.text = "Misadventures In The River Of God";
			break;
        }

        for (int inc = 0; inc < mapPanels.Length; inc++)
        {
         //   Debug.Log("Inc:" + inc+ " i:"+i);
            if (inc == i)
            {
                mapPanels[inc].SetActive(true);//only set the request panel active, turn off others
            }
            else
            {
                mapPanels[inc].SetActive(false);
            }
        }
    }


	public bool isOpen=false;
	public void ToggleMe(bool b)
	{
		if (b == isOpen) return;
		if (b)
		{
			GameManager.Instance.InputEnabled = false;
			ct.CallFadeCanvas (1);
			Face newFace = InventoryManager.Instance.GetFace (GameManager.Instance.equippedFace);
            
			NPCManager.Instance.ToggleNPCCanvas(false);
			TownCanvas.Instance.ToggleSoulText(false);
			TownCanvas.Instance.ToggleMe (false);
			MenuCanvas.Instance.ToggleMe (false);
            EventManager.Instance.CallMapRefreshed();//refresh all the map tiles to lock or unlock based on what we've completed
		}
		else
		{
			ct.CallFadeCanvas (0);
			GameManager.Instance.InputEnabled = true;
			TownCanvas.Instance.ToggleSoulText(true);
			TownCanvas.Instance.ToggleMe (true);
			MenuCanvas.Instance.ToggleMe (true);
		}
		isOpen = b;
	}

	public void MapTileClicked(string mapName){
		//NEED THIS STUFF
			LoadingManager.Instance.CloseAllPanels ();
			LoadingManager.Instance.CallFadeCanvas (1);
			LoadingManager.Instance.StartLobbyTimeout ();
		MapSelectCanvas.Instance.ToggleMe (false);
				GameManager.Instance.selectedMap = mapName;
				if (mapName == "QuickStart") {
					LobbyCanvas.Instance.quickJoinTry = 0;
					PhotonNetwork.LeaveRoom ();
					return;
				}

		NotifyOthersAboutLeaving (mapName);
				GameManager.Instance.CallLobby ();
		ToggleMe (false);
	}

	public void NotifyOthersAboutLeaving(string _mapName){
		Debug.Log ("Notify that you're leaving! MapSelectCanvas.cs");
		_mapName=GameManager.Instance.GetFormattedTerm (_mapName);
		TownManager.Instance.myAdventurerTownNetwork.photonView.RPC("CallChatLog", PhotonTargets.All,"["+ GameManager.Instance.PlayerName+"]" + " joined ["+_mapName+"] lobby.");

		//string msg = 
		//BattleManager.Instance.myAdventurer.photonView.RPC("CallChatLog",PhotonTargets.All,msg);

	}
}

