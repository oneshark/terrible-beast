﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Mannequin : AnimationController
{
	public GameObject particleLeftHand, particleRightHand;

	new void Start ()
	{
		base.Start ();
	}


	string currentAnimation="";
	public override void SetAnimation (string animation, bool isLoop, bool isOnComplete)
	{
		base.SetAnimation (animation, isLoop, isOnComplete);
		currentAnimation = animation;
	}

	public void SetParticles (Item _item)
	{
		bool isLeftVisi = true;
		bool isRightVisi = true;

		if (_item == null) {
			isLeftVisi = false;
			isRightVisi = false;
		} else {
			//Debug.Log ("Trying to set it to:" + _item.weaponParticles);
			if (_item.weaponParticles == "") {
				isLeftVisi = false;
				isRightVisi = false;
			}
		}

		//Turn off all the onehanders
		if (_item.stance == Stance.Onehander) {
			isRightVisi = false;
		}
		if (_item.stance == Stance.Bow) {
			isRightVisi = false;
		}
		if (_item.stance == Stance.Cannon) {
			isRightVisi = false;
		}
		if (_item.stance == Stance.Rod) {
			isLeftVisi = false;
		}

		// Debug.Log("VISIBILITY:" + isVisi);


		particleLeftHand.SetActive (isLeftVisi);
		particleRightHand.SetActive (isRightVisi);

		//try to set correct color
		if (isLeftVisi) {
			WeaponParticleSelector wps = particleLeftHand.GetComponent<WeaponParticleSelector> ();
			wps.ToggleParticleByColor (_item.weaponParticles);
		}
		if (isRightVisi) {
			WeaponParticleSelector wps = particleRightHand.GetComponent<WeaponParticleSelector> ();
			wps.ToggleParticleByColor (_item.weaponParticles);
		}

	}

	public void SetSkin (string skin)
	{
		// Debug.Log("SetSkin:"+skin);
		SkeletonAnimation skeletonAnimation = GetComponentInChildren<SkeletonAnimation> ();
		if (skin == "") {
			//    Debug.Log("SetSkin = ''");
			skin = "Default";
		}

		try {
			skeletonAnimation.skeleton.SetSkin (skin);
		} catch (System.Exception ex) {
			Debug.Log (ex);
		}
	}

	public void SetAttachment (string slotName, string slotName2, string attachmentName, string attachmentName2)
	{
			Debug.Log (" SlotName:" + slotName + " attachmentName:" + attachmentName);
			Debug.Log (" SlotName:" + slotName2 + " attachmentName:" + attachmentName2);
		SkeletonAnimation skeletonAnimation = GetComponentInChildren<SkeletonAnimation> ();

		//slot 1
		try {
			skeletonAnimation.skeleton.SetAttachment (slotName, attachmentName);
		} catch (System.Exception ex) {
			Debug.Log (ex);
		}

		//slot 2
		if (slotName2 == "NOSLOT")
			return;
		try {
			skeletonAnimation.skeleton.SetAttachment (slotName2, attachmentName2);
		} catch (System.Exception ex) {
			Debug.Log (ex);
		}
	}

//	string sampleText="";
//	void OnGUI ()
//	{
//		sampleText = GUI.TextArea (new Rect (230, 0, 120, 30), sampleText, 200);
//
//		//COLUMN ONE
//		if (GUI.Button (new Rect (350, 0, 120, 30), "Set Weap")) {
//			int _id = 0;
//			Int32.TryParse (sampleText, out _id);
//			SetWeapon (_id);
//		}
//		if (GUI.Button (new Rect (350, 30, 120, 30), "Set Cost")) {
//			int _id = 0;
//			Int32.TryParse (sampleText, out _id);
//			SetCostume (_id);
//		}
//	}

	public void SetWeapon(int _id){
		Item newItem = InventoryManager.Instance.GetItem (ItemType.weapon, _id);
		SetAttachment(newItem.slotName, newItem.slotName2, newItem.atachmentName, newItem.attachmentName2);
		string stanceName = newItem.stance.ToString ();
		SetAnimation (stanceName + "-Idle", true, false);
	}

	public void SetCostume(int _id){
		Item newItem = InventoryManager.Instance.GetItem (ItemType.costume, _id);
		if (newItem.costumeType == CostumeType.body) {
			SetSkin (newItem.skin);
		} else {
			SetAttachment (newItem.slotName, newItem.slotName2, newItem.atachmentName, newItem.attachmentName2);
		}
		string stanceName = newItem.stance.ToString ();
	//	SetAnimation (stanceName + "-Idle", true, false);
	}

}
