﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

public class SkillTreeNode : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [Header("What must be unlocked before this one is available?")]
    public int prerequisiteSkillId;
    [Header("What skill is this node for?")]
    public int skillId;
	public bool forceUnlocked=false;
	//public bool locked=true;
	public bool owned=false;
    public Image img;


	public Color colorLocked, colorUnlocked;

	void OnEnable()
	{
		EventManager.OnSkillTreeRefreshed += OnSkillTreeRefreshed;
	}

	void OnDisable()
	{
		EventManager.OnSkillTreeRefreshed += OnSkillTreeRefreshed;
	}

	void OnSkillTreeRefreshed(){
		Debug.Log ("Hey im a node, and i know this is refreshed");
        CheckOwned();
	}


    public void OnPointerClick(PointerEventData eventData)
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SkillTreeCanvas.Instance.skillTreeToolTip.SetSkill(skillId);
        SkillTreeCanvas.Instance.skillTreeToolTip.ToggleMe(true);
        Debug.Log("open tooltip");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SkillTreeCanvas.Instance.skillTreeToolTip.ToggleMe(false);
        Debug.Log("close tooltip");
    }

    public void ClickedBuy()
    {
        if (HasPrerequisiteSkillId())
        {
            Debug.Log("You Bought Skill");
        }else
        {
            Debug.Log("This Skill Locked");
        }
    }

    public bool HasPrerequisiteSkillId()
    {
        //check if we own the skill required before we can buy this skill

        return true;
    }

    public void CheckOwned()
    {
        
        Skill s = GameManager.Instance.skillsOwned.Where(x => x.skillId == skillId).FirstOrDefault();
        if (s != null)
        {
            owned = true;
            img.color = colorUnlocked;
        }
        else
        {
            owned = false;
            img.color = colorLocked;
        }

    }

}
