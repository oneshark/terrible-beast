﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundWithinRange : MonoBehaviour {
	//public string audiostring;
    AudioSource audioSource;
	void Awake(){
		audioSource = GetComponent<AudioSource> ();
		audioSource.volume = 0;
	}

   public bool isPlaying = false;
    public bool isLooping = false;
    private void Update()
    {
        isPlaying = audioSource.isPlaying;
        isLooping = audioSource.loop;
    }

    void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Adventurer") {
			if (other.GetComponent<AdventurerNetwork> () == BattleManager.Instance.myAdventurer)
			//	AudioManager.Instance.PlayClip (audiostring, true, true);
              StartCoroutine(AudioFadeIn(audioSource, AudioManager.Instance.getSFXVolume()));

        }

    }

	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "Adventurer") {
			if (other.GetComponent<AdventurerNetwork> () == BattleManager.Instance.myAdventurer) {
				//AudioManager.Instance.StopClip (audiostring);
                   StartCoroutine(AudioFadeOut(audioSource, 5));

            }
        }
	}

    public bool isFadingIn, isFadingOut;

    public IEnumerator AudioFadeOut(AudioSource audioSource, float FadeTime)
    {
        //   StartCoroutine(AudioFadeOut(audioSource, 5));
        isFadingIn = false;
        isFadingOut = true;
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            Debug.Log("FadingOut");
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
            if (!isFadingOut) break;
        }

        //audioSource.volume = startVolume;
    }

    public IEnumerator AudioFadeIn(AudioSource audioSource, float fullVolume)
    {
        //  StartCoroutine(AudioFadeIn(audioSource, AudioManager.Instance.getSFXVolume()));
        isFadingIn = true;
        isFadingOut = false;
        if (!audioSource.isPlaying)
        {
            //audioSource.PlayOneShot(audioSource.clip);
            audioSource.Play();// (audioSource.clip);

        }
        while (audioSource.volume < fullVolume)
        {
            audioSource.volume += .015f;
            yield return null;
            if (!isFadingIn) break;
        }
    }



}
