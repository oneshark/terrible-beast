﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableSprites : MonoBehaviour {

	void Awake(){
		GetComponent<SpriteRenderer> ().enabled = false;
	}
}
