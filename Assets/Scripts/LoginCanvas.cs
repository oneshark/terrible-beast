﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class LoginCanvas : MonoBehaviour {
    public InputField loginName;
    public SaveData saveData;

    void OnEnable()
    {
        EventManager.OnLoadComplete += OnLoadComplete;
    }

    void OnDisable()
    {
        EventManager.OnLoadComplete -= OnLoadComplete;

    }

    void OnLoadComplete()
    {
        Debug.Log("Game was loaded");
      //  SceneManager.LoadScene("Gameplay");
    }

    public void Play()
    {
        SceneManager.LoadScene("Gameplay");
    }


}
