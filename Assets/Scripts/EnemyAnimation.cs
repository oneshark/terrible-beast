﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class EnemyAnimation : AnimationController
{
    EnemyNetwork enemyNetwork;
    public string currentAnimation;
    public int maxAttacks;
	public bool areaDamageType = false;

    new void Start()
    {
        enemyNetwork = GetComponentInParent<EnemyNetwork>();
        base.Start();
        // base.skeletonAnimation.loop = false;
        maxAttacks = base.getMaxAttacks();
    }

    public void SyncAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        ///END OF TEST BLOCK
        if (animation == currentAnimation) return;//no need to try setting repeated animation of same kind
        base.SetAnimation(animation, isLoop, isOnComplete);
    }

    public override void SetAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        //Debug.Log("enemyAnimation:" + animation);
        if (animation == "Hit")
        {
            if (!currentAnimation.Contains("Idle")) return;//Only can play hit whne you're in Idle!
            enemyNetwork.photonView.RPC("AttackGraphic", PhotonTargets.Others);
            enemyNetwork.AttackGraphic();
        }
        
        //DEATH CHECKS
        if (currentAnimation.Contains("Death"))
        {
            return;  //death is permanent
        }

        //GET RANDOM DEATH
        if (animation == "Death")
        {
            int deathNumber = Random.Range(0, GetMaxDeaths());
            if (deathNumber > 1)
            {
                animation = animation + deathNumber.ToString();
            }
            AudioManager.Instance.PlayClip(enemyNetwork.enemyDeathSound);
            enemyNetwork.photonView.RPC("TurnOffCollider", PhotonTargets.All);
            if (enemyNetwork.deathParticleName != "None")
            {
                Debug.Log("Try calling death particle");
                enemyNetwork.photonView.RPC("DeathParticle", PhotonTargets.All);
            }

        }

        if (enemyNetwork == null) enemyNetwork = GetComponentInParent<EnemyNetwork>();

        //call the same animation for all others
        object[] args = new object[] { animation, isLoop };
        enemyNetwork.photonView.RPC("CallSyncAnimation", PhotonTargets.Others, args);
        if (!enemyNetwork.photonView.isMine) return;

        //ENEMY IS DOING AN AOE ATTACK
        if (enemyNetwork.useSkillAOE & animation == enemyNetwork.skillAOEAnimationName)
        {
           // Debug.Log("CALL AOE GRAPHIC");
            enemyNetwork.photonView.RPC("AOEGraphic", PhotonTargets.Others);
            enemyNetwork.AOEGraphic();
        }

        if (currentAnimation.Contains("Attack"))
        {            
                //Debug.Log("Dont allow overwrite of attack animation");
                if (animation.Contains("Idle") & !isOnComplete) return;

            if (animation.Contains("Attack")) return;
        }
        if (animation == currentAnimation) return;//already playing it on this track
        base.SetAnimation(animation, isLoop, isOnComplete);
        currentAnimation = animation;
    }

    public override void ForceAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        //Debug.Log("FORCE Animation");
        if (enemyNetwork.isDying) return;
        if (animation == currentAnimation) return;//already playing it on this track
        base.SetAnimation(animation, isLoop, isOnComplete);
        currentAnimation = animation;
    }


	public override void ForceAnimation(string animation, bool isLoop, bool isOnComplete, bool ignoreRules)
	{
		base.SetAnimation(animation, isLoop, isOnComplete);
		currentAnimation = animation;
	}


    public override void AnimationComplete(int trackIndex, string animationName)
    {
        //print("Enemy animation complete:" + animationName);
        if (!enemyNetwork.photonView.isMine) return;

        switch (animationName)
        {
            case "Attack1":
                SetAnimation("Idle", true, true);
                break;
            case "Attack2":
                SetAnimation("Idle", true, true);
                break;
            case "Attack3":
                SetAnimation("Idle", true, true);
                break;
            case "Attack4":
                SetAnimation("Idle", true, true);
                break;
            case "Attack5":
                SetAnimation("Idle", true, true);
                break;
            case "Attack6":
                SetAnimation("Idle", true, true);
                break;
            case "Hit":
                SetAnimation("Idle", true, true);
                break;                  
        }
        if (animationName.Contains("Attack"))
        {
         //   Debug.Log("Animation.contaions(Attack)");
        }
        if (animationName.Contains("Death"))
        {
            enemyNetwork.FinalDestroy();
        }
    }

    IEnumerator CallTryAttackLate(float delay)
    {
        yield return new WaitForSeconds(delay);
        enemyNetwork.TryAttack(true);
        enemyNetwork.attackTimer = enemyNetwork.currentEntityStat.atkCooldown;
    }

    public override void AnimationOnHit(string animationName)
    {
        if (!enemyNetwork.photonView.isMine) return;
        if (animationName.Contains("Attack"))
        {
            //turn this into rpc
            if (enemyNetwork.currentTarget == null)
            {
              //  Debug.Log("EnemyAnimationOnHit: but had a null adventurerTarget");
                return;
            }

			//ROLL PROTECTION
			if (enemyNetwork.currentTarget.battleMover.isRolling) {
				//Debug.Log ("Player is rolling and cannot get attacked by normal attacks");
				return;
			}

			//CHECK IF AOE SKILL IS BEING USED
			//IF SO, PROCESS IT ACCORDINGLY AND IGNORE THE REST OF THIS METHOD
			if (enemyNetwork.useSkillAOE & animationName == enemyNetwork.skillAOEAnimationName) {
				//Debug.Log ("Here is our AOE skill!");


				if (enemyNetwork.skillAOEOnHitParticleName != "None") {
					enemyNetwork.photonView.RPC ("AOEOnHit", PhotonTargets.All, enemyNetwork.enemyAttackSound);
				}

				List<AdventurerNetwork>allLivingAdvents = BattleManager.Instance.GetAllLivingAdventurers();
				for (int inc = 0; inc < allLivingAdvents.Count(); inc++) {
					// Debug.Log(Vector2.Distance("AOE DISTANCE:"+allLivingAdvents[inc].footLocation.transform.position, enemyNetwork.rootPosition.transform.position));// < enemyNetwork.skillAOERange
					//float dist = Vector2.Distance (allLivingAdvents [inc].footLocation.transform.position, enemyNetwork.rootPosition.transform.position);
					//Debug.Log ("Dist was:" + dist);
					if (Vector2.Distance (allLivingAdvents [inc].footLocation.transform.position, enemyNetwork.rootPosition.transform.position) < enemyNetwork.skillAOERange) {
						//Debug.Log("Is In Range!");
						ProcessDamage (allLivingAdvents[inc],enemyNetwork.skillAOEPercent,true);
					}
				}
				return;
			}

            int advViewID = enemyNetwork.currentTarget.photonView.viewID;
            enemyNetwork.currentTarget.photonView.RPC("Hit", PhotonTargets.All, advViewID);

			//HERE WE CHOOSE AOE IF APPLICABLE!
			if (enemyNetwork.isAOE) {
				//get list of all living advents
				List<AdventurerNetwork> livingAdvents = BattleManager.Instance.GetAllLivingAdventurers();

				//Attack(force) each one
				for (int inc = 0; inc < livingAdvents.Count; inc++) {
					ProcessDamage (livingAdvents[inc],true);
				}
				return;
			}




			ProcessDamage (enemyNetwork.currentTarget,false);
			//DONT ADD NEW CODE BELOW HERE UNLESS YOU CHECK ALL THE RETURN STATEMENTS
        }
    }

/// <summary>
/// USED TO TELL US WHAT IS PLAYING
/// </summary>
/// <returns></returns>
public string TrueAnimation()
    {
        string realAnim = base.skeletonAnimation.AnimationName;
        return realAnim;

    }


    //WRAPPER
	void ProcessDamage(AdventurerNetwork playerTargetted, bool overrideRoll){
		ProcessDamage (playerTargetted, 1,overrideRoll);
	}

	void ProcessDamage(AdventurerNetwork playerTargetted, float multiplier, bool overrideRoll){
		bool isCrit = enemyNetwork.RollForCritical();
		float damage = Random.Range(enemyNetwork.currentEntityStat.atkMin, enemyNetwork.currentEntityStat.atkMax);

		//APPLY DAMAGE MULTIPLIER
		damage = damage * multiplier;

		//APPLY CRIT MULTIPLIER
		if (isCrit) damage = damage * 2;
		damage = damage - enemyNetwork.currentTarget.RollForDef();
		if (damage <= 0) damage = 1;

		object[] args = new object[] { damage, playerTargetted.photonView.viewID, false, isCrit,overrideRoll };
		playerTargetted.photonView.RPC("TakeDamageMaster", PhotonTargets.MasterClient, args);
		AudioManager.Instance.PlayClip(enemyNetwork.enemyAttackSound);
	}

    //Damage all enemies and players within these bounds
    void AreaDamageAll(Vector3 upperLeft, Vector3 lowerRight)
    {

    }




}
