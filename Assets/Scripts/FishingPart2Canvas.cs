﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FishingPart2Canvas : MonoBehaviour
{
	public List<FishingReward> rewards30_49, rewards50_69, rewards70_89, rewards90_99, rewards100;
	public float speedlvl30, speedlvl50, speedlvl70, speedlvl90, speed100;
	public float beadSpeed = 1;
	public GameObject bead;
	public Animator m_Animator;
	public GameObject RewardBar;
	public BeadTester beadTester;

	#region singleton

	private static FishingPart2Canvas _instance;

	public static FishingPart2Canvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<FishingPart2Canvas> ();
			return _instance;
		}
	}
	//Singleton complete

	#endregion

	#region CanvasSetup

	public CanvasTools ct;

	void Awake ()
	{
		ct = GetComponent<CanvasTools> ();
	}

	#endregion

		
	int	m_LevelChosen = 0;

	public void SetBeadSpeed (int levelChosen)
	{
		RewardBar.SetActive (false);
		//bead.SetActive(true);
		ClearItems ();

		m_LevelChosen = levelChosen;
		if (levelChosen >= 30 & levelChosen < 50) {
			beadSpeed = speedlvl30;
		}
		if (levelChosen >= 50 & levelChosen < 70) {
			beadSpeed = speedlvl50;
		}
		if (levelChosen >= 70 & levelChosen < 90) {
			beadSpeed = speedlvl70;
		}
		if (levelChosen >= 90 & levelChosen < 100) {
			beadSpeed = speedlvl90;
		}
		if (levelChosen >= 100) {
			beadSpeed = speed100;
		}
	}

	public void Click ()
	{
		if (!isMoving)
			return;
		if (!clickReady)
			return;
		clickReady = false;
		//Debug.Log ("StopMoving");
		beadSpeed = 0;
		StopMoving ();
	}

	bool clickReady = true;
	bool isMoving = false;

	public void StartMoving ()
	{
		AudioManager.Instance.PlayClip ("fishingreel", true, true);
		isMoving = true;
		clickReady = true;
		m_Animator.CrossFade ("Move", 0);
		//Debug.Log ("***** SetAnimatorSpeed:" + beadSpeed);
		m_Animator.speed = beadSpeed;
	}

	//	public RectTransform beadMIN, BeadMAX;

	public void StopMoving ()
	{
		AudioManager.Instance.StopClip ("fishingreel");
		isMoving = false;
		clickReady = false;
		m_Animator.speed = 0;
 
		Debug.Log ("BeadTester Triggered:" + beadTester.isTriggered);

		if (beadTester.isTriggered) {
			Invoke ("RollForItems", .5f);

			GameObject nextObj2 = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj2.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("You landed the", "Fish!");
			puc.SetAcceptIcon ();
			puc.SetTextColor ("white");
			nextObj2.SetActive (true);
			AudioManager.Instance.PlayClip ("click", true, false);
		} else {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Your fishing line", "Snapped");
			puc.SetWormIcon ();
			puc.SetTextColor ("white");
			nextObj.SetActive (true);
			AudioManager.Instance.PlayClip ("fishfail", true, false);
			//Invoke ("Reset", 3);
			Invoke ("Close", 2);
			GameManager.Instance.InputEnabled = true;
		}
	}

	void RollForItems ()
	{

		List<FishingReward> fishingReward = new List<FishingReward> ();
		if (m_LevelChosen >= 30 & m_LevelChosen < 50) {
			fishingReward = rewards30_49;
		}
		if (m_LevelChosen >= 50 & m_LevelChosen < 70) {
			fishingReward = rewards50_69;
		}
		if (m_LevelChosen >= 70 & m_LevelChosen < 90) {
			fishingReward = rewards70_89;
			;
		}
		if (m_LevelChosen >= 90 & m_LevelChosen < 100) {
			fishingReward = rewards90_99;
		}
		if (m_LevelChosen >= 100) {
			fishingReward = rewards100;
		}

		Debug.Log ("start roll soon");

		for (int inc = 0; inc < fishingReward.Count; inc++) {
			if (Random.Range (0, 1f) < fishingReward [inc].chances) {
				WonItem (fishingReward [inc].itemType, fishingReward [inc].itemId);
				Invoke ("CallItemEffect", inc * .5f);
			} else {
				Debug.Log ("Missed Item:" + fishingReward [inc].itemId);
			}
		}
		Invoke ("Close", 5);
		Invoke ("OpenBait", 5);
		_GameSaveLoad.Instance.Save (false);
	}

	void OpenBait ()
	{
		BaitCanvas.Instance.ct.CallFadeCanvas (1);
	}

	void CallItemEffect ()
	{
		AudioManager.Instance.PlayClip ("rareitem", true, false);
		//FEATHERS REWARD
		GameObject feathers = ObjectPool.Instance.NextObject ("NormalFeathers");
		feathers.transform.position = TownManager.Instance.myAtn.transform.position + new Vector3 (0, 0, -5);
		feathers.SetActive (true);
		RewardBar.SetActive (true);
	}

	public GameObject fishingItemsParent;

	void WonItem (ItemType _itemType, int _id)
	{
		//Debug.Log ("Won Item:" + _id);

		Item _item = new Item ();
		if (_itemType == ItemType.junk) {
			_item = InventoryManager.Instance.junkDb.Where (x => x.ID == _id).FirstOrDefault ();
		}
		if (_itemType == ItemType.costume) {
			_item = InventoryManager.Instance.costumesDb.Where (x => x.ID == _id).FirstOrDefault ();
		}
		if (_itemType == ItemType.weapon) {
			_item = InventoryManager.Instance.weaponsDb.Where (x => x.ID == _id).FirstOrDefault ();
		}
		if (_itemType == ItemType.equipment) {
			_item = InventoryManager.Instance.equipmentDb.Where (x => x.ID == _id).FirstOrDefault ();
		}

		InventoryCanvas.Instance.AddItemSlot (_item, 1);
		InventoryManager.Instance.AddItem (_itemType, _id);


		GameObject newObj = (GameObject)Instantiate (Resources.Load ("FishingRewardIcon"), new Vector3 (0, 0, 0), Quaternion.identity);
		newObj.transform.parent = fishingItemsParent.transform;
		FishingRewardIcon fri = newObj.GetComponent<FishingRewardIcon> ();
		fri.SetItem (_item);

	}

	void ClearItems ()
	{
		foreach (Transform t in fishingItemsParent.transform) {
			Destroy (t.gameObject);
		}
	}


	public void Close ()
	{
		AudioManager.Instance.StopClip ("fishingambience");
		_GameSaveLoad.Instance.Save (false);

		MenuCanvas.Instance.ToggleMe (true);
		//GameManager.Instance.InputEnabled = true;
		//Debug.Log("Close Fishing Part 2");
		GameManager.Instance.SetAllEquipment (TownManager.Instance.myAdventurerTownNetwork.photonView);

		ct.CallFadeCanvas (0);
	}
}

[System.Serializable]
public class FishingReward
{
	public ItemType itemType;
	public int itemId;
	[Range (0, 1)]
	public float chances;
}
