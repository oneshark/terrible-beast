﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

public class ToolTip : MonoBehaviour
{
    public float resolutionOffsetX = 0, resolutionOffsetY = 0;
    public bool ignoreOffset = false;
    public Item associatedItem;
    public Image itemSprite;
    public Image itemSprite2;
    public Image itemBG;
	public Image medCrownOrSoul;
	public Sprite CrownSprite;
	public Sprite SoulSprite;
    public Text itemTitle;
    public Text gradeAndType;
    public Text itemDescription;
    public Text weapAtkLabel, weapAtk, weapAtkSpd, weapCrit, weapDef, weapCritPow;
    public Text neckDef, neckHP;
    public Text ringAtk, ringHP;
    public Text artifactStat;
    public GameObject LongBlack, ShortBlack, MediumBlack, LongBlackDivider;
    public Text soulAmount, soulAmountShort, soulAmountMedium;
    public Text rankText;
    public GameObject rankObj;
    public Color cOrdinary, cUnique, cRare, cVeryRare, cElite;
    public GameObject  dividerShort;
    public Vector3 offset;
    public Vector3 screenEdgeOffset;
	public Color soulBlue, crownGold;
    private static ToolTip _instance;
    public static ToolTip Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<ToolTip>();
            return _instance;
        }
    }

    public void Update()
    {
        Vector3 tempOffset = new Vector3(0, 0, 0);

        if (!ignoreOffset)
        {
            tempOffset = OffsetScreenEdge();
            transform.position = Input.mousePosition + offset + tempOffset;

        }
        else
        {
            transform.position = Input.mousePosition + new Vector3(200, 0, 0);
        }

        //Debug.Log("mouse:" + Input.mousePosition);
    }

    public Vector3 OffsetScreenEdge()
    {

        Vector3 tempOffset = screenEdgeOffset;
        if (Input.mousePosition.x > Screen.width - 170)
        {
            tempOffset.x -= 175;
        }
        else if (Input.mousePosition.x < 150)
        {
            tempOffset.x += 175;
        }
        if (Input.mousePosition.y < 200)
        {
            tempOffset.y += 175;
        }
        return tempOffset;
    }

    public void SetItem(Item _currItem, bool isPurchasable)
    {
        associatedItem = _currItem;
        ignoreOffset = false;
        itemTitle.text = associatedItem.name;
        itemDescription.text = "";

        itemSprite.sprite = associatedItem.icon;
        itemSprite2.sprite = associatedItem.icon;
        int cost = associatedItem.cost;
		if (CrownBuyItemsCanvas.Instance.isOpen) {
			//SETUP CROWNS ICON AND CROWNS TEXT
			cost = associatedItem.crownCost;
			soulAmount.color = crownGold;
			soulAmountMedium.color = crownGold;
			soulAmountShort.color = crownGold;

		}else{
			soulAmount.color = soulBlue;
			soulAmountMedium.color = soulBlue;
			soulAmountShort.color = soulBlue;		}


        if (!isPurchasable & _currItem.itemGrade==ItemGrade.ordinary)cost=cost/ 2;
        soulAmount.text = cost.ToString();
        soulAmountShort.text = cost.ToString();
        soulAmountMedium.text = cost.ToString();

        string prefix = GameManager.Instance.GetFormattedTerm(associatedItem.itemGrade.ToString());
        string suffix = "";

        rankText.text = "Lvl " + associatedItem.requiredLevel;
        LongBlackDivider.SetActive(true);
        dividerShort.SetActive(false);
        //turn on or off all Weapon
        if (associatedItem.itemType == ItemType.weapon)
        {
            rankObj.SetActive(true);
            weapAtkLabel.text = "ATK";
            weapAtk.text = associatedItem.stat.atkMin + "~" + associatedItem.stat.atkMax;
            weapAtkSpd.text = "ATK SPD " + associatedItem.stat.atkCooldown + "s";
            weapCrit.text = "CRIT " + associatedItem.stat.crit + "%";
            weapCritPow.text = "CRIT PWR " + associatedItem.stat.critPower + "%";
            weapDef.text = "DEF " + associatedItem.stat.defMin + "~" + associatedItem.stat.defMax;
            LongBlack.SetActive(true);

            ShortBlack.SetActive(false);
            MediumBlack.SetActive(false);
            itemSprite.enabled = true;
            itemSprite2.enabled = false;
            suffix = GameManager.Instance.GetFormattedTerm(associatedItem.stance.ToString());
        }
        else
        {
            weapDef.text = "";
            weapCritPow.text = "";
            weapAtkLabel.text = "";
            weapAtk.text = "";
            weapAtkSpd.text = "";
            weapCrit.text = "";
            LongBlack.SetActive(false);
        }

        //Turn on or off all equipment refs
        if (associatedItem.itemType == ItemType.equipment)
            //EQUIPMENT REFS
        {
            rankObj.SetActive(true);
            dividerShort.SetActive(true);

            itemSprite.enabled = false;
            itemSprite2.enabled = true;

            if (associatedItem.equipmentType == EquipmentType.ring)
            {
                ringAtk.text = "ATK " + (associatedItem.stat.atkPerc*100) + "%";
                ringHP.text = "HP " + associatedItem.stat.hpMax.ToString();
            }

            if (associatedItem.equipmentType == EquipmentType.necklace)
            {
                neckHP.text = "HP " + associatedItem.stat.hpMax.ToString();
                neckDef.text = "DEF " + associatedItem.stat.defMin + "~" + associatedItem.stat.defMax;
            }

            if (associatedItem.equipmentType == EquipmentType.artifact)
            {
                neckHP.text = "";
                neckDef.text = "";
                ringAtk.text = "";
                ringHP.text = "";
                artifactStat.text = "";
                SetArtifactStat();//code too long put into its own method to keep it tidy
            }else
            {
                artifactStat.text = "";
            }
                 
            LongBlack.SetActive(false);
            ShortBlack.SetActive(true);
            MediumBlack.SetActive(false);
            suffix = associatedItem.equipmentType.ToString();
        }
        else
        {
            neckHP.text = "";
            neckDef.text = "";
            ringAtk.text = "";
            ringHP.text = "";
            artifactStat.text = "";

        }

        if (associatedItem.itemType == ItemType.junk)
            //
        {
            itemDescription.text = associatedItem.description;

            itemSprite.enabled = false;
            itemSprite2.enabled = true;
            LongBlack.SetActive(true);
            LongBlackDivider.SetActive(false);
            MediumBlack.SetActive(false);
            ShortBlack.SetActive(false);
            rankObj.SetActive(false);
            soulAmount.text = associatedItem.cost + "";
        }

        if (associatedItem.itemType == ItemType.costume)
            //
        {
            itemDescription.text = associatedItem.description;

            rankObj.SetActive(false);

          //  rankText.text = "Lvl " + associatedItem.requiredLevel;

            itemSprite.enabled = false;
            itemSprite2.enabled = true;
            //LongBlack.SetActive(false);
            //ShortBlack.SetActive(false);
            //MediumBlack.SetActive(true);

            LongBlackDivider.SetActive(false);
            MediumBlack.SetActive(true);
			if (CrownBuyItemsCanvas.Instance.isOpen) {
				medCrownOrSoul.sprite=CrownSprite;

			} else {
				//not crowns
				medCrownOrSoul.sprite=SoulSprite;
			}
            ShortBlack.SetActive(false);
            prefix = associatedItem.costumeType.ToString();
            suffix = "Costume";

        }
      
        
        gradeAndType.text = prefix + " " + suffix;
        SetBG();
    }


    void ToggleCraftingDropdown()
    {

    }

    public void SetArtifactStat() 
    {
        if (associatedItem.stat.atkMax > 0)
        {
            artifactStat.text = "ATK " + associatedItem.stat.atkMin + "~" + associatedItem.stat.atkMax;
        }
        if (associatedItem.stat.defMax > 0)
        {
            artifactStat.text = "DEF " + associatedItem.stat.defMin + "~" + associatedItem.stat.defMax;
        }
        if (associatedItem.stat.hpPerc > 0)
        {
            artifactStat.text = "HP " + (associatedItem.stat.hpPerc*100) + "%";
        }
        if (associatedItem.stat.crit > 0)
        {
            artifactStat.text = "CRIT " + associatedItem.stat.crit + "%";
        }
        if (associatedItem.stat.critPower > 0)
        {
            artifactStat.text = "CRIT PWR " + associatedItem.stat.critPower + "%";
        }
        if (associatedItem.stat.atkPerc > 0)
        {
            artifactStat.text = "ATK " + (associatedItem.stat.atkPerc * 100) + "%";
        }
        if (associatedItem.stat.defPerc > 0)
        {
            artifactStat.text = "DEF " + (associatedItem.stat.defPerc * 100) + "%";
        }
    }

    void SetBG()
    {
        if (associatedItem.itemGrade == ItemGrade.ordinary)
        {
            itemBG.color = cOrdinary;
        }
        if (associatedItem.itemGrade == ItemGrade.unique)
        {
            itemBG.color = cUnique;
        }
        if (associatedItem.itemGrade == ItemGrade.rare)
        {
            itemBG.color = cRare;
        }
        if (associatedItem.itemGrade == ItemGrade.veryrare)
        {
            itemBG.color = cVeryRare;
        }
        if (associatedItem.itemGrade == ItemGrade.elite)
        {
            itemBG.color = cElite;
        }
        if (associatedItem.itemType == ItemType.costume)
        {
            itemBG.color = cUnique;
        }
    }


}
