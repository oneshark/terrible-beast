﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifebarCanvas : MonoBehaviour
{
    public bool lifeBarVisible = true;
    public CanvasGroup cg;
    public Image HPFillImage;
    public Text hpText;
    bool fadingIn = false;
    bool fadingOut = false;

    void Start()
    {
        cg.alpha = 0;
       // Invoke("FirstFadeIn", 1);

    }

    public void SetHP(float hp, float hpMax)
    {
        hp = (int)hp;
        hpMax = (int)hpMax;
        if (hp < 0) hp = 0;
        HPFillImage.fillAmount = hp / hpMax;
        hpText.text = hp + "/" + hpMax;
    }

    public void CallFadeCanvas(float _desiredAlpha)
    {
        StartCoroutine(FadeCanvasGroup(_desiredAlpha));
    }

    IEnumerator FadeCanvasGroup(float _newAlpha)
    {
        
        if (_newAlpha == 0)
        {
            fadingIn = false;
            fadingOut = true;
        }
        else
        {
            fadingIn = true;
            fadingOut = false;
        }


        float _currentAlpha = cg.alpha;

        //Fading out
        if (_newAlpha < _currentAlpha)
        {
            for (float alpha = _currentAlpha; alpha >= _newAlpha; alpha -= .1f)
            {
                yield return new WaitForSeconds(0);
                cg.alpha = alpha;
            }
        }

        //Fading in
        if (_newAlpha > _currentAlpha)
        {
            for (float alpha = _currentAlpha; alpha <= _newAlpha; alpha += .1f)
            {
                yield return new WaitForSeconds(0);
                cg.alpha = alpha;
            }
        }
        cg.alpha = _newAlpha;
        if (cg.alpha == 1)
        {
            lifeBarVisible = true;
        }
        else
        {
            lifeBarVisible = false;
        }
    }
}
