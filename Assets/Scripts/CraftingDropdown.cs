﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class CraftingDropdown : MonoBehaviour
{
	public Text soulText;
	public Color clrGreen, clrWhite;
	bool menuOpen = false;
	public Animator anim;
	public Item associatedItem;
	public GameObject materialsParent;
	public List<CraftingMaterials> materialsRequired = new List<CraftingMaterials> ();
	public CraftingMaterials associatedMaterials = new CraftingMaterials ();

	public void SetMaterialList ()
	{
		ClearMaterialRow ();
		//for each material required addmaterial row
		associatedMaterials = CraftingCanvas.Instance.craftingMaterialsList.Where (x => x.itemId == associatedItem.ID & x.itemType == associatedItem.itemType).FirstOrDefault ();
		soulText.text = associatedMaterials.requiredSouls.ToString ();

		List<int> itemsSeen = new List<int> ();
		foreach (int junkId in associatedMaterials.requiredJunkItems) {
			if (itemsSeen.Contains (junkId)) {
				continue;
			}
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("MaterialRow")));
			newItem.transform.parent = materialsParent.transform;
			Text txt = newItem.GetComponent<Text> ();
			int qtyOwned = InventoryManager.Instance.GetQTY (ItemType.junk, junkId);
			int qtyRequired = associatedMaterials.requiredJunkItems.Where (x => x == junkId).Count ();
			Item craftingItem = InventoryManager.Instance.GetItem (ItemType.junk, junkId);
			txt.text = craftingItem.name + " " + qtyOwned + "/" + qtyRequired;
			if (qtyOwned < qtyRequired)
				txt.color = clrWhite;

			itemsSeen.Add (junkId);

		}
	}

	void ClearMaterialRow ()
	{
		foreach (Transform t in materialsParent.gameObject.transform) {
			Destroy (t.gameObject);
			//t.gameObject.SetActive(false);
		}
	}

	public Text mainStatRange, stat1, stat2, stat3, stat4, mainStatLabel, mainDesc;
	public GameObject gridlines;

	public void SetAssociatedItem (Item _item)
	{
		associatedItem = _item;
		//UNIVERSAL SETTINGS
		gridlines.SetActive (true);
		mainDesc.text = "";
		//SPECIFIC SETTINGS
		if (_item.itemType == ItemType.weapon) {
			mainStatLabel.text = "ATK";
			mainStatRange.text = _item.stat.atkMin + "~" + _item.stat.atkMax;
			stat1.text = "ATK SPD " + _item.stat.atkCooldown + "s";
			stat2.text = "DEF " + _item.stat.defMin + "~" + _item.stat.defMax;
			stat3.text = "CRIT " + _item.stat.crit + "%";
			stat4.text = "CRIT PWR " + _item.stat.critPower;
		}
		if (_item.itemType == ItemType.equipment & _item.equipmentType == EquipmentType.necklace) {
			mainStatLabel.text = "DEF";
			mainStatRange.text = _item.stat.defMin + "~" + _item.stat.defMax;
			stat1.text = "HP " + _item.stat.hpMax;
			stat2.text = "";
			stat3.text = "";// "CRIT " + _item.stat.crit + "%";
			stat4.text = "";// "CRIT PWR " + _item.stat.critPower;
		}
		if (_item.itemType == ItemType.equipment & _item.equipmentType == EquipmentType.ring) {
			mainStatLabel.text = "ATK";
			mainStatRange.text = (_item.stat.atkPerc * 100) + "%"; //+ _item.stat.defMax;
			stat1.text = "HP " + _item.stat.hpMax;
			stat2.text = "";
			stat3.text = "";// "CRIT " + _item.stat.crit + "%";
			stat4.text = "";// "CRIT PWR " + _item.stat.critPower;
		}
		if (_item.itemType == ItemType.junk) {
			mainStatLabel.text = "Info:";
			mainDesc.text = _item.description;
			mainStatRange.text = "";// _item.description + "~" + _item.stat.atkMax;
			stat1.text = "";// ATK SPD " + _item.stat.atkCooldown + "s";
			stat2.text = "";// DEF " + _item.stat.defMin + "~" + _item.stat.defMax;
			stat3.text = "";// CRIT " + _item.stat.crit + "%";
			stat4.text = "";// CRIT PWR " + _item.stat.critPower;
		}

		if (_item.itemType == ItemType.costume) {
			mainStatLabel.text = "Info:";
			mainDesc.text = _item.description;
			mainStatRange.text = "";// _item.description + "~" + _item.stat.atkMax;
			stat1.text = "";// ATK SPD " + _item.stat.atkCooldown + "s";
			stat2.text = "";// DEF " + _item.stat.defMin + "~" + _item.stat.defMax;
			stat3.text = "";// CRIT " + _item.stat.crit + "%";
			stat4.text = "";// CRIT PWR " + _item.stat.critPower;
		}
	}

	public void TryCraft ()
	{
		//Debug.Log("TryCraft()");
		if (GameManager.Instance.souls < associatedMaterials.requiredSouls) {
			//Debug.Log("NotEnoughSouls");
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Not Enough", "Souls");
			puc.SetSoulIcon ();
			puc.SetTextColor ("blue");
			nextObj.SetActive (true);
			//pop up not enough souls
			return;
		}
		//check all required materials
		bool hasAllMaterials = true;
		List<int> itemsSeen = new List<int> (0);
		foreach (int junkId in associatedMaterials.requiredJunkItems) {
			if (itemsSeen.Contains (junkId)) {
				continue;
			}
        
			int qtyRequired = associatedMaterials.requiredJunkItems.Where (x => x == junkId).Count ();
			if (InventoryManager.Instance.GetQTY (ItemType.junk, junkId) < qtyRequired) {
				hasAllMaterials = false;
			}

		}

		if (!hasAllMaterials) {
			//Some Materials not there pop up
			//Debug.Log("Missing Materials to craft");
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Gather More", "Materials");
			puc.SetSoulIcon ();
			puc.SetTextColor ("white");
			puc.SetRejectIcon ();
			nextObj.SetActive (true);
			return;
		}

		//you passed - you may craft
		Craft ();
	}

	public void Craft ()
	{
		//  Debug.Log("Craft success");
		//UNITY ANALYITICS
		Analytics.CustomEvent ("CRAFTED_ITEM", new Dictionary<string, object> {
			{ "ITEM_ID",associatedItem.ID},
			{ "ITEM_TYPE",associatedItem.itemType},
			{ "NAME", GameManager.Instance.PlayerName },
			{ "LVL", GameManager.Instance.baseEntityStat.xpLevel }
		});

		//Take souls
		AudioManager.Instance.PlayClip ("click", true, false);

		AudioManager.Instance.PlayClip ("blacksmith1", true, false);
		GameObject nextObj = ObjectPool.Instance.NextObject ("CraftingFeathers");
		nextObj.SetActive (true);

		if (associatedItem.itemType != ItemType.junk) {
			GameObject craftedPopUp = ObjectPool.Instance.NextObject ("CraftedCanvas");
			craftedPopUp.SetActive (true);
			CraftedCanvas cc = craftedPopUp.GetComponent<CraftedCanvas> ();
			cc.SetItem (associatedItem);
		}

		GameManager.Instance.IncSouls (-associatedMaterials.requiredSouls);
		//Take Items
		foreach (int junkId in associatedMaterials.requiredJunkItems) {
			InventoryManager.Instance.RemoveItem (ItemType.junk, junkId);
			InventoryCanvas.Instance.UpdateInventorySlot (ItemType.junk, junkId);
            
		}

		InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);

		InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);

		SetAssociatedItem (associatedItem);//acts like a refresh
	}



	public void ToggleMe (bool b)
	{
		if (b == menuOpen)
			return;
		if (b) {
			SetMaterialList ();
			anim.CrossFade ("DropdownIn", 0);
		} else {
			anim.CrossFade ("DropdownOut", 0);
		}
		menuOpen = b;
	}
}
