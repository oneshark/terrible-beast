﻿using UnityEngine;
using System.Collections;

public class SmoothFollow : MonoBehaviour
{

    public float interpVelocity;
    public GameObject target;
    public Vector3 offset;
    Vector3 targetPos;
    [Header("What is deepest and farthest Z we can go to")]
   public float maxZ, minZ;
    // Use this for initialization
    void Start()
    {
        targetPos = transform.position;
    }

    // Update is called once per frame
    public Vector3 OhWatchMe = new Vector3();
    void FixedUpdate()
    {
        if (target)
        {
            Vector3 posNoZ = transform.position;
            posNoZ.z = target.transform.position.z;

            Vector3 targetDirection = (target.transform.position - posNoZ);
            targetDirection.z += offset.z;

            interpVelocity = targetDirection.magnitude * 5f;

            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            //KEEP Z WITHIN BOUNDARY
            targetPos.z= Mathf.Clamp(targetPos.z, minZ, maxZ);

            transform.position = Vector3.Lerp(transform.position, targetPos + offset, 0.35f);
            OhWatchMe = targetPos + offset;
        }
    }
}
//using UnityEngine;
//using System.Collections;

//public class SmoothFollow : MonoBehaviour
//{

//    public float interpVelocity;
//    public float minDistance;
//    public float followDistance;
//    public GameObject target;
//    public Vector3 offset;
//    Vector3 targetPos;
//    // Use this for initialization
//    void Start()
//    {
//        targetPos = transform.position;
//    }

//    // Update is called once per frame
//    void FixedUpdate()
//    {
//        if (target)
//        {
//            Vector3 posNoZ = transform.position;
//            posNoZ.z = target.transform.position.z;

//            Vector3 targetDirection = (target.transform.position - posNoZ);

//            interpVelocity = targetDirection.magnitude * 5f;

//            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

//            transform.position = Vector3.Lerp(transform.position, targetPos + offset, 0.25f);

//        }
//    }
//}