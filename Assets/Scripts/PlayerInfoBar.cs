﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerInfoBar : MonoBehaviour {
    public Text nameText;
    public Text levelText;
    public Image hpFill;
    public Image xpFill;
    int playerPosition = 0;

    public void SetText(string nameVal, int level)
    {
        
        if (nameVal == null) nameVal = "Not Initialized";
        if (level == null) level = 0;
        nameText.text = nameVal;
        //TRYING IT
        levelText.text = level.ToString();
    }

    public void SetHPFill(float val, float max)
    {
        hpFill.fillAmount = val / max;
    }
    public void SetXPFill(float val, float max)
    {
        xpFill.fillAmount = val / max;
    }
}