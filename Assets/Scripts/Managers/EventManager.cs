﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour
{
	public delegate void LoadComplete ();

	public static event LoadComplete OnLoadComplete;

	public delegate void AudioLevelChanged ();

	public static event AudioLevelChanged OnAudioLevelChanged;

	public delegate void InvasionFinished ();

	public static event InvasionFinished OnInvasionFinished;

	public delegate void InvasionUnlocks ();

	public static event InvasionUnlocks OnInvasionUnlocks;

	public delegate void LockInvasions ();

	public static event LockInvasions OnInvasionLocks;

	public delegate void SceneChange ();

	public static event SceneChange OnSceneChange;

	public delegate void BattleStart ();

	public static event BattleStart OnBattleStart;


	public delegate void GuillotineDropped ();

	public static event GuillotineDropped OnGuillotineDropped;

	public delegate void TownStart ();

	public static event TownStart OnTownStart;

	public delegate void InventoryStyleChange (bool isDrop);

	public static event InventoryStyleChange OnInventoryStyleChange;

	public delegate void ElevatorKeyChange ();

	public static event ElevatorKeyChange OnElevatorKeyChange;

	public delegate void BossDefeated ();

	public static event BossDefeated OnBossDefeated;

    public delegate void MapRefreshed();

    public static event MapRefreshed OnMapRefreshed;

	public delegate void SkillTreeRefreshed();

	public static event SkillTreeRefreshed OnSkillTreeRefreshed;

    // public delegate void GameReset();
    // public static event GameReset OnGameReset;

    private static EventManager _instance;

	public static EventManager Instance {
		get {
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<EventManager> ();

			return _instance;
		}
	}

	public void CallAudioLevelChanged ()
	{
		if (OnAudioLevelChanged != null)
			OnAudioLevelChanged ();
	}

	public void CallLoadComplete ()
	{
		if (OnLoadComplete != null)
			OnLoadComplete ();
	}

	public void CallInvasionFinished ()
	{
		if (OnInvasionFinished != null)
			OnInvasionFinished ();
	}

	public void CallInvasionUnlocks ()
	{
		if (OnInvasionUnlocks != null)
			OnInvasionUnlocks ();
	}

	public void CallInvasionLocks ()
	{
		if (OnInvasionLocks != null)
			OnInvasionLocks ();
	}

	public void CallSceneChange ()
	{
		if (OnSceneChange != null)
			OnSceneChange ();
	}

	bool m_inventoryIsDrop = false;

	public void CallInventoryStyleChange (bool isDrop)
	{
		m_inventoryIsDrop = isDrop;
		if (OnInventoryStyleChange != null)
			OnInventoryStyleChange (isDrop);
	}

	public void CallElevatorKeyChange ()
	{
		if (OnElevatorKeyChange != null)
			OnElevatorKeyChange ();
	}

	public void CallBattleStart ()
	{
		if (OnBattleStart != null)
			OnBattleStart ();
	}

	public void CallGuillotineDropped ()
	{
        Debug.Log("CallGuillotineDroppedEvent!");
		if (OnGuillotineDropped != null)
			OnGuillotineDropped ();
	}

	public void CallBossDefeated ()
	{
		if (OnBossDefeated != null)
			OnBossDefeated ();
	}

    public void CallMapRefreshed()
    {
        Debug.Log("Call Map Refresh");
        if (OnMapRefreshed != null)
            OnMapRefreshed();
    }

	public void CallSkillTreeRefreshed()
	{
		if (OnSkillTreeRefreshed != null)
			OnSkillTreeRefreshed();
	}


    public void CallTownStart ()
	{
		if (OnTownStart != null)
			OnTownStart ();
	}


	public bool GetInventoryStyle ()
	{
		return m_inventoryIsDrop;
	}

	//public void CallGameReset()
	//{
	//    if (OnGameReset != null) OnGameReset();
	//}

}