﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	private static GameManager _instance;

	public static GameManager Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = FindObjectOfType<GameManager> ();
			return _instance;
		}
	}

	public bool waitingOnGuillotine=false;

	/// <summary>
	/// Want to know how many times something was completed? Look int he "Completed Things" list lol
	/// </summary>
	public List<string> completedThings = new List<string> ();
	public List<CustomDictionary> customDictionary;
	public List<InvasionImageMap> invasionList = new List<InvasionImageMap> ();
	public string PlayerName;
	public string UserName;//logged in name not the same
	public bool ADMINCONTROL = false;
	public float souls;
	public float crowns;
	public float statPoints = 0;
	public bool InputEnabled = true;
	//walking around
	public List<ObjectByScene> objectsByScene = new List<ObjectByScene> ();
	public CameraEffects cameraEffects;

	public bool joinRandomTown = true;
	public byte MaxTownPlayers = 99;
	public int MaxBattlePlayers = 4;
	public MainMenu mainMenu;
	public CustomSceneType currentScene = CustomSceneType.Start;
	public CustomSceneType previousScene = CustomSceneType.Town;
	public List<Skill> skillsOwned = new List<Skill> ();

	[Header ("Set Player Stats")]
	public Stats baseEntityStat, originalBaseStats;
	[Header ("-NO NEED TO SET CURRENT STATS THIS TIME-")]
	public Stats currentEntityStat;
	[Header ("Equipped Stuff")]
	public int equippedFace;
	//name of the face attached
	public Item equippedCostumeHead;
	public Item equippedCostumeBody, equippedCostumeBack, equippedWeapon, equippedEquipmentRing,
		equippedEquipmentNecklace, equippedEquipmentArtifact;
	public bool headEquipped = false, bodyEquipped = false, backEquipped = false, weapEquipped = false, ringEquipped = false, necklaceEquipped = false, artifactEquipped = false;

	public List<string> CustomCriticalPhrases = new List<string> ();
	public Animator BlackFader;
	public string selectedMap;
	public string NextTownSpawn = "PlayerSpawn0";
	public string InvasionUnlock = "";
    public int skillPoints = 99;
    public int permSkillPoints = 0;

	public void Awake ()
	{
		//Connect to the main photon server. This is the only IP and port we ever need to set(!)
		//if (!PhotonNetwork.connected)
		//  PhotonNetwork.ConnectUsingSettings("v1.0"); // version of the game/demo. used to separate older clients from newer ones (e.g. if incompatible)
		originalBaseStats = originalBaseStats.MirrorStat (baseEntityStat);

		// mainMenu = GetComponent<MainMenu>();
		UserName=LoginGUI.Instance.userName;//set the registered or logged in username
		ActivateDeactivate ("Town");
		//  SetBlackFader("Black");
		CalculateStats ();
	}

	void OnEnable ()
	{
		EventManager.OnLoadComplete += OnLoadComplete;
	}

	void OnDisable ()
	{
		EventManager.OnLoadComplete -= OnLoadComplete;
	}

	void OnLoadComplete ()
	{
		//   Debug.Log("OnLoadComplete");
		Invoke ("LoadComplete", 1);//delaying this slightly to let adventurer be ready for equipment
	}

	void LoadComplete ()
	{
		//  Debug.Log("LoadComplete");

		SaveData sd = _GameSaveLoad.Instance.saveData;

		//GUEST LOGINS USE THIS BLOCK
		if (sd == null) {
			NewToMorningWood ();
			return;
		}


		PlayerName = sd.PlayerName;
		if (PlayerName == "Guest" || PlayerName == "") {
			//call up first time canvas
			if (!completedThings.Contains ("NewPlayerCanvas")) {
				NewToMorningWood ();
				completedThings.Add ("NewPlayerCanvas");
			}
		}
		if (currentScene == CustomSceneType.Town) {
			TownManager.Instance.myAdventurerTownNetwork.CallBroadcastName ();
		}

		AudioManager.Instance.soundLevel = sd.soundLevel;
		AudioManager.Instance.musicLevel = sd.musicLevel;

		baseEntityStat.xp = sd.baseStat.xp;
		baseEntityStat.xpMax = sd.baseStat.xpMax;
		baseEntityStat.xpLevel = sd.baseStat.xpLevel;
		//hp max setting
		baseEntityStat.hpMax = sd.baseStat.hpMax;
		//  Debug.Log("Completed Things Touched");
		completedThings = sd.completedTHings;
		souls = sd.souls;
		crowns = sd.crowns;
		IncSouls (0);//update it quick in the graphic UI
		//Equip Items
		//Weapon EQUIP
		Item _item = InventoryManager.Instance.weaponsDb.Where (x => x.ID == sd.weaponEquipped).FirstOrDefault ();
		InventoryCanvas.Instance.equipWeapon.SetItem (_item, 1);
		equippedWeapon = _item;
		//face equipping
		Face myFace = InventoryManager.Instance.GetFace (sd.faceEquipped);
		ChangeFace (myFace, TownManager.Instance.myAtn.photonView);
		equippedFace = myFace.ID;
		weapEquipped = true;
		ChangeEquipment (_item, TownManager.Instance.myAtn.photonView);
		////TRY TO KEEP OUR ITEM THAT WAS EQUIPPED
		//TownManager.Instance.myAtn.equippedWeapon = _item;

		//Equipment EQUIP
		for (int inc = 0; inc < sd.equipmentEquipped.Count; inc++) {
			_item = InventoryManager.Instance.equipmentDb.Where (x => x.ID == sd.equipmentEquipped [inc]).FirstOrDefault ();
			if (_item.equipmentType == EquipmentType.artifact) {
				InventoryCanvas.Instance.equipSlotArtifact.SetItem (_item, 1);
				equippedEquipmentArtifact = _item;
				artifactEquipped = true;
			} else if (_item.equipmentType == EquipmentType.necklace) {
				InventoryCanvas.Instance.equipSlotNecklace.SetItem (_item, 1);
				equippedEquipmentNecklace = _item;
				necklaceEquipped = true;
			} else if (_item.equipmentType == EquipmentType.ring) {
				InventoryCanvas.Instance.equipSlotRing.SetItem (_item, 1);
				equippedEquipmentRing = _item;
				ringEquipped = true;
			}
			ChangeEquipment (_item, TownManager.Instance.myAtn.photonView);
		}

		//Costume EQUIP
		for (int inc = 0; inc < sd.costumesEquipped.Count; inc++) {
			_item = InventoryManager.Instance.costumesDb.Where (x => x.ID == sd.costumesEquipped [inc]).FirstOrDefault ();
			if (_item.costumeType == CostumeType.back) {
				InventoryCanvas.Instance.costumeSlotBack.SetItem (_item, 1);
				equippedCostumeBack = _item;
				backEquipped = true;
			} else if (_item.costumeType == CostumeType.body) {
				InventoryCanvas.Instance.costumeSlotBody.SetItem (_item, 1);
				equippedCostumeBody = _item;
				bodyEquipped = true;
			} else if (_item.costumeType == CostumeType.head) {
				equippedCostumeHead = _item;
				headEquipped = true;
				InventoryCanvas.Instance.costumeSlotHead.SetItem (_item, 1);
			}
			ChangeEquipment (_item, TownManager.Instance.myAtn.photonView);
		}
		//After all this gear, set the stats
		CalculateStats ();

		//Load Inventory
		//JUNK
		for (int inc = 0; inc < sd.junkOwned.Count; inc++) {
			InventoryManager.Instance.AddItem (ItemType.junk, sd.junkOwned [inc]);
			Item _newItem = InventoryManager.Instance.junkDb.Where (x => x.ID == sd.junkOwned [inc]).FirstOrDefault ();
			InventoryCanvas.Instance.AddItemSlot (_newItem, 1);
		}

		//WEAP
		for (int inc = 0; inc < sd.weaponsOwned.Count; inc++) {
			InventoryManager.Instance.AddItem (ItemType.weapon, sd.weaponsOwned [inc]);
			Item _newItem = InventoryManager.Instance.weaponsDb.Where (x => x.ID == sd.weaponsOwned [inc]).FirstOrDefault ();
			InventoryCanvas.Instance.AddItemSlot (_newItem, 1);
		}

		//Equips
		for (int inc = 0; inc < sd.equipmentOwned.Count; inc++) {
			InventoryManager.Instance.AddItem (ItemType.equipment, sd.equipmentOwned [inc]);
			Item _newItem = InventoryManager.Instance.equipmentDb.Where (x => x.ID == sd.equipmentOwned [inc]).FirstOrDefault ();
			InventoryCanvas.Instance.AddItemSlot (_newItem, 1);
		}

		//Costumes
		for (int inc = 0; inc < sd.costumesOwned.Count; inc++) {
			InventoryManager.Instance.AddItem (ItemType.costume, sd.costumesOwned [inc]);
			Item _newItem = InventoryManager.Instance.costumesDb.Where (x => x.ID == sd.costumesOwned [inc]).FirstOrDefault ();
			InventoryCanvas.Instance.AddItemSlot (_newItem, 1);
		}

		_GameSaveLoad.Instance.Save(false);


	}

	public void DelayedNewToMorningWood (float delay)
	{
		Invoke ("NewToMorningWood", delay);
	}

	public void NewToMorningWood ()
	{
	GameManager.Instance.InputEnabled=false;
		GameObject nextObj = ObjectPool.Instance.NextObject ("FirstTimeCanvas");
		nextObj.SetActive (true);
		nextObj.GetComponent<FirstTimeCanvas> ().SetFaceButtons ();
	
		ToggleVoid (true);Debug.Log ("THIS JUST HAPPENDD");
		MenuCanvas.Instance.ToggleMe (false);
		GameManager.Instance.InputEnabled = false;

		//Delay, ClipIndex
		CallPlayTutorialBlock(1.5f,5,true);
	}

	void OnJoinedLobby ()
	{
		// Debug.Log("Joined lobby");
	}

	#region CONNECTING TO TOWN or Battle

	void OnReceivedRoomListUpdate ()
	{
		if (joinRandomTown) {
			JoinRandomTown ();
		}
	}

	public void SetInputEnabledFalse(){
		InputEnabled = false;
	}

	public void SetInputEnabledTrue(){
		InputEnabled = true;
	}

	void OnJoinedRoom ()
	{

		LoadingManager.Instance.CallDelayedClose (1.5f);

		joinRandomTown = false;//if its true - flip this flag back off
	}

	void OnPhotonRandomJoinFailed ()
	{
		//   Debug.Log("OnPhotonRandomJoinFailed");
		if (joinRandomTown) {
			CreateTownRoom ();
		}
	}

	public void JoinRandomTown ()
    //Join first town with extra room in it
	{
		//  Debug.Log("Join Random Town");
		string townName = "";
		foreach (RoomInfo game in PhotonNetwork.GetRoomList()) {
			//Use the room custom properties to find a town
			// Debug.Log("Room Type was:" + game.customProperties["Type"].ToString());
			if (game.customProperties ["Type"].ToString () == "Town_Final") {
				townName = game.name;
			}
		}
		//Debug.Log("JoinRandomTown()");
		if (townName == "") {
			CreateTownRoom ();
			return;
		} else {
			PhotonNetwork.JoinRoom (townName);
			return;
		}
		//Debug.Log ("Didn't create town, didnt join random town room.");
	}

	void CreateTownRoom ()
    //Towns not found - create one
	{
		Debug.Log ("Create town room");
		//SET PARAM FOR ROOM INFO
		ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable ();
		h ["Wave"] = 1;
		h ["Type"] = "Town_Final";

		// PhotonNetwork.room.SetCustomProperties(h);
		string[] customPropertiesForLobby = new string[2];
		customPropertiesForLobby [0] = "Wave";
		customPropertiesForLobby [1] = "Type";

		string roomName = "Town_" + Random.Range (0, 9999);

		PhotonNetwork.CreateRoom (roomName, new RoomOptions {
			MaxPlayers = 50,
			CustomRoomProperties = h,
			CustomRoomPropertiesForLobby = customPropertiesForLobby
		}, TypedLobby.Default);
		//PhotonNetwork.CreateRoom(roomName, new RoomOptions() { MaxPlayers = 4, CustomRoomProperties = h, CustomRoomPropertiesForLobby = customPropertiesForLobby }, TypedLobby.Default);

	}

	#endregion

	public void ActivateDeactivate (string sceneName)
    //objects whichshould only appear in certain situations (my custom scene logic
	{
		
		_GameSaveLoad.Instance.Save(false);

		EventManager.Instance.CallSceneChange ();
		// Debug.Log("ActiveateDeactiveate:" + sceneName);
		//RESET THINGS TO THEIR ORIG STATE
		if (currentScene == CustomSceneType.Battle) {
			BattleManager.Instance.playerPositions = new int[4] { 0, 0, 0, 0 };
			BattleCanvas.Instance.pib0Obj.SetActive (false);
			BattleCanvas.Instance.pib1Obj.SetActive (false);
			BattleCanvas.Instance.pib2Obj.SetActive (false);
			BattleCanvas.Instance.pib3Obj.SetActive (false);

			PlayerCanvas.Instance.ToggleMe (false);
			BattleCanvas.Instance.ToggleMe (false);
		}

		if (currentScene == CustomSceneType.Town) {
			GameManager.Instance.InputEnabled = true;
		}

		//keep track of our custom scene
		// Debug.Log("ActivateDeactivate!");
		previousScene = currentScene;
		if (sceneName == "Town")
			currentScene = CustomSceneType.Town;
		if (sceneName == "Battle")
			currentScene = CustomSceneType.Battle;
		if (sceneName == "Clear")
			currentScene = CustomSceneType.Clear;

		cameraEffects.ActivateDeactivate (sceneName);

		//FOR LOOP

		for (int inc = 0; inc < objectsByScene.Count; inc++) {
			if (objectsByScene [inc].homeScene == sceneName) {
				objectsByScene [inc].obj.SetActive (true);
			} else {
				objectsByScene [inc].obj.SetActive (false);
			}
		}
		//mainMenu.showGUI = false;

		//MORE SCENE BASED INSTRUCTIONS 
		if (GameManager.Instance.currentScene == CustomSceneType.Town) {
			ToggleVoid(false);
					GameManager.Instance.CalculateStats();
			TownCanvas.Instance.ToggleMe (true);
			TownCanvas.Instance.ToggleSoulText (true);
			IncSouls (0);//refreshes the texts
			AudioManager.Instance.StopClip ("ambience1");
			if (InvasionUnlock != "") {
				Invoke ("CallInvasionUnlocked", 4);
				CallPlayTutorialBlock(1.5f,10,true);

			}
			cameraEffects.smoothFollowRandomizer.enabled = true;
			//turn off game over hands
			GameObject[] gameovers = GameObject.FindGameObjectsWithTag ("GameOver");
			foreach (GameObject obj in gameovers) {
				obj.SetActive (false);
			}
			if (GameManager.Instance.baseEntityStat.xpLevel >= 30) {
				//Delay, ClipIndex
				CallPlayTutorialBlock(4,8,true);
			}
			EventManager.Instance.CallTownStart ();

			GameObject[] allVoidedItems = GameObject.FindGameObjectsWithTag("ItemPickup");
			for (int inc = 0; inc < allVoidedItems.Count(); inc++) {
				Destroy (allVoidedItems [inc]);
			}
		
		}
		if (GameManager.Instance.currentScene == CustomSceneType.Battle) {
			MenuCanvas.Instance.ToggleMe (false);
			PlayerCanvas.Instance.ToggleMe (false);
			//BattleCanvas.Instance.ToggleMe(false);
			BattleCanvas.Instance.ToggleGuillotineObjects (false);
		}

		//Final Non Specific actions

		Chat.Instance.ClearChatBox ();
		if (cameraEffects != null)
			cameraEffects.ToggleGrayscale (false);

		//FIX CHATS
		//Debug.Log ("**** reset chat");
		Invoke ("ResetChatPlacement", 1);
	}

	public void CallPlayTutorialBlock(float delay, int clipIndex, bool isTracked){
		StartCoroutine (PlayTutorialBlock(delay,clipIndex, isTracked));
	}
	IEnumerator PlayTutorialBlock(float delay, int clipIndex, bool isTracked){
		yield return new WaitForSeconds (delay);
		TutorialBlock (clipIndex, isTracked);
	}
	void TutorialBlock(int clipIndex, bool isTracked){
		//TUTUROIAL BLOCK - replace clipIndex
		if (!completedThings.Contains("wizard"+clipIndex)){
			Debug.Log ("Tried to do wizard:" + clipIndex);
			GameObject newObj = (GameObject)Instantiate (Resources.Load ("WizardSkeleton"), new Vector3 (0, 0, 0), Quaternion.identity);
			WizardSpeechAnimationController wiz = newObj.GetComponent<WizardSpeechAnimationController> ();

			bool _includeGraphics = true;
			if (clipIndex == 6)
				_includeGraphics = false;
			if (clipIndex == 2)
				_includeGraphics = false;




			wiz.InitAudioClip (clipIndex,_includeGraphics);
			if (isTracked)AddCompletedThing ("wizard" + clipIndex);
		}
	}

	void ResetChatPlacement ()
	{
		GameObject chatParent = GameObject.FindGameObjectWithTag ("ChatBoxParent");
		if (chatParent == null) {
			Invoke ("ResetChatPlacement", 2);
			return;
		}
		RectTransform rt = chatParent.GetComponent<RectTransform> ();
		rt.localPosition = new Vector3 (rt.localPosition.x, 5000, rt.localPosition.z);
	}

	public void CallDelayedActivate (GameObject obj, float delay, bool b)
	{
		StartCoroutine (DelayedActivate (obj, delay, b));
	}

	IEnumerator DelayedActivate (GameObject obj, float delay, bool b)
	{
		//Debug.Log("DelayedActivate:"+obj.name+" " + b);
		yield return new WaitForSeconds (delay);
		obj.SetActive (b);
		Debug.Log ("DelayedActivate:" + obj.name + " " + b);

	}

	public void IncSouls (int inc)
	{
		//Debug.Log("Souls+" + inc);
		souls += inc;
		if (currentScene == CustomSceneType.Town) {
			if (CraftingCanvas.Instance != null)
				CraftingCanvas.Instance.soulText.text = souls.ToString ();
			TownCanvas.Instance.soulAmountText.text = Utilities.FormatCurrency((int)souls).ToString ();
		}
		if (currentScene == CustomSceneType.Battle) {
			PlayerCanvas.Instance.CallSFEat ();
			PlayerCanvas.Instance.soulAmountText.text = Utilities.FormatCurrency((int)souls).ToString ();
		}
	}

	public void SetMeshActiveLater (MeshRenderer mr, float delay)
	{
		StartCoroutine (SetMeshActiveAfterDelay (mr, delay));
	}

	IEnumerator SetMeshActiveAfterDelay (MeshRenderer mr, float delay)
	{
		yield return new WaitForSeconds (delay);
		if (mr != null)
			mr.enabled = true;
	}

	public void ToggleInputEnabled (bool b)
	{
		// Debug.Log(b);
		InputEnabled = b;
	}

	public void CalculateStats ()
	{
		//zero out the percents
		currentEntityStat.atkPerc = 0;
		currentEntityStat.defPerc = 0;
		currentEntityStat.hpPerc = 0;

		//GRAB FROM GAME MANAGER
		Stats baseStat = baseEntityStat.MirrorStat (baseEntityStat); //deep copy to keep them untied from each other

		//add in all the upgrades
		currentEntityStat.atkMin = baseStat.atkMin;
		currentEntityStat.atkMax = baseStat.atkMax;

		currentEntityStat.crit = baseStat.crit;
		currentEntityStat.critPower = baseStat.critPower;
		currentEntityStat.hp = (int)(baseStat.hp);
		currentEntityStat.hpMax = (int)(baseStat.hpMax);
		currentEntityStat.defMin = baseStat.defMin;
		currentEntityStat.defMax = baseStat.defMax;
		currentEntityStat.atkCooldown = baseStat.atkCooldown;


		currentEntityStat.regen = baseStat.regen;

		//EQUIPMENT
		// Debug.Log("EquippedWeaponn.stat:" + equippedWeapon.stat.atkCooldown);
		if (weapEquipped){
			currentEntityStat = AddStats (currentEntityStat, equippedWeapon.stat);
		currentEntityStat.range = equippedWeapon.stat.range;
	}
		if (ringEquipped)
			currentEntityStat = AddStats (currentEntityStat, equippedEquipmentRing.stat);
		if (necklaceEquipped)
			currentEntityStat = AddStats (currentEntityStat, equippedEquipmentNecklace.stat);
		if (artifactEquipped)
			currentEntityStat = AddStats (currentEntityStat, equippedEquipmentArtifact.stat);
		if (backEquipped)
			currentEntityStat = AddStats (currentEntityStat, equippedCostumeBack.stat);
		if (bodyEquipped)
			currentEntityStat = AddStats (currentEntityStat, equippedCostumeBody.stat);
		if (headEquipped)
			currentEntityStat = AddStats (currentEntityStat, equippedCostumeHead.stat);

		//add atk perc to both
		if (currentEntityStat.atkPerc != 0) {
			currentEntityStat.atkMin *= (1 + currentEntityStat.atkPerc);
			currentEntityStat.atkMax *= (1 + currentEntityStat.atkPerc);
		}

		//add def perc to both
		if (currentEntityStat.defPerc != 0) {
			currentEntityStat.defMin *= (1 + currentEntityStat.defPerc);
			currentEntityStat.defMax *= (1 + currentEntityStat.defPerc);
		}

		//add hp perc to both
		if (currentEntityStat.hpPerc != 0) {
			currentEntityStat.hpPerc *= (1 + currentEntityStat.hpPerc);
		}


		InventoryCanvas.Instance.SetStats ();
	}

	public Stats AddStats (Stats main, Stats toAdd)
	{
		if (toAdd == null)
			return main;//null catch
		main.atkMin += toAdd.atkMin;
		main.atkMax += toAdd.atkMax;
		main.atkPerc += toAdd.atkPerc;
		main.crit += toAdd.crit;
		main.critPower += toAdd.critPower;
		main.hpMax += toAdd.hpMax;
		main.hpPerc += toAdd.hpPerc;

		main.hpMax += main.hpMax * toAdd.hpPerc;
		//apply int to make it nice for UI
		main.hpMax = (int)main.hpMax;

		main.defPerc += toAdd.defPerc;

		main.defMin += toAdd.defMin;
		main.defMax += toAdd.defMax;
		main.atkCooldown += toAdd.atkCooldown;

		main.regen += toAdd.regen;
		return main;
	}

	public void SetDefaultWeapon ()
	{
		//Debug.Log("Had to set default fists");
		PhotonView photonViewOfAdventurer = null;
		if (currentScene == CustomSceneType.Town) {
			photonViewOfAdventurer = TownManager.Instance.myAtn.photonView;
			TownManager.Instance.myAtn.stance = Stance.Bareknuckle;//fist
		}

		if (currentScene == CustomSceneType.Battle) {
			photonViewOfAdventurer = BattleManager.Instance.myAdventurer.photonView;
			BattleManager.Instance.myAdventurer.stance = Stance.Bareknuckle;//fist
		}
		photonViewOfAdventurer.RPC ("CallSetAttachment", PhotonTargets.All, photonViewOfAdventurer.viewID, "Weapon1", "Weapon2", "Weapons/Empty", "Weapons/Empty");
	}

	public void SetDefaultBody ()
	{
		//Debug.Log("Had to set default fists");
		PhotonView photonViewOfAdventurer = null;
		if (currentScene == CustomSceneType.Town) {
			photonViewOfAdventurer = TownManager.Instance.myAtn.photonView;
			//  TownManager.Instance.myAtn.stance = Stance.Bareknuckle;//fist
		}

		if (currentScene == CustomSceneType.Battle) {
			photonViewOfAdventurer = BattleManager.Instance.myAdventurer.photonView;
			BattleManager.Instance.myAdventurer.stance = Stance.Bareknuckle;//fist
		}
		photonViewOfAdventurer.RPC ("CallSetSkin", PhotonTargets.All, photonViewOfAdventurer.viewID, "");
	}



	public void SetDefaultFace ()
	{
		// Debug.Log("Set Default Face");
	}

	public string GetFormattedTerm (string orig)
	{
		CustomDictionary cd = customDictionary.Where (x => x.original == orig).FirstOrDefault ();
		if (cd == null)
			return orig;
		return cd.formatted;
	}

	public void SetAllEquipment (PhotonView _photonView)
	{
		//SET BODY SKIN
		if (equippedCostumeBody == null) {
			_photonView.RPC ("CallSetSkin", PhotonTargets.All, _photonView.viewID, "");
		} else if (equippedCostumeBody.name == "") {
			_photonView.RPC ("CallSetSkin", PhotonTargets.All, _photonView.viewID, "");
		} else {
			ChangeEquipment (equippedCostumeBody, _photonView);
		}

		//SET HEAD ATTACH
		if (equippedCostumeHead == null) {
			_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, "Hat", "NOSLOT", "Weapons/Empty", "");
		} else if (equippedCostumeHead.name == "") {
			_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, "Hat", "NOSLOT", "Weapons/Empty", "");
		} else {
			ChangeEquipment (equippedCostumeHead, _photonView);
		}

		//SET BACK ATTACH
		if (equippedCostumeBack == null) {
			_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, "BackObj/Bag", "NOSLOT", "Weapons/Empty", "");
		} else if (equippedCostumeBack.name == "") {
			_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, "BackObj/Bag", "NOSLOT", "Weapons/Empty", "");
		} else {
			ChangeEquipment (equippedCostumeBack, _photonView);
		}

		//SET COSTUME ATTACH
		//	ChangeEquipment (equippedCostumeBack, _photonView);

		//SET WEAPON LAST
		//Debug.Log("SetAllEquipment:"+_photonView.viewID+" { ChangeEquipment(equippedWeapon) }");
		ChangeEquipment (equippedWeapon, _photonView);

		//Debug.Log("TRYING TO SET FACE!");
		ChangeFace (InventoryManager.Instance.GetFace (equippedFace), _photonView);

		if (equippedWeapon == null) {
			SetDefaultWeapon ();
		} else {
			if (equippedWeapon.name == "")
				SetDefaultWeapon ();
		}
	}

	public void ChangeFace (Face newFace, PhotonView _photonView)
	{
		equippedFace = newFace.ID;
		//Debug.Log ("CHANGE FACE:" + newFace.attachmentName);
		_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, "h", "NOSLOT", newFace.attachmentName, "");
	}

	public void ChangeEquipment (Item _item, PhotonView _photonView)
        //not all code paths finish execution
        //Dont put things at the end of the method
	{
		//apply slots/skins
		if (_item == null) {
			//print("******* Warning GameManager.ChangeEquipment Null Item passed as param");
			//SetDefaultWeapon();
			return;
		}
		if (_item.itemType == ItemType.weapon) {

			//Debug.Log("*** SetStanceClient called and weapon is:" + (int)equippedWeapon.stance);
			object[] args = new object[] { _photonView.viewID, (int)equippedWeapon.stance };
			_photonView.RPC ("SetStanceClient", PhotonTargets.All, args);
			_photonView.RPC ("SetParticlesAll", PhotonTargets.All, equippedWeapon.ID, _photonView.viewID);

			//Debug.Log("&& CHANGE EQUIPMENT CallSetAttachment");
			_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, _item.slotName, _item.slotName2, _item.atachmentName, _item.attachmentName2);

			//IN TOWN/BATTLE THIS IS HOW WE CHANGE EQUIPS
			//  Debug.Log("xxxxx ChangeEquipment SetStance:"+equippedWeapon.stance);


		}
		if (_item.itemType == ItemType.costume) {

			if (_item.costumeType == CostumeType.body) {
				_photonView.RPC ("CallSetSkin", PhotonTargets.All, _photonView.viewID, _item.skin);
			}
			if (_item.costumeType == CostumeType.head) {
				_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, _item.slotName, _item.slotName2, _item.atachmentName, _item.attachmentName2);
			}
			if (_item.costumeType == CostumeType.back) {
				_photonView.RPC ("CallSetAttachment", PhotonTargets.All, _photonView.viewID, _item.slotName, _item.slotName2, _item.atachmentName, _item.attachmentName2);
			}
		}
	}

	public string GetRandomCriticalPhrase ()
	{
		return CustomCriticalPhrases [Random.Range (0, CustomCriticalPhrases.Count ())];
	}

	public string GetSpecificCriticalPhrase (int val)
	{
		return CustomCriticalPhrases [val];
	}

	public void AddCompletedThing (string s)
	{
		completedThings.Add (s);
		//Debug.Log ("CompletedTHings.count:" + completedThings.Count);

	}

	public void CallInvasionUnlocked ()
	{
        return;
        //SOON WE WILL MAKE THIS HAVE LITTLE POP UPS AND BIG POP UPS

		// Debug.Log("Invasion Unlocked!");
		GameObject nextObj = ObjectPool.Instance.NextObject ("NewInvasionCanvas");
		NewInvasionCanvas nic = nextObj.GetComponent<NewInvasionCanvas> ();
		nic.SetImage (GetNextInvasionName (InvasionUnlock));//it will reach back and get the sprite from the GetInvasionImage Method
		nextObj.SetActive (true);
		cameraEffects.BustFeatheryLoad ();

		AudioManager.Instance.PlayClip ("invasion_unlock", true, false);
		GameObject feathers = ObjectPool.Instance.NextObject ("NormalFeathers");
		feathers.SetActive (true);

		InvasionUnlock = "";
	}

	public string GetNextInvasionName (string s)
	{
		string next = "";
		if (s == "WhenAllHopeIsLost")
			next = "BlasphemousMoonlightRuins";
		if (s == "BlasphemousMoonlightRuins")
			next = "CaveOfHumiliation";
		if (s == "CaveOfHumiliation")
			next = "MisadventuresInTheRiverOfGod";
		if (s == "MisadventuresInTheRiverOfGod")
			next = "TheGreatPurge";
		if (s == "TheGreatPurge")
			next = "SlimeKingdom";
		return next;
	}

	public Sprite GetInvasionImage (string s)
	{
		InvasionImageMap choseniim = new InvasionImageMap ();
		foreach (InvasionImageMap iim in invasionList) {
			//Debug.Log ("Get INvasion Image:" + s + " next iim.name:" + iim.name);

			if (iim.name == s) {

				choseniim = iim;
				break;
			}
		}
		return choseniim.icon;
	}

	public GameObject VoidObject;
	public SmoothFollow smoothFollowVoid;

	public void ToggleVoid (bool b)
	{
		
		//if (!VoidObject)return;
		VoidObject.SetActive (b);
		if (b) {
			TownCanvas.Instance.ToggleMe (false);

			AdventurerTownNetwork atn = TownManager.Instance.myAdventurerTownNetwork;
			atn.transform.position = GameObject.FindGameObjectWithTag ("VoidSpawn").transform.position;
			smoothFollowVoid.target = TownManager.Instance.myAtn.gameObject;
		}
	}

	public void CallLobby ()
	{
		InputEnabled = false;
		//Debug.Log("Clicked on the Lobby Button");
		PhotonNetwork.LeaveRoom ();
		// GameManager.Instance.mainMenu.showGUI = true;
		TownCanvas.Instance.ToggleMe (false);
		MenuCanvas.Instance.ToggleMe (false);
		LobbyCanvas.Instance.ToggleMe (true);
		LobbyCanvas.Instance.ToggleGoClose (false);//let it be toggled upon entering lobby again

		if (BattleCanvas.Instance)
			BattleCanvas.Instance.gameObject.SetActive (false);
	}



}

[System.Serializable]
public class ObjectByScene
{
	public string homeScene;
	public GameObject obj;

}

[System.Serializable]
public enum CustomSceneType
{
	Town = 0,
	Battle = 1,
	Clear = 2,
	Start = 3
}

[System.Serializable]
public class CustomDictionary
{
	public string original;
	public string formatted;

}


[System.Serializable]
public class InvasionImageMap
{
	public Sprite icon;
	public string name;
}
