﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WaveManager : MonoBehaviour
{
	//public WaveSet EpicRoamers = new WaveSet ();

	public List<EnemySeen> enemiesSeen = new List<EnemySeen> ();
	//add to this once per enemy per wave

	[Header ("How much these go up on each time you've seen an enemy")]
	private static WaveManager _instance;

	public static WaveManager Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<WaveManager> ();
			return _instance;
		}
	}

	//	void Start ()
	//	{
	//		WhenAllHopeIsLost.SetOrig ();
	//		BlasphemousMoonlightRuins.SetOrig ();
	//		CaveOfHumiliation.SetOrig ();
	//		MisadventuresInTheRiverOfGod.SetOrig ();
	//	}

	public void ResetEnemySeen ()
	{
		enemiesSeen.Clear ();
	}

	public int RegisterEnemySeen (string enemyName, int wave)
	{
		EnemySeen es = new EnemySeen ();
		es.enemyName = enemyName;
		es.waveSeenIn = wave;
		int _count = 0;

		//Get a count of how many times enemy been seen
		for (int inc = 0; inc < enemiesSeen.Count; inc++) {
			if (enemiesSeen [inc].enemyName == enemyName & enemiesSeen [inc].waveSeenIn != wave) {//dont want to accidentally increase them based on being seen in this current wave
				_count++;
			}
		}

		//See how many there have been in this wave
		int enemySeenB4 = enemiesSeen.Where (x => x.enemyName == enemyName & x.waveSeenIn == wave).Count ();
		//None? so add one - only one
		if (enemySeenB4 == 0)
			enemiesSeen.Add (new EnemySeen { enemyName = enemyName, waveSeenIn = wave });

		//Debug.Log("Count:" + _count);
		return _count;
	}

}
//
//[System.Serializable]
//public class WaveSet
//{
//	public List<Wave> waves;
//	[Header ("% Applies to all of the waves in this invasion wave set")]
//	[Range (0, 1f)]
//	public float atkGrowthRate, hpGrowthRate, xpGrowthRate;
//	float origAtkGrowthRate, origHpGrowthRate, origXpGrowthRate;
//	[Range (0, 1f)]
//	public float playerCountAtkGrow, playerCountHpGrow, playerCountXpGrow, playerCountItemChancesGrow;
//	float origPlayerCountAtkGrow, origPlayerCountHpGrow, origPlayerCountXpGrow, origPlayerCountItemChancesGrow;
//	}

[System.Serializable]
public class Wave
{
	public List<EnemySpawn> enemySpawns;
	public GameObject[]spawnPoints;
	public bool isUnlocksBarrier;
	[Header("Last enemy spawned, no barrier to unlock tho")]
	public bool isLastEnemyNoBarrier;
	public bool isFinal;
	public bool isBoss;
	[Header("Should be linked to the invisible barrier")]
	public GameObject[] barrier;
}

[System.Serializable]
public class EnemySpawn
{
	public string enemyName;
	[Header ("Currently supports 1,2,3,4,5,6,7,8")]
	public int spawner;
	[Range (0, 1f)]
	public float chance = 1;
}

[System.Serializable]
public class EnemySeen
{
	public string enemyName;
	public int waveSeenIn;
}