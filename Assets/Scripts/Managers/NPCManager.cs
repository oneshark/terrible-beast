﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NPCManager : MonoBehaviour {
    private static NPCManager _instance;
    public static NPCManager Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<NPCManager>();
            return _instance;
        }
    }
    public GameObject shopButton, craftButton;
   public List<Greetings> npcGreetings = new List<Greetings>();
    public List<NPCInfo> npcIndex = new List<NPCInfo>();
    public float MaxDistanceTalking = 3;
    public Animator anim, shopAnim;
    public CanvasGroup cg;
    public Text nameText, roleText;
    public string currentNPC = "";
    public bool NPCCanvasOpen = false;

   void Awake()
    {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetAvailableButtons(NPCInfo npcInfo)
    {
         shopButton.SetActive(npcInfo.hasShop);
         craftButton.SetActive(npcInfo.hasCrafting);
    }

    public string GetGreeting(string _npcName)
    {
        currentNPC = _npcName;
        List<Greetings> validGreetings = npcGreetings.Where(x => x.npcName == _npcName).ToList();
        string greeting = validGreetings[Random.Range(0, validGreetings.Count)].greeting;
        return greeting;
    }

    public NPCInfo GetNPCInfo(string _npcName)
    {
        NPCInfo _npcInfo = npcIndex.Where(x => x.npcName == _npcName).FirstOrDefault();
        return _npcInfo;
    }

    public void SetNPCText(string _npcNameText, string _npcNameRole)
    {
        nameText.text = _npcNameText;
        roleText.text = "◘ " + _npcNameRole + " ◘";
    }

    public void Exit()
    {
        ToggleNPCCanvas(false);
        MenuCanvas.Instance.ToggleMe(true);
        GameManager.Instance.InputEnabled = true;
        if (ShopManager.Instance.shopCanvasOpen) ShopManager.Instance.ToggleMe(false);
        if (InventoryCanvas.Instance.equipmentOpen) InventoryCanvas.Instance.ToggleEquipment(false);
    }

    public void OpenShop()
    {

        InventoryCanvas.Instance.ToggleEquipment(true);
        ShopManager.Instance.ToggleMe(true);
        InventoryCanvas.Instance.ToggleEquipmentSlots(false);
        InventoryCanvas.Instance.ToggleEquipmentStats(false);
        EventManager.Instance.CallInventoryStyleChange(false);
    }


    public void OpenCraft ()
	{
		if (!NPCCanvasOpen) {
		//	Debug.Log ("OPEnCRaFt");
			return;
		}
        //  InventoryCanvas.Instance.ToggleEquipment(true);
        // ShopManager.Instance.ToggleMe(true);
        ToggleNPCCanvas(false);
        CraftingCanvas.Instance.FadeMeIn();
    }

    public void ToggleNPCCanvas(bool b)
    {
        if (b == NPCCanvasOpen) return;
        if (b)
        {
            anim.CrossFade("NPCCanvasIn", 0);
            if (!GameManager.Instance.cameraEffects.isZoomed)
            {
                GameManager.Instance.cameraEffects.SetScrollValues(-7);
                GameManager.Instance.cameraEffects.ScrollLocal(TownManager.Instance.myAtn.transform.position + new Vector3(0, 2, 0));
                GameManager.Instance.cameraEffects.isZoomed = true;
            }
        }
        else
        {
            anim.CrossFade("NPCCanvasOut", 0);
            if (GameManager.Instance.cameraEffects.isZoomed)
            {
                GameManager.Instance.cameraEffects.SetScrollValues(-10);
                GameManager.Instance.cameraEffects.isZoomed = false;

            }
            GameManager.Instance.cameraEffects.InvokeFollow(.7f);
        }
        NPCCanvasOpen = b;
    }
}

[System.Serializable]
public class Greetings
{
    public string npcName;
    public string greeting;
}

[System.Serializable]
public class NPCInfo
{
    public string npcName;
    public string npcRole;
    public bool hasShop;
    public bool hasCrafting;
}