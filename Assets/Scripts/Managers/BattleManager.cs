using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.Analytics;


public class BattleManager : Photon.MonoBehaviour
{

    private static BattleManager _instance;

    public static BattleManager Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<BattleManager>();
            return _instance;
        }
    }

    [Header("How many seconds between regenerate calls")]
    public float regenInterval = 3;
    bool isPositionSynched = false;
    public int[] playerPositions = new int[4] { 0, 0, 0, 0 };
    Dictionary<string, int> playerPositionsIndex = new Dictionary<string, int>();

    public bool isStillSpawning = false;
    public List<Wave> currentWaveset = new List<Wave>();
    public int waveIndex = 0;
    [HideInInspector]
    public int nextPositionIndex;
    public AdventurerNetwork myAdventurer;
    public bool AdminControl = false;
    public int sessionSouls;
    public int highestWaveReached = 0;

    public string[] objToInstantiate;
    public GameObject[] objToDeactivateOnStartGame;
    public bool battleJoinSuccess = false;
    public float battleJoinTimerMax = 10;


    public void RefreshBattle()
    {
        waveIndex = 0;
        //if we are first, clear it out
        //   if(!BattleCanvas.Instance.m_matchStarted)playerPositions = new int[4] { 0, 0, 0, 0 };
        WaveManager.Instance.ResetEnemySeen();
        sessionSouls = 0;
        GameManager.Instance.skillsOwned.Clear();
        GameManager.Instance.statPoints = 0;
        EventManager.Instance.CallBattleStart();
        currentWave = null;
		photonView.RPC ("ToggleWaveSetActivated", PhotonTargets.All, false);

    }

    void OnJoinedRoom()
    {
        GameManager.Instance.waitingOnGuillotine = true;
        //Debug.Log("RoomName:" + PhotonNetwork.room.name);
        //Make it not joinable until character is spawned again
        photonView.RPC("ToggleJoinableMaster", PhotonTargets.MasterClient, false);
        RefreshBattle();
        Debug.Log("Point 0");
        StartGame();
        Debug.Log("Point 1");
        battleJoinSuccess = true;

        //set battle ping
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h["Ping"] = PhotonNetwork.GetPing();
        PhotonNetwork.room.SetCustomProperties(h);
    }

    [PunRPC]
    public void ToggleLockedMaster(bool b)
    {
        ToggleLocked(b);
    }

    public void ToggleLocked(bool b)
    {
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h["Locked"] = b;
        PhotonNetwork.room.SetCustomProperties(h);
    }

    [PunRPC]
    public void ToggleJoinableMaster(bool b)
    {
        //when a player joins, they toggle it to false
        //when the player is fully spawned, it toggles back to true
        //if its false and you tried to join, it will make you in an endless invoke and check value until true
        ToggleJoinable(b);
    }
    //
    //	public void DelayedJoinableTrue ()
    //	{
    //		ToggleJoinable (true);
    //	}
    public void ToggleJoinable(bool b)
    {
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h["Joinable"] = b;
        PhotonNetwork.room.SetCustomProperties(h);
    }

    IEnumerator BattleJoinTimer()
    {
        yield return new WaitForSeconds(battleJoinTimerMax);
        if (!battleJoinSuccess)
        {
            //Debug.Log("FAILED ATTEMPT AT JOINING A ROOM - BATTLE JOIN TIMER");
        }
        else
        {
            battleJoinSuccess = false;
        }
    }


    void OnPhotonJoinFailed()
    {
        Debug.Log("Caught! OnPhotonJoinFailed");
    }
   
    IEnumerator CallRegens()
    {
        //call it only on master but let everyone else run it
        yield return new WaitForSeconds(regenInterval);
        //call for every adventurer from masters
        if (PhotonNetwork.isMasterClient)
        {
            List<AdventurerNetwork> livingAdvent = GetAllLivingAdventurers();
            foreach (AdventurerNetwork an in livingAdvent)
            {
                an.CallHeal(0, true, true);
            }
        }

        StartCoroutine("CallRegens");
    }

    public void DeactivateObjectsOnStartGame()
    {
        foreach (GameObject obj in objToDeactivateOnStartGame)
        {
            obj.SetActive(false);
        }
    }

    IEnumerator OnLeftRoom()
    {
        while (PhotonNetwork.room != null || PhotonNetwork.connected == false)
            yield return 0;
    }

	public GameObject mapWhenAllHopeIsLost1, mapWhenAllHopeIsLost2, mapWhenAllHopeIsLost3, mapWhenAllHopeIsLost4, mapRuins1, mapRuins2, mapRuins3, mapRuins4,mapRiverOfGod1,mapRiverOfGod2,mapRiverOfGod3;

    void StartGame()
    {
        SetUpMap();//map contains all the spawn points, its needed before spawning anything else

        isGameOver = false;
        DeactivateObjectsOnStartGame();
        BackgroundMusicManager.Instance.PlayClip(mapStarted, 2);



        Invoke("SpawnCharacter", 2);
        BattleCanvas.Instance.ToggleMe(true);

        if (PhotonNetwork.isMasterClient)
        {
            BattleCanvas.Instance.SetStartInvasionButton();
        }
        else
        {
            Invoke("CheckGuillotineIn", 1);
        }
        StartCoroutine("CallRegens");


    }

    public void SpawnCharacter()//CANT SPAWN UNTIL WE KNOW OUR SPOT
    {
        //UNITY ANALYITICS
        Analytics.CustomEvent("STARTED_MAP", new Dictionary<string, object> {
            { "MAP",mapStarted},
            { "NAME", GameManager.Instance.PlayerName },
            { "LVL", GameManager.Instance.baseEntityStat.xpLevel }
        });

        int nextPositionIndex = GetNextOpenAdventurerPosition();

        object[] objs = new object[1]; // Put our data in an object array, to send

        Vector3 playerSpawnPoint = new Vector3(999,999, 0);
        playerSpawnPoint = GameObject.FindGameObjectWithTag("PlayerSpawn" + nextPositionIndex).transform.position;

        photonView.RPC("CallPlayerSpawnEffect", PhotonTargets.All, playerSpawnPoint.x, playerSpawnPoint.y);
        LoadingManager.Instance.CallDelayedClose(0);
        GameObject newObj = PhotonNetwork.Instantiate("entities/Adventurer", playerSpawnPoint, Quaternion.identity, 0, objs);
        //		if (mapStarted == "TheGreatPurge") {
        //			newObj.transform.localScale = new Vector3 (1.6f, 1.6f, 1.6f);
        //		}
        AdventurerNetwork adventurerNetwork = newObj.GetComponent<AdventurerNetwork>();
        adventurerNetwork.playerID = PhotonNetwork.player.ID;
        myAdventurer = adventurerNetwork;
        adventurerNetwork.Initialize();

        myAdventurer.playerPosition = nextPositionIndex;

        //DISABLE Mesh Renderer - play cool effect - then turn back on
        myAdventurer.mr.enabled = false;
        GameManager.Instance.SetMeshActiveLater(adventurerNetwork.mr, .5f);
        PhotonNetwork.player.name = GameManager.Instance.PlayerName;

        object[] args = new object[] { nextPositionIndex, 1, PhotonNetwork.player.name };
        photonView.RPC("SetPositionMaster", PhotonTargets.MasterClient, args);

        //if this is first player - break the chains right away
        GameObject[] myPlayerCount = GameObject.FindGameObjectsWithTag("Adventurer");
        // Debug.Log("Adventurers in game:" + myPlayerCount.Length);
        //ClientEnemyFlipping ();//if not the master, it will skip this

        //HARDCODED SOULBOUND OFF
        myAdventurer.photonView.RPC("BreakChains", PhotonTargets.All);
        BattleCanvas.Instance.ToggleYouAreSoulBound(false);

        CameraEffects ce = Camera.main.GetComponent<CameraEffects>();
        ce.scrollingCamera.enabled = false;
        ce.smoothFollowRandomizer.enabled = false;
        ce.smoothFollow.enabled = true;
        ce.smoothFollow.offset = new Vector3(0, 0, 0);
        ce.smoothFollow.target = myAdventurer.gameObject;
        ce.gameObject.transform.position = new Vector3(myAdventurer.transform.position.x, myAdventurer.transform.position.y,ce.gameObject.transform.position.z);

        Invoke("CallJoinableTrue", 1);
    }

    public void CallJoinableTrue()
    {
        photonView.RPC("ToggleJoinableMaster", PhotonTargets.MasterClient, true);

    }

    //public string AmbienceWhenAllHopeIsLost1, AmbienceWhenAllHopeIsLost2, AmbienceWhenAllHopeIsLost3;
    void SetUpMap()
    {
        mapStarted = GameManager.Instance.selectedMap;
        if (GameManager.Instance.selectedMap == "WhenAllHopeIsLost1")
        {
            mapWhenAllHopeIsLost1.SetActive(true);
        }
        if (GameManager.Instance.selectedMap == "WhenAllHopeIsLost2")
        {
            mapWhenAllHopeIsLost2.SetActive(true);
        }
        if (GameManager.Instance.selectedMap == "WhenAllHopeIsLost3")
        {
            mapWhenAllHopeIsLost3.SetActive(true);
        }
		if (GameManager.Instance.selectedMap == "WhenAllHopeIsLost4")
		{
			mapWhenAllHopeIsLost4.SetActive(true);
		}
        if (GameManager.Instance.selectedMap == "Ruins1")
        {
            mapRuins1.SetActive(true);
        }
        if (GameManager.Instance.selectedMap == "Ruins2")
        {
            mapRuins2.SetActive(true);
        }
        if (GameManager.Instance.selectedMap == "Ruins3")
        {
            mapRuins3.SetActive(true);
        }
		if (GameManager.Instance.selectedMap == "Ruins4")
		{
			mapRuins4.SetActive(true);
		}
		if (GameManager.Instance.selectedMap == "RiverOfGod1")
		{
			mapRiverOfGod1.SetActive(true);
		}
		if (GameManager.Instance.selectedMap == "RiverOfGod2")
		{
			mapRiverOfGod2.SetActive(true);
		}
		if (GameManager.Instance.selectedMap == "RiverOfGod3")
		{
			mapRiverOfGod3.SetActive(true);
		}
    }

    [PunRPC]
    public void BroadcastWaveIndexMaster()
    {
        //THIS IS IN THE MASTER AND IN TEH CLIENT VERSION OF THE CALL. IT SEEMS TO NOW WORK HERE
        photonView.RPC("BroadcastWaveIndex", PhotonTargets.Others, waveIndex);
    }

    [PunRPC]
    public void BroadcastWaveIndex(int _index)
    {
        waveIndex = _index;
        BattleCanvas.Instance.waveLevelText.text = (waveIndex).ToString();
    }

    public void SynchEnemyStats()
    {
        List<EnemyNetwork> enemyRemain = GetAllEnemies();
        for (int inc = 0; inc < enemyRemain.Count; inc++)
        {
            enemyRemain[inc].photonView.RPC("SyncStatsMaster", PhotonTargets.MasterClient, enemyRemain[inc].photonView.viewID);

        }
    }

    [PunRPC]
    public void CallPlayerSpawnEffect(float locX, float locY)
    {
        Vector3 playerSpawnPoint = new Vector3(locX, locY, 0);
        GameObject nextObj = ObjectPool.Instance.NextObject("PlayerSpawnEffect");
        nextObj.transform.position = playerSpawnPoint + new Vector3(1.75f, -.5f, 0);
        nextObj.SetActive(true);

    }

    #region adventurerPosition Code

    [PunRPC]
    public void SetPositionMaster(params object[] args)
    {
        try
        {
            int index = (int)args[0];
            int value = (int)args[1];
            string name = (string)args[2];

            if (index == 3 & value == 1)
            {
                photonView.RPC("ToggleLockedMaster", PhotonTargets.MasterClient, true);
            }

            playerPositions[index] = value;
            playerPositionsIndex.Add(name, index);
        }
        catch (System.Exception e)
        {
            Debug.Log("Error Occured:" + e);
            int index = (int)args[0];
            int value = (int)args[1];
            string name = (string)args[2];
            Debug.Log("** ERROR SetPositionMaster index:" + index + " value:" + value + " name:" + name);
            Debug.Log("Couldnt complete the set position to zero");
        }
    }

    //local
    int GetNextOpenAdventurerPosition()
    {
        //find number of players logged on
        for (int inc = 0; inc < playerPositions.Length; inc++)
        {
            if (playerPositions[inc] == 0)
            {
                // Debug.Log("&&&&&&&&&&&&   GetNextOpenAdventurerPosition:" + playerPositions[inc]);
                return inc;
            }
        }
        return 0;
        //return PhotonNetwork.countOfPlayers;
    }

    #endregion


    public void ChangeMyStance(Stance myNewStance)
    {
        //  Debug.Log("Called Change My Stance");
        object[] args = new object[] { myAdventurer.photonView.viewID, (int)myNewStance };
        myAdventurer.photonView.RPC("SetStanceMaster", PhotonTargets.MasterClient, args);
    }

    bool isGameOver = false;

    [PunRPC]
    public void GameOver()
    {
        if (isGameOver)
            return;
        isGameOver = true;
        BattleCanvas.Instance.ToggleGuillotineObjects(false);

        PlayerCanvas.Instance.gameObject.SetActive(false);
        BattleCanvas.Instance.ToggleMe(false);
        //we are all dead, chained or inactive so call game over
        AudioManager.Instance.PlayClip("defeat", true, false);
        waveSetActivated = false;//locally reset this one just in case
        photonView.RPC("ToggleWaveSetActivated", PhotonTargets.All, false);


        //	GameOverCanvas goc = goCanvas.GetComponent<GameOverCanvas> ();
        //	goc.SetHighestLevelReached (waveIndex);
        //skeleton hands and positions
        //	GameObject goSkeletonHands = ObjectPool.Instance.NextObject ("GameOver");
		GameObject goSkeletonHands = (GameObject)Instantiate(Resources.Load("GameOver"), new Vector3(Camera.main.transform.position.x,Camera.main.transform.position.y, 0), Quaternion.identity);

        //goSkeletonHands.transform.position = new Vector3 (0, myAdventurer.transform.position.y, myAdventurer.transform.position.z);
        goSkeletonHands.SetActive(true);
        //AnimationController animCont = goSkeletonHands.GetComponentInChildren<AnimationController> ();
        //animCont.SetAnimation ("animation", false, false);

        Invoke("DisableEnemies", 2.2f);//wait for black stuff to kind of cover up enemy before disabling them all

        BattleCanvas.Instance.ToggleYouAreDead(false);
        Invoke("DelayedGameOver", 4);
    }

    void DelayedGameOver()
    {

        GameObject[] gameovers = GameObject.FindGameObjectsWithTag("GameOver");
        foreach (GameObject obj in gameovers)
        {
            obj.SetActive(false);
        }

        GameObject defeatGraphics = ObjectPool.Instance.NextObject("DefeatGraphics");
        defeatGraphics.SetActive(true);
    }

    public void DisableEnemies()
    {
        //We are at game over, let's turn off all of this stuff
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        for (int inc = 0; inc < enemies.Length; inc++)
        {
            enemies[inc].SetActive(false);
        }
    }

    public void CheckAdventurersRemain()
    {
        //check if all adventurers dead
        //if so, call game over canvas up
        bool anyoneNormal = false;
        GameObject[] advRemain = GameObject.FindGameObjectsWithTag("Adventurer");
        List<AdventurerNetwork> advNetworkRemain = new List<AdventurerNetwork>();
        foreach (GameObject obj in advRemain)
        {
            AdventurerNetwork nextAdvNetwork = obj.GetComponent<AdventurerNetwork>();
            // Debug.Log("Check Adv Remain adventurer status:" + nextAdvNetwork.charState);
            if (nextAdvNetwork.charState == CharacterState.normal)
                anyoneNormal = true;
        }
        if (!anyoneNormal)
        {
            photonView.RPC("GameOver", PhotonTargets.All);
        }
    }

    #region WAVES Logic

    public int EnemiesRemaining()
    {

        GameObject[] enemyRemain = GameObject.FindGameObjectsWithTag("Enemy");
        return enemyRemain.Length;
    }

    public int AdventurersRemaining()
    {
        GameObject[] advRemain = GameObject.FindGameObjectsWithTag("Adventurer");
        return advRemain.Length;
    }

    public List<AdventurerNetwork> GetAllLivingAdventurers()
    {
        GameObject[] advRemain = GameObject.FindGameObjectsWithTag("Adventurer");
        List<AdventurerNetwork> advNetworkRemain = new List<AdventurerNetwork>();
        foreach (GameObject obj in advRemain)
        {
            AdventurerNetwork nextAdvNetwork = obj.GetComponent<AdventurerNetwork>();
            if (nextAdvNetwork.charState == CharacterState.dead || nextAdvNetwork.charState == CharacterState.chained)
                continue; //dont let the dead ones into the loop
            if (!nextAdvNetwork.isInitialized)
                continue;
            advNetworkRemain.Add(nextAdvNetwork);
        }
        return advNetworkRemain;
    }

	public void KillAllEnemies(){
		//Shuffled OrderBy code
		List<EnemyNetwork> allEnemies = BattleManager.Instance.GetAllEnemies ().OrderBy (a => Guid.NewGuid ()).ToList ();

		List<EnemyNetwork> enemiesChosen = new List<EnemyNetwork> ();
		// Debug.Log("ExcludeID:" + excludePhotonId);
		for (int inc = 0; inc < allEnemies.Count; inc++) {

			EnemyNetwork nextEn = allEnemies [inc];


				float damage = 99999;
				bool isSilent = false;
				bool isCrit = false;
				object[] args = new object[] { damage, nextEn.photonView.viewID, isSilent, isCrit, photonView.viewID };
			nextEn.photonView.RPC ("TakeDamageMaster", PhotonTargets.MasterClient, args);

		}
	}


    public AdventurerNetwork GetLivingAdventurerByViewID(int _id)
    {
        GameObject[] advRemain = GameObject.FindGameObjectsWithTag("Adventurer");
        List<AdventurerNetwork> advNetworkRemain = new List<AdventurerNetwork>();
        //Debug.Log("(((( _id:" + _id);
        foreach (GameObject obj in advRemain)
        {
            AdventurerNetwork nextAdventurer = obj.GetComponent<AdventurerNetwork>();
            //Debug.Log("(((( GetLivingAdventurerByViewID:" + nextAdventurer.photonView.viewID);
            if (nextAdventurer.charState == CharacterState.dead)
                continue; //dont let the dead ones into the loop
                          //Debug.Log("(((( Not Dead");
            if (!nextAdventurer.isInitialized)
                continue;
            //Debug.Log("(((( isInitialized");
            //Debug.Log("nextAdv id:"+nextAdventurer.photonView.viewID);
            if (nextAdventurer.photonView.viewID == _id)
                return nextAdventurer;
        }
        //Debug.Log("(((( About to return Null");
        Debug.Log("WARNING! BattleManager Get Adventurer By View ID FAILED!");

        return null;
    }

    public List<EnemyNetwork> GetAllEnemies()
    {
        GameObject[] enemyRemain = GameObject.FindGameObjectsWithTag("Enemy");
        List<EnemyNetwork> enemyNetworkRemain = new List<EnemyNetwork>();
        foreach (GameObject obj in enemyRemain)
        {
            enemyNetworkRemain.Add(obj.GetComponent<EnemyNetwork>());
        }
        return enemyNetworkRemain;
    }

    public void CheckWaveComplete()
    {
       // Debug.Log("### Check Wave Complete");
        if (EnemiesRemaining() <= 0 & !isStillSpawning)
        {//last enemy called dead and we are done spawning
            WaveComplete();
        }
      //  Invoke("CheckWaveComplete", 2);//trying this in case it misses the firsst one!
    }

    public void WaveComplete()
    {
       // Debug.Log("### Wave Complete");
        if (currentWave==null)
        {
            Debug.Log("BattleManager.WaveComplete() Warning - there is no current wave. Possibly because host left!");
            return;
        }

        if (currentWave.isFinal)
        {
            photonView.RPC("CallVictory", PhotonTargets.All);

            return;
        }
        if (currentWave.isUnlocksBarrier) //LAST WAVE OF TRIGGER
        {
			photonView.RPC ("ToggleWaveSetActivated", PhotonTargets.All, false);
			photonView.RPC ("CallWaveCompleteCanvas", PhotonTargets.All);
            RemoveBarrier();

		}else if(currentWave.isLastEnemyNoBarrier){  //LAST WAVE OF TRIGGER
			photonView.RPC ("ToggleWaveSetActivated", PhotonTargets.All, false);
			photonView.RPC ("CallWaveCompleteCanvas", PhotonTargets.All);
			//do nothing
		}
        else
        {
            waveIndex++;
            SpawnWave(currentWaveset[waveIndex]);
        }
    }

	[PunRPC]
	void CallWaveCompleteCanvas(){
        //GameObject waveCompleteCanvas = (GameObject)Instantiate(Resources.Load("UI/WaveCompleteCanvas"), new Vector3(0,0, 0), Quaternion.identity);
        Invoke("WaveCompleteCanvasDelayed", 1.5f);
	}

    void WaveCompleteCanvasDelayed()
    {
        GameObject waveCompleteCanvas = (GameObject)Instantiate(Resources.Load("UI/WaveCompleteCanvas"), new Vector3(0, 0, 0), Quaternion.identity);
    }

    void RemoveBarrier()
    {
        for (int inc = 0; inc < currentWave.barrier.Count(); inc++)
        {
            PhotonView barrierView = currentWave.barrier[inc].GetComponent<PhotonView>();
            barrierView.RPC("RemoveBarrier", PhotonTargets.All, barrierView.viewID);
        }
    }

	[PunRPC]
	public void ToggleWaveSetActivated(bool b){
		waveSetActivated=b;
	}

    [PunRPC]
    public void CallVictory()
    {
        //Debug.Log("### Call Victory!");
        //UNITY ANALYITICS
        Analytics.CustomEvent("FINISHED_MAP", new Dictionary<string, object> {
            { "MAP",mapStarted},
            { "NAME", GameManager.Instance.PlayerName },
            { "LVL", GameManager.Instance.baseEntityStat.xpLevel }
        });


        GameManager.Instance.statPoints = 0;

        GameManager.Instance.ActivateDeactivate("Clear");
        Invoke("CallVictoryDelayed", 7.2f);
        if (!GameManager.Instance.completedThings.Contains(mapStarted))
        {
            GameManager.Instance.InvasionUnlock = mapStarted;
            // EventManager.Instance.CallInvasionFinished();
        }
        GameManager.Instance.completedThings.Add(mapStarted);
    }

    public string mapStarted = "";

    public void CallVictoryDelayed()
    {
        BackgroundMusicManager.Instance.StopAudioSource();
        AudioManager.Instance.PlayClip("victory", true, false);
        Invoke("CallVictoryDelayedFadeIn", 1.5f);
    }

    void CallVictoryDelayedFadeIn()
    {
        GameObject nextObj = ObjectPool.Instance.NextObject("Victory");
        nextObj.SetActive(true);
    }

    [PunRPC]
    void BossEffects()
    {
        GameManager.Instance.CallPlayTutorialBlock(0, 1, true);//1 is boss notification

        Invoke("BossEffectsDelayed", 1.5f);
    }

    void BossEffectsDelayed()
    {
        AudioManager.Instance.PlayClip("boss", true, false);
        CameraEffects ce = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraEffects>();
        ce.CallCameraShake(.6f);
        //GameObject nextObj = ObjectPool.Instance.NextObject ("BossExplosions");
        //nextObj.SetActive (true);
    }

   

    [PunRPC]
    public void UnchainAll()
    {
        //Debug.Log("UNCHAIN ALL");
        GameObject[] advRemain = GameObject.FindGameObjectsWithTag("Adventurer");
        List<AdventurerNetwork> advNetworkRemain = new List<AdventurerNetwork>();

        //Turn off local camera blur
        if (myAdventurer.charState == CharacterState.chained)
            BattleCanvas.Instance.ToggleYouAreSoulBound(false);

        foreach (GameObject obj in advRemain)
        {
            AdventurerNetwork nextAdvNetwork = obj.GetComponent<AdventurerNetwork>();
            if (nextAdvNetwork.charState == CharacterState.chained)
            {
                nextAdvNetwork.BreakChains();
            }
        }

    }

    [PunRPC]
    public void DuplicateWave()
    {
        List<Wave> newWaves = Utilities.DeepClone(currentWaveset);
        foreach (Wave w in newWaves)
        {
            currentWaveset.Add(w);
        }
        // Debug.Log("______DOUBLED THE WAVES");
    }

    [PunRPC]
    public void SetWave(int _waveIndex)
    {
        waveIndex = _waveIndex;
        //TRYING THIS
        int INTERVAL_FOR_STATPOINTS = 3;
        if (waveIndex != 0 & Utilities.IsDivisble(waveIndex, INTERVAL_FOR_STATPOINTS))
        {
            GameManager.Instance.statPoints += 1;
            //Delay, ClipIndex
            //GameManager.Instance.CallPlayTutorialBlock(1.5f,3,true);
        }

        if (waveIndex != 0 & Utilities.IsDivisble(waveIndex, 5))
        {
            //Delay, ClipIndex
            GameManager.Instance.CallPlayTutorialBlock(0, 9, true);
        }
        //END


        BattleCanvas.Instance.waveLevelText.text = (waveIndex).ToString();
        PlayerCanvas.Instance.CallWave(waveIndex);
    }

    [PunRPC]
    public void SyncWaveTextMaster()
    {

        photonView.RPC("SyncWaveTextClient", PhotonTargets.Others, waveIndex + 1);
    }

    [PunRPC] //CALL THIS ON MASTER TO SYNCH ALL
    public void SyncWaveTextClient(int _waveIndex)
    {
        BattleCanvas.Instance.waveLevelText.text = (waveIndex).ToString();
    }

    /// <summary>
    /// SEND THIS A WAVE AND IT WILL SPAWN YOUR ENEMIES!
    /// </summary>
    /// <param name="nextWave">Next wave.</param>
    /// 
    public bool waveSetActivated = false;
    public void StartWaveset(List<Wave> wavesIncoming)
    {
		photonView.RPC ("ToggleWaveSetActivated", PhotonTargets.All, true);
        currentWaveset = wavesIncoming;
        waveIndex = 0;
        SpawnWave(currentWaveset[waveIndex]);
    }

    public Wave currentWave = new Wave();
    public void SpawnWave(Wave nextWave)
    {

        currentWave = nextWave;
        if (nextWave.isBoss)
        {
            photonView.RPC("BossEffects", PhotonTargets.All);
        }

        isStillSpawning = true;//keep this set until all are spawned;

        //Set room props - who cares if we do it 4 times
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h["Wave"] = waveIndex;
        h["Ping"] = PhotonNetwork.GetPing();

        PhotonNetwork.room.SetCustomProperties(h);

        GameObject spawner0 = nextWave.spawnPoints[0];
        GameObject spawner1 = nextWave.spawnPoints[1];
        GameObject spawner2 = nextWave.spawnPoints[2];
        GameObject spawner3 = nextWave.spawnPoints[3];
        GameObject spawner4 = nextWave.spawnPoints[4];
        GameObject spawner5 = nextWave.spawnPoints[5];
        GameObject spawner6 = nextWave.spawnPoints[6];
        GameObject spawner7 = nextWave.spawnPoints[7];
        GameObject spawner8 = nextWave.spawnPoints[8];

        Vector3 nextSpawnPoint = new Vector3(0, 5, 0);//default position if nothing chosen

        int incrementor = 0;
        foreach (EnemySpawn enemySpawn in nextWave.enemySpawns)
        {
            if (UnityEngine.Random.Range(0, 1f) > enemySpawn.chance)
                continue;//spawn chance

            string enemySpawnString = "entities/enemy/" + enemySpawn.enemyName;

            if (enemySpawn.spawner == 0)
                nextSpawnPoint = spawner1.transform.position;
            if (enemySpawn.spawner == 1)
                nextSpawnPoint = spawner1.transform.position;
            if (enemySpawn.spawner == 2)
                nextSpawnPoint = spawner2.transform.position;
            if (enemySpawn.spawner == 3)
                nextSpawnPoint = spawner3.transform.position;
            if (enemySpawn.spawner == 4)
                nextSpawnPoint = spawner4.transform.position;
            if (enemySpawn.spawner == 5)
                nextSpawnPoint = spawner5.transform.position;
            if (enemySpawn.spawner == 6)
                nextSpawnPoint = spawner6.transform.position;
            if (enemySpawn.spawner == 7)
                nextSpawnPoint = spawner7.transform.position;
            if (enemySpawn.spawner == 8)
                nextSpawnPoint = spawner8.transform.position;

            StartCoroutine(SpawnEnemy(enemySpawnString, nextSpawnPoint, (float)incrementor));
            incrementor++;
        }
        isStillSpawning = false;
    }

    IEnumerator SpawnEnemy(string enemySpawnString, Vector3 nextSpawnPoint, float delay)
    {
        yield return new WaitForSeconds(delay);
        int countOfEnemies = WaveManager.Instance.RegisterEnemySeen(enemySpawnString, waveIndex + 1);
        object[] args = new object[1]; // Put our data in an object array, to send
                                       //Debug.Log("Spawn Enemy");
        GameObject newObj = PhotonNetwork.InstantiateSceneObject(enemySpawnString, nextSpawnPoint, Quaternion.identity, 0, args);

        EnemyNetwork en = newObj.GetComponent<EnemyNetwork>();
        en.Initialize();

        //Debug.Log ("*******SPAWN ENEMYMAP STARTED:" + mapStarted);
        //		if (mapStarted == "TheGreatPurge") {
        //			//float origScale = newObj.transform.localScale.x;
        //			//Debug.Log ("**OrigScale:" + origScale);
        //			newObj.transform.localScale = new Vector3 (1.6f, 1.6f,1.6f);
        //		//	Debug.Log ("*NewScalex:" + newObj.transform.localScale.x);
        //		}

        //enemies seen multiplier
        //	float atkInc = countOfEnemies * currentWaveset.atkGrowthRate;
        //	float hpInc = countOfEnemies * currentWaveset.hpGrowthRate;
        //	float xpInc = countOfEnemies * currentWaveset.xpGrowthRate;

        //player count multiplier
        //if it's zero - then this will be an empty value
        int playerCount = PhotonNetwork.playerList.Length - 1;
        float playerCountAtkInc = playerCount * .15f;
        float playerCountHpInc = playerCount * .25f;
        float playerCountXpInc = playerCount * .1f;

        en.baseEntityStat.atkMin *= (1 + playerCountAtkInc);
        en.baseEntityStat.atkMax *= (1 + playerCountAtkInc);
        en.baseEntityStat.hp *= (1 + playerCountHpInc);
        en.baseEntityStat.hpMax *= (1 + playerCountHpInc);
        en.expGiven *= (1 + playerCountXpInc);
        en.currentEntityStat.atkMin *= (1 + playerCountAtkInc);
        en.currentEntityStat.atkMax *= (1 + playerCountAtkInc);
        en.currentEntityStat.hp *= (1 + playerCountHpInc);
        en.currentEntityStat.hpMax *= (1 + playerCountHpInc);
        //en.currentEntityStat.xp *=(1+xpInc);

        en.InvokeCallSyncStatMaster(.2f);
    }

    #endregion

    void OnDisconnectedFromPhoton()
    {
        //Debug.Log("OnDisconnectedFromPhoton");
    }

    void OnPhotonPlayerDisconnected(PhotonPlayer photonPlayer)
    {
        Debug.Log("OnPhotonPlayerDisconnected() so try to clear up the right spot next time");
        //	CallRefreshPositions ();
    }

    [PunRPC]
    public void SpawnGrave(bool isGrave, int position)
    {

        GameObject pos = GameObject.FindGameObjectWithTag("PlayerSpawn" + position);

        GameObject nextObj = ObjectPool.Instance.NextObject("PoofEffect");
        nextObj.transform.position = pos.transform.position;
        nextObj.SetActive(true);

        string markerName = "Grave";
        if (!isGrave)
            markerName = "Shit";
        GameObject markerObj = ObjectPool.Instance.NextObject(markerName);
        markerObj.transform.position = pos.transform.position;
        markerObj.SetActive(true);
    }

    //ONLY CALL ON MASTER!!!!!!!!!!
    public void CallRefreshPositions()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

		playerPositions = new int[] { 0, 0, 0, 0 };

	//11.5.17 REPLACE TO ACTIVE AND NOT COMM OUT
       // photonView.RPC("ToggleJoinableMaster", PhotonTargets.MasterClient, false);
        //		Invoke ("DelayedJoinableTrue", 1.25f);//dont let people joinw while we r waiting for the positions to be symched again
     
		//11.4.17 REPLACE To .75
		//Invoke("DelayedJoinableTrue", .75f);//dont let people joinw while we r waiting for the positions to be symched again

        //Call broadcast positions to all (including master)
        photonView.RPC("BroadcastPosition", PhotonTargets.All);
    }

    [PunRPC]
    public void BroadcastPosition()
    {
        int playerIndex;
        string _playerName = PhotonNetwork.player.name;
        playerPositionsIndex.TryGetValue(_playerName, out playerIndex);

        object[] args = new object[] { playerIndex, 1, _playerName };
        photonView.RPC("SetPositionMaster", PhotonTargets.MasterClient, args);

    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data
            stream.SendNext(playerPositions[0]);
            stream.SendNext(playerPositions[1]);
            stream.SendNext(playerPositions[2]);
            stream.SendNext(playerPositions[3]);
        }
        else
        {
            //Network player, receive data
            playerPositions[0] = (int)stream.ReceiveNext();
            playerPositions[1] = (int)stream.ReceiveNext();
            playerPositions[2] = (int)stream.ReceiveNext();
            playerPositions[3] = (int)stream.ReceiveNext();

        }
    }

    void CheckGuillotineIn()//if there are no enemies, pull guillotine in
    {
        //  Debug.Log("***************************** match started:" + BattleCanvas.Instance.m_matchStarted);
        if (!BattleCanvas.Instance.m_matchStarted)
        {
            BattleCanvas.Instance.guillotineScript.ToggleMe(true);
        }
        else
        {
            //  Debug.Log("Turn off guillotine");
            //    BattleCanvas.Instance.guillotineScript.ForceOff();
            BattleCanvas.Instance.TurnOnBattleMenus();
        }
    }

    public EnemyNetwork GetRandomEnemyBySide(int side)
    {
        List<EnemyNetwork> allEnemies = BattleManager.Instance.GetAllEnemies().OrderBy(a => Guid.NewGuid()).ToList();

        List<EnemyNetwork> enemiesChosen = new List<EnemyNetwork>();
        // Debug.Log("ExcludeID:" + excludePhotonId);
        for (int inc = 0; inc < allEnemies.Count; inc++)
        {

            EnemyNetwork nextEn = allEnemies[inc];

            if (nextEn.gameObject.transform.position.x < transform.position.x & side == 0)
                return nextEn;

            if (nextEn.gameObject.transform.position.x > transform.position.x & side == 1)
                return nextEn;

        }
        return null;
    }


}

[System.Serializable]
public class WaveMultipliers
{
    public float hpPerc;
    public float atkPerc;
    public float defPerc;
    public float critPerc;
    public float xpPerc;
}

[System.Serializable]
public class Stats
{
    public bool isRanged;
    [Header("New stat for how far away you can attack from!")]
    public float range = 0f;
    public float atkMin, atkMax;
    public float atkPerc;
    public float defPerc;
    public float atkCooldown;
    public float defMin, defMax;
    [Header("EX: 1=happens 1% of the time, 50=critical happens on 50% of hits")]
    public float crit;
    [Header("Ex: 1.1 = Critical is 10% higher, 2 = crit hit twice as hard as normal")]
    public float critPower;
    public float hp, hpMax, hpPerc;
    public float xp, xpMax, xpLevel;
    public float regen;
    public float walkSpeed;
	[Range(0,1500)]
	public float knockbackPower;
    public Stats MirrorStat(Stats statsOriginal)
    {
        Stats statsNew = new Stats();
        statsNew.isRanged = statsOriginal.isRanged;
        statsNew.atkMin = statsOriginal.atkMin;
        statsNew.atkMax = statsOriginal.atkMax;
        statsNew.atkPerc = statsOriginal.atkPerc;
        statsNew.atkCooldown = statsOriginal.atkCooldown;
        statsNew.defPerc = statsOriginal.defPerc;

        statsNew.defMin = statsOriginal.defMin;
        statsNew.defMax = statsOriginal.defMax;

        statsNew.crit = statsOriginal.crit;
        statsNew.critPower = statsOriginal.critPower;

        statsNew.hp = statsOriginal.hp;
        statsNew.hpPerc = statsOriginal.hpPerc;

        statsNew.hpMax = statsOriginal.hpMax;
        statsNew.xp = statsOriginal.xp;
        statsNew.xpMax = statsOriginal.xpMax;
        statsNew.xpLevel = statsOriginal.xpLevel;
        statsNew.regen = statsOriginal.regen;
        statsNew.walkSpeed = statsOriginal.walkSpeed;
		statsNew.knockbackPower = statsOriginal.knockbackPower;

        return statsNew;
    }

}

[System.Serializable]
public enum Stance ///KEEP THESE IN ORDER
{
    Onehander = 0,
    Bow = 1,
    Dualwield = 2,
    Twohander = 3,
    Cannon = 4,
    Rod = 5,
    Bareknuckle = 6
}

[System.Serializable]
public class MapRealNames{
	public string systemName;
	public string realName;
}

public class StanceInfo
{
    public bool isRanged;
    public bool AOEbySide;
    public int AOENumberEnemies = 1;
    public float AOEPerc = 1;

    public StanceInfo GetStanceInfo(Stance stance)
    {
        StanceInfo stanceInfo = new StanceInfo();
        //ranged
        stanceInfo.isRanged = false;
        stanceInfo.AOEbySide = false;
        if (stance == Stance.Bow)
            stanceInfo.isRanged = true;
        if (stance == Stance.Cannon)
        {
            stanceInfo.isRanged = true;
            stanceInfo.AOEbySide = true;
            stanceInfo.AOENumberEnemies = 2;
            stanceInfo.AOEPerc = .5f;
        }
        if (stance == Stance.Rod)
            stanceInfo.isRanged = true;

        return stanceInfo;
    }
}

