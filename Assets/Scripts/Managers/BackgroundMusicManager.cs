﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class BackgroundMusicManager : MonoBehaviour
{
	bool isFadingOut, isFadingIn;
	string pastScene = "";
	public AudioSource audioSource;
	public BackgroundMusicClip[] clips;
	public float maxPause = 3;
	//Singleton and checks for null
	private static BackgroundMusicManager _instance;

	public static BackgroundMusicManager Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<BackgroundMusicManager> ();

			return _instance;
		}
	}
	//Singleton complete

	// Use this for initialization

	void StartMusic (string clipName)
	{
		float nextPause = UnityEngine.Random.Range (0, maxPause);
		PlayClip (clipName, nextPause);
	}

	public void StopAudioSource(){
		StartCoroutine (AudioFadeOut (audioSource, 1.5f));
	}
	public void PlayClip (string clipName, float pauseAfter)
	{
		//   Debug.Log("CLIP CALLED:" + backgroundMusicType);
		//get all valid clips
		List<BackgroundMusicClip> clipsOfType = clips.Where (x => x.clipName == clipName).ToList ();
		//choose a random clip
		UnityEngine.AudioClip chosenClip = clipsOfType [Random.Range (0, clipsOfType.Count)].clip;
		//feed audiosource a clip
		audioSource.clip = chosenClip;
		audioSource.Play ();
		StartCoroutine (AudioFadeIn (audioSource, AudioManager.Instance.getBGVolume (), 1.6f));

		audioSource.loop = true;
		//StopAllCoroutines();
	}

	public void Restart (string LevelToLoad)
	{
		//fade out the audio
		StartCoroutine (AudioFadeOut (audioSource, 1.5f));
		// StartMusic(LevelToLoad);
		StartCoroutine (AudioFadeIn (audioSource, AudioManager.Instance.getBGVolume (), 1.6f));
		//fade in the new audio
	}


	public IEnumerator AudioFadeOut (AudioSource audioSource, float FadeTime)
	{
		isFadingIn = false;
		isFadingOut = true;
		float startVolume = audioSource.volume;

		while (audioSource.volume > 0) {
			audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
			yield return null;
			if (!isFadingOut)
				break;
		}
		audioSource.Stop();

	}

	public IEnumerator AudioFadeIn (AudioSource audioSource, float fullVolume, float initialDelay)
	{
		//audioSource.Play();
		yield return new WaitForSeconds (initialDelay);
		//StartMusic (LevelToLoad);//start new track and fade that beyotch in
		isFadingIn = true;
		isFadingOut = false;
		if (!audioSource.isPlaying) {
			audioSource.PlayOneShot (audioSource.clip);
		}
		while (audioSource.volume < fullVolume) {
			audioSource.volume += .03f;
			yield return null;
			if (!isFadingIn)
				break;
		}
	}
}

[System.Serializable]
public struct BackgroundMusicClip
{
	public UnityEngine.AudioClip clip;
	public string clipName;
}

