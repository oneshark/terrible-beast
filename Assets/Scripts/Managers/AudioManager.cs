﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    public AudioClipRef[] clips;
    public AudioSource[] audioSource;
    public AudioSource musicOnlyAudioSource;
    public PhotonView photonView;
    public float soundLevel, origSoundLevel;
    public float musicLevel, origMusicLevel;
    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<AudioManager>();

            return _instance;
        }
    }

    void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    void Start()
    {
        DontDestroyOnLoad(this);
        audioSource = GetComponents<AudioSource>();
        Invoke("setLevels", 2);
    }

    public void setLevels()
    {
        //Debug.Log("SetLevels");
        float sfxVolume = getSFXVolume();
        float bgVolume = getBGVolume();
        if (EventManager.Instance) EventManager.Instance.CallAudioLevelChanged();
        audioSource[0].volume = sfxVolume;
        audioSource[1].volume = sfxVolume;
        audioSource[2].volume = sfxVolume;
        audioSource[3].volume = sfxVolume;
        audioSource[4].volume = sfxVolume;
        audioSource[5].volume = sfxVolume;
		audioSource[6].volume = sfxVolume;
		audioSource[7].volume = sfxVolume;
		audioSource[8].volume = sfxVolume;
		audioSource[9].volume = sfxVolume;
    }

    public float getSFXVolume()
    {
        return soundLevel;
    }

    public float getBGVolume()
    {
        return musicLevel;
    }

    public void PlayAttackSound(string weaponSound)
    {
        PlayClip(weaponSound);
    }

    public enum DeathType { blackfeather, juicy, human_male, human_female }
    public void PlayDeathSound(DeathType _deathType)
    {
        //print("Played Attack Sound");
        switch (_deathType)
        {
            case DeathType.human_male:
                PlayClip("male_death");
                break;
        }
    }

    public void Hover()
    {
        PlayClip("hover");
    }

    public void PlayClip(string _clipName)
    //auto format it
    {
        PlayClip(_clipName, false, false);
    }

    public void PlayClip(string _clipName, bool isLocal, bool shouldLoop)
    {
        //Sword Rotation
        if (_clipName == "sword")
        {
            _clipName = _clipName + Random.Range(1, 4).ToString();
        }

        if (_clipName == "punch")
        {
            _clipName = _clipName + Random.Range(1, 5).ToString();
        }

        if (_clipName == "twohander")
        {
            _clipName = _clipName + Random.Range(1, 5).ToString();
        }

        if (_clipName == "dualwield")
        {
            _clipName = _clipName + Random.Range(1, 5).ToString();
        }
        if (isLocal)
        {//LOCAL SOUND
            ClipRouterPlay(_clipName, shouldLoop);
        }
        else
        {//NETWORK SOUND - CALL FOR ALL
            photonView.RPC("ClipRouterPlay", PhotonTargets.All, _clipName, shouldLoop);
        }
    }



    [PunRPC]
    public void ClipRouterPlay(string _clipName, bool shouldLoop)
    {
        AudioSource nextFreeAudioSource;
        if (!audioSource[0].isPlaying)
        {
            nextFreeAudioSource = audioSource[0];
        }
        else if (!audioSource[1].isPlaying)
        {
            nextFreeAudioSource = audioSource[1];
        }
        else if (!audioSource[2].isPlaying)
        {
            nextFreeAudioSource = audioSource[2];
        }
        else if (!audioSource[3].isPlaying)
        {
            nextFreeAudioSource = audioSource[3];
        }
        else if (!audioSource[4].isPlaying)
        {
            nextFreeAudioSource = audioSource[4];
        }
        else if (!audioSource[5].isPlaying)
        {
            nextFreeAudioSource = audioSource[5];
        }
        else if (!audioSource[6].isPlaying)
        {
            nextFreeAudioSource = audioSource[6];
        }
        else if (!audioSource[7].isPlaying)
        {
            nextFreeAudioSource = audioSource[7];
        }
        else if (!audioSource[8].isPlaying)
        {
            nextFreeAudioSource = audioSource[8];
        }
        else if (!audioSource[9].isPlaying)
        {
            nextFreeAudioSource = audioSource[9];
        }
        else
        {
            nextFreeAudioSource = audioSource[0];
            nextFreeAudioSource.Stop();
        }
        nextFreeAudioSource.loop = shouldLoop;

        AudioClipRef audioClipRef = clips.Where(x => x.clipName == _clipName).FirstOrDefault();
        if (audioClipRef.clipName == "")
        {
            Debug.Log("AUDIO CLIP NOT FOUND:" + _clipName);
            return;
        }
        nextFreeAudioSource.pitch = GetPitch(audioClipRef.clipName);
        nextFreeAudioSource.clip = audioClipRef.clip;
        nextFreeAudioSource.Play();
    }

    [PunRPC]
    public void StopLoop()
    {
        if (GameManager.Instance.currentScene == CustomSceneType.Battle)
        {
            //	Debug.Log ("========== WHOAH - tried to stop loops while in battle!");
        }

        AudioSource nextFreeAudioSource;
        if (!audioSource[0].isPlaying)
        {
            audioSource[0].loop = false;
        }
        else if (!audioSource[1].isPlaying)
        {
            audioSource[1].loop = false;
        }
        else if (!audioSource[2].isPlaying)
        {
            audioSource[2].loop = false;
        }
        else if (!audioSource[3].isPlaying)
        {
            audioSource[3].loop = false;
        }
        else if (!audioSource[4].isPlaying)
        {
            audioSource[4].loop = false;
        }
        else if (!audioSource[5].isPlaying)
        {
            audioSource[5].loop = false;
        }
    }

    public void StopClip(string _clipName)
    {
        string _clipNameOfficial = GetAudioClipOfficialName(_clipName);

        if (audioSource[0].isPlaying)
        {
            if (audioSource[0].clip.name == _clipName || audioSource[0].clip.name == _clipNameOfficial)
            {
                audioSource[0].Stop();
                audioSource[0].loop = false;
            }

        }
        if (audioSource[1].isPlaying)
        {
            if (audioSource[1].clip.name == _clipName || audioSource[1].clip.name == _clipNameOfficial)
            {
                audioSource[1].Stop();
                audioSource[1].loop = false;
            }
        }
        if (audioSource[2].isPlaying)
        {
            if (audioSource[2].clip.name == _clipName || audioSource[2].clip.name == _clipNameOfficial)
            {
                audioSource[2].Stop();
                audioSource[2].loop = false;

            }
        }
        if (audioSource[3].isPlaying)
        {
            if (audioSource[3].clip.name == _clipName || audioSource[3].clip.name == _clipNameOfficial)
            {
                audioSource[3].Stop();
                audioSource[3].loop = false;
            }
        }
        if (audioSource[4].isPlaying)
        {
            if (audioSource[4].clip.name == _clipName || audioSource[4].clip.name == _clipNameOfficial)
            {
                audioSource[4].Stop();
                audioSource[4].loop = false;
            }

        }
        if (audioSource[5].isPlaying)
        {
            if (audioSource[5].clip.name == _clipName || audioSource[5].clip.name == _clipNameOfficial)
            {
                audioSource[5].Stop();
                audioSource[5].loop = false;
            }
        }
    }

    string GetAudioClipOfficialName(string _clipName)
    {
        AudioClipRef audioClipRef = clips.Where(x => x.clipName == _clipName).FirstOrDefault();
        if (audioClipRef.clip == null) return "";
        return audioClipRef.clip.ToString();

    }

    public float GetPitch(string ac)
    {
        bool shouldShift = false;

        if (ac == "fist") shouldShift = true;

        //if pitch should shift
        if (shouldShift) return Random.Range(.8f, 1);
        return 1;

    }

    public void musicLevelChanged(float f)
    {
        musicLevel = f;
        //ba.setVolume(musicLevel);
        //print("Music Lev Changed to:" + f);
    }

    public void soundLevelChanged(float f)
    {
        // f = f * .7f;//setting it 30% less because it's too loud usually
        soundLevel = f;
        setLevels();
        //print("Sound Lev Changed to:" + f);
    }


}

[System.Serializable]
public struct AudioClipRef
{
    public string clipName;
    public AudioClip clip;
}