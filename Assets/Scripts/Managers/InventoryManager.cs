﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;


public class InventoryManager : MonoBehaviour
{
	Color colorOrdinary, colorUnique, colorRare, colorVeryrare, colorElite, colorSpecial;

	[Header ("-Master Database-")]
	public List<Item> junkDb = new List<Item> ();
	public List<Item> weaponsDb = new List<Item> ();
	public List<Item> costumesDb = new List<Item> ();
	public List<Item> equipmentDb = new List<Item> ();
	public List<Face> faceDb = new List<Face> ();


	[Header ("-Player Owned-")]
	public List<Item> junkOwned = new List<Item> ();
	public List<Item> weaponsOwned = new List<Item> ();
	public List<Item> costumesOwned = new List<Item> ();
	public List<Item> equipmentOwned = new List<Item> ();

	private static InventoryManager _instance;

	public static InventoryManager Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<InventoryManager> ();
			return _instance;
		}
	}

	public void ClaimMallItem(string mallItemName){
		if (mallItemName == "100 Crowns") {
			GameManager.Instance.crowns += 100;
		}
	}

	//Todo Fix This
	public void AddItem (ItemType _itemType, int _id)
	{
		Item _item = null;
		if (_itemType == ItemType.junk) {
			_item = junkDb.Where (x => x.ID == _id).FirstOrDefault ();
			junkOwned.Add (_item);
		}
		if (_itemType == ItemType.costume) {
			_item = costumesDb.Where (x => x.ID == _id).FirstOrDefault ();
			costumesOwned.Add (_item);
		}
		if (_itemType == ItemType.weapon) {
			_item = weaponsDb.Where (x => x.ID == _id).FirstOrDefault ();
			weaponsOwned.Add (_item);
		}
		if (_itemType == ItemType.equipment) {
			_item = equipmentDb.Where (x => x.ID == _id).FirstOrDefault ();
			equipmentOwned.Add (_item);
		}

		//ELEVATOR KEYS
		if (_itemType == ItemType.junk & _id == 16) {
			//its the elevatorkey
			EventManager.Instance.CallElevatorKeyChange ();
		}
	}

	public void RemoveItem (ItemType _itemType, int _id)
	{
		Item _item = null;
		if (_itemType == ItemType.junk) {
			_item = junkDb.Where (x => x.ID == _id).FirstOrDefault ();
			junkOwned.Remove (_item);
		}
		if (_itemType == ItemType.costume) {
			_item = costumesDb.Where (x => x.ID == _id).FirstOrDefault ();
			costumesOwned.Remove (_item);
		}
		if (_itemType == ItemType.weapon) {
			_item = weaponsDb.Where (x => x.ID == _id).FirstOrDefault ();
			weaponsOwned.Remove (_item);
		}
		if (_itemType == ItemType.equipment) {
			_item = equipmentDb.Where (x => x.ID == _id).FirstOrDefault ();
			equipmentOwned.Remove (_item);
		}

		//ELEVATOR KEYS
		if (_itemType == ItemType.junk & _id == 16) {
			//its the elevatorkey
			EventManager.Instance.CallElevatorKeyChange ();
		}
	}

	public int GetQTY (ItemType _itemType, int _id)
	{
		int _qty = 0;
		if (_itemType == ItemType.junk) {
			_qty = junkOwned.Where (l => l.ID == _id).Count ();
		}
		if (_itemType == ItemType.weapon) {
			_qty = weaponsOwned.Where (l => l.ID == _id).Count ();
		}
		if (_itemType == ItemType.equipment) {
			_qty = equipmentOwned.Where (l => l.ID == _id).Count ();
		}
		if (_itemType == ItemType.costume) {
			_qty = costumesOwned.Where (l => l.ID == _id).Count ();
		}
		return _qty;
	}

	public Item GetItem (ItemType _itemType, int _id)
	{
		Item _item = null;

		if (_itemType == ItemType.junk) {
			_item = junkDb.Where (x => x.ID == _id).FirstOrDefault ();
		}
		if (_itemType == ItemType.equipment) {
			_item = equipmentDb.Where (x => x.ID == _id).FirstOrDefault ();
		}
		if (_itemType == ItemType.weapon) {
			_item = weaponsDb.Where (x => x.ID == _id).FirstOrDefault ();
		}
		if (_itemType == ItemType.costume) {
			_item = costumesDb.Where (x => x.ID == _id).FirstOrDefault ();
		}
		return _item;
	}

	public Face GetFace (int _id)
	{
		Face _item = null;
		_item = faceDb.Where (x => x.ID == _id).FirstOrDefault ();
		return _item;
	}

	public void ClearInventory ()
	{
		//Strip off items first
		if (GameManager.Instance.weapEquipped)
			InventoryCanvas.Instance.equipWeapon.Unequip ();
		if (GameManager.Instance.bodyEquipped)
			InventoryCanvas.Instance.costumeSlotBody.Unequip ();
		if (GameManager.Instance.backEquipped)
			InventoryCanvas.Instance.costumeSlotBack.Unequip ();

		foreach (Transform t in InventoryCanvas.Instance.cgWeap.gameObject.transform) {
			Destroy (t.gameObject);
		}
		foreach (Transform t in InventoryCanvas.Instance.cgCostume.gameObject.transform) {
			Destroy (t.gameObject);
		}
		foreach (Transform t in InventoryCanvas.Instance.cgEquip.gameObject.transform) {
			Destroy (t.gameObject);
		}
		foreach (Transform t in InventoryCanvas.Instance.cgJunk.gameObject.transform) {
			Destroy (t.gameObject);
		}

		weaponsOwned.Clear ();
		costumesOwned.Clear ();
		equipmentOwned.Clear ();
		junkOwned.Clear ();

		//EQUIPPED
		GameManager gm = GameManager.Instance; //so many refs needed - just grabbed a ref to it
		gm.equippedWeapon = new Item { name = "", icon = null };
		gm.equippedCostumeBack = new Item { name = "", icon = null };
		gm.equippedCostumeBody = new Item { name = "", icon = null };
		gm.equippedEquipmentArtifact = new Item { name = "", icon = null };
		gm.equippedCostumeHead = new Item { name = "", icon = null };
		gm.equippedEquipmentNecklace = new Item { name = "", icon = null };
		gm.equippedEquipmentRing = new Item { name = "", icon = null };

		//Bools
		gm.weapEquipped = false;
		gm.backEquipped = false;
		gm.bodyEquipped = false;
		gm.artifactEquipped = false;
		gm.ringEquipped = false;
		gm.necklaceEquipped = false;
		gm.headEquipped = false;

		InventoryCanvas.Instance.equipSlotArtifact.ResetEquipItemSlot ();
		InventoryCanvas.Instance.equipSlotNecklace.ResetEquipItemSlot ();
		InventoryCanvas.Instance.equipSlotRing.ResetEquipItemSlot ();
		InventoryCanvas.Instance.equipWeapon.ResetEquipItemSlot ();
		InventoryCanvas.Instance.costumeSlotBack.ResetEquipItemSlot ();
		InventoryCanvas.Instance.costumeSlotBody.ResetEquipItemSlot ();// (gm.equippedCostumeBody, 1);
		InventoryCanvas.Instance.costumeSlotHead.ResetEquipItemSlot ();// (gm.equippedCostumeHead, 1);

		InventoryCanvas.Instance.allItemSlots.Clear();

		gm.souls = 0;
		gm.IncSouls (0);
		gm.PlayerName = "Guest";

	//	EventManager.Instance.CallInvasionUnlocks();
	}


	public void AddClaimedItems(List<string> itemsClaimed){
		string amountText = "0";
		for (int inc = 0; inc < itemsClaimed.Count; inc++) {
			
			if (itemsClaimed [inc] == "100 Crowns") {
				GameManager.Instance.crowns += 100;
				amountText = "100";
			}
			if (itemsClaimed [inc] == "525 Crowns") {
				GameManager.Instance.crowns += 525;
				amountText = "525";
			}
			if (itemsClaimed [inc] == "1100 Crowns") {
				GameManager.Instance.crowns += 1100;
				amountText = "1100";
			}
			if (itemsClaimed [inc] == "5600 Crowns") {
				GameManager.Instance.crowns += 5600;
				amountText = "5600";
			}
			if (itemsClaimed [inc] == "11450 Crowns") {
				GameManager.Instance.crowns += 11450;
				amountText = "11450";
			}

		}
		AudioManager.Instance.PlayClip ("rareitem", true, false);
		GameObject newObj = (GameObject)Instantiate (Resources.Load ("UI/CrownsClaimedObject"), new Vector3 (0, 0, 0), Quaternion.identity);
		Text txt = newObj.GetComponentInChildren<Text> ();
		txt.text = amountText;
		CrownMallManager.Instance.SetCrownsText ();
		//_GameSaveLoad.Instance.Save (true);

	}

}

public enum ItemGrade
{
	ordinary,
	unique,
	rare,
	veryrare,
	elite,
	special

}

public enum ItemType
{
	junk,
	weapon,
	equipment,
	costume

}


[System.Serializable]
public class Face
{
	public string name;
	public int ID;
	public string description;
	public Sprite icon;
	public string attachmentName;

	//string name of the icon
}

[System.Serializable]
public class Item
{
	public string name;
	public int ID;
	public string description;
	public Sprite icon;
	//string name of the icon
	public ItemType itemType;
	public int requiredLevel = 1;
	[Header ("If you dont pick one it wont play")]
	public string weaponSound;
	[Header ("Ex: green")]
	public string weaponParticles = "";
	public int cost;
	public int crownCost;
	public bool tradeable=true;
	public ItemGrade itemGrade;
	// Common, Rare, VeryRare
	public Stats stat;
	[Header ("Only use if needed for that item type")]
	public CostumeType costumeType;
	public EquipmentType equipmentType;
	[Header ("-Check with matiss for these names-")]
	public string slotName;
	public string slotName2;
	public string atachmentName;
	public string attachmentName2;
	public string skin;
	public Stance stance;
}

[System.Serializable]
public enum CostumeType
{
	back,
	body,
	head
}

[System.Serializable]
public enum EquipmentType
{
	ring,
	necklace,
	artifact
}