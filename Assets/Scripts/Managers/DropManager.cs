﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropManager : MonoBehaviour
{
	private static DropManager _instance;

	public static DropManager Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<DropManager> ();
			return _instance;
		}
	}



	public void DropSouls (SoulSize size, int min, int max, int value, Vector3 dropLocation)
	{
		//Debug.Log("Start Drop:"+size.ToString());
		//Debug.Log("SoulDrop location:" + dropLocation);

		int rand = Random.Range (min, max + 1);
		string prefabName = "smallSoulDrop";
		switch (size) {
		case SoulSize.big:
			prefabName = "bigSoulDrop";
			break;
		case SoulSize.med:
			prefabName = "medSoulDrop";
			break;
		}

		for (int inc = 0; inc < rand; inc++) {
			//Debug.Log ("** Drop soul:" + dropLocation);
			GameObject obj = ObjectPool.Instance.NextObject (prefabName);
			dropLocation.z = 0;
			obj.transform.position = dropLocation + new Vector3 (0, 1, 0);

			obj.SetActive (true);
			SoulDrop sd = obj.GetComponent<SoulDrop> ();
			sd.isActive = false;
			sd.isExpired = false;
			sd.expireTimeRemain = sd.expireTime;

			//X offset
			float xOffset = 0;
			if (BattleManager.Instance.mapStarted == "MisadventuresInTheRiverOfGod")
				xOffset = 1;
			//add force and fling it
			sd.rb.velocity = new Vector3 (0, 0, 0);
			//Debug.Log ("DROP SOULS - Z POS:" + sd.transform.position.z);
			//sd.rb.AddForce (new Vector3 (Random.Range (-2f + xOffset, 2 - xOffset), 5, 0), ForceMode.Impulse);

			float xForce = UnityEngine.Random.Range (5 + xOffset, 5 - xOffset) * (UnityEngine.Random.value < 0.5 ? -1 : 1);
			sd.rb.AddForce (new Vector2 (xForce, 0), ForceMode2D.Impulse);

			sd.value = value;

			sd.CallLand ();

			//todo add force here at random
		}
		//  Debug.Log("Done Drop:"+size.ToString());
	}

    
	[PunRPC]
	public void DropItems (List<ItemDrops>itemDrops, Vector3 dropLocation)
	{
		//Debug.Log("Drop Items Called");
		for (int inc = 0; inc < itemDrops.Count; inc++) {
			float chance = Random.Range (0, 1f);

//			//Player Count Item Boost
//			 int playerCount = PhotonNetwork.playerList.Length - 1;
//			if (playerCount > 1) {
//				float chancesIncreased = playerCount * BattleManager.Instance.currentWaveset.playerCountItemChancesGrow;
//				chance += chancesIncreased;
//			} 

			if (chance > itemDrops [inc].chances)
				continue;//if we didnt get the item, try for the next one
			string prefabName = "ItemPickup";
			AdventurerNetwork chosenAdvent = BattleManager.Instance.myAdventurer;// [Random.Range (0, livingAdvent.Count)];
			chosenAdvent.DropItemLocal(itemDrops[inc].itemType, itemDrops[inc].id, dropLocation);
		}
	}

	public void CallItemPickupCooldown ()
	{
		StartCoroutine (ItemPickupCooldown ());
	}

	public bool itemPickupCool = true;

	IEnumerator ItemPickupCooldown ()
	{
		itemPickupCool = false;
		yield return new WaitForSeconds (.1f);
		itemPickupCool = true;
	}
}


public enum SoulSize
{
	small,
	med,
	big
}

[System.Serializable]
public class ItemDrops
{
	public ItemType itemType;
	public int id;
	[Range (0, 1f)]
	public float chances;
}