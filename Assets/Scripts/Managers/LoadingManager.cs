﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingManager : MonoBehaviour
{
	public float timeoutMax = 3;
	public float timoutTimer = 0;
	bool timerRunning = false;

	private static LoadingManager _instance;

	public static LoadingManager Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<LoadingManager> ();
			return _instance;
		}
	}

	string panelChoice = "none";
	public GameObject craftPanel, battlePanel, townPanel, mainLoadPanel;
	CanvasGroup cg;

	private void Awake ()
	{
		cg = GetComponent<CanvasGroup> ();

	}

	void Start ()
	{
		OpenPanel ("Main");
		TownCanvas.Instance.ToggleMe (false);
		CallFadeCanvas (1);
	}

	private void Update ()
	{
		if (timerRunning)
			TimeOutChecker ();
	}

	void TimeOutChecker ()
	{
		if (cg.alpha != 1)
			return;//if its not visible who cares
		timoutTimer += Time.deltaTime;
		if (timoutTimer >= timeoutMax) {
			// Debug.Log("You have timed out:" + timoutTimer + " max:" + timeoutMax);
			if (LobbyCanvas.Instance.lobbyCanvasOpen) {
				LobbyCanvas.Instance.LeaveLobby ();
				timerRunning = false;
			}
		}

	}

	public void StartLobbyTimeout ()
	{
		timerRunning = true;
	}

	public void StopLobbyTimeout ()
	{
		timerRunning = false;
	}

	void OnJoinedLobby ()
	{
		StopLobbyTimeout ();
	}

	public void CallFadeCanvas (float _desiredAlpha)
	{
		timoutTimer = 0;
		StartCoroutine (FadeCanvasGroup (_desiredAlpha));
	}

	bool fadingIn = false;
	bool fadingOut = false;
	bool menuVisible = true;

	IEnumerator FadeCanvasGroup (float _newAlpha)
	{
		
		if (_newAlpha == 0) {
			fadingIn = false;
			fadingOut = true;

		} else {
			fadingIn = true;
			fadingOut = false;

		}


		float _currentAlpha = cg.alpha;

		//Fading out
		if (_newAlpha < _currentAlpha) {
			for (float alpha = _currentAlpha; alpha >= _newAlpha; alpha -= .1f) {
				yield return new WaitForSeconds (0);
				cg.alpha = alpha;
			}
		}

		//Fading in
		if (_newAlpha > _currentAlpha) {
			for (float alpha = _currentAlpha; alpha <= _newAlpha; alpha += .1f) {
				yield return new WaitForSeconds (0);
				cg.alpha = alpha;
			}
		}
		cg.alpha = _newAlpha;
		if (cg.alpha == 1) {
			menuVisible = true;
		} else {
			if (GameManager.Instance.currentScene == CustomSceneType.Town) {
				TownCanvas.Instance.ToggleMe (true);
			}
			menuVisible = false;
		}
	}

	//  public bool canvasOpen = false;
	//public void ToggleMe(bool b)
	//{

	//    if (b == canvasOpen) return;
	//    if (b)
	//    {
	//        GameManager.Instance.InputEnabled = false;
	//        //AudioManager.Instance.PlayClip("inventoryslide", true, false);
	//        TownCanvas.Instance.ToggleMe(false);
	//        cg.interactable = true;
	//        cg.blocksRaycasts = true;
	//    }
	//    else
	//    {
	//        GameManager.Instance.InputEnabled = true;
	//        //AudioManager.Instance.PlayClip("inventoryslide", true, false);
	//        cg.interactable = false;
	//        cg.blocksRaycasts = false;
	//    }
	//    canvasOpen = b;
	//}

	public void CloseAllPanels ()
	{
		craftPanel.SetActive (false);
		townPanel.SetActive (false);
		battlePanel.SetActive (false);
		mainLoadPanel.SetActive (false);
	}

	public void CallDelayedClose (float delay)
	{
		StartCoroutine (DelayedClose (delay));
	}

	public void CallCloseWithCallback (System.Action followingMethod, float delay)
	{
		//delay, close, call following action
		StartCoroutine (CloseWithCallBack (followingMethod, delay));
	}

	IEnumerator CloseWithCallBack (System.Action followingMethod, float delay)
	{
		yield return new WaitForSeconds (delay);
		CallFadeCanvas (0);
		followingMethod ();
	}

	IEnumerator DelayedClose (float delay)
	{
		yield return new WaitForSeconds (delay);
		CallFadeCanvas (0);
		if (GameManager.Instance.currentScene == CustomSceneType.Town
		    & !CraftingCanvas.Instance.craftingCanvasOpen
		    & !InventoryCanvas.Instance.equipmentOpen) {
			MenuCanvas.Instance.ToggleMe (true);
		}
	}

	public void OpenPanel (string _panel)
	{
		panelChoice = _panel;
		Debug.Log ("OpenPanel:" + panelChoice);
		CloseAllPanels ();

		switch (panelChoice) {
		case "Craft":
			craftPanel.SetActive (true);
			break;
		case "Town":
			townPanel.SetActive (true);
			break;
		case "Battle":
			battlePanel.SetActive (true);
			break;
		case "Main":
			mainLoadPanel.SetActive (true);
			break;
		}
	}



	//void OnGUI()
	//{
	//    if (GUI.Button(new Rect(0,0, 120, 30), "Craft"))
	//    {
	//        OpenPanel("Craft");
	//        CallFadeCanvas(1);
	//    }
	//    if (GUI.Button(new Rect(0, 30, 120, 30), "Town"))
	//    {
	//        OpenPanel("Town");
	//        CallFadeCanvas(1);
	//    }
	//    if (GUI.Button(new Rect(0, 60, 120, 30), "Battle"))
	//    {
	//        OpenPanel("Battle");
	//        CallFadeCanvas(1);
	//    }
	//    if (GUI.Button(new Rect(0, 90, 120, 30), "FadeOut"))
	//    {
	//        CallFadeCanvas(0);
	//       // ToggleMe(false);
	//    }
	//}
}
