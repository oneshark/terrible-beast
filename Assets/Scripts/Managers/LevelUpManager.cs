﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpManager : MonoBehaviour
{
    public int LevelCap = 40;
    public List<LevelEntry> levelConfig = new List<LevelEntry>();
    [Header("How many waves before we let them have another skill?")]
    private static LevelUpManager _instance;
    public static LevelUpManager Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<LevelUpManager>();
            return _instance;
        }
    }

    #region LEVEL UP GRAPHIC AND OOLDOWN
    public bool isLevelUpCool = true;
    public void CallLevelUp()
    {
        //Debug.Log("IsLevelUpCool?" + isLevelUpCool);
        if (!isLevelUpCool) return;
        isLevelUpCool = false;
        GameObject levUp = ObjectPool.Instance.NextObject("LevelUp");
        LevelUpCanvas luc = levUp.GetComponent<LevelUpCanvas>();
        luc.SetLevelText(GameManager.Instance.baseEntityStat.xpLevel.ToString());
        levUp.SetActive(true);
        AudioManager.Instance.PlayClip("levelup", true, false);
		_GameSaveLoad.Instance.Save (true);

        Invoke("LevelUpCool", 1.5f);
    }

    public void LevelUpCool()
    {
        isLevelUpCool = true;
    }
    #endregion
}

[System.Serializable]
public class LevelEntry
{
    //list of each level and 
    public float xpRequired;//how much xp to get to next level
                            // public float statPointsGiven = 0;//do they get a stat point?
}
