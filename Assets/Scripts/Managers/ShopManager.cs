﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShopManager : MonoBehaviour
{

    private static ShopManager _instance;
    public static ShopManager Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = FindObjectOfType<ShopManager>();
            return _instance;
        }
    }

    public List<NPCShop> npcShops = new List<NPCShop>();
    public bool shopCanvasOpen = false;
    public Animator anim;
    public CanvasGroup cgOnehander, cgDualwield, cgTwohander, cgCannon, cgRod, cgBow, cgNecklace, cgRing, cgCostume;
    public GameObject tabOnehander, tabDualwield, tabTwohander, tabCannon, tabRod, tabBow, tabNecklace, tabRing, tabCostume;

    void Awake()
    {
       // anim = GetComponent<Animator>();
    }

    public void ToggleMe(bool b)
    {
        if (b == shopCanvasOpen) return;
        if (b)
        {
			//Delay, ClipIndex
			GameManager.Instance.CallPlayTutorialBlock(2,6,true);
            anim.CrossFade("ShopCanvasIn", 0);
            InventoryCanvas.Instance.ToggleBottomStats(false);
            NPCManager.Instance.ToggleNPCCanvas(false);
            //TownCanvas.Instance.ToggleSoulText(false);
        }
        else
        {
            anim.CrossFade("ShopCanvasOut", 0);
            // TownCanvas.Instance.ToggleSoulText(true);

        }
        shopCanvasOpen = b;
    }
    
    public void ChooseSlotPanel(string tab)
    //turn on correct panel - turn off others
    {
        Debug.Log("Clicked on Slot Panel:" + tab);

        tabOnehander.SetActive(false);
        tabTwohander.SetActive(false);
        tabDualwield.SetActive(false);
        tabRod.SetActive(false);
        tabCannon.SetActive(false);
        tabRing.SetActive(false);
        tabNecklace.SetActive(false);
        tabCostume.SetActive(false);
        tabBow.SetActive(false);


        cgOnehander.alpha = 0;
        cgOnehander.blocksRaycasts = false;
        cgOnehander.interactable = false;

        cgTwohander.alpha = 0;
        cgTwohander.blocksRaycasts = false;
        cgTwohander.interactable = false;

        cgDualwield.alpha = 0;
        cgDualwield.blocksRaycasts = false;
        cgDualwield.interactable = false;

        cgCannon.alpha = 0;
        cgCannon.blocksRaycasts = false;
        cgCannon.interactable = false;

        cgBow.alpha = 0;
        cgBow.blocksRaycasts = false;
        cgBow.interactable = false;

        cgRod.alpha = 0;
        cgRod.blocksRaycasts = false;
        cgRod.interactable = false;

        cgCostume.alpha = 0;
        cgCostume.blocksRaycasts = false;
        cgCostume.interactable = false;

        cgRing.alpha = 0;
        cgRing.blocksRaycasts = false;
        cgRing.interactable = false;

        cgNecklace.alpha = 0;
        cgNecklace.blocksRaycasts = false;
        cgNecklace.interactable = false;

        switch (tab)
        {
            case "Onehander":
                cgOnehander.alpha = 1;
                cgOnehander.blocksRaycasts = true;
                cgOnehander.interactable = true;
                tabOnehander.SetActive(true);
                break;
            case "Twohander":
                cgTwohander.alpha = 1;
                cgTwohander.blocksRaycasts = true;
                cgTwohander.interactable = true;
                tabTwohander.SetActive(true);
                                break;
            case "Dualwield":
                cgDualwield.alpha = 1;
                cgDualwield.blocksRaycasts = true;
                cgDualwield.interactable = true;
                tabDualwield.SetActive(true);
                break;
            case "Rod":
                cgRod.alpha = 1;
                cgRod.blocksRaycasts = true;
                cgRod.interactable = true;
                tabRod.SetActive(true);
                break;
            case "Cannon":
                cgCannon.alpha = 1;
                cgCannon.blocksRaycasts = true;
                cgCannon.interactable = true;
                tabCannon.SetActive(true);
                break;
            case "Ring":
                cgRing.alpha = 1;
                cgRing.blocksRaycasts = true;
                cgRing.interactable = true;
                tabRing.SetActive(true);
                break;
            case "Necklace":
                cgNecklace.alpha = 1;
                cgNecklace.blocksRaycasts = true;
                cgNecklace.interactable = true;
                tabNecklace.SetActive(true);
                break;
            case "Bow":
                cgBow.alpha = 1;
                cgBow.blocksRaycasts = true;
                cgBow.interactable = true;
                tabBow.SetActive(true);
                break;
            case "Costume":
                cgCostume.alpha = 1;
                cgCostume.blocksRaycasts = true;
                cgCostume.interactable = true;
                tabCostume.SetActive(true);
                break;
        }
    }

    public void ClearShopItems()
    {
        foreach (Transform child in cgOnehander.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgTwohander.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgDualwield.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgBow.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgCannon.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgNecklace.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgRing.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgRod.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cgCostume.gameObject.transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    public void LoadShopItems(string _npcName)
    {
        NPCShop myNPCShop = npcShops.Where(x => x.npcName == _npcName).FirstOrDefault();
        InventoryManager inventoryManager = InventoryManager.Instance;
        
        //Onehanders
        for (int inc=0; inc<myNPCShop.onehanders.Count;inc++)
        {
            Item nextItem = inventoryManager.weaponsDb.Where(x => x.ID == myNPCShop.onehanders[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgOnehander.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
            itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //DualWield
        for (int inc = 0; inc < myNPCShop.dualwields.Count; inc++)
        {
            Item nextItem = inventoryManager.weaponsDb.Where(x => x.ID == myNPCShop.dualwields[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgDualwield.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
            itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Twohander
        for (int inc = 0; inc < myNPCShop.twohanders.Count; inc++)
        {
            Item nextItem = inventoryManager.weaponsDb.Where(x => x.ID == myNPCShop.twohanders[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgTwohander.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
            itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Cannon
        for (int inc = 0; inc < myNPCShop.cannons.Count; inc++)
        {
            Item nextItem = inventoryManager.weaponsDb.Where(x => x.ID == myNPCShop.cannons[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgCannon.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Bow
        for (int inc = 0; inc < myNPCShop.bows.Count; inc++)
        {
            Item nextItem = inventoryManager.weaponsDb.Where(x => x.ID == myNPCShop.bows[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgBow.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Rod
        for (int inc = 0; inc < myNPCShop.rods.Count; inc++)
        {
            Item nextItem = inventoryManager.weaponsDb.Where(x => x.ID == myNPCShop.rods[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgRod.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Costumes - Body
        for (int inc = 0; inc < myNPCShop.bodycostumes.Count; inc++)
        {
            Item nextItem = inventoryManager.costumesDb.Where(x => x.ID == myNPCShop.bodycostumes[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgCostume.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Costumes - back
        for (int inc = 0; inc < myNPCShop.backcostumes.Count; inc++)
        {
            Item nextItem = inventoryManager.costumesDb.Where(x => x.ID == myNPCShop.backcostumes[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgCostume.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Equip - Ring
        for (int inc = 0; inc < myNPCShop.ringequipment.Count; inc++)
        {
            Item nextItem = inventoryManager.equipmentDb.Where(x => x.ID == myNPCShop.ringequipment[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgRing.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }

        //Equip - Necklace
        for (int inc = 0; inc < myNPCShop.necklaceequipment.Count; inc++)
        {
            Item nextItem = inventoryManager.equipmentDb.Where(x => x.ID == myNPCShop.necklaceequipment[inc]).FirstOrDefault();
            GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/ItemSlot")));
            newItem.transform.parent = cgNecklace.gameObject.transform;
            ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
            itemSlot.equip.SetActive(false);
            itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
            itemSlot.SetItem(nextItem, 1);
            itemSlot.purchasable = true;
        }
    }
}

[System.Serializable]
public class NPCShop
{
    public string npcName;
	public List<int> junk = new List<int>();

    public List<int> onehanders = new List<int>();
    public List<int> bows = new List<int>();
    public List<int> dualwields = new List<int>();
    public List<int> twohanders = new List<int>();
    public List<int> cannons = new List<int>();
    public List<int> rods = new List<int>();
    public List<int> knuckles = new List<int>();

    public List<int> bodycostumes = new List<int>();
    public List<int> backcostumes = new List<int>();
    public List<int> headcostumes = new List<int>();

    public List<int> ringequipment = new List<int>();
    public List<int> necklaceequipment = new List<int>();
}

