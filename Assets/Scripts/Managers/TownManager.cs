﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownManager : Photon.MonoBehaviour
{
	//   public bool isFirstLoad = true;
	public AdventurerTownNetwork myAdventurerTownNetwork;
	public GameObject skillTreeEnviroCuzItsTurnedOff;
	public PhotonView photonView;
	public string playerPrefabName;
	public AdventurerTownNetwork myAtn;

	private static TownManager _instance;

	public static TownManager Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<TownManager> ();
			return _instance;
		}
	}

	void OnJoinedRoom ()
	{
		//  Debug.Log("TownManager OnJoined Room");
		StartGame ();
	}

	void StartGame ()
	{
		Camera.main.farClipPlane = 1000; //Main menu set this to 0.4 for a nicer BG    

		StartCoroutine ("WaitForSynch");
		isSynched = true;//hardcoded for now
	}

	public bool isLoaded = false;

	public void SpawnCharacter ()//CANT SPAWN UNTIL WE KNOW OUR SPOT
	{
		BackgroundMusicManager.Instance.StopAudioSource ();

		//  Debug.Log("TownManger Spawn Character at:"+GameManager.Instance.NextTownSpawn);
		float randX = Random.Range (-4, 4);
		float randY = Random.Range (0, 4);

		Vector3 playerSpawnPoint = GameObject.FindGameObjectWithTag (GameManager.Instance.NextTownSpawn).transform.position + new Vector3 (randX, randY, 0);
		photonView.RPC ("CallPlayerSpawnEffect", PhotonTargets.All, playerSpawnPoint.x, playerSpawnPoint.y);
		AudioManager.Instance.PlayClip ("townspawn", true, false);//do this local

		GameObject newObj = PhotonNetwork.Instantiate (this.playerPrefabName, playerSpawnPoint, Quaternion.identity, 0);
		AdventurerTownNetwork atn = newObj.GetComponent<AdventurerTownNetwork> ();
		atn.transform.position = playerSpawnPoint;
        atn.transform.position= new Vector3(atn.transform.position.x, atn.transform.position.y, -2);
		myAdventurerTownNetwork = atn;
		atn.playerID = PhotonNetwork.player.ID;
		atn.Initialize ();
		myAtn = atn;


		//Prevent loading game all over again on 2nd and 3rd town visits
		if (!isLoaded) {
		Debug.Log("****Start Load Town manager");
			_GameSaveLoad.Instance.StartLoad ();

		}
		isLoaded = true;

		//set follow Cam
		if (atn.photonView.isMine) {
			SmoothFollow sf = Camera.main.GetComponent<SmoothFollow> ();
			//sf.target = atn.mr.gameObject;
			sf.target=atn.cameraTarget;
			//HERE WE ASK FOR EACH OF THE ADVENTURERS TO BROADCAST THEIR EQUIPMENT!
			//THE OWNER OF HTEM WILL HAVE THE RIGHT EQUIPPED ITEMS

		}

		//Call Synch Anim for all 
	}

   
	[PunRPC]
	public void CallPlayerSpawnEffect (float locX, float locY)
	{
		Vector3 playerSpawnPoint = new Vector3 (locX, locY, 0);
		GameObject nextObj = ObjectPool.Instance.NextObject ("PlayerSpawnEffect");
		nextObj.transform.position = playerSpawnPoint + new Vector3 (1.75f, .5f, 0);
		nextObj.SetActive (true);

	}

	bool isSynched = false;

	IEnumerator WaitForSynch ()
	{
		//  Debug.Log("isPositionSynched" + isPositionSynched);
		while (!isSynched) {
			//Debug.Log("Loop");
			yield return null;
		}
		SpawnCharacter ();
	}
}
