﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTextManager : MonoBehaviour
{
    private static BattleTextManager _instance;
    public static BattleTextManager Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<BattleTextManager>();
            return _instance;
        }
    }


    public void CreateText(BattleTextType textType, Vector3 startLocation, float val)
    {
        string prefabName = "WhiteText";
        switch (textType)
        {
            case BattleTextType.white:
                prefabName = "WhiteText";
                break;
            case BattleTextType.red:
                prefabName = "WhiteText";
                break;
            case BattleTextType.pink:
                prefabName = "WeakText";
                break;
            case BattleTextType.crit:
                prefabName = "CritText";
                break;
            case BattleTextType.enemycrit:
                prefabName = "CritText";
                break;
            case BattleTextType.green:
                prefabName = "WhiteText";
                break;
            case BattleTextType.attack_up:
                prefabName = "AttackUpText";
                break;
        }

        GameObject obj = ObjectPool.Instance.NextObject(prefabName);
        startLocation.z = 0;
        obj.transform.position = startLocation + new Vector3(0, 1, 1);
        obj.SetActive(true);
        TextPopUp tpu = obj.GetComponent<TextPopUp>();

        //set custom word:
        if (textType == BattleTextType.crit)
        {
            tpu.customText.text = GameManager.Instance.GetRandomCriticalPhrase();
            tpu.customText.fontSize = 61;
        }
        if (textType == BattleTextType.enemycrit)
        {
            tpu.customText.text = "Crit";
            tpu.customText.fontSize = 50;
        }

        bool skipFlyout = false;
        string suffix = "";
        if (textType == BattleTextType.white)
        {
            tpu.TintWhite();
        }
        if (textType == BattleTextType.red)
        {
            tpu.TintRed();
        }
        //if (textType == BattleTextType.pink)
        //{
        //    tpu.TintPink();
        //}
        if (textType == BattleTextType.green)
        {
            tpu.TintGreen();
        }
        if (textType == BattleTextType.crit || textType==BattleTextType.enemycrit || textType==BattleTextType.pink)
        {
            tpu.CallCritAnim();
            skipFlyout = true;
        }
        if (textType == BattleTextType.attack_up)
        {
            tpu.CallAttackAnim();
            suffix = "%";
            skipFlyout = true;
        }

        tpu.txt.text = val + suffix;
        //add force and fling it
        tpu.rb.velocity = new Vector3(0, 0,0);
        if (!skipFlyout)
        {
            tpu.rb.useGravity = true;
            tpu.rb.AddForce(new Vector3(Random.Range(-4f, 4f), Random.Range(12, 18), Random.Range(2, 5)), ForceMode.Impulse);
        }else
        {
            tpu.rb.useGravity = false;
        }
        
        
    }
    
}


public enum BattleTextType
{
    white,
    red,
    crit,
    pink,
    green,
    attack_up,
    enemycrit
}