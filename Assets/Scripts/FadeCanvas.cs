﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeCanvas : MonoBehaviour
{
    public bool canvasVisible = false;
    public CanvasGroup cg;

    void Awake()
    {
        CanvasGroup cg = GetComponent<CanvasGroup>();
        cg.alpha = 0;
    }


    public void CallFadeCanvas(float _desiredAlpha, bool _finalFade)
    {
        StartCoroutine(fadeCanvas(_desiredAlpha, _finalFade));
    }

    IEnumerator fadeCanvas(float _newAlpha, bool _finalFade)
    {
        float _currentAlpha = cg.alpha;

        //Fading out
        if (_newAlpha < _currentAlpha)
        {
            for (float alpha = _currentAlpha; alpha >= _newAlpha; alpha -= .1f)
            {
                yield return new WaitForSeconds(0);
                cg.alpha = alpha;
            }
        }

        //Fading in
        if (_newAlpha > _currentAlpha)
        {
            for (float alpha = _currentAlpha; alpha <= _newAlpha; alpha += .1f)
            {
                yield return new WaitForSeconds(0);
                cg.alpha = alpha;
            }
        }
        cg.alpha = _newAlpha;
        if (cg.alpha == 1)
        {
            cg.interactable = true;
            cg.blocksRaycasts = true;
            canvasVisible = true;
        }
        else
        {
            cg.interactable = false;
            cg.blocksRaycasts = false;
            canvasVisible = false;
        }
    }
}