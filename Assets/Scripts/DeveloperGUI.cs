﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;
using System;
using System.Text.RegularExpressions;

public class DeveloperGUI : MonoBehaviour
{
	public SaveData _SaveData;
	bool isAllowed = false;
	public Vector3 offsetAdminPanel = new Vector3 ();
	public bool showAmityDropdown = false;

	string sampleText = "";
	string sampleText2 = "";

	void Start ()
	{
		#if UNITY_EDITOR
		isAllowed = true;
		#endif
	
	}

	void Update ()
	{
		if (isAllowed) {
			if (Input.GetKeyDown (KeyCode.LeftShift)) {
				toggleAdmin ();
			}
		}
	}

	void toggleAdmin ()
	{

		GameManager.Instance.ADMINCONTROL = !GameManager.Instance.ADMINCONTROL;
	}

	void OnGUI ()
	{


		if (GameManager.Instance.ADMINCONTROL) {

			//if (Application.loadedLevelName == "Town")
			//{
			GUI.Label (new Rect (170 + offsetAdminPanel.x, 0 + offsetAdminPanel.y, 420, 100), "Number:");
			sampleText = GUI.TextArea (new Rect (230 + offsetAdminPanel.x, 00 + offsetAdminPanel.y, 120, 30), sampleText, 200);

			GUI.Label (new Rect (170 + offsetAdminPanel.x, 30 + offsetAdminPanel.y, 420, 100), "Name:");
			sampleText2 = GUI.TextArea (new Rect (230 + offsetAdminPanel.x, 30 + offsetAdminPanel.y, 120, 30), sampleText2, 200);
			//Tell what items:
			if (sampleText != "") {
				//junk
				int _id = 0;
				Int32.TryParse (sampleText, out _id);
				var currentItem0 = InventoryManager.Instance.junkDb.Where (t => t.ID == _id).FirstOrDefault ();
				if (currentItem0 != null) {
					GUI.Label (new Rect (230 + offsetAdminPanel.x, 60 + offsetAdminPanel.y, 420, 100), "Junk:" + currentItem0.name);
				}
				//costume
				_id = 0;
				Int32.TryParse (sampleText, out _id);
				var currentItem1 = InventoryManager.Instance.costumesDb.Where (t => t.ID == _id).FirstOrDefault ();
				if (currentItem1 != null) {
					GUI.Label (new Rect (230 + offsetAdminPanel.x, 90 + offsetAdminPanel.y, 420, 100), "Costume:" + currentItem1.name);
				}
				//weapon
				_id = 0;
				Int32.TryParse (sampleText, out _id);
				var currentItem2 = InventoryManager.Instance.weaponsDb.Where (t => t.ID == _id).FirstOrDefault ();
				if (currentItem2 != null) {
					GUI.Label (new Rect (230 + offsetAdminPanel.x, 120 + offsetAdminPanel.y, 420, 100), "weapon:" + currentItem2.name);
				}
				//equipment
				_id = 0;
				Int32.TryParse (sampleText, out _id);
				var currentItem3 = InventoryManager.Instance.equipmentDb.Where (t => t.ID == _id).FirstOrDefault ();
				if (currentItem3 != null) {
					GUI.Label (new Rect (230 + offsetAdminPanel.x, 150 + offsetAdminPanel.y, 420, 100), "equip:" + currentItem3.name);
				}
			}

			//BUTTONS BELOW
			//COLUMN ONE
			if (GUI.Button (new Rect (350 + offsetAdminPanel.x, 0 + offsetAdminPanel.y, 120, 30), "Add Junk")) {
				int _id = 0;
				Int32.TryParse (sampleText, out _id);
				Item newItem = InventoryManager.Instance.junkDb.Where (x => x.ID == _id).FirstOrDefault ();
				InventoryManager.Instance.AddItem (ItemType.junk, _id);
				InventoryCanvas.Instance.AddItemSlot (newItem, 1);
			}
			if (GUI.Button (new Rect (350 + offsetAdminPanel.x, 30 + offsetAdminPanel.y, 120, 30), "Add Costume")) {
				int _id = 0;
				Int32.TryParse (sampleText, out _id);
				Item newItem = InventoryManager.Instance.costumesDb.Where (x => x.ID == _id).FirstOrDefault ();
				InventoryManager.Instance.AddItem (ItemType.costume, _id);
				InventoryCanvas.Instance.AddItemSlot (newItem, 1);
			}
			if (GUI.Button (new Rect (350 + offsetAdminPanel.x, 60 + offsetAdminPanel.y, 120, 30), "Add Weapon")) {
				int _id = 0;
				Int32.TryParse (sampleText, out _id);
				Item newItem = InventoryManager.Instance.weaponsDb.Where (x => x.ID == _id).FirstOrDefault ();
				InventoryManager.Instance.AddItem (ItemType.weapon, _id);
				InventoryCanvas.Instance.AddItemSlot (newItem, 1);
			}
			if (GUI.Button (new Rect (350 + offsetAdminPanel.x, 90 + offsetAdminPanel.y, 120, 30), "Add Equipment")) {
				int _id = 0;
				Int32.TryParse (sampleText, out _id);
				Item newItem = InventoryManager.Instance.equipmentDb.Where (x => x.ID == _id).FirstOrDefault ();
				InventoryManager.Instance.AddItem (ItemType.equipment, _id);
				InventoryCanvas.Instance.AddItemSlot (newItem, 1);
			}
			if (GUI.Button (new Rect (350 + offsetAdminPanel.x, 120 + offsetAdminPanel.y, 120, 30), "Add CROWNS")) {
				GameManager.Instance.crowns += 10000;
			}
			if (GUI.Button (new Rect (350 + offsetAdminPanel.x, 150 + offsetAdminPanel.y, 120, 30), "Kill ALl Enemies")) {
				BattleManager.Instance.KillAllEnemies ();

			}

			if (GUI.Button (new Rect (350 + offsetAdminPanel.x, 180 + offsetAdminPanel.y, 120, 30), "Amnon Mode")) {
				AdventurerNetwork m_atn = BattleManager.Instance.myAdventurer;
				m_atn.currentEntityStat.atkMin = 9999;
				m_atn.currentEntityStat.atkMin = 99999;
				m_atn.currentEntityStat.hpMax = 999999;
				m_atn.currentEntityStat.hp = 999999;

				m_atn.currentEntityStat.defMin = 9999;
				m_atn.currentEntityStat.defMax = 9999;
				m_atn.currentEntityStat.hpMax = 99999;
				m_atn.currentEntityStat.hp = 99999;
				m_atn.currentEntityStat.atkCooldown = .01f;

			}
     
			//COLUMN TWO
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 0 + offsetAdminPanel.y, 120, 30), "Add Souls")) {
				int _id = 0;
				Int32.TryParse (sampleText, out _id);
				GameManager.Instance.IncSouls (_id);
			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 30 + offsetAdminPanel.y, 120, 30), "Add XP Level")) {
				GameManager.Instance.baseEntityStat.xpLevel++;
				GameManager.Instance.baseEntityStat.hpMax += 10;
			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 60 + offsetAdminPanel.y, 120, 30), "UNLOCK ALL MAPS")) {
				EventManager.Instance.CallInvasionUnlocks ();
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost1");
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost2");
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost3");
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost4");
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost5");
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost6");
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost7");
				GameManager.Instance.completedThings.Add ("WhenAllHopeIsLost8");
				GameManager.Instance.completedThings.Add ("Ruins1");
				GameManager.Instance.completedThings.Add ("Ruins2");
				GameManager.Instance.completedThings.Add ("Ruins3");
				GameManager.Instance.completedThings.Add ("Ruins4");
				GameManager.Instance.completedThings.Add ("Ruins5");
				GameManager.Instance.completedThings.Add ("Ruins6");
				GameManager.Instance.completedThings.Add ("Ruins7");
				GameManager.Instance.completedThings.Add ("Ruins8");
				GameManager.Instance.completedThings.Add ("River1");
				GameManager.Instance.completedThings.Add ("River2");
				GameManager.Instance.completedThings.Add ("River3");
				GameManager.Instance.completedThings.Add ("River4");
				GameManager.Instance.completedThings.Add ("River5");
				GameManager.Instance.completedThings.Add ("River6");
				GameManager.Instance.completedThings.Add ("River7");
				GameManager.Instance.completedThings.Add ("River8");
			}

			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 90 + offsetAdminPanel.y, 120, 30), "Become Host")) {
				PhotonNetwork.SetMasterClient (PhotonNetwork.player);
			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 120 + offsetAdminPanel.y, 120, 30), "CLAIM MALL")) {
				

			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 150 + offsetAdminPanel.y, 120, 30), "Open MALL")) {
				//string decryptedPass = StringUtil.DecryptString (sampleText, "02Ksi#89j");
				//Debug.Log("Decrypted Password:"+decryptedPass);
			Application.OpenURL ("http://tod.dx.am/Main/TerribleBeastStore/index.php?PlayerName="+GameManager.Instance.PlayerName);
				//MgDxmczG8ZPgspGjO7VizQ==
			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 180 + offsetAdminPanel.y, 120, 30), "Decrypt Pswrd")) {
				string decryptedPass = StringUtil.DecryptString (sampleText, "02Ksi#89j");
				Debug.Log("Decrypted Password:"+decryptedPass);
			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 210 + offsetAdminPanel.y, 120, 30), "Decrypt String")) {
				//string decryptedPass = StringUtil.DecryptString (sampleText, "02Ksi#89j");
				string raw="4u48edKLEZ1t5N3vD2h0WBGFz8Fly6ACiC/K1rZOfZep4BrFhABOaqNORFyxwzJ5bnEHxqWwg3O7feR1ckX1F66d/hsn6Vs2h/trPdKvoVZiCckxOihZRa4dttXp2J5PyZ+PaHb0NKyqZz8bryQSErJFQrb/4MRQJG0B4cXmQJAJEmAamZk4OCqEAxjqd7LSWGHuER2tcRM0GaQCu3ypSHvIAttEwaDk9vx9eeu+GBK15b1g5ph0oV5Xqq/d1JFVvMyIooikdfE+TiTPpgmRC73Ei45EJ7oLkBtYxl/hwMSsIRb+z6M1BgKSzbdY6sQ7X8Ap4W00WdEmdrcqcff+TVcG+hjxbzemPCopdFlOdn8X6+7NpQjn2/UwkxDaA1WBOLHUfTs3FsEKUR8sqv3/oCKq/kwDezwmlgoxYkvcah0ZgnbB9RqzP+ZBwZ+fCYHL9tRCYuOa/KDrV0s22p9P7R6VZOXib2I02BgsEVweHFjp7W6JRAJkrGl3hwechT1cbyZl+TcOM8NooD/emXDV+5w9vFcAr1itXz42XD86FLxkU3FOPq3rdBQOLl/KpvHOpVo9CTXSVuxGdQ9PHHTCtISlSl47yNbi0Ik4RSRvtEBsM758cIVgtGkiuZ8HhppLs2DB27zG3I9A2nX7vxIDMnd/C5ykxSdSCvlT0cLLeRE/1TUyRWhms8GxR/JM7tHV+w8ApYpwvAInzVP58X8/IxRGDBCrih83XjhDU3SRDJP09T2W/yWLzyFVr3svdNc052ZSbrYEDdo+dU/C9KUpX3A8NnT+9Ey5IXSOZWqk/NTW55h+IVjX71Gi9zHOIKjPFlQZv9xmnidNes68CDTfYqNwR1gi/FNskALzKrRSiWNNbDKds9HCgw2OtxTK7QbR0+o7tFVlc4kNWu4Ims+ehAv2nUNvLbYIdJSfQe3NMQNEzIxEGO1ed+qoVz0LXaM+sECcBXuv9P/hj6rxNR9yFIkBX4nhIu/TXUUvADvDuafAdkumpNJxEgw7tYomigpnxvlJ5STCka5HqVBcdo1pYNMa1ExqWh0rmIkZlewRY5F4S8R0Yh4LGQ7wG5KhSCDGt2swVPQk47peV62zmq/2NXzOFSPEZt08vLYnwVF1KaI2ONKgfJOEQ0990YUp+5HIfNAEC117ZFNcTtEPkjMZv+qe6VcTMKJ72x+gNzcM4JKF8rV/TSaUK1GsjzsQRR/ardnbzy7MsAIRvbF8bRKbgOb4A7Mf+O2Et7SVN2l6i630K/J1fNLXBxIXTmHuU1PAikbuUIGYKuhTvyYjoZOlzk4dsXfb27pjcWYP0tNPpX03TxLK2NNMLbfESAlLgJgYMok6BZQRUg1rbJJsvUgShFzi2RYcMOWlqGom0abqkQ6bRpSVuR8gXQJFlAT8hnwz78weFilKyF7k35fxs9wtQ8HeaEXnaZj8kf+pjaOv4eOlMVGaVAb8UaxRSEXChpoSHVzCBF6pW9ubYjLeqh9p3x1JBzPCSPLY4khEPXfZb/PCGg/EeUJ0RM5P8vaJdwEU2t9qRoMZD2h7dJSQ6kN8OTHPVh9qcd5UBR199GXilNUK+XLVr3WMJ67C/6P+8igFsqhF2QoU4Jvg0MC48nBFE+nEa1YTrZNCrt7/qRwdNh+QYH4aovwFZwh66MWXFO6bUBYm3SksSdguBXIMs2HNNX1zWIAVg/99da4dgXYf15m1703eack1fyLf3wGcJHPchGZgjJCqkwYuBJbzxYboTyOYRNez8Vyyo9R3mvZlHOdLT1zQ37OtqCXKX32VAIJYuKd/ewK5lR9iNOiEpPMYcPX95Z4tjHIWJRCYSa59hs7mioguat+MRwCKBbFNMqXUn/I5CuaDu4tckoYjIx4GHzACck08vsr2fSKjCrIxg67TwpM4quHMa9tt8b2rsk92ntl/Hg/e38rwC0Zxux0bjGfC2GA1zEUI6f8nseHeMaXub9Hk9VZ8ZA/AAAH2qkme40+eABSuuIr7DnhFbO2GAXEIdB17xR/whHIqXSkRXFgXbRTYGgAcDx79eMDbwGuDzrFgKWKPX4n/sUM8/rmrr23GvG0DmwEpkC3U2KJGxB4s5D2ea2/hq8gxFncntcDsU9oTF4nh/JCZSKZTF+88pN89QxAu+LovzlDr+/Urm/e73/+AosbSlIw87L2FbAASRzXUVV00r6nJIXLmj6rTPitgCl4Jf+B8MGk75w0m4iOLWs/8ZaEaZ/haFQO6Oyqen3wyhr3Uy4e8BjuyFaaYEW6ijWk4QvtAUrlQOBIIluvrcoGE8P6xaYWoujrJvt3OWyC9AgjdeN8CWaTSfPVDHfQc6zusA9UB3QRaOwzRU30/4l+xezg8BGnLYTawJIss047YngE9gjiaD91skqvodbq/ACU6zauxnXx6LzDoRpHI/YELwlnGE2lIrbf8/yKUNziM2VN5PW81/Pkb0sp0yLTHEiaVuL45PUXPE0KiIL+16LgDHkmlyyapruzkDpXc3t5R0zLW9WSK1dcdKS6SaqBuhN9oMycLTHAH+23OrrnB7Er6ci/R5/JYVYa/Myz+f3P1JgOsfBgSRpGBd7VybMVocEZRDGMNNDtsVKScaFLAFnXbM4AuUM6OPN2UzeuMSPPiebFViwD1T7bci52V3BC/EVsIhNsbarvJZe6VA2L3KKCJ3wO2V4FxbGLLXz6X3DzFO9+cwn8YA8OL24dI3uAIEchuK1DV7t7/KMEujejwanL6s3tgNgrGJyretmkrA2FgzffxIypeFgQLEzAzhnGqSNBwzlio5GUTypYPIw1CZxpuEGu8afoct2KENN+HCTEBk90FP6+QKdVmzVyLSU5W4atSjXr4s7/cvv/PazQyeD+oELBhuxSxCG9OqaU2BGe8CG1VKFCWP4wLz8qjCfhwZ1WCyDzBncQL9ZgQf/kKvRDEmYIj+lIUpu03G7UINqGcUSbtp/u6CBNfyATirlDS5qRJxjYH19NXfTyOB9eSC5owMMRP6HZ+/1RTTRh/VT/UC4U/js6J1Ox92QKVEzH2JGqfX1PC0s9TgSQpBBBnDq3fLd3ovf0Ncz/yOBYwlpvRV5+azSsH9PHFnuM4XHJDbtLAEDNGus/CGVY3jNhQa4FvGlu0fc3jzVn2SDr08qttkK+8SLwSMdIsjW4DPnZ3lS5o6MdmiP74Rgza/69QUCBUUwJbB06HKEskvp4K0Ts8ZlyBB6YI3lkbZWbbz3FgjehpJfx9vCia0kI+f8CnoYDgSXhlgj0JWdGkIFoFquKaMMqgOk3xxzPwyOwzmEbpiuZHeOkbVm5bIB5ZKaEmFm8kZaVjsnaTsVvZlgRwwHmZ0h92C42TbR5XRc6sf8dIjlcKYiCFqO4MG2VVcC/JdAXWFrG6XZRR/uPk1gaJ5+E9AgiWUs+7RhT1PUveoc2bzEYdJNjLL5WH5gekIPfZ2pWbj6/j/jo4JrcoLlEDOrTk2T9qFE92mHeDQZ9BBPgNkDCcfzNehkDmEVVXOHs/mJ1LAx31Q7x7FSqQfOV1E9CJZl3DE9gQIPskBky7phnNBT33Wekq3A04TkFG7LeX0pj8WtuBrXOEs8thdRDVRJLCHKckkTiJU7EJBYeBQ/JkkioZ8T8d5KzGPKovbBUJ5/5wIyV4Jdq+WKYh0IWWkHu4sMKfuhuZj1+vFt8aDJ5wyAu4CD7CJFawUU0yGpkX3O3igJjEz//EeNtmx4m/0XQwZMwo3ZB7LA44zasvFzJ3/Dw46mQcJ2xWmuzZ0T6yMNj5Chdszr3ZyDksaiSczuA8MXv6T03zQfjFOaDCRP67QKS7LcTsYsL4msUR/TNkMZ0pVxmmn1FFGPhqm5/p55Q4efC1pwpgRuEIf4D0VrO6lWa5K/Siocj4ii1XHL3jOZAqsHNz59b7ErKLMoew8i89IYVvFONqYhqta/0Tn2X2kyaZb0Zln3y4jn1h2cnGwLopuOXdyaVl7/PC3Zz0XoYi6ZEy+4lJMujilB2iosTr8Wil9/57+LgsDzgxR6dxSvDGgabmxBG6iu5oMRLZNuHY5jAWlyKOZ5qNtQC8Rx2nA+37uUTQi2Eiqy48DujHJN/Lhcs04UgUl/1+wiWo4tM74xdhZAFmL61mc5DtVwo2Y78i6uNtUQL9Irn08t3bHKxKjeUo/zJGbA3FnhFb5e1rd2xixGn0qjp0Q4PaTF6dw4DhO6R0miVFLnjEQHaZrSfmjDeHPUu3ukTrb7tGPYJzdjYOTMzi8YNkIWpHc4UmUl+5MHiZaoIyZKfW5TykEV0yHBiuHTgz0Q8scRk1dIsTZTlCLqPC+RYEXTJIgdOq5jcSxdF8+hWezBe8WIdMTZyMvGeECxDZdjDFpnjr49CVVTgDybVL/ZkQIeUPf+F+y3+7gBn3NNqLOGHYbwCb4fAuZqQ54o4O9JBfWnVfF67Y70AedUUPnkpc0iR+ZejoH3KbOx8dOTqmCwdSYcCZtnNkGZQ69mz7ESAfrzTGkKVRdPOAhSLi24uFPeWr4guGVeqTocBkbu5NGQwvLQfREfwqMkpdPSyLyNQZG3bhfzfVKyBS8eFgxOOLPXdxCS5gxM+DRRqQE0W0Lua24LPf+9mdYxjJKwGgFJHYVmVY7Ew67fZo5cuhTKjMlF4syi9ep3pMmu2v+J8pmKYC52d/ahCVULGGdafRT3QOLcGm1VEEAD9EZWoVc853L8+wPLIoC5tIqPS5Q4n1FjsW9UX1LqC1vCRs+oiZ4XnLk+em2Q0yDJNpuzCvhcLpFGSU/LsMeq/GMTOejiBqusEPflsEQacgB7LtpU/xJpAanN6Y0ZwNPiEb7YvUTh6hLav5hCkqDFZSAV4u6P9xolz503YisbyAf8Qbwi1AvPWIyvC+w03RmhPH861o8Nge4INwQcXwwt2XDSdBn2EcGEfX1IF6imcI1a+z4zvNk+bqJULQA8I4eMhLg5np9/Rwb6+DVbuZhIZIOUo9adjGixW01fU3E0QCC1e5iQWmBnf3GkA8ef2tFmi3jLW52jHqslJb1liYHo7A1luD1E5gotGHQnJjV5DvV3eDBUkpFRQmzOiI2xTHUavtU8iisQk/vMTYdJzRAlyaECendztCSekXHnm7Lj380nMKr2CtYq7xAg+4IFGwsbza+t6l5AFqAGcWSbQK41G6K/rBsJwy5QEXACNFzyaJM6oNaPSiHYaVGMUkTu3IkvxbhNk1qc8xpHM1uOXcL+KzrZqAVAOMLwn4YePDASVdYDUTLqviQiL+SI+fJIhwlBrw+bye9Okx1l4nlaCKES4usmNZBBYJ/DD0Pp2rAAaLlBnr6kI8OqzCljhSYgTRIMQ2hHPj0dY6O0QqpuAA1Axn1HqrCaiKFzXAjrWS+IWbpB8e2S8EewVWixlCkzKOGzjTJyQxhQmiBTLnWp/D0l6Nz+A3iTX1mwLGL84LXYG5G7XhA2V2GgQmlUwREeCjKN6I+KKPMraZzcA48s0Xhy81ijhMK8PyNqxUer5LpykWZwzGyqaFeCiKKslVr1M3oyKv0AHRCdR74qlsb/x5QTrSWcWVpSUZapD7QeFDAUNJ+kyohay1UxGSAmjKEyH9KqnPAX6c/KdupJsWG81Z4Jg9Cal9kAPKEdSueTdEdGGt9zU3rsjAvFyHe9xN46HWsrXwAsbGwMSRqkIuomdoO0Q+2b1IwheaUcRszKZ/KK+Z2byr35+0pxnhPXk5bpFVH+RV3xptdc29XMqgIlxlkrk3ANRn55EjgqV44pQJ2CHTvbYlzr+gJzoWnCSO6SHT1+PlJrt9GXibFyI1UyfVrzjYa5EA4THR7cEEz1xMwr5briweQfHLb4cVVLHMOOqplD01TgUccPen2YMrwHoUcD/KyjdqD7vPYVGTmAHDzcUGSBWM5D2vpO1FFYIgqTLugz2Ty7FMaw+GWwzM0EgS3JUiFDjYgI0WmW5TlmHPhEhYF4O0wrlxN5Gj5HXKCzBkFTa4/pKb/2QqeiL/zj4YnFFpPfM3e3XgFKKlLBVGw2UXVFlQHy6PauAZ207BMOXzGa4ZpDIFJAVsst1ym7pt1y11bSXoqCoCi0/4ARbAYDScoCAnyaMURCYyUz4+YnkhHdXt9u3HLkbq8JX5Am56VORluoSD49KfNU1r4EjcWHt9ccsVIyjOroYF+MEdTI/cx6MwqHzRuNJuZkyk1GtkIONbpelL+Q6ayLyEI2RG5BAHXH7LqIfsuKZQVcfhs2q9dLBUCqaRSqjpoPnYZcAXo3fd4x56CZYJubGYEV7Tv0Dfg4Sb5C3p1ylCvGQe1TVY1a0mL/fzVkefwSkZKe6zlr5kMpbCvF47gdTquIWYN3V3gYKL2cb78eW1iT1+IsziFj3p8AZLS/xu8ivKJf9EtHFGTwiZrW20J3rNhipErHeTMovCaehgyxF/qkg7D2p3mQlFw2BLUpbIcaoO9ATc1wbit9ThllInij8Do2RyzlCF8M/pbLA0ITLcyfHCA+xA3D5m09iHVBnDfoNgBv0BS8e+DWXZPeukwPcCvqOyms4u2OE21TAbckjiLWTeqAo57JIvlm65ddID6/B1PNrVvtqdkVYQ3nXqRMboTfIczQ3/86GbfaQN6vGLr0x7heneP5kVBGTiiIkc9XLMU+3KaPbR+LeSjkVnYc8BGDDYvQzGfD3aKz/6LPE0Y9JIpeuNVO/1nWreL2ozmDX9+jA2pcKwVodo1xLrJkSZEYY9T5mggw2bh72wrIbhjrKnR4gg9T9hdR9yobzlmO+sA5sRZqUCSx2h4WUQagN1NpS1Pet7x8ckPO1H7sdjlLYzl+0MG8GGNCqyVmTGVGZVcOAyhZKO4zM+s2ISl6i9fiz3qd4PqmF7+3BwWRsZNy+Uwa18qEfBeyrcqyCAgCo6uPUKhq7CLmMj9K7/gB5yIMOat/NUF2e3Svp3p2+QYB46U0nzXC7soWu9YvbZ/eZC6NPHiywemE5UOEJkrYprI6h1u7rG7ME+g8CN/TtjUc6/4IapIEl7IoawaarW52xTl3fKB7DpAGhy0ke7HMgirqcJh7iO+Kvi2mJKVNMwaHdK4RbNQFLIQp3Lm2OMUn1FippPd9JstnfR3Uj5UN8Ns+USk0GDFl/aGqgkHbWr8enDLqWLOUGyWF2HliioGle5l5BVRrLlLWRdNHRY8uf6WUtwF6YDZMgdj7Z9L7Za+fzWwrKNn72hMit6DK4oJhR0v4zsWRL27XxfcMtkPT33GcbLzUPW8UoXqwi9iSk6rFoReZVRu+0it2Q5x+d89CtFGTHLm9NGWJcnw+hk9E3d3jGG1gd7a3G41Q0lFqZheFoqiND0SQGjS8XL//gCGPXPQcVcOw5n1vbG+CENLW8PQQ02jdDehQAkkzMWodhi4J5KeSqUCSXmOJNZLrYn62dQatFYz2PIEky1eyxzO6taq9uR7IVPOTss+FMErQU39RDi7YYRt0b67fVOW1sIlnF8iefZ2BVElIgCo67c0tUgnSoWxh3LHv7Uqb9Ip3giTYgwJyW64MAbc8CUqDpGWNLUvznTQyFKBVekEgvRkCVEAttAzExP0cF9jU4ckRrHovTiMeWSAisU33L4pG4r3kUTdZ8pL9vTETgw3PoT+YNuhEWqsiS1n3BBvuHMeETF72vmL/mStYPXYa3FbRQDBVsHCKx3eg1mVGAUy1+9dCBOEMy/jzzRlV3nw7ybZ2IEI8W2jPdcLZdXX96EGqWk8QNajs1VNwXj09kjMoIuPMnvsP8BW6GRGPQn5Cnd0RvePRxZklqBO8S1DHk6qUf+EDfU6OO3aZuoIu55hMG4DXFva8eixwCI8SnLJmvcSzy+yCFxqEW9GDAbCLko8shOgXwlmP4iR+G4PRQgJBE+cVUFWYL4oplwlE1ymgO4hmF6+FruZ45ehzKlFALgExQslVZV193HP935XwIlosqtaCrOIliBev8HQ7lFgeZg9CteIL6N4pPspnFrW9NyjZtBG1kHvo+SqtBaggf68LBktWIVtsG1NKCpQYeFB18OkqLh93M7csFUCD7xECaDS6pJ1y+od57WkBK9fC/jyrS0Yh65jqH4o6BOnaw42AoGZFbmviMQIrQbkj9g32UnKno8rmd1jFOJg8oj9KKk14ONDf6vBt7L9aWUscffivlMBRxCAR8YutFuQzk0LrcXJ72vSGtXl3MfYFNqcNgQiXt92qGP/2y1OjSm7yiLUl72gtub6yp/ckZ1Ss2IaW4bUk8/7KQ0ZHCS2LjqrSbMN4H2NgHXQJqS5QGMrxTmed+F8wbgluHlhkzhYlC6u+vju6zMGYxYRHvBCS0+yFbpkrTF3LwG7N/KX4IPgqQS8pO4+VEqC9DTNK3gedz9tB1SPlPnGGcVCWFm0YXCeH2ierpmZcHWkjuHs+dBU37LU1KK+kr8RiSE7dtnCj2gwqDW1BzTlyA0x6LDTRQeHD/27zZuu/C0BvoSzCggoZYOfX0LFgCIPoIiO6In6Qz/aWcdoD7h/MlZave3ix/WAy2daApUsYPKk31/KZPJv5+nGqhUorQwagcR55wqSnDbRXPCjVbM80ERgE/67jFa3r0iNl7dgvOksSdTobmNSPgR9G38GUbOeCCu/BfFuSZdZtYtlnwxGJZuQPLp1AylFFQu5QpJj7R/OXtaa/gKkEvgr4SDnQKOvdH51bXC9JXepM4QaAjra5mZpTu0IrJvK6yMXyS1Nkg1AjjRApodl+w7MthsT7GMTqbV4DJUdroAUKxxTCqNT3KhNqa7sPVrTbU2zkiYBC3jKnqcZTyF29HV7+vc6Q6Ko9mWMtQargNan+tIx7kBMfn/CCVdJ99ExvoZ8FV6EZaRwZVWFvqtbgJzvwgq8fhowB+/V3GKbN/lkbiJ+CPxZIptN6RGswj1d04Gi3G6TDdyIcUreJqCDX5hZFHg+PncAtcnlYYwG0NoBVV5snpYCJdKiImoBxtGCZ20F2IBVHD7eH3l1Myj4//ej2Q7m5M3LD+CM13aF/Z0VfTW+7+JRkdubPjERP46f1Vg53wjHWPyT957zUcWFzC2Y0JIjGtf97ajVNDQzBfVA2YJK9T5EdD3T5EdaQsf6msw7lc37SVbJbhQfFXVEfVMVh4bMl5KCfw/tQjkBDQLsP/pKMog1nLyJz0FFUJ74eIXR7kVk0yv/LRcPXpkpxP8K5dZxtKF0E6Sp+Lq64DXxaobNqOQkZ91ai5aZiSOuaITAL7O3afB0U+p76g57wU0lYycOyeLIOp78hQIib4kcz/15kW+iHjXFlH4vEYOPBZaCc4kprF1vfIZXhLwoVN8CB9XUPr90Fkj1YsDyNc2vc8nLnZdO8H11lZ73QIX/yA+c06hxnNCW8takRtlD8OF1Bgubx4lGLJRmZpGNuE8LRwe+eRU273sdV4ZRau9HVSuLspLlXzMxeb8G8367NXB1AAuCThAz1Kv5qaTy1JTEqw2xe3udQSA9DeYfeTajoZkwzQ9f5GMNHi31kIbIkKrmIBnYGLWMRJ7wYC6SmSVuJsE9yHqVpSyOnHY6araqJ1kZQeFV7rc0zbaGndFM6QwSBeo+qpj9wHr4drBF4JAGlK1lKB4P/Xgp7UlIrN5ZBSAUbqd40zlA+FATak4YONV+r1uZzphq+0xBWjQJVnPDC86lwpq5s7CXI2Pbok2GYJqW1eX6N0iqhS9rOHTLMGp55ijIznG2BZ58UKZZKVrgDM3s89IDWwvsG2EyO+G+C5PT4kGI2HdGkreDFQYoY7TuStV2vsI/6sIse2KSJUI4KCJlMG62Linywt/M4muXOnmctY2FzNqNM+4c1ISJSZ7LDVJUQImV3sm2CpEdaplwDn0DTsPyY6Nf1YJSI30WI7bRa0BQSCi1SMpDqsv1Z7dcpertue9vKQxQ5+jgGMFUQ41666i4yIhl9Pgf4GvjO6SSlyJzwNV+xO6dGk+8ckG3I6fZuU9/c1tYEnqgLpjDfwpqcPh0BP7scuMAXGBkOd+tCDuEnQ8+38DKiAqp6bzrD1lgd3G0HCiCSr+NeScgA76eXL";
				string fix = raw.Replace(" ","+");
				string decrypt = StringUtil.DecryptString (fix, "Ozark");

				//Debug.Log("Decrypted Password:"+decryptedPass);
			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 240 + offsetAdminPanel.y, 120, 30), "FLY!")) {
				//string decryptedPass = StringUtil.DecryptString (sampleText, "02Ksi#89j");
			
				AdventurerTownNetwork myAtn = TownManager.Instance.myAtn;
				myAtn.photonView.RPC ("ToggleFlying", PhotonTargets.All,myAtn.photonView.viewID, true);
				//Debug.Log("Decrypted Password:"+decryptedPass);
			}
			if (GUI.Button (new Rect (470 + offsetAdminPanel.x, 270 + offsetAdminPanel.y, 120, 30), "Stop FLY")) {
				//string decryptedPass = StringUtil.DecryptString (sampleText, "02Ksi#89j");

				AdventurerTownNetwork myAtn = TownManager.Instance.myAtn;
				myAtn.photonView.RPC ("ToggleFlying",  PhotonTargets.All,myAtn.photonView.viewID, false);
				//Debug.Log("Decrypted Password:"+decryptedPass);
			}
		}
	}



}
