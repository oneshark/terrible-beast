﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableNPC : MonoBehaviour
{
    public ChatBubbleCanvas chatBubbleCanvas;
    public string NPCName;
    public Collider myCollider;
    public bool useOnHitSound;
    public string onHitAudio = "blacksmith2";
    public SkeletonAnimation skeletonAnimation;
    public float soundDistance = 8;
    public bool soundAllowedDuringMenu = false;
    public AdventurerTownNetwork atn;
    #region NPC Dialogue


    #endregion
    void Awake()
    {
        myCollider = GetComponent<Collider>();
    }

    public void Start()
    {
        if (useOnHitSound)
        {
            skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();
            skeletonAnimation = GetComponent<SkeletonAnimation>();
            skeletonAnimation.state.Event += OnEvent;
        }
    }

    void OnEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
        if (e.Data.name == "OnHit")
        {
            if (atn==null) atn = TownManager.Instance.myAdventurerTownNetwork;
            if (atn == null) return; //try to get ref bt if stilll cant, give up for now

            if (CraftingCanvas.Instance.craftingCanvasOpen & !soundAllowedDuringMenu) return;// if we dont allow sound during menus - get rid of this

            if (Vector3.Distance(atn.transform.position, gameObject.transform.position) <= soundDistance)
            {
                AudioManager.Instance.PlayClip(onHitAudio, true, false);
            }
            //string animationName = skeletonAnimation.state.GetCurrent(trackIndex).animation.name;
        }
    }

    void Update()
    {
        Raycasts();
    }

    //Check for hovering over enemy
    void Raycasts()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100))
        {
            ClickLogic(hit.collider);
        }
    }
    void ClickLogic(Collider col)
    {
        if (InventoryCanvas.Instance.equipmentOpen) return;//cant click on it when inventory open!
        if (SettingsCanvas.Instance.canvasOpen) return;//cant click on it when inventory open!
        //if (SettingsCanvas.Instance.canvasOpen) return;//cant click on it when inventory open!
		//Debug.Log("Ray Hit"+col.gameObject.name);

        if (Input.GetMouseButton(0))
        {
            if (col == myCollider)
            {
                //normal damage
                if (Vector3.Distance(TownManager.Instance.myAdventurerTownNetwork.transform.position, transform.position) > NPCManager.Instance.MaxDistanceTalking) return;
                OnClick();
            }
        }
    }

    public void OnClick()
    {
        //Debug.Log("Clicked on the Lobby Button");
        Greeting();
    }


    public void Greeting()
    {
        if (!greetingCool) return;
        GameManager.Instance.InputEnabled = false;
        if (NPCManager.Instance.NPCCanvasOpen) return;
        NPCInfo _npcInfo = NPCManager.Instance.GetNPCInfo(NPCName);
        NPCManager.Instance.SetNPCText(_npcInfo.npcName, _npcInfo.npcRole);

        if (_npcInfo.hasShop)
        {
            ShopManager.Instance.ClearShopItems();
            ShopManager.Instance.LoadShopItems(NPCName);
        }
        NPCManager.Instance.ToggleNPCCanvas(true);
        MenuCanvas.Instance.ToggleMe(false);
        NPCManager.Instance.SetAvailableButtons(_npcInfo);
        string msg = NPCManager.Instance.GetGreeting(NPCName);
        chatBubbleCanvas.OpenBubble(msg);
        CancelInvoke("CloseChatBubble");
        Invoke("CloseChatBubble", 4.5f);
        StartCoroutine(GreetingCooldown());
    }

    bool greetingCool = true;
    IEnumerator GreetingCooldown()
    {
        greetingCool = false;
        yield return new WaitForSeconds(1.5f);
        greetingCool = true;
    }


    void CloseChatBubble()
    {
        chatBubbleCanvas.CloseBubble();
    }

}