﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class InventoryCanvas : MonoBehaviour
{

	private static InventoryCanvas _instance;

	public static InventoryCanvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<InventoryCanvas> ();
			return _instance;
		}
	}
	//STATS and EQUIP MENU
	public GameObject atkObj, atkSpdObj;

	[Header ("-Stats To Show-")]
	public Text atkText, atkSpeedText, critText, critPowText, defText, regenText, hpText, xpText, xpLevelText;
	//costume equips
	public ItemSlot costumeSlotBack, costumeSlotBody, costumeSlotHead;
	//Equipment equips
	public ItemSlot equipSlotRing, equipSlotNecklace, equipSlotArtifact;
	//Weapon equips
	public ItemSlot equipWeapon;

	//EQUIPMENT MENU
	public CanvasGroup cgEquipmentStats, cgEquipmentSlots;

	//INVENTORY MENU
	public List<ItemSlot> allItemSlots = new List<ItemSlot> ();

	public GameObject blueTabWeap, blueTabEquip, blueTabCostume, blueTabJunk;

	public CanvasGroup cgWeap, cgEquip, cgCostume, cgJunk;

	public ItemType selectedPanel = ItemType.weapon;
	//whichever this is, has a blue tab, text and shows the items

	public Sprite origWeap, origHeadCostume, origBodyCostume, origBackCostume, origRing, origNecklace, origArtifact;

	public ItemSlot lastItemAdded;

	public void AddItemSlot (Item item, int qty)
	{
		if (item == null) {
			//Debug.Log ("Null Item");
			return;
		}
		//Get the right CG panel
		ItemType it = item.itemType;
		CanvasGroup currentCG = null;

		//TYPE SPECIFIC LOGIC
		if (it == ItemType.junk) {
			currentCG = cgJunk;
			//JUNK SHOULD BE STACKED
		//	Debug.Log(" Searching for:"+item.ID);
			ItemSlot existingItemSlot = allItemSlots.Where (x => x.associatedItem.ID == item.ID & x.associatedItem.itemType == ItemType.junk).FirstOrDefault ();
			if (existingItemSlot != null) {
				//Debug.Log ("Already had one of these!!");
				existingItemSlot.IncrementItemSlot (qty);
				return;
			} else {
				Debug.Log ("no item found");
			}
		}
		if (it == ItemType.weapon)
			currentCG = cgWeap;
		if (it == ItemType.costume)
			currentCG = cgCostume;
		if (it == ItemType.equipment)
			currentCG = cgEquip;

		GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/ItemSlot")));
		ItemSlot itemSlot = newItem.GetComponent<ItemSlot> ();
		lastItemAdded = itemSlot;
		itemSlot.SetItem (item, qty);
		newItem.transform.SetParent (currentCG.gameObject.transform, false);

		//newItem.transform.parent = currentCG.gameObject.transform;
		newItem.transform.position = new Vector3 (0, 0, 0);

		allItemSlots.Add (itemSlot);//track all of them so you know when to increment qty
	}

	public void UpdateInventorySlot (ItemType _it, int _id)
    //match it up with inventory list again after removing items
	{
		ItemSlot slotToUpdate = allItemSlots.Where (x => x.associatedItem.itemType == _it & x.associatedItem.ID == _id).FirstOrDefault ();
		int newQTY = InventoryManager.Instance.GetQTY (_it, _id);
		if (newQTY >= 1) {
			slotToUpdate.m_qty = newQTY;
			slotToUpdate.qtyText.text = newQTY.ToString ();
		} else {
			slotToUpdate.RemoveSlot ();
		}
	}

	public void EquipLastAddedItem ()
	{
		//Debug.Log ("Equipe Last Added Item fired!");
		lastItemAdded.Equip ();
		//Debug.Log ("LastItemAdded:" + lastItemAdded.associatedItem.name);
	}

	//public void IncrementItemSlot(ItemSlot _itemSlot, int qtyAdded)
	//{
	//    //increment the qty field
	//    _itemSlot.m_qty += qtyAdded;
	//    _itemSlot.qtyText.text = _itemSlot.m_qty + "";
	//}

	public void ButtonClicked (string button)
	{
		AudioManager.Instance.PlayClip ("click", true, false);

		if (button == "Weapon") {
			SetPanel (ItemType.weapon);
		}
		if (button == "Equipment") {
			SetPanel (ItemType.equipment);
		}
		if (button == "Costume") {
			SetPanel (ItemType.costume);
		}
		if (button == "Junk") {
			SetPanel (ItemType.junk);
		}
	}

	public void SetStats ()
	{
		Stats currentStat = GameManager.Instance.currentEntityStat;
		atkText.text = (int)currentStat.atkMin + "~" + (int)currentStat.atkMax;
		defText.text = currentStat.defMin + "~" + currentStat.defMax;
		atkSpeedText.text = currentStat.atkCooldown + " sec";
		critText.text = currentStat.crit + "%";
		critPowText.text = currentStat.critPower + "";
		regenText.text = currentStat.regen + "";
		hpText.text = currentStat.hpMax + "";
		float calcXPPerc = GameManager.Instance.baseEntityStat.xp / GameManager.Instance.baseEntityStat.xpMax;
		calcXPPerc *= 100;
		xpText.text = ((int)calcXPPerc) + "%";// GameManager.Instance.baseEntityStat.xp + "";
		xpLevelText.text = GameManager.Instance.baseEntityStat.xpLevel + "";

	}


	public void SetPanel (ItemType it)
	{
		//set selected
		selectedPanel = it;

		//blue tab
		ChooseTab (it);

		//slots
		ChooseSlotPanel (it);
	}

	public void ChooseTab (ItemType it)
    //Turn on the correct tab - turn off others
	{
		blueTabWeap.SetActive (false);
		blueTabEquip.SetActive (false);
		blueTabCostume.SetActive (false);
		blueTabJunk.SetActive (false);

		switch (it) {
		case ItemType.weapon:
			blueTabWeap.SetActive (true);
			break;
		case ItemType.equipment:
			blueTabEquip.SetActive (true);
			break;
		case ItemType.costume:
			blueTabCostume.SetActive (true);
			break;
		case ItemType.junk:
			blueTabJunk.SetActive (true);
			break;
		}
	}

	public void ChooseSlotPanel (ItemType it)
    //turn on correct panel - turn off others
	{
		cgWeap.alpha = 0;
		cgWeap.blocksRaycasts = false;
		cgWeap.interactable = false;

		cgEquip.alpha = 0;
		cgEquip.blocksRaycasts = false;
		cgEquip.interactable = false;

		cgCostume.alpha = 0;
		cgCostume.blocksRaycasts = false;
		cgCostume.interactable = false;

		cgJunk.alpha = 0;
		cgJunk.blocksRaycasts = false;
		cgJunk.interactable = false;

		switch (it) {
		case ItemType.weapon:
			cgWeap.alpha = 1;
			cgWeap.blocksRaycasts = true;
			cgWeap.interactable = true;
			break;
		case ItemType.equipment:
			cgEquip.alpha = 1;
			cgEquip.blocksRaycasts = true;
			cgEquip.interactable = true;
			break;
		case ItemType.costume:
			cgCostume.alpha = 1;
			cgCostume.blocksRaycasts = true;
			cgCostume.interactable = true;
			break;
		case ItemType.junk:
			cgJunk.alpha = 1;
			cgJunk.blocksRaycasts = true;
			cgJunk.interactable = true;
			break;
		}
	}

	public void Close ()
	{
		_GameSaveLoad.Instance.Save(false);

		if (equipmentOpen)
			ToggleEquipment (false);
		MenuCanvas.Instance.ToggleMe (true);
		if (!SettingsCanvas.Instance.canvasOpen)
			GameManager.Instance.InputEnabled = true;

		//Debug.Log("ShopManagerOpen:" + ShopManager.Instance.shopCanvasOpen);
		if (ShopManager.Instance.shopCanvasOpen)
			ShopManager.Instance.ToggleMe (false);
		if (NPCManager.Instance.NPCCanvasOpen)
			NPCManager.Instance.ToggleNPCCanvas (false);
	}

	public Animator equipmentAnimator;
	public bool equipmentOpen = false;

	public void ToggleEquipmentSlots (bool b)
	{
		if (b) {
			cgEquipmentSlots.alpha = 1;
			cgEquipmentSlots.interactable = true;
			cgEquipmentSlots.blocksRaycasts = true;
		} else {
			cgEquipmentSlots.alpha = 0;
			cgEquipmentSlots.interactable = false;
			cgEquipmentSlots.blocksRaycasts = false;
		}
	}

	public void ToggleEquipmentStats (bool b)
	{
		if (b) {
			cgEquipmentStats.alpha = 1;
			cgEquipmentStats.interactable = true;
			cgEquipmentStats.blocksRaycasts = true;
			//TownCanvas.Instance.ToggleSoulText(false);

		} else {
			cgEquipmentStats.alpha = 0;
			cgEquipmentStats.interactable = false;
			cgEquipmentStats.blocksRaycasts = false;
			// TownCanvas.Instance.ToggleSoulText(true);
		}
	}



	public void ToggleBottomStats (bool b)
	{
		atkObj.SetActive (b);
		atkSpdObj.SetActive (b);
	}

	public void ToggleEquipment (bool b)
	{
		ToggleEquipment (b, false);

	}

	public void ToggleEquipment (bool b, bool dropGive)
	{
		if (equipmentOpen == b)
			return;//we are already open/close the way that was requested
		if (dropGive) {
			//Debug.Log ("Call Event to set ItemSlots to dropGive");
		} else {
			//Debug.Log ("Call Event to set Itemslots to Normal again");
		}
		EventManager.Instance.CallInventoryStyleChange(dropGive);

		if (b) {
			if (!GameManager.Instance.cameraEffects.isZoomed) {
				GameManager.Instance.cameraEffects.SetScrollValues (-7);
				GameManager.Instance.cameraEffects.ScrollLocal (TownManager.Instance.myAtn.transform.position);
				GameManager.Instance.cameraEffects.isZoomed = true;
			}
			EquipmentIn ();
			SetStats ();
			ToggleBottomStats (true);

		} else {
			if (GameManager.Instance.cameraEffects.isZoomed) {
				GameManager.Instance.cameraEffects.SetScrollValues (-10);
				GameManager.Instance.cameraEffects.isZoomed = false;
			}
			GameManager.Instance.cameraEffects.InvokeFollow (.7f);

			//  GameManager.Instance.cameraEffects.Follow();
			ToolTipCanvas.Instance.ToggleMe (false);
			EquipmentOut ();
		}
		GameManager.Instance.InputEnabled = !b;
	}

	public void EquipmentIn ()
	{
		AudioManager.Instance.PlayClip ("inventoryslide", true, false);
		equipmentAnimator.CrossFade ("EquipmentIn", 0);
		equipmentOpen = true;
	}

	public void EquipmentOut ()
	{
		AudioManager.Instance.PlayClip ("inventoryslide", true, false);
		equipmentAnimator.CrossFade ("EquipmentOut", 0);
		equipmentOpen = false;
	}


}
