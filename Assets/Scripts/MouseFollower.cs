﻿using UnityEngine;
using System.Collections;

public class MouseFollower : MonoBehaviour
{
    public Vector3 offset = new Vector3(0, 0, 0);
    // Update is called once per frame
    void Update()
    {
        transform.position = Input.mousePosition+offset;
    }
}
