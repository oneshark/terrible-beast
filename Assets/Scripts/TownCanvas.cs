﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TownCanvas : MonoBehaviour {
    public Text soulAmountText;
    public CanvasGroup cgSoulText;
    public GameObject soulImage;

    private static TownCanvas _instance;
    public static TownCanvas Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<TownCanvas>();
            return _instance;
        }
    }

    public void ToggleSoulText(bool b)
    {
        if (b)
        {
            cgSoulText.alpha = 1;
            soulImage.SetActive(true);
        }else
        {
            cgSoulText.alpha = 0;
            soulImage.SetActive(false);
        }
    }

    bool townCanvasOpen = true;
    public CanvasGroup cg;
    public void ToggleMe(bool b)
    {
        if (b == townCanvasOpen) return;
        if (b)
        {
            cg.alpha = 1;
            cg.interactable = true;
            cg.blocksRaycasts = true;
        }
        else
        {
            cg.alpha = 0;
            cg.interactable = false;
            cg.blocksRaycasts = false;
        }
        townCanvasOpen = b;
    }

}
