using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    public bool showGUI = false;

    void Awake()
    {
        //PhotonNetwork.logLevel = NetworkLogLevel.Full;

        //Connect to the main photon server. This is the only IP and port we ever need to set(!)
        if (!PhotonNetwork.connected)
            PhotonNetwork.ConnectUsingSettings("v0.1"); // version of the game/demo. used to separate older clients from newer ones (e.g. if incompatible)

        //Load name from PlayerPrefs
        PhotonNetwork.playerName = PlayerPrefs.GetString("playerName", "Guest" + Random.Range(1, 9999));
        string roomName = "myRoom" + Random.Range(0, 9999);

        //Set camera clipping for nicer "main menu" background
        //Camera.main.farClipPlane = Camera.main.nearClipPlane + 0.1f;

    }

    private string roomName;// = "myRoom";// + Random.Range(0, 9999);
    private Vector2 scrollPos = Vector2.zero;

    void OnGUI()
    {
        if (roomName == null) roomName = "Invasion_" + Random.Range(0, 9999);
        //if (!PhotonNetwork.connected)
        //{
        //    ShowConnectingGUI();
        //    return;   //Wait for a connection
        //}

        if (PhotonNetwork.room != null)
            return; //Only when we're not in a Room

        if (!showGUI)
            return; //Only when we're not in a Room


        GUILayout.BeginArea(new Rect((Screen.width - 400) / 2, (Screen.height - 300) / 2, 400, 300));

        GUILayout.Label("Main Menu");

        //Player name
        GUILayout.BeginHorizontal();
        GUILayout.Label("Player name:", GUILayout.Width(150));
        PhotonNetwork.playerName = GUILayout.TextField(PhotonNetwork.playerName);
        if (GUI.changed)
        {//Save name
            PlayerPrefs.SetString("playerName", PhotonNetwork.playerName);
            // GameManager.Instance.guestUsername = PhotonNetwork.playerName;
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(15);


        //Join room by title
        GUILayout.BeginHorizontal();
        GUILayout.Label("JOIN ROOM:", GUILayout.Width(150));
        roomName = GUILayout.TextField(roomName);
        if (connectedToMaster)
        {
            if (GUILayout.Button("GO"))
            {
                PhotonNetwork.JoinRoom(roomName);
            }
        }
        else
        {
            GUILayout.Label("Connecting...", GUILayout.Width(150));
        }
        GUILayout.EndHorizontal();

        //Create a room (fails if exist!)
        GUILayout.BeginHorizontal();
        GUILayout.Label("CREATE ROOM:", GUILayout.Width(150));
        roomName = GUILayout.TextField(roomName);
        if (connectedToMaster)
        {
            if (GUILayout.Button("GO"))
            {
                //SET PARAM FOR ROOM INFO
                ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
                h["Wave"] = 1;
                // PhotonNetwork.room.SetCustomProperties(h);
                string[] customPropertiesForLobby = new string[1];
                customPropertiesForLobby[0] = "Wave";

                PhotonNetwork.CreateRoom(roomName, new RoomOptions() { MaxPlayers = 4, CustomRoomProperties = h, CustomRoomPropertiesForLobby = customPropertiesForLobby }, TypedLobby.Default);

            }
        }
        else
        {
            GUILayout.Label("Connecting...", GUILayout.Width(150));
        }
        GUILayout.EndHorizontal();

        //Join random room
        GUILayout.BeginHorizontal();
        GUILayout.Label("JOIN RANDOM ROOM:", GUILayout.Width(150));
        if (PhotonNetwork.GetRoomList().Length == 0)
        {
            GUILayout.Label("..no games available...");
        }
        else
        {
            if (GUILayout.Button("GO"))
            {
                PhotonNetwork.JoinRandomRoom();
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(30);
        GUILayout.Label("ROOM LISTING:");
        if (PhotonNetwork.GetRoomList().Length == 0)
        {
            GUILayout.Label("..no games available..");
        }
        else
        {
            //Room listing: simply call GetRoomList: no need to fetch/poll whatever!
            scrollPos = GUILayout.BeginScrollView(scrollPos);
            foreach (RoomInfo game in PhotonNetwork.GetRoomList())
            {
               // Debug.Log("Player Count:" + game.playerCount + "/"+game.maxPlayers);
                GUILayout.BeginHorizontal();
                GUILayout.Label(game.name + " " + game.playerCount + "/" + game.maxPlayers);
                if (GUILayout.Button("JOIN"))
                {
                    PhotonNetwork.JoinRoom(game.name);
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndScrollView();
        }

        GUILayout.EndArea();
    }


    void ShowConnectingGUI()
    {
        GUILayout.BeginArea(new Rect((Screen.width - 400) / 2, (Screen.height - 300) / 2, 400, 300));

        GUILayout.Label("Connecting to Photon server.");
        GUILayout.Label("Hint: This demo uses a settings file and logs the server address to the console.");

        GUILayout.EndArea();
    }

    public bool connectedToMaster = false;
    public void OnConnectedToMaster()
    {
        connectedToMaster = true;
        // this method gets called by PUN, if "Auto Join Lobby" is off.
        // this demo needs to join the lobby, to show available rooms!
        PhotonNetwork.JoinLobby();  // this joins the "default" lobby
    }
}
