﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.Analytics;

public class NewInvasionCanvas : MonoBehaviour {
	public Image icon;


	public void SetImage(string invasionName){
		

		icon.sprite=GameManager.Instance.GetInvasionImage (invasionName);
		GameManager.Instance.InputEnabled=false;
	}

	public void ClickOk(){

		GameManager.Instance.InputEnabled=true;
        EventManager.Instance.CallInvasionFinished();
		AudioManager.Instance.PlayClip("click",true,false);
		gameObject.SetActive (false);


    }

}

