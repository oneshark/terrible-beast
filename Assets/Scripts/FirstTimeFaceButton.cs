﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FirstTimeFaceButton : MonoBehaviour
{
	public GameObject border;
	FirstTimeCanvas ftc;
	public Image icon;
	public int faceID;


	void Awake ()
	{
		ftc = GetComponentInParent<FirstTimeCanvas> ();
		//SetFace();
	}


	public void SetFace ()
	{
	Face thisFace = InventoryManager.Instance.GetFace(faceID);
	icon.sprite = thisFace.icon;
	}
	public void Clicked ()
	{
	ftc.SetSelectedFace(faceID);
		ToggleBorder (true);
		TurnOffSiblings ();//turn off sibs
	}

	public void ToggleBorder (bool b)
	{
		border.SetActive (b);
	}

	public void TurnOffSiblings ()
	{
		GameObject[] obj = GameObject.FindGameObjectsWithTag ("FirstTimeFaceButton");
		foreach (GameObject nextObj in obj) {
			if (this.gameObject == nextObj)
				continue;
			nextObj.GetComponent<FirstTimeFaceButton> ().ToggleBorder (false);
		}

	}
}
