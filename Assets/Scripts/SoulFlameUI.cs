﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulFlameUI : MonoBehaviour
{
    public GameObject idleObj, eatObj;
    bool isIdle = true;

    public void Eat()
    {
        isIdle = false;
        eatObj.SetActive(true);
        idleObj.SetActive(false);
        Invoke("Idle", .85f);
    }

    public void Idle()
    {
        if (isIdle)
        {
            return;
        }
        isIdle = true;
        eatObj.SetActive(false);
        idleObj.SetActive(true);
    }

    //void OnGUI()
    //{
    //    if (GUI.Button(new Rect(100, 0, 200, 30), "eat"))
    //    {
    //        Eat();
    //    }
    //}
}