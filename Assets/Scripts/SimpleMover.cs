﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SimpleMover : MonoBehaviour
{
    [HideInInspector]
    public bool hasFocus = true;
	public Image filledRollCircle;

    public float walkSpeedHorizontal = 1;
    public float walkSpeedVertical = 1;
	public float flySpeedVertical = 0;

    AdventurerTownNetwork atn;
    Rigidbody m_Body;
    PhotonView m_PhotonView;

    void Start()
    {
        atn = GetComponent<AdventurerTownNetwork>();
    }


    void Awake()
    {
        m_Body = GetComponent<Rigidbody>();
        m_PhotonView = GetComponent<PhotonView>();
    }

    void Update()
    {
        if (!m_PhotonView.isMine) return;
        //CHOOSE ANIMATION
        ChooseAnimation();
        //CHOOSE FACING
			currentFill += Time.deltaTime;
			FillImage();
    }


	float currentFill = 0;
	public void FillImage()
	{
		//turn off the fill if this is not our character
		if (!m_PhotonView.isMine)
		{
			filledRollCircle.fillAmount = 0;
		}

		float fillAmount = currentFill / RollCoolTime;
		if (currentFill >= 2.5f) fillAmount = 0;
		filledRollCircle.fillAmount = fillAmount;
	}

    void FixedUpdate()
    {
        if (m_PhotonView.isMine == false)
        {
            return;
        }

        bool canMove = true;
        if (Chat.Instance.chatInputCustom.isFocused) canMove = false;
        if (!GameManager.Instance.InputEnabled) canMove = false;

        if (canMove)
        {
			CallFacing();
			ListenKeys();
            UpdateMovement();

        }
    }

    void UpdateFacingDirection()
    {
        if (m_Body.velocity.x > 0.9f)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (m_Body.velocity.x < -0.9f)//.2
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    void UpdateMovement()
    {
        Vector3 movementVelocity = m_Body.velocity;

        if (Input.GetAxisRaw("Horizontal") > 0.5f)
        {
            movementVelocity.x = walkSpeedHorizontal;
        }
        else if (Input.GetAxisRaw("Horizontal") < -0.5f)
        {
            movementVelocity.x = -walkSpeedHorizontal;
        }
        else
        {
            movementVelocity.x = 0;
        }

        if (Input.GetAxisRaw("Vertical") > 0.5f)
        {
            movementVelocity.z = walkSpeedVertical;
			//convert depth to height
			if (atn.isFlying) {
				movementVelocity.z = 0;
				movementVelocity.y = flySpeedVertical;
			}
        }
        else if (Input.GetAxisRaw("Vertical") < -0.5f)
        {
            movementVelocity.z = -walkSpeedVertical;
			if (atn.isFlying) {
				movementVelocity.z = 0;
				movementVelocity.y = -flySpeedVertical;
			}
        }
        else
        {
            movementVelocity.z = 0;
        }

        //if we arent using WASD then check for mouse move
        if (movementVelocity.x == 0 & movementVelocity.z == 0)
        {
            Vector3 nextMotion = ScreenEdgeMovementDetect();
            movementVelocity.x = nextMotion.x;
            movementVelocity.z = nextMotion.z;
        }

		//IsRolling dont allow change of direction
		if (isRolling)
		{
			if (atn.facing == 0 & movementVelocity.x > 0)
				movementVelocity.x = 0;
			if (atn.facing == 1 & movementVelocity.x < 0)
				movementVelocity.x = 0;
		}

        float xPos = transform.position.x + (movementVelocity.x * walkSpeedHorizontal * .15f);
		//y is only for flyers
		float yPos = transform.position.y + (movementVelocity.y * flySpeedVertical * .15f);
        float zPos = transform.position.z + (movementVelocity.z * walkSpeedVertical * .15f);

        transform.position = new Vector3(xPos, transform.position.y, zPos);

		if (atn.isFlying) {
			transform.position = new Vector3(xPos,yPos, zPos);

		} else {
			transform.position = new Vector3(xPos, transform.position.y, zPos);
		}
       

        m_Body.velocity = movementVelocity;
    }

	void ListenKeys()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (Input.GetAxisRaw("Horizontal") == 0)
				return;
			StartRoll();
		}
	}

	public float rollForce = 200;
	bool isRollCool = true;
	public bool isRolling = false;
	public float RollCoolTime = 2;
	void StartRoll()
	{
		//COOLDOWN SO WE CANT REPEAT CALL ROLL
		if (!isRollCool)
			return;
		isRollCool = false;
		Invoke("RollCooldown", RollCoolTime);

		isRolling = true;
		currentFill = 0;

		//add a force to it, then lock motion
		if (atn.facing == 0)
		{
			atn.rb.AddForce(Vector3.left * rollForce, ForceMode.Force);
		}
		else
		{
			atn.rb.AddForce(Vector3.right * rollForce, ForceMode.Force);
		}
		AudioManager.Instance.PlayClip("roll", true, false);

		atn.adventurerAnimation.SetAnimationTown(atn.stance + "-Roll", true, false);
		//	walkSpeedHorizontal = walkSpeedHorizontal * 2;
	}

	void RollCooldown()
	{
		isRollCool = true;
	}

    public void ChooseAnimation ()
    //Walk or run
	{
		if (isRolling)
			return;
		if (!InventoryCanvas.Instance.equipmentOpen & !GameManager.Instance.InputEnabled) {
		return;
		}
			if (Mathf.Abs (m_Body.velocity.x) < .001f & Mathf.Abs (m_Body.velocity.z) < 0.001f) {
				atn.adventurerAnimation.SetAnimationTown (atn.stance + "-Idle", true, false);
			} else {
				atn.adventurerAnimation.SetAnimationTown (atn.stance + "-Run1", true, false);
			}
		//}
    }

    public void CallFacing()
    {
       //Debug.Log(m_Body.velocity.x);
        if (m_Body.velocity.x < -0.01f)
        {
            atn.SetFacing(0);
        }
        if (m_Body.velocity.x > 0.01f)
        {
            atn.SetFacing(1);
        }
    }


    void OnApplicationFocus(bool _hasFocus)
    {
        hasFocus = _hasFocus;
    }

    Vector2 ScreenEdgeMovementDetect()
    {
        if (!hasFocus) return new Vector3(0, 0);
        if (EventSystem.current.IsPointerOverGameObject())
        {
            //  Debug.Log("We are hovering over the UI of some kind!");
            return new Vector3(0, 0);
        }

        Vector2 motion = new Vector3(0, 0, 0);
        //WALK LEVELS
        //if (Input.mousePosition.y >= Screen.height * 0.9f)
        //{
        //    motion = new Vector2(0, .3f);
        //}
        //if (Input.mousePosition.y <= Screen.height * .1f)
        //{
        //    motion = new Vector2(0, -.3f);
        //}
        if (Input.mousePosition.x >= Screen.width * .9f)
        {
            motion = new Vector3(0.8f, 0);
        }
        if (Input.mousePosition.x <= Screen.width * .1f)
        {
            motion = new Vector3(-0.8f, 0);
        }

        //RUN LEVELS
        //if (Input.mousePosition.y >= Screen.height * 0.95f)
        //{
        //    motion = new Vector2(0, 1f);
        //}
        //if (Input.mousePosition.y <= Screen.height * .05f)
        //{
        //    motion = new Vector2(0, -1f);
        //}
        if (Input.mousePosition.x >= Screen.width * .95f)
        {
            motion = new Vector3(1f, 0);
        }
        if (Input.mousePosition.x <= Screen.width * .05f)
        {
            motion = new Vector3(-1f, 0);
        }
        return motion;
    }

}


