﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseFillAndPop : MonoBehaviour
{
    // AdventurerNetwork adventurerNetwork;
    public Color originalColor;
    public Image attackCoolImage;
    public Animator anim;
    public float cooldownMax, cooldown;
    public bool isCool;

    bool isHideCooldown = false;
    // Use this for initialization
    void Start()
    {
      //  adventurerNetwork = BattleManager.Instance.myAdventurer;
 //       anim = GetComponentInChildren<Animator>();
        //  SetMaterials();
    }

    void Update()
    {
        cooldown += Time.deltaTime; 
        if (cooldown > cooldownMax)
        {
            cooldown = cooldownMax;
           if (!isCool)anim.SetTrigger("Explosion");
            isCool = true;
            attackCoolImage.color = new Color(0, 0, 0, 0);
        }
        SetAttackCool(cooldown, cooldownMax);
    }

    public void StartCooldown(float _max)
    {
        attackCoolImage.color = new Color(255,255,255,255);

        isCool = false;
        cooldown = 0;
        cooldownMax = _max;
        Debug.Log("## StartCooldown:" + cooldown + "  cooldownmax:" + cooldownMax);

        SetAttackCool(cooldown, cooldownMax);
    }

    public void SetAttackCool(float atkCool, float atkCoolMax)
    {
     
        attackCoolImage.fillAmount = atkCool / atkCoolMax;
    }

    public Image redx;
    public void PopUpX()
    {
        redx.gameObject.SetActive(true);
        Invoke("PopUpOffX", .3f);
        AudioManager.Instance.PlayClip("reject", true, false);

    }
    
    void PopUpOffX()
    {
        redx.gameObject.SetActive(false);
    }
}
