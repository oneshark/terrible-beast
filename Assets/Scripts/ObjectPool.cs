﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour
{
    [Header("SUPPORTS LOCAL OBJ ONLY")]
    private static ObjectPool _instance;
    public static ObjectPool Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<ObjectPool>();
            return _instance;
        }
    }

    public List<GameObject> pool = new List<GameObject>();
    public List<PreloadedGameObject> preloadPool = new List<PreloadedGameObject>();

    void Start()
    {
        PreloadObjects();
    }

    public GameObject NextObject(string objName)
    {

        foreach (var instance in pool)
        {
            if (instance.activeSelf != true & instance.gameObject.name==objName)
            {
                if (instance.GetComponent<DisableDestroyOnTimer>())
                {
                    DisableDestroyOnTimer dd = instance.GetComponent<DisableDestroyOnTimer>();
                    dd.Init();
                }
                return instance;
            }
        }
        return CreateInstance(objName);
    }

    private GameObject CreateInstance(string nextObjectName)
    {
       // Debug.Log("nextObject:" + nextObjectName);
        GameObject newObj = (GameObject)Instantiate(Resources.Load(nextObjectName), new Vector3(0, 0, 0), Quaternion.identity);

        if (newObj.GetComponent<DisableDestroyOnTimer>())
        {
            DisableDestroyOnTimer dd = newObj.GetComponent<DisableDestroyOnTimer>();
            dd.Init();
        }
        return newObj;
    }

    public void PreloadObjects()
    {
        for (int inc = 0; inc < preloadPool.Count; inc++) {
            string nextObjectName = preloadPool[inc].path + preloadPool[inc].formalName;
           // GameObject newObj = PhotonNetwork.Instantiate(nextObjectName, new Vector3(0, 0, 0), Quaternion.identity, 0);
			Debug.Log("Next:"+nextObjectName);
            GameObject newObj = (GameObject)Instantiate(Resources.Load(nextObjectName), new Vector3(0,0,0), Quaternion.identity);
            newObj.transform.parent = this.transform;
            //give them perfect name
            newObj.name = preloadPool[inc].formalName;
            pool.Add(newObj);
            newObj.SetActive(false);
        }
    }

}

[System.Serializable]
public class PreloadedGameObject
{
    public string formalName;
    public string path;
}