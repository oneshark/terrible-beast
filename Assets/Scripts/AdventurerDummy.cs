﻿using UnityEngine;
using System.Collections;

public class AdventurerDummy : MonoBehaviour
{
    //network
    private bool isRemotePlayer = true;

    //general movement
    public bool isMoving = false;
    public float walkSpeedHorizontal, walkSpeedVertical;
    public int facing = 1;
    public GameObject graphics;
    PhotonView photonView;
    //  AdventurerAnimation advAnimation;
    void Start()
    {
        photonView = GetComponent<PhotonView>();
        if (photonView.isMine)
        {
            //MINE: local player, simply enable the local scripts
            Camera.main.transform.parent = transform;
            Camera.main.transform.localPosition = new Vector3(0, 8, -17);
            Camera.main.transform.localEulerAngles = new Vector3(15, 0, 0);
        }
        gameObject.name = gameObject.name + photonView.viewID;
    }

    void FixedUpdate()
    {
        if (isRemotePlayer) return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        float xPos = transform.position.x + (h * walkSpeedHorizontal);
        float zPos = transform.position.z + (v * walkSpeedVertical);


        transform.position = new Vector3(xPos, transform.position.y, zPos);

        if (h == 0 & v == 0) { isMoving = false; } else isMoving = true;
        //setRun(h, v);
    }

    public void SetIsRemotePlayer(bool val)
    {
        isRemotePlayer = val;
    }


}
