﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LoginGUI : MonoBehaviour
{
	public string VERSION_NUMBER = "0.4.0";
	public string CURRENT_SERVER_VERSION_NUMBER = "";
	//what version we must be on to play
	public bool isNewUser = false;
	public MainMenuCanvas mmc;
	AudioSource audioSource;
	public GameObject LoginObj;
	public GameObject RegisterObj;

	public string nameInput = "";
	public string passInput = "";
	public Text nameText;
    public Text serverResponseText;
	public InputField nameIF, passIF, passAgainIF;
	public GameObject nameX, passX, passAgainX;
	public string saveString = "";
	public Image checkMarkRememberLogin;

	public CanvasTools ct;

	private static LoginGUI _instance;

	public static LoginGUI Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<LoginGUI> ();
			return _instance;
		}
	}

	// Use this for initialization
	public AudioClip[] audioClips;
	EventSystem eventSystem;

	void Awake ()
	{
		ct = GetComponent<CanvasTools> ();
		audioSource = GetComponent<AudioSource> ();
		eventSystem = GameObject.FindGameObjectWithTag ("EventSystem").GetComponent<EventSystem> ();
		SetServerVersion ();

		//SET FOCUS TO NAME FIELD RIGHT AWAY
		//EventSystem.current.SetSelectedGameObject(nameIF.gameObject);
 
		//remember the login
		string savedLogin = PlayerPrefs.GetString("SAVED_USERNAME");
		string savedPassword = PlayerPrefs.GetString ("SAVED_PASSWORD");

		if (savedLogin != "" & savedLogin != null) {
			nameIF.text = savedLogin;
			passIF.text = savedPassword;
		}
//eventSystem.SetSelectedGameObject(nameIF.gameObject);
	}

	public void ToggleCheckMarkRememberLogin(){
		 saveMyLogin=!saveMyLogin;
		checkMarkRememberLogin.enabled = saveMyLogin;
	}

	//
	//	void Update ()
	//	{
	//		if (Input.GetKeyDown (KeyCode.Tab)) {
	//			if (nameIF.isFocused) {
	//				eventSystem.SetSelectedGameObject (passIF.gameObject, new BaseEventData (eventSystem));
	//				//SetSelectedGameObject (passAgainIF.gameObject);
	//			}
	//			if (passIF.isFocused) {
	//				eventSystem.SetSelectedGameObject (passAgainIF.gameObject, new BaseEventData (eventSystem));
	//				//SetSelectedGameObject (passAgainIF.gameObject);
	//			}
	//			if (passAgainIF.isFocused) {
	//				eventSystem.SetSelectedGameObject (nameIF.gameObject, new BaseEventData (eventSystem));
	//				//SetSelectedGameObject (passAgainIF.gameObject);
	//			}
	//		}
	//	}
	//
	//	void Update ()
	//	{
	//		if (Input.GetKeyDown (KeyCode.Tab)) {
	//			if (nameIF.isFocused) {
	//				eventSystem.SetSelectedGameObject (passIF.gameObject, null);
	//				passIF.OnPointerClick (null);
	//				return;
	//			}
	//			if (passIF.isFocused) {
	//				eventSystem.SetSelectedGameObject (passAgainIF.gameObject, null);
	//				passAgainIF.OnPointerClick (null);
	//				return;
	//			}
	//
	//
	//		}
	//	}

	//	void OnGUI ()
	//	{
	//		GUI.Label (new Rect (170, 0, 420, 100), "Name:");
	//		nameInput = GUI.TextArea (new Rect (230, 0, 120, 30), nameInput, 200);
	//
	//		GUI.Label (new Rect (170, 30, 420, 100), "Password:");
	//		passInput = GUI.TextArea (new Rect (230, 30, 120, 30), passInput, 200);
	//
	//		if (GUI.Button (new Rect (350, 0, 120, 30), "Check Player Name")) {
	//			DoesPlayerNameExist (null);
	//		}
	//	}

	void PopFillAllFields ()
	{
		audioSource.clip = audioClips [1];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Please Complete All", "Fields!");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}

	bool saveMyLogin=false;
	public void PopLoginSuccess ()
	{
		audioSource.clip = audioClips [0];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Logged In", "Successfully!");

		if (saveMyLogin) {
			PlayerPrefs.SetString ("SAVED_USERNAME", userName);
			PlayerPrefs.SetString ("SAVED_PASSWORD", passIF.text);
		}
		puc.SetAcceptIcon ();
		puc.SetTextColor ("white");
		Invoke ("PopStartFinal", 1.5f);
		ct.CallFadeCanvas (0);
		userName = nameText.text;

		mmc.BlackFade ();
	}


	public string userName = "";

	public void PopAccountSuccess ()
	{
		audioSource.clip = audioClips [0];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Account created", "Successfully!");
		puc.SetAcceptIcon ();
		puc.SetTextColor ("white");
		Invoke ("PopStartFinal", 1.5f);
		isNewUser = true;
		ct.CallFadeCanvas (0);
		userName = nameText.text;
		mmc.BlackFade ();

	}

	void PopStartFinal ()
	{
		isGuestLogin = false;

		mmc.TryClick ();
	}

	public void PopNameTaken ()
	{
		nameX.SetActive (true);
		audioSource.clip = audioClips [1];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Account Name", "Taken!");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}



	public void PopPasswordMismatch ()
	{
		passX.SetActive (true);
		passAgainX.SetActive (true);
		audioSource.clip = audioClips [1];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Passwords Must", "Match!");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}

	public void PopTooShort ()
	{
		passX.SetActive (true);
		passAgainX.SetActive (true);
		audioSource.clip = audioClips [1];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Password Too", "Short!");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}

	public void PopPasswordWrong ()
	{
		passX.SetActive (true);
		audioSource.clip = audioClips [1];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Login", "Failed!");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}

	public void PopPasswordShort ()
	{
		passX.SetActive (true);

		passAgainX.SetActive (true);
	
		audioSource.clip = audioClips [1];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Password Invalid", "Min (6 char)");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}

	void ResetXs ()
	{
		nameX.SetActive (false);
		passX.SetActive (false);
		passAgainX.SetActive (false);
	}

	public void Login ()
	{
		ResetXs ();

		if (nameText.text == "" || passIF.text == "") {
			PopFillAllFields ();
			return;
		}
		TryGetSave ();

	}

	public bool isGuestLogin = false;
	public void GuestLogin(){
		mmc.BlackFade ();

		ct.CallFadeCanvas (0);
		userName = "Guest";
		//Invoke ("PopLoginSuccess", 1);// ();
		isGuestLogin = true;
		Invoke("DelayedGuestLogin",.75f);
	}

	void DelayedGuestLogin(){
		mmc.TryClick ();
	}

	public void Register ()
	{
		ResetXs ();

		if (nameText.text == "" || passIF.text == "" || passAgainIF.text == "") {
			PopFillAllFields ();
			return;
		}

		if (passIF.text != "") {
			Char[] checkLength = passIF.text.ToCharArray ();
			if (checkLength.Length < 6) {
				PopTooShort ();
				return;
			}
		}
		TryCreateLogin ();
	}

	public void SetServerVersion ()
	{
		StartCoroutine (HandleWWWGetServerVersion ("http://tod.dx.am/Main/CurrentVersion.php"));
	}

	void EnableLogin ()
	{
		Debug.Log ("Up to date");
		ct.CallFadeCanvas (1);
	}

	void PopVersionConflict (string response)
	{
		audioSource.clip = audioClips [1];
		audioSource.Play ();

		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();

		string errormsgPart1 = "Version is too";
		string errormsgPart2 = "Old!";

		if (response.Contains ("<!DOCTYPE")) {
			errormsgPart1 = "Connection was";
			errormsgPart2 = "rejected.";
			puc.SetPopup (errormsgPart1, errormsgPart2, response);

		} else {
			puc.SetPopup (errormsgPart1, errormsgPart2);
		}

		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}


	IEnumerator HandleWWWGetServerVersion (string url)
	{
		WWW www = new WWW (url);
		yield return www;
		//Debug.Log ("LatestVersion:" + www.text);
		CURRENT_SERVER_VERSION_NUMBER = www.text;
        //Debug.Log ("www version number:" + CURRENT_SERVER_VERSION_NUMBER);
        serverResponseText.text = www.text;
		if (CURRENT_SERVER_VERSION_NUMBER == VERSION_NUMBER) {
			EnableLogin ();
		} else {
			PopVersionConflict (www.text);
		}
		
		//   JsonUtility.FromJson(www.text);
	}


	void TryGetSave ()
	{
		{
			//string encryptedSaveData = StringUtil.EncryptString ("excelent Save data", "Zipper1!");

			Dictionary<string, string> formPost = new Dictionary<string, string> ();
			//formPost.Add ("JsonData", userName);
			formPost.Add ("userName", nameText.text);
			string encryptedPass = StringUtil.EncryptString (passIF.text, "02Ksi#89j");
			Debug.Log ("1");
			formPost.Add ("password", encryptedPass);

			StartCoroutine (HandleWWWCheckPassword ("http://tod.dx.am/Main/CheckPassword.php", formPost, GetSave));
		}

	}

	public void GetSave ()
	{
		//string encryptedSaveData = StringUtil.EncryptString ("excelent Save data", "Zipper1!");

		Dictionary<string, string> formPost = new Dictionary<string, string> ();
		//formPost.Add ("JsonData", userName);
		formPost.Add ("userName", nameText.text);

		StartCoroutine (HandleWWWGetSave ("http://tod.dx.am/Main/GetSaveData.php", formPost));
	}

	IEnumerator HandleWWWGetSave (string url, Dictionary<string, string> post)
	{
		//Debug.Log("HandleWWWPOSTItems");
		WWWForm form = new WWWForm ();
	//	Debug.Log ("GetSave");
		foreach (KeyValuePair<String, String> post_arg in post) {
			form.AddField (post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW (url, form);
		yield return www;
		saveString = www.text;
		//Debug.Log ("GETsave success - www:" + www.text);
		ct.CallFadeCanvas (0);
		userName = nameIF.text;
		PopLoginSuccess ();

		//TURN OFF THE TAB SO IT WONT BREAK STUFF
		GetComponent<UIHotkeySelect>().enabled=false;

		//_GameSaveLoad.Instance.saveData.
		//EventManager.Instance.CallLoadComplete();
		//   JsonUtility.FromJson(www.text);
	}

	public void PostSave (bool includeMilestone, string mySaveData, string myClearSaveData)
	{
		Dictionary<string, string> formPost = new Dictionary<string, string> ();
		//formPost.Add ("JsonData", userName);
		formPost.Add ("userName", GameManager.Instance.UserName);
		formPost.Add ("playerName", GameManager.Instance.PlayerName);

		//Debug.Log ("Post Save and playerName:" + GameManager.Instance.PlayerName);
		//formPost.Add ("password", passInput);
		Debug.Log ("mySaveData Clear:" + myClearSaveData);
		formPost.Add ("saveData", mySaveData);
		formPost.Add ("clearData", myClearSaveData);

		//Call Post
		if (includeMilestone) {
			StartCoroutine (HandleWWWPOSTSave ("http://tod.dx.am/Main/SaveDataMilestone.php", formPost));

		} else {
			//Debug.Log ("NORMAL SAVE DATA");
			StartCoroutine (HandleWWWPOSTSave ("http://tod.dx.am/Main/SaveDataV2.php", formPost));
		}
	}

	IEnumerator HandleWWWPOSTSave (string url, Dictionary<string, string> post)
	{
		//Debug.Log("HandleWWWPOSTItems");
		WWWForm form = new WWWForm ();

		foreach (KeyValuePair<String, String> post_arg in post) {
			form.AddField (post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW (url, form);
		yield return www;

		//GameObject registeredItemCanvas = (GameObject)Instantiate (Resources.Load ("ItemRegisteredCanvas"));
		//Debug.Log ("POST Save success - www:" + www.text);

		//   JsonUtility.FromJson(www.text);
	}





	void TryCreateLogin ()
	{
		if (passIF.text != passAgainIF.text) {
			Debug.Log ("Passwords dont match!");
			PopPasswordMismatch ();
			return;
		}

		Debug.Log ("TRY CREATE LOGIN");

		DoesUserExist (CreateLogin);
	}

	public void DoesUserExist (System.Action followingMethod)
	{
		Debug.Log ("DOES USER EXIST");

		Dictionary<string, string> formPost = new Dictionary<string, string> ();
		formPost.Add ("userName", nameText.text);
		StartCoroutine (HandleWWWGetDoesUserExist ("http://tod.dx.am/Main/DoesUserExist.php", formPost, followingMethod));
	}

	bool userExists = true;

	IEnumerator HandleWWWGetDoesUserExist (string url, Dictionary<string, string> post, System.Action followingMethod)
	{
		Debug.Log ("HANDLE DOES USER EXIST");

		userExists = true;//this tells us if this user is in the db
		WWWForm form = new WWWForm ();
		foreach (KeyValuePair<String, String> post_arg in post) {
			form.AddField (post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW (url, form);
		yield return www;

		//GameObject registeredItemCanvas = (GameObject)Instantiate (Resources.Load ("ItemRegisteredCanvas"));
		Debug.Log ("GetUserExist - www:" + www.text);
		if (www.text == "0") {
			userExists = false;
		} else if (www.text == "1") {
			userExists = true;
		} 


		if (userExists) {
			PopNameTaken ();
		} else {
			if (followingMethod != null)
				followingMethod ();
		}
		//   JsonUtility.FromJson(www.text);
	}



	public void CreateLogin ()
	{
		//test of the save data
		Debug.Log ("CREATE LOGIN");
		Dictionary<string, string> formPost = new Dictionary<string, string> ();
		//formPost.Add ("JsonData", userName);
		formPost.Add ("userName", nameText.text);
		string encryptedPass = StringUtil.EncryptString (passIF.text, "02Ksi#89j");

		formPost.Add ("password", encryptedPass);

		//Call Post
		StartCoroutine (HandleWWWPOSTCreateAccount ("http://tod.dx.am/Main/CreateLogin.php", formPost));
	}

	IEnumerator HandleWWWPOSTCreateAccount (string url, Dictionary<string, string> post)
	{
		//Debug.Log("HandleWWWPOSTItems");
		WWWForm form = new WWWForm ();

		foreach (KeyValuePair<String, String> post_arg in post) {
			form.AddField (post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW (url, form);
		yield return www;
		PopAccountSuccess ();
		//GameObject registeredItemCanvas = (GameObject)Instantiate (Resources.Load ("ItemRegisteredCanvas"));
		Debug.Log ("POSTCreateAccount success - www:" + www.text);
		//   JsonUtility.FromJson(www.text);
	}

	IEnumerator HandleWWWCheckPassword (string url, Dictionary<string, string> post, System.Action followingMethod)
	{
		bool passwordCorrect = true;//this tells us if this user is in the db
		WWWForm form = new WWWForm ();
		foreach (KeyValuePair<String, String> post_arg in post) {
			form.AddField (post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW (url, form);
		yield return www;

		if (www.text == "0") {
			passwordCorrect = false;
		}


		if (!passwordCorrect) {
			Debug.Log ("password not right.");
			PopPasswordWrong ();
		} else {
			if (followingMethod != null)
				followingMethod ();
		}
		//   JsonUtility.FromJson(www.text);
	}


	public void ToLogin ()
	{
		ResetXs ();


		RegisterObj.SetActive (false);
		LoginObj.SetActive (true);
	}

	public void ToRegister ()
	{
		ResetXs ();


		RegisterObj.SetActive (true);
		LoginObj.SetActive (false);
	}


}

[System.Serializable]
public class LoginData
{
	public string userName;
	public string password;
	public string data;
}
