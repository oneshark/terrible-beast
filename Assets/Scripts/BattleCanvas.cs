﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleCanvas : MonoBehaviour
{
	public bool m_matchStarted = false;

	public GameObject Guillotine;
	public Guillotine guillotineScript;
	public Scrollbar scrollbar;

	public PhotonView photonView;
	int myPositionNumber = 0;

	public PlayerInfoBar pib0, pib1, pib2, pib3;
	public GameObject pib0Obj, pib1Obj, pib2Obj, pib3Obj;

	public Text waveLevelText;
	public Image youAreDead;
	public Image youAreSoulbound;
	public Text soulBoundText;

	//SYNCHEDP
	public float player0Hp, player0HpMax, player0Xp, player0XpMax, player0Lvl;
	public string player0Name;
	public float player1Hp, player1HpMax, player1Xp, player1XpMax, player1Lvl;
	public string player1Name;
	public float player2Hp, player2HpMax, player2Xp, player2XpMax, player2Lvl;
	public string player2Name;
	public float player3Hp, player3HpMax, player3Xp, player3XpMax, player3Lvl;
	public string player3Name;

	public List<GameObject> ObjectsToHideOnGuillotine = new List<GameObject> ();
	public GameObject RunButton;
	public CanvasGroup cg;
	private static BattleCanvas _instance;

	public static BattleCanvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<BattleCanvas> ();
			return _instance;
		}
	}

	void Awake ()
	{
		photonView = GetComponent<PhotonView> ();
		pib0Obj = pib0.gameObject;
		pib1Obj = pib1.gameObject;
		pib2Obj = pib2.gameObject;
		pib3Obj = pib3.gameObject;

		pib0Obj.SetActive (false);
		pib1Obj.SetActive (false);
		pib2Obj.SetActive (false);
		pib3Obj.SetActive (false);
		//leaveRoomCooled = true;
		// Invoke("SetPositionNumber", 1);
	}

	void OnJoinedRoom ()
	{
		Guillotine.SetActive (true);
		Invoke("ActivateRunButton",2);
		Invoke ("SetPositionNumber", 2.25f);
		leaveRoomCooled = true;
		//Catch the ddead/win bug
		if (youAreDead.enabled)
			ToggleYouAreDead (false);

	}

	void ActivateRunButton(){
		RunButton.SetActive(true);
	}

	void SetPositionNumber ()
	{
		AdventurerNetwork myAdventurer = BattleManager.Instance.myAdventurer;
		if (myAdventurer == null) {// keep trying until we capture this
			Invoke ("SetPositionNumber", 1);
		} else {
			myPositionNumber = myAdventurer.playerPosition;
		}
	}

	void Update ()
	{
		SetPlayerInfoGUI ();
		//auto open and close
	
	}

    bool isStartInvasionButtonCool = true;
    public GameObject startInvasionButton;
	public void SetStartInvasionButton ()
	{
        if (!isStartInvasionButtonCool) return;
		startInvasionButton.SetActive (false);
		Invoke ("DelayedInvasionButton", 4.5f);
        isStartInvasionButtonCool = false;
	}

	public void DelayedInvasionButton ()
	{
        isStartInvasionButtonCool = true;
        Debug.Log ("DelayedInvasionButton");
		if (PhotonNetwork.isMasterClient) {
			startInvasionButton.SetActive (true);
			m_matchStarted = false;
            EventManager.Instance.CallGuillotineDropped();
        }
        guillotineScript.CallToggleMe (true);
	}

	public void StartInvasionClicked ()
	{
		BattleManager.Instance.photonView.RPC ("ToggleJoinableMaster", PhotonTargets.MasterClient, false);
		BattleManager.Instance.photonView.RPC ("ToggleLockedMaster", PhotonTargets.MasterClient, true);

		m_matchStarted = true;
		startInvasionButton.SetActive (false);
		guillotineScript.CallSetAnimation ("Action", false);
		AudioManager.Instance.PlayClip ("click", true, false);

		AudioManager.Instance.PlayClip ("guillotine", false, false);

		StartCoroutine (DelayedGuillotineToggle ());
	}

	IEnumerator DelayedGuillotineToggle ()
	{
		yield return new WaitForSeconds (2);
		guillotineScript.CallSetAnimation ("Idle", true);

		ToggleGuillotineObjects (true);
		//guillotineScript.CallToggleMe (false);
		PlayerCanvas.Instance.ToggleMe (true);
		photonView.RPC ("TurnOnBattleMenus", PhotonTargets.All);
		BattleManager.Instance.photonView.RPC ("ToggleJoinableMaster", PhotonTargets.MasterClient, true);

	}

	[PunRPC]
	public void TurnOnBattleMenus ()
	{
		ToggleGuillotineObjects (true);
		guillotineScript.CallToggleMe (false);
		PlayerCanvas.Instance.ToggleMe (true);
	}

	

	void SetPlayerInfoGUI ()
	{
		if (BattleManager.Instance.playerPositions [0] == 1 & myPositionNumber != 0) {
			if (pib0Obj.activeSelf == false)
				pib0Obj.SetActive (true);
			//player
			pib0.SetText (player0Name, (int)player0Lvl);
			pib0.SetHPFill (player0Hp, player0HpMax);
			pib0.SetXPFill (player0Xp, player0XpMax);
		} else {
			pib0Obj.SetActive (false);
		}
		if (BattleManager.Instance.playerPositions [1] == 1 & myPositionNumber != 1) {
			if (pib1Obj.activeSelf == false)
				pib1Obj.SetActive (true);
			//player
			pib1.SetText (player1Name, (int)player1Lvl);
			pib1.SetHPFill (player1Hp, player1HpMax);
			pib1.SetXPFill (player1Xp, player1XpMax);
		} else {
			pib1Obj.SetActive (false);
		}
		if (BattleManager.Instance.playerPositions [2] == 1 & myPositionNumber != 2) {
			if (pib2Obj.activeSelf == false)
				pib2Obj.SetActive (true);
			//player
			pib2.SetText (player2Name, (int)player2Lvl);
			pib2.SetHPFill (player2Hp, player2HpMax);
			pib2.SetXPFill (player2Xp, player2XpMax);
		} else {
			pib2Obj.SetActive (false);
		}
		if (BattleManager.Instance.playerPositions [3] == 1 & myPositionNumber != 3) {
			if (pib3Obj.activeSelf == false)
				pib3Obj.SetActive (true);
			//player
			pib3.SetText (player3Name, (int)player3Lvl);
			pib3.SetHPFill (player3Hp, player3HpMax);
			pib3.SetXPFill (player3Xp, player3XpMax);
		} else {
			pib3Obj.SetActive (false);
		}
	}

	public void ToggleYouAreDead (bool b)
	{
		youAreDead.enabled = b;
		BattleManager.Instance.myAdventurer.CallGray (b);
	}

	public void ToggleYouAreSoulBound (bool b)
	{
		youAreSoulbound.enabled = b;
		soulBoundText.enabled = b;
		soulBoundText.text = "Wait for other players to reach wave->" + GetNextWaveMultiple () + 1 + " to spawn";
		CameraEffects ce = Camera.main.GetComponent<CameraEffects> ();
		ce.ToggleBlur (b);
	}

	int GetNextWaveMultiple ()
	{
		int currWave = BattleManager.Instance.waveIndex + 1;
		// Debug.Log("CurrentWave divis:" + currWave);
		for (int inc = currWave; inc < 500; inc++) {
			//  Debug.Log("inc tried:" + inc);
			if (Utilities.IsDivisble (inc, 5))
				return inc;
		}
		return 0;
	}

	[PunRPC]
	public void SetPlayerInfoMaster (params object[] args)
	{
		//Update the master host with the player's new stats
		//from there it is synchronized
		Debug.Log ("CALLED SET PLAYER INFO MASTER");
		int viewId = (int)args [0];
		int playerPositionNumber = (int)args [1];
		float playerHp = (float)args [2];
		float playerHpMax = (float)args [3];
		float playerXp = (float)args [4];
		float playerXpMax = (float)args [5];
		float playerLvl = (float)args [6];
		string playerName = (string)args [7];

		if (playerPositionNumber == 0) {
			player0Hp = playerHp;
			player0HpMax = playerHpMax;
			player0Xp = playerXp;
			player0XpMax = playerXpMax;
			player0Lvl = playerLvl;
			player0Name = playerName;
		} else if (playerPositionNumber == 1) {
			player1Hp = playerHp;
			player1HpMax = playerHpMax;
			player1Xp = playerXp;
			player1XpMax = playerHpMax;
			player1Lvl = playerLvl;
			player1Name = playerName;// GameManager.Instance.PlayerName;
		} else if (playerPositionNumber == 2) {
			player2Hp = playerHp;
			player2HpMax = playerHpMax;
			player2Xp = playerXp;
			player2XpMax = playerHpMax;
			player2Lvl = playerLvl;
			player2Name = playerName;//GameManager.Instance.PlayerName;
		} else if (playerPositionNumber == 3) {
			player3Hp = playerHp;
			player3HpMax = playerHpMax;
			player3Xp = playerXp;
			player3XpMax = playerHpMax;
			player3Lvl = playerLvl;
			player3Name = playerName;//GameManager.Instance.PlayerName;
		}
		AdventurerNetwork myAdventurer = BattleManager.Instance.myAdventurer;
		if (playerPositionNumber == myAdventurer.playerPosition) {
			//        Debug.Log("This is my player");
			//your own info bar should be hidden
		}
		//when its set correct on master, the serialzieview takes care of the rest of the synch

	}

	public void CallChangeMyStanceByString (string stance)
	{
		if (stance == "Onehander") {
			BattleManager.Instance.ChangeMyStance (Stance.Onehander);
		} else if (stance == "Dualwield") {
			BattleManager.Instance.ChangeMyStance (Stance.Dualwield);
		} else if (stance == "Bow") {
			BattleManager.Instance.ChangeMyStance (Stance.Bow);
		} else if (stance == "Twohander") {
			BattleManager.Instance.ChangeMyStance (Stance.Twohander);
		} else if (stance == "Cannon") {
			BattleManager.Instance.ChangeMyStance (Stance.Cannon);
		} else if (stance == "Rod") {
			BattleManager.Instance.ChangeMyStance (Stance.Rod);
		}
	}

	public void LeaveRoom (bool playChickenSound)
	{
	bool isGrave=false;
	if (BattleManager.Instance.myAdventurer.charState==CharacterState.dead)isGrave=true;
		BattleManager.Instance.photonView.RPC ("SpawnGrave", PhotonTargets.All, isGrave, BattleManager.Instance.myAdventurer.playerPosition);

		LoadingManager.Instance.OpenPanel ("Town");
		LoadingManager.Instance.CallFadeCanvas (1);

		GameManager.Instance.statPoints = 0;

		if (!leaveRoomCooled) {
			// Debug.Log("BattleCanvas leave button not cooled");
			return;
		}
		StartCoroutine (LeaveRoomCooldown ());

		//CLEAR OUR SPOT OUT AND UPDATE THE MASTER HOST
		// object[] args = new object[] { BattleManager.Instance.myAdventurer.playerPosition, 0, GameManager.Instance.PlayerName };
		// BattleManager.Instance.photonView.RPC("SetPositionMaster", PhotonTargets.MasterClient, args);
		//SetPlayerInfoGUI();

		GameManager.Instance.NextTownSpawn = "PlayerSpawnLobby";
		if (playChickenSound) {
			AudioManager.Instance.PlayClip ("leave", true, false);
		}

		ToggleMe (false);
		PlayerCanvas.Instance.ToggleMe (false);
		Invoke ("LeaveRoomDelayed", 2.1f);
	}

	bool leaveRoomCooled = true;

	IEnumerator LeaveRoomCooldown ()
	{
		leaveRoomCooled = false;
		yield return new WaitForSeconds (5);
		leaveRoomCooled = true;
	}

	public void LeaveRoomDelayed ()
	{
		PhotonNetwork.LeaveRoom ();
		GameManager.Instance.joinRandomTown = true;
		//gotta do this before ActivateDeactivate();
		BattleManager.Instance.mapWhenAllHopeIsLost1.SetActive (false);
		BattleManager.Instance.mapWhenAllHopeIsLost2.SetActive (false);
		BattleManager.Instance.mapWhenAllHopeIsLost3.SetActive (false);
		BattleManager.Instance.mapWhenAllHopeIsLost4.SetActive (false);
        BattleManager.Instance.mapRuins1.SetActive(false);
        BattleManager.Instance.mapRuins2.SetActive(false);
        BattleManager.Instance.mapRuins3.SetActive(false);
		BattleManager.Instance.mapRuins4.SetActive(false);

        GameManager.Instance.ActivateDeactivate ("Town");
	}

	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			//We own this player: send the others our data
			stream.SendNext (player0Hp);
			stream.SendNext (player0HpMax);
			stream.SendNext (player0Xp);
			stream.SendNext (player0XpMax);
			stream.SendNext (player0Lvl);
			stream.SendNext (player0Name);

			stream.SendNext (player1Hp);
			stream.SendNext (player1HpMax);
			stream.SendNext (player1Xp);
			stream.SendNext (player1XpMax);
			stream.SendNext (player1Lvl);
			stream.SendNext (player1Name);

			stream.SendNext (player2Hp);
			stream.SendNext (player2HpMax);
			stream.SendNext (player2Xp);
			stream.SendNext (player2XpMax);
			stream.SendNext (player2Lvl);
			stream.SendNext (player2Name);

			stream.SendNext (player3Hp);
			stream.SendNext (player3HpMax);
			stream.SendNext (player3Xp);
			stream.SendNext (player3XpMax);
			stream.SendNext (player3Lvl);
			stream.SendNext (player3Name);

			stream.SendNext (m_matchStarted);

		} else {
			//Network player, receive data
			player0Hp = (float)stream.ReceiveNext ();
			player0HpMax = (float)stream.ReceiveNext ();
			player0Xp = (float)stream.ReceiveNext ();
			player0XpMax = (float)stream.ReceiveNext ();
			player0Lvl = (float)stream.ReceiveNext ();
			player0Name = (string)stream.ReceiveNext ();

			player1Hp = (float)stream.ReceiveNext ();
			player1HpMax = (float)stream.ReceiveNext ();
			player1Xp = (float)stream.ReceiveNext ();
			player1XpMax = (float)stream.ReceiveNext ();
			player1Lvl = (float)stream.ReceiveNext ();
			player1Name = (string)stream.ReceiveNext ();

			player2Hp = (float)stream.ReceiveNext ();
			player2HpMax = (float)stream.ReceiveNext ();
			player2Xp = (float)stream.ReceiveNext ();
			player2XpMax = (float)stream.ReceiveNext ();
			player2Lvl = (float)stream.ReceiveNext ();
			player2Name = (string)stream.ReceiveNext ();

			player3Hp = (float)stream.ReceiveNext ();
			player3HpMax = (float)stream.ReceiveNext ();
			player3Xp = (float)stream.ReceiveNext ();
			player3XpMax = (float)stream.ReceiveNext ();
			player3Lvl = (float)stream.ReceiveNext ();
			player3Name = (string)stream.ReceiveNext ();

			m_matchStarted = (bool)stream.ReceiveNext ();


		}
	}



	bool battleCanvasOpen = true;

	public void ToggleMe (bool b)
	{
		if (b == battleCanvasOpen)
			return;
		if (b) {
			cg.alpha = 1;
			cg.interactable = true;
			cg.blocksRaycasts = true;
		} else {
			cg.alpha = 0;
			cg.interactable = false;
			cg.blocksRaycasts = false;
		}
		battleCanvasOpen = b;
	}

	public void ToggleGuillotineObjects (bool b)
	{
		foreach (GameObject obj in ObjectsToHideOnGuillotine) {
			obj.SetActive (b);
		}
	//	BattleCanvas.Instance.ToggleMe(true);
	}
}
