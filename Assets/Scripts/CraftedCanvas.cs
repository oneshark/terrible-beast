﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftedCanvas : MonoBehaviour
{
	public Text itemName;
	public Image icon;

	public void SetItem (Item item)
	{
		itemName.text = item.name;
		icon.sprite = item.icon;
	}
}
