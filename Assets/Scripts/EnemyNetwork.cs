﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnemyNetwork : Photon.MonoBehaviour
{
    [Header("AI options")]
    public float hitRange = 3;
    public bool fightAtADistance = false;
    public float walkSpeed = 5;
    public bool isBreakableObject = false;
    public bool isExplosive = false;
    public float explosiveDamage = 500;
    public bool isHelperEnemy = false;
    public bool includeBackPedal = false;
    public bool isFairy = false;

    [Header("only active if including backpedal...")]
    public float backpedalPercentChance = 1;
    public float backPedalSpeed = 1.5f;

    //[Header("AI Tracking (logans)")]
    public bool isBackPedaling = false;

    [Header("AOE Skills")]
    public float skillAOEPercent = 1.5f;
    public float skillAOERange = 4;
    public string skillAOEAnimationName = "Attack4";
    public bool useSkillAOE = false;
    public Animator m_AOEAnimator;
    public GameObject rootCollider;
    public string skillAOEOnHitParticleName = "None";
    public string deathParticleName = "None";


    [Header("- Soul / Item Drops -")]
    public float bigSoulMin;
    public float bigSoulMax, bigSoulValue;
    public float medSoulMin, medSoulMax, medSoulValue;
    public float smallSoulMin, smallSoulMax, smallSoulValue;
    public List<ItemDrops> itemDrops = new List<ItemDrops>();
    public Vector3 targetOffset;//where enemy likes to stand
    [Header("- XP & Item TO GIVE -")]
    public float expGiven = 1;

    public int lastAttackerID = 0;

    public Rigidbody2D rb;
    public AdventurerNetwork lastAttacker;
    public AdventurerNetwork currentTarget;
    public GameObject fairyTarget;
    public string deathType = "Splash";

    public EnemyType enemyType = EnemyType.normal;
    [Header("- Customization -")]
    public bool isAOE = false;


    [Header("- Stats -")]
    public Stats baseEntityStat;
    [Header("- Runtime stats -")]
    public Stats currentEntityStat;
    public LifebarCanvas lifebarCanvas;
    PhotonView photonView;
    public EnemyAnimation enemyAnimation;
    private bool appliedInitialUpdate;
    public float attackTimer = 99;

    public GameObject rootPosition;
    public string enemyAttackSound;
    public string enemyDeathSound;
    public string enemyEntranceSound;
    [Range(0, 2)]
    public float maxSize = 1;
    [Range(0, 2)]
    public float minSize = 1;
    SkeletonUtility su;

    private void Awake()
    {
        if (GameManager.Instance.currentScene != CustomSceneType.Battle)
            Destroy(gameObject);
        su = GetComponent<SkeletonUtility>();
        TurnOnCollider();
        if (enemyEntranceSound != "")
            EntranceSound();

        targetOffset = new Vector3(Random.Range(-1.25f, 1.25f), Random.Range(-1.25f, 1.25f), 0);

        if (isFairy)
        {
            //	gameObject.tag = "Fairy";
            InvokeRepeating("PickNewTarget", 4f, 4f);
        }

        //randomizebackpedal
        if (includeBackPedal)
        {
            InvokeRepeating("CheckBackPedal", 1.5f, .25f);

            float percChance = Random.Range(0, 1f);
            if (backpedalPercentChance < percChance)
                includeBackPedal = false;
        }
    }

    void EntranceSound()
    {
        AudioManager.Instance.PlayClip(enemyEntranceSound, false, false);
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //Debug.Log("Enemy Start Called");
        photonView = GetComponent<PhotonView>();
        //try disabling this one
        if (!photonView.isMine)
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
            SynchronizeHealth();
        }
    }

    //WAY POINT IS FOR WHEN NOT PURSUING ENEMY, set a waypoitn and go to it
    public Vector3 wayPoint = new Vector2(0, 0);

    private void FixedUpdate()
    {
        if (isBreakableObject)
            return;

        if (!PhotonNetwork.isMasterClient)
        {
            setFacing(facing);
            return;
        }

        if (!isDying) setFacing();

        attackTimer -= Time.deltaTime;

        //STATE MACHINE
        if (isBackPedaling & !isDying)
        {
            BackPedal();
        }
        else
        {
            StateRunAttack();
        }
    }


    void BackPedal()
    {
        //  Debug.Log("Backpedalling!");
        float targX = 2000;
        if (currentTarget.transform.position.x > transform.position.x)
            targX = -2000;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(targX, transform.position.y, transform.position.z) + targetOffset, backPedalSpeed * Time.deltaTime);

        if (enemyAnimation.currentAnimation != "Run2")
            enemyAnimation.SetAnimation("Run2", true, false);
    }

    IEnumerator StartBackPedal()
    {
        isBackPedaling = true;
        backPedalCool = false;
        Invoke("BackPedalCooldown", 5);
        yield return new WaitForSeconds(1f);//BackPedal Duration
        //Debug.Log("BackPedalFalse");
        isBackPedaling = false;
        if (!isDying)
        {
            enemyAnimation.SetAnimation("Idle", true, false);
        }

    }

    bool backPedalCool = true;
    void BackPedalCooldown()
    {
        backPedalCool = true;
    }

    void StateRunAttack()
    {
        if (!CanReachTarget() & !isDying & !enemyAnimation.TrueAnimation().Contains("Attack"))
        {
            MoveToTarget();
        }
        else
        {

            if (enemyAnimation.currentAnimation == "Run")
            {
                enemyAnimation.SetAnimation("Idle", true, false);
                attackTimer = 0;
            }

            if (attackTimer <= 0)
            {
                TryAttack(false);
            }
        }

        //Reset trial attack
        if (attackTimer <= 0)
        {
            float percAttackTimer = Random.Range(-currentEntityStat.atkCooldown * .4f, currentEntityStat.atkCooldown * .4f);
            attackTimer = currentEntityStat.atkCooldown + percAttackTimer;// * Random.Range(-currentEntityStat.atkCooldown, currentEntityStat.atkCooldown);
        }
    }

    void MoveToTarget()
    {
        if (isFairy)
        {
            transform.position = Vector3.MoveTowards(transform.position, fairyTarget.transform.position + targetOffset, walkSpeed * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, currentTarget.transform.position + targetOffset, walkSpeed * Time.deltaTime);
        }

        if (enemyAnimation.currentAnimation != "Run")
            enemyAnimation.SetAnimation("Run", true, false);
    }


    void setFacing()
    {
        if (isBreakableObject)
            return;

        //Bypass all of this for a fairy type
        if (isFairy)
        {
            if (fairyTarget == null)
            {
                Debug.Log("Pick New Target 1");
                PickNewTarget();
            }

            //Target is to the left and we arent facing left
            if (fairyTarget.transform.position.x < transform.position.x & facing != 0)
                photonView.RPC("setFacing", PhotonTargets.All, 0);

            //Target is to the right and we aren't facing right
            if (fairyTarget.transform.position.x > transform.position.x & facing != 1)
                photonView.RPC("setFacing", PhotonTargets.All, 1);
            return;
        }

        if (currentTarget == null)
        {
            Debug.Log("Fuck1");
            PickNewTarget();
        }
        //Target is to the left and we arent facing left
        if (currentTarget.transform.position.x < transform.position.x & facing != 0)
            photonView.RPC("setFacing", PhotonTargets.All, 0);

        //Target is to the right and we aren't facing right
        if (currentTarget.transform.position.x > transform.position.x & facing != 1)
            photonView.RPC("setFacing", PhotonTargets.All, 1);
    }



    public int facing = 1;

    [PunRPC]
    public void setFacing(int i)
    {
        // if y == -1 then it is facing right
        bool isFlip = false;
        if (facing != i)
            isFlip = true;
        if (isFlip)
        {
            facing = i;
            //transform.Rotate (Vector3.up * 180);
            //lifebarCanvas.transform.Rotate (Vector3.up * 180);
        }


        //////////////////// TEST FIX /////////////////////////////
        if (facing == 0 & transform.rotation.y == 0)
        {
            transform.rotation = Quaternion.identity;
            transform.Rotate(Vector3.up * 180);
            lifebarCanvas.transform.Rotate(Vector3.up * 180);
        }

        if (facing == 1 & transform.rotation.y != 0)
        {
            transform.rotation = Quaternion.identity;
            lifebarCanvas.transform.rotation = Quaternion.identity;
        }
    }

	[PunRPC]
	public void CallPushBack(float amount, int pushDirection){
        if (isBreakableObject) return;
        if (isExplosive) return;
		if (pushDirection == 1)
		{
			rb.AddForce(Vector2.right * (amount), ForceMode2D.Force);
		}
		else
		{
			rb.AddForce(Vector2.left * (amount), ForceMode2D.Force);
		}
		Invoke ("KillVel", .2f);
	}

	void KillVel(){
		rb.velocity = new Vector2 (0, 0);
	}

    bool CanReachTarget()
    {
        if (!isFairy)
        {
            if (currentTarget == null)
            {
                PickNewTarget();
                return false;
            }
        }
        else
        {
            if (fairyTarget == null)
            {
                Debug.Log("Pick New Target  2");

                PickNewTarget();
                return false;
            }
            float fairyDist = Vector2.Distance(fairyTarget.transform.position, transform.position);
            return (fairyDist < hitRange);

        }

        float dist = Vector2.Distance(currentTarget.transform.position, transform.position);

        return (dist < hitRange);
    }

    void CheckBackPedal()
    {
        if (currentTarget == null)
        {
            PickNewTarget();
            return;
        }

        float dist = Vector2.Distance(currentTarget.transform.position, transform.position);

        //SHOULD WE RUN AWAY? BACKPEDAL
        if (includeBackPedal & !isBackPedaling)
        {
            //if we are closer than x% of our hitRange = BACKPEDAL
            if (dist <= hitRange * .8f)
            {
                //Debug.Log ("Too Close to Player - StartBackPedal");
                if (backPedalCool & !isDying) StartCoroutine("StartBackPedal");
            }
        }
    }

    [PunRPC]
    public void SetNetworkActive(params object[] args)
    {
        if (photonView == null)
            photonView = GetComponent<PhotonView>();
        int viewID = (int)args[0];
        bool isActive = (bool)args[1];
        gameObject.SetActive(isActive);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.gameObject.tag == "Enemy")
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), col.collider, true);
        }
        if (col.collider.gameObject.tag == "Adventurer")
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), col.collider, true);
        }
    }

    public void TryAttack(bool force)
    {


        if (!CanReachTarget() || isFairy)
        {
            return;
        }



        bool pickNewTarget = false;


        if (currentTarget == null)
        {
            pickNewTarget = true;
        }
        else
        {
            if (currentTarget.charState == CharacterState.dead)
                pickNewTarget = true;

            if (Random.Range(0, 2) == 1)
                pickNewTarget = true;
        }

        if (pickNewTarget)
        {
            PickNewTarget();
        }

        if (currentTarget != null & !isFairy)
        {
            Attack(force);
        }
    }


    void PickNewTarget()
    {
        if (isFairy)
        {
            //create a target
            GameObject newTarg = new GameObject();
            newTarg.transform.position = BattleManager.Instance.myAdventurer.gameObject.transform.position;
            newTarg.transform.position = new Vector3(Random.Range(newTarg.transform.position.x - 25f, newTarg.transform.position.x + 25f), Random.Range(newTarg.transform.position.y - 25f, newTarg.transform.position.y + 25f), 0);
            currentTarget = null;
            fairyTarget = newTarg;
            return;
        }

        List<AdventurerNetwork> adventurers = BattleManager.Instance.GetAllLivingAdventurers();

        bool preventRepeatTarget = false;
        if (currentTarget) preventRepeatTarget = true;
        if (adventurers.Count > 0)
        {
            //someone is within range
            int indexChosen = Random.Range(0, adventurers.Count);
            //Debug.Log("IndexChosen:"+indexChosen);
            if (preventRepeatTarget & adventurers.Count > 1 & currentTarget == adventurers[indexChosen])
            {
                indexChosen = Random.Range(0, adventurers.Count);//roll one more time to try and avoid same player
            }

            if (preventRepeatTarget & adventurers.Count > 1 & currentTarget == adventurers[indexChosen])
            {
                indexChosen = Random.Range(0, adventurers.Count);//roll 2nd time to try and avoid same player
            }


            currentTarget = adventurers[indexChosen];
            //photonView.RPC ("BroadcastTarget", PhotonTargets.Others, photonView.viewID, currentTarget.photonView.viewID);
            photonView.RPC("setFacing", PhotonTargets.All, facing);

        }


    }



    [PunRPC]
    public void ClientHandleTarget(int myViewId, int _targViewId)
    {
        if (myViewId != photonView.viewID)
            return;
        currentTarget = BattleManager.Instance.GetLivingAdventurerByViewID(_targViewId);
    }

    public void Attack(bool force)
    {
        photonView.RPC("setFacing", PhotonTargets.All, facing);

        int attackIndex = Random.Range(1, enemyAnimation.maxAttacks);
        string attackString = "Attack" + attackIndex;
        if (force)
        {
            enemyAnimation.ForceAnimation(attackString, false, false);
        }
        else
        {
            enemyAnimation.SetAnimation(attackString, false, false);
        }
    }

    public void Initialize()
    {
        if (photonView == null)
            photonView = GetComponent<PhotonView>();
        gameObject.name = gameObject.name + photonView.viewID;
        //Debug.Log("Enemy Initialize Called");
        currentEntityStat = currentEntityStat.MirrorStat(baseEntityStat);//NEW ENEMY - MAKE STATS

        if (isBreakableObject)
        {
            isDying = false;
            rootCollider.SetActive(true);
            rootCollider.GetComponent<Collider2D>().enabled = true;
            currentEntityStat.hp = currentEntityStat.hpMax;
            enemyAnimation.ForceAnimation("Idle", true, false, true);

            lifebarCanvas.SetHP(currentEntityStat.hp, currentEntityStat.hpMax);
            return;//they dont need any of the rest of this crap
        }

        attackTimer = currentEntityStat.atkCooldown;

        //RestartAttackSequence();
        Vector3 startingPosition = new Vector3(0, 0, 0);
        GameObject playerSpawn0 = GameObject.FindGameObjectWithTag("PlayerSpawn0");



        RandSize();
        RandSpeed();

    }

    void RandSize()
    {
        float randScale = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(randScale, randScale, randScale);
    }

    void RandSpeed()
    {
        float randWalkSpeed = Random.Range(.8f, 1.2f);
        walkSpeed *= randWalkSpeed;
    }

    //	[PunRPC]
    //	public void FlipGraphic ()
    //	{
    //		transform.Rotate (Vector3.up * 180);
    //		lifebarCanvas.transform.Rotate (Vector3.up * 180);
    //	}

    public void SynchronizeHealth()
    {
        //THIS METHOD WILL SET THE HEALTH OF THIS ENEMY FOR LATE JOINERS
        //   Debug.Log("SynchHealth:" + photonView.viewID);
        float damageAdjustment = 1;
        object[] args = new object[] { 0f, photonView.viewID, true, false, 0, damageAdjustment };
        //Call the damage on Master, which calls on all clients 
        photonView.RPC("TakeDamageMaster", PhotonTargets.MasterClient, args);
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //if (!photonView.isMine) return;
        if (stream.isWriting)
        {
            //We own this player: send the others our data
            // stream.SendNext((int)controllerScript._characterState);
            stream.SendNext(transform.position);
            stream.SendNext(GetComponent<Rigidbody2D>().velocity);

            float _atkCooldown = currentEntityStat.atkCooldown;
            stream.SendNext(_atkCooldown);

            float _atkMax = currentEntityStat.atkMax;
            stream.SendNext(_atkMax);

            float _atkMin = currentEntityStat.atkMin;
            stream.SendNext(_atkMin);

            float _crit = currentEntityStat.crit;
            stream.SendNext(_crit);

            float _defMin = currentEntityStat.defMin;
            stream.SendNext(_defMin);

            float _defMax = currentEntityStat.defMax;
            stream.SendNext(_defMax);

            stream.SendNext(facing);



        }
        else
        {
            //Network player, receive data
            //controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();
            correctPlayerPos = (Vector3)stream.ReceiveNext();
            GetComponent<Rigidbody2D>().velocity = (Vector2)stream.ReceiveNext();

            currentEntityStat.atkCooldown = (float)stream.ReceiveNext();
            currentEntityStat.atkMax = (float)stream.ReceiveNext();
            currentEntityStat.atkMin = (float)stream.ReceiveNext();
            currentEntityStat.crit = (float)stream.ReceiveNext();
            currentEntityStat.defMin = (float)stream.ReceiveNext();
            currentEntityStat.defMax = (float)stream.ReceiveNext();
            facing = (int)stream.ReceiveNext();

            if (!appliedInitialUpdate)
            {
                appliedInitialUpdate = true;
                transform.position = correctPlayerPos;
                GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            }
        }
    }

    private Vector3 correctPlayerPos = Vector3.zero;
    //We lerp towards this
    private Quaternion correctPlayerRot = Quaternion.identity;
    //We lerp towards this

    [PunRPC]
    public void CallSyncAnimation(params object[] args)
    {
        bool isOnComplete;

        string animationName = (string)args[0];
        bool isLoop = (bool)args[1];
        if (args.Length > 2)
        {//if the sync animation missing the onComplete we set it automatically
            isOnComplete = (bool)args[2];
        }
        else
        {
            isOnComplete = false;
        }
        enemyAnimation.SyncAnimation(animationName, isLoop, isOnComplete);
    }

    //FIRST WE ALWAYS CALL THE MASTER
    //THEN THE MASTER CALLS ALL
    [PunRPC]
    public void TakeDamageMaster(params object[] args)
    {
        if (photonView == null)
            return;
        bool isSilent = (bool)args[2];
        lastAttackerID = (int)args[4];
        //Debug.Log("TakeDamageMaster lastAttackerID:" + lastAttackerID);
        float damageAdjustment = 1;
        if (args.Length > 5)
        {
            damageAdjustment = (float)args[5];
        }

        if (!PhotonNetwork.isMasterClient)
            return;
        args[0] = Mathf.Round((float)args[0]);//round damage

        object[] newArgs = new object[] {
            args [0],
            args [1],
            (int)currentEntityStat.hp,
            (int)currentEntityStat.hpMax,
            isSilent
        };
        photonView.RPC("TakeDamage", PhotonTargets.All, newArgs);
        //Debug.Log("EnemyArgs:" + args[0]+args[1]+args[2]+args[3]);
        if (!isSilent)
            photonView.RPC("CallPopUpText", PhotonTargets.All, args);
        photonView.RPC("AttackGraphic", PhotonTargets.All);

        // AttackGraphic();

    }

    [PunRPC] //ALSO CAN USE THIS TO SET HEALTH ;)
    public void TakeDamage(params object[] args)
    {
        if (photonView == null)
            return;
        //  Debug.Log("TakeDamage");

        //Wrong Enemy? Then quit method
        //ERROR HAPPENED HERE????????????????
        if ((int)args[1] != photonView.viewID)
            return;

        //Debug.Log("Args:" + args);
        float damage = (float)args[0];
        int viewID = (int)args[1];
        int hp = (int)args[2];
        int hpMax = (int)args[3];
        bool isSilent = (bool)args[4];

        //First we sync hp stats with the master 
        if (!PhotonNetwork.isMasterClient)
        {
            //set hp correctly
            currentEntityStat.hp = hp;//set this to the masters hp
            currentEntityStat.hpMax = hpMax;//set this to the masters hpMax
        }

        //now we apply damage
        currentEntityStat.hp -= damage;
        // Debug.Log("Enemy:" + viewID + " took damage:" + damage);

        //Only master can destroy them
        try
        {
            if (PhotonNetwork.isMasterClient)
            {
                if (currentEntityStat.hp <= 0)
                {
                    if (isDying)
                    {
                        //ANy overkill stuff happnes here
                        return;
                    }

                    photonView.RPC("DropSouls", PhotonTargets.All);

                    if (itemDrops.Count > 0)
                    {

                        photonView.RPC("DropItems", PhotonTargets.All);
                    }

                    //Debug.Log("lastattackerid:" + lastAttackerID);
                    AdventurerNetwork lastAttackingAdventurer = BattleManager.Instance.GetLivingAdventurerByViewID(lastAttackerID);

                    // Debug.Log("LAST ATTACKER:" + lastAttackerID);
                    if (lastAttackingAdventurer != null)
                    {

                        lastAttackingAdventurer.photonView.RPC("CallIncXP", PhotonTargets.All, (int)lastAttackerID, (int)expGiven);

                        //Add the 10 percent experience sharing
                        List<AdventurerNetwork> remainingAdventurers = BattleManager.Instance.GetAllLivingAdventurers();
                        for (int inc = 0; inc < remainingAdventurers.Count; inc++)
                        {
                            if (remainingAdventurers[inc].photonView.viewID != lastAttackerID)
                            {
                                float expTrimmed = expGiven * .1f;
                                remainingAdventurers[inc].photonView.RPC("CallIncXP", PhotonTargets.All, (int)lastAttackerID, (int)expTrimmed);
                            }
                        }
                    }
                    // DropSouls(); //send the soul drops to the manager so we are free to destroy this object later
                    EnemyDeath();
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.Log("Exception happened in EnemyNetwork.TakeDamage():" + e);
        }

        lifebarCanvas.SetHP(currentEntityStat.hp, currentEntityStat.hpMax);
    }

    public bool RollForCritical()
    {
        bool isCrit = false;
        int rand = Random.Range(0, 100);
        if (rand <= currentEntityStat.crit)
        {
            isCrit = true;
        }
        //Debug.Log("Enemy Attack is Critical:" + isCrit);
        return isCrit;
    }

    [PunRPC]
    public void AttackGraphic()
    {
        GameObject obj = ObjectPool.Instance.NextObject("YellowHit");
        Vector3 newPos = rootPosition.transform.position;
        newPos.z -= 1;
        obj.transform.position = newPos;
        obj.SetActive(true);
    }

    [PunRPC]
    public void AOEOnHit(string _sound)
    {
        //aoe range from the master
        //dist between boss and current player

        CameraEffects ce = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraEffects>();
        ce.CallCameraShake(.6f);

        AudioManager.Instance.PlayClip(_sound, true, false);

        GameObject obj = ObjectPool.Instance.NextObject(skillAOEOnHitParticleName);
        Vector3 newPos = rootPosition.transform.position + new Vector3(0, -2, 0);
        obj.transform.position = newPos;
        obj.SetActive(true);
    }

    [PunRPC]
    public void DeathParticle()
    {
        //aoe range from the master
        //dist between boss and current player

        CameraEffects ce = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraEffects>();
        ce.CallCameraShake(.6f);

        //    AudioManager.Instance.PlayClip(_sound, true, false);

        GameObject obj = ObjectPool.Instance.NextObject(deathParticleName);
        Vector3 newPos = rootPosition.transform.position + new Vector3(0, -2, 0);
        obj.transform.position = newPos;
        obj.SetActive(true);
    }


    [PunRPC]
    public void AOEGraphic()
    {
        m_AOEAnimator.CrossFade("AOECircleIn", 0);
    }



    [PunRPC]
    public void DropItems()
    {
        //THIS RPC WILL NEVER BE CALLED IF NO ITEMS PRESENT
        DropManager.Instance.DropItems(itemDrops, rootPosition.transform.position);
    }

    [PunRPC]
    public void DropSouls()
    {

        //small
        if (smallSoulMax > 0)
        {
            //	Debug.Log ("**DROP SOULS SMALL>0");
            DropManager.Instance.DropSouls(SoulSize.small, (int)smallSoulMin, (int)smallSoulMax, (int)smallSoulValue, rootPosition.transform.position);
        }
        //med
        if (medSoulMax > 0)
        {
            //	Debug.Log ("**DROP SOULS MED>0");

            DropManager.Instance.DropSouls(SoulSize.med, (int)medSoulMin, (int)medSoulMax, (int)medSoulValue, rootPosition.transform.position);
        }
        //big
        if (bigSoulMax > 0)
        {
            //Debug.Log ("**DROP SOULS Big>0");

            DropManager.Instance.DropSouls(SoulSize.big, (int)bigSoulMin, (int)bigSoulMax, (int)bigSoulValue, rootPosition.transform.position);
        }
    }


    [PunRPC]
    public void CallPopUpText(params object[] args)
    {
        float val = (float)args[0];
        bool isCrit = (bool)args[3];
        float damageAdjustment = 1;
        if (args.Length > 5)
            damageAdjustment = (float)args[5];
        if (isCrit)
        {
            BattleTextManager.Instance.CreateText(BattleTextType.crit, rootPosition.transform.position, val);
        }
        else
        {
            BattleTextType btt = BattleTextType.white;
            if (damageAdjustment != 1)
                btt = BattleTextType.pink;
            BattleTextManager.Instance.CreateText(btt, rootPosition.transform.position, val);
        }
    }

    [PunRPC]
    public void Hit(int _viewID)
    {
        if (photonView == null)
            return;
        if (_viewID != photonView.viewID)
            return;//only apply to your correct enemy
        string hitString = "Hit";
        enemyAnimation.SetAnimation(hitString, false, false);
    }


    public void OnPhotonPlayerConnected()
    {
        if (!PhotonNetwork.isMasterClient)
            return;
        //THIS IS FIRING ON MASTER
        //photonView.RPC("BroadcastFacing",PhotonTargets.Others, facing,photonView.viewID);
        //	photonView.RPC("BroadcastPickNew",PhotonTargets.master, facing,photonView.viewID);

    }

    //call this on master
    [PunRPC]
    void BroadcastFacing(int _facing, int viewId)
    {
        //this FIRES PROPERLY		//IS THIS FIRING?
        if (photonView == null) photonView = GetComponent<PhotonView>();
        if (viewId != photonView.viewID)
            return;

        //	Debug.Log ("SYNC ENEMY:" + viewId + " FACING:" + _facing);
        //reset to pure
        transform.rotation = Quaternion.identity;
        //set to the way it should be done
        setFacing(_facing);
    }



    public void InvokeCallSyncStatMaster(float delay)
    {
        Invoke("CallSyncStatMaster", delay);
    }

    void CallSyncStatMaster()
    {
        photonView.RPC("SyncStatMaster", PhotonTargets.MasterClient, photonView.viewID);

    }

    [PunRPC]
    public void SyncStatMaster(int _viewID)
    {
        if (!PhotonNetwork.isMasterClient)
            return;
        //Debug.Log("SyncStatMaster:" + (int)currentEntityStat.hpMax);
        object[] newArgs = new object[] {
            _viewID,
            (int)currentEntityStat.atkMin,
            (int)currentEntityStat.atkMax,
            (int)currentEntityStat.hpMax,
            (int)currentEntityStat.xp
        };
        photonView.RPC("SyncStat", PhotonTargets.All, newArgs);
    }

    [PunRPC]
    public void SyncStat(int _viewID, int atkMin, int atkMax, int currHPMax, int xp)
    {
        if (photonView == null)
            return;
        if (_viewID != photonView.viewID)
            return;
        //First we sync hp stats with the master 
        if (!PhotonNetwork.isMasterClient)
        {
            currentEntityStat.hp = currHPMax;//set this to the masters hpMax
            currentEntityStat.hpMax = currHPMax;//set this to the masters hpMax
            currentEntityStat.atkMin = atkMin;//set this to the masters hpMax
            currentEntityStat.atkMax = atkMax;//set this to the masters hpMax
            currentEntityStat.xp = xp;//set this to the masters hpMax

        }

        lifebarCanvas.SetHP(currentEntityStat.hp, currentEntityStat.hpMax);

    }

    public bool isDying = false;

    public void EnemyDeath()
    {
        if (isDying)
            return;
        this.gameObject.tag = "DeadEnemy";
        if (PhotonNetwork.isMasterClient)
        {

            if (!isBreakableObject)
                BattleManager.Instance.CheckWaveComplete();
        }

        if (enemyAnimation.currentAnimation == "Entrance")
        {
            Invoke("FinalDestroy", .4f);
        }
        else
        {
            enemyAnimation.SetAnimation("Death", false, false);//if it is entrance it will cancel this
        }

        PhotonNetwork.RemoveRPCs(photonView);
        photonView.RPC("CallDeathGraphic", PhotonTargets.All);
        photonView.RPC("CallDeath", PhotonTargets.All);

    }

    [PunRPC]
    public void CallDeath()
    {
        lifebarCanvas.CallFadeCanvas(0);
        isDying = true;
        StartCoroutine("AudioFadeOut");
        if (enemyType == EnemyType.boss)
        {
            EventManager.Instance.CallBossDefeated();
        }
    }

    public IEnumerator AudioFadeOut()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            //no audio source quit this

        }
        else
        {
            float FadeTime = 2;
            float startVolume = audioSource.volume;
            while (audioSource.volume > 0)
            {
                //Debug.Log("FadingOut");
                audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
                yield return null;
            }
        }
    }

    [PunRPC]
    public void CallDeathGraphic()
    {
        GameObject obj = ObjectPool.Instance.NextObject(deathType);
        obj.SetActive(true);
        obj.transform.position = rootPosition.transform.position;
    }

    [PunRPC]
    public void TurnOffCollider()
    {
        ClickableEnemy ce = GetComponentInChildren<ClickableEnemy>();
        ce.myCollider.enabled = false;
    }

    public void TurnOnCollider()
    {
        ClickableEnemy ce = GetComponentInChildren<ClickableEnemy>();
        ce.myCollider.enabled = true;
    }

    public void FinalDestroy()
    {
        if (isExplosive) Explode();
        if (isBreakableObject)
        {
            photonView.RPC("RootColliderOff", PhotonTargets.All, photonView.viewID);

            return;
        }
        PhotonNetwork.Destroy(gameObject);

    }

    public void Explode()
    {
        Debug.Log("Explode");
        //DAMAGE THE ENEMIES HERE
        List<EnemyNetwork> allEnemies = BattleManager.Instance.GetAllEnemies().ToList();
        foreach (EnemyNetwork en in allEnemies)
        {
            if (Vector3.Distance(transform.position, en.transform.position) < hitRange)
            {
                float damage = UnityEngine.Random.Range(explosiveDamage * .9f, explosiveDamage * 1.1f);
                //  damage = damage - (UnityEngine.Random.Range(en.currentEntityStat.defMin, en.currentEntityStat.defMax));
                bool isSilent = false;
                bool isCrit = false;
                object[] args = new object[] { explosiveDamage, en.photonView.viewID, isSilent, isCrit, photonView.viewID };
                en.photonView.RPC("TakeDamageMaster", PhotonTargets.MasterClient, args);
            }
        }

        List<AdventurerNetwork> livingAdvent = BattleManager.Instance.GetAllLivingAdventurers();
        foreach (AdventurerNetwork an in livingAdvent)
        {
            float damage = UnityEngine.Random.Range(explosiveDamage * .9f, explosiveDamage * 1.1f);
            // damage = damage - (UnityEngine.Random.Range(an.currentEntityStat.defMin, an.currentEntityStat.defMax));
            bool isSilent = false;
            bool isCrit = false;
            object[] args = new object[] { damage, an.photonView.viewID, isSilent, isCrit, photonView.viewID };
            an.photonView.RPC("TakeDamageMaster", PhotonTargets.MasterClient, args);
        }
    }

    [PunRPC]
    public void RootColliderOff(int viewId)
    {
        if (photonView.viewID != viewId)
            return;
        rootCollider.SetActive(false);
        lifebarCanvas.CallFadeCanvas(0);
    }


    public void OnPhotonPlayerDisconnected()
    {
        attackTimer = currentEntityStat.atkCooldown;
    }


}
