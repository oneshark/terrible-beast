﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPopUp : MonoBehaviour {
    public Rigidbody rb;
    public Text txt;
    public Text customText;
    public Color redTint;
    public Color whiteTint;
    public Color pinkTint;
    public Color greenTint;

    public void TintRed()
    {
        txt.color = redTint;
    }

    public void TintPink()
    {
        txt.color = pinkTint;
        Debug.Log("Tinted Pink");
    }

    public void TintWhite()
    {
        txt.color = whiteTint;
    }

    public void TintGreen()
    {
        txt.color = greenTint;
    }

    public void CallCritAnim()
    {
        Animator anim = GetComponentInChildren<Animator>();
        anim.CrossFade("Critical", 0);
    }

    public void CallWeakAnim()
    {
        Animator anim = GetComponentInChildren<Animator>();
        anim.CrossFade("Critical", 0);
    }

    public void CallAttackAnim()
    {
        Animator anim = GetComponentInChildren<Animator>();
        anim.CrossFade("AttackUp", 0);
    }

}
