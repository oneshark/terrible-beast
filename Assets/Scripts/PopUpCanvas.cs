﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpCanvas : MonoBehaviour
{
	public Text msg1, msg2, fullMsg;
	public Image icon;

	public Sprite soulIcon;
	public Sprite rejectIcon;
	public Sprite wormIcon;
	public Sprite checkIcon;
	public Sprite fishIcon;

	public Color blue, red, white;

	public void SetPopup (string _msg1, string _msg2)
	{
	//	Debug.Log ("Set Message!");
		msg1.text = _msg1;
		msg2.text = _msg2;
		fullMsg.text = "";
	}

	public void SetPopup (string _msg1, string _msg2, string _fullmsg)
	{
		//Debug.Log ("Set Message!");
		msg1.text = _msg1;
		msg2.text = _msg2;
		fullMsg.text = _fullmsg;
	}


	public void SetSoulIcon ()
	{
		icon.sprite = soulIcon;
	}


	public void SetWormIcon ()
	{
		icon.sprite = wormIcon;
	}


	public void SetRejectIcon ()
	{
		icon.sprite = rejectIcon;
	}

	public void SetAcceptIcon ()
	{
		icon.sprite = checkIcon;
	}

	public void SetFishIcon ()
	{
		icon.sprite = fishIcon;
	}

	public void SetTextColor (string _color)
	{
		if (_color == "blue") {
			msg1.color = blue;
			msg2.color = blue;
		}
		if (_color == "red") {
			msg1.color = red;
			msg2.color = red;
		}
		if (_color == "white") {
			msg1.color = white;
			msg2.color = white;
		}
	}
}
