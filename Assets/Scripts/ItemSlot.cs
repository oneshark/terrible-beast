﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	public GameObject equip, sell, drop;
	public Image iconImage;
	public Text qtyText;
	public Text titleText;
	public Text levelText;
	public Item associatedItem;
	public int m_qty;
	public bool isEquipmentSlot;
	//only allow to unequip these, not sell directly
	// public EquipmentSlotType equipmentSlotType;
	public CraftingDropdown cd;
	public Sprite originalIcon;
	public bool purchasable = false;
	public bool isCraftingItem = false;
	public bool isCrownItem=false;

	void OnEnable ()
	{
		EventManager.OnInventoryStyleChange += OnInventoryStyleChange;
	}

	void OnDisable ()
	{
		EventManager.OnInventoryStyleChange -= OnInventoryStyleChange;
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		if (isCrownItem) {
			CrownMallEquip ();
			return;
		}
		if (purchasable)
			Purchase ();
	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		if (associatedItem == null)
			return;//assert !=null
		if (associatedItem.icon == null)
			return;
        
		ToolTip.Instance.SetItem (associatedItem, purchasable);
		if (isCraftingItem) {
			cd.ToggleMe (true);
		} else {
			ToolTipCanvas.Instance.ToggleMe (true); //AVOID SHOWING TOOL TIPS FOR CRAFTING ITEMS 
		}
	}

	public void OnPointerExit (PointerEventData eventData)
	{
		ToolTipCanvas.Instance.ToggleMe (false);
		if (isCraftingItem) {
			cd.ToggleMe (false);
		}
	}

	void OnInventoryStyleChange (bool isDrop)
	{
		if (drop == null) {
			//Debug.Log ("This object was null:" + gameObject.name);
			return;
		}
		Debug.Log ("Inventory Style Change isDrop:" + isDrop);
		drop.SetActive (isDrop);
		sell.SetActive (!isDrop);
		equip.SetActive (!isDrop);
		if (associatedItem.itemType==ItemType.junk)equip.SetActive(false);//hard coded fixer upper

		if (purchasable || isCraftingItem || isCrownItem) {
			equip.SetActive (false);
			drop.SetActive (false);
			sell.SetActive (false);
		}
	}

	void CrownMallEquip(){
		if (CrownBuyItemsCanvas.Instance.shoppingCartItems.Count > 5)
			return;//too many wont fit in the store window
		//Debug.Log ("Try To Equip this on mannequin");
		AudioManager.Instance.PlayClip ("click", true, false);

			CrownBuyItemsCanvas.Instance.EquipOnMannequin(associatedItem);
		//CrownBuyItemsCanvas.Instance.SetItemSlot(associatedItem);
		CrownBuyItemsCanvas.Instance.shoppingCartItems.Add (associatedItem);
		CrownBuyItemsCanvas.Instance.RefreshCartItems ();
	}


	public void Purchase ()
	{
		if (GameManager.Instance.souls >= associatedItem.cost) {
			AudioManager.Instance.PlayClip ("boughtitem", true, false);
			GameManager.Instance.IncSouls (-associatedItem.cost);
			InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
			InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
			InventoryCanvas.Instance.SetPanel (associatedItem.itemType);
		} else {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Not Enough", "Souls");
			puc.SetSoulIcon ();
			puc.SetTextColor ("blue");
			nextObj.SetActive (true);
		}
	}

	public void ResetEquipItemSlot ()
	{
		iconImage.sprite = originalIcon;
		titleText.enabled = true;
	}

	public void SetItem (Item item, int qty)
	{
		associatedItem = item;
		if (titleText != null)
			titleText.enabled = false;//.text = associatedItem.name;
		iconImage.sprite = item.icon;
		m_qty = qty;
		if (qtyText != null)
			qtyText.text = m_qty.ToString ();

		//by itemtype 
		if (item.itemType == ItemType.weapon) {
			if (qtyText != null)
				qtyText.text = "";
		}
		if (item.itemType == ItemType.junk) {
			if (equip != null)
				equip.SetActive (false);
		}
		if (item.itemType == ItemType.costume) {
			if (qtyText != null)
				qtyText.text = "";
		}
		if (item.itemType == ItemType.equipment) {
			if (qtyText != null)
				qtyText.text = "";
		}
		if (levelText != null) {
			levelText.text = item.requiredLevel.ToString ();
			if (isCraftingItem)
				levelText.text = "Level " + item.requiredLevel;
		}

		if (isCraftingItem) {
			titleText.enabled = true;
			titleText.text = item.name;
		}
		if (isEquipmentSlot) {//Cant do anything but click to unequip
			//if (qtyText != null)
			//    qtyText.text = "";
			//equip.SetActive(false);
			//sell.SetActive(false);
		}

		if (isCraftingItem) {
			cd.SetAssociatedItem (associatedItem);
		}

		OnInventoryStyleChange(EventManager.Instance.GetInventoryStyle());
	}


	public void Sell ()
	{
		if (!InventoryCanvas.Instance.equipmentOpen)
			return;
		//by itemtype 
		int _cost = associatedItem.cost;
		if (associatedItem.itemGrade == ItemGrade.ordinary & associatedItem.itemType != ItemType.junk) {
			_cost = _cost / 2;
		}
		RemoveSlot ();
		GameManager.Instance.IncSouls (_cost);
		ToolTipCanvas.Instance.ToggleMe (false);
		AudioManager.Instance.PlayClip ("sellitems", true, false);
	}

	public void RemoveSlot ()
	{ 
		bool remove = false;

		if (associatedItem.itemType == ItemType.weapon) {
			InventoryCanvas.Instance.allItemSlots.Remove (this);
			InventoryManager.Instance.weaponsOwned.Remove (associatedItem);
			remove = true;
		}
		if (associatedItem.itemType == ItemType.junk) {
			if (m_qty == 1) {
				InventoryCanvas.Instance.allItemSlots.Remove (this);
				remove = true;
			} else {
				IncrementItemSlot (-1);
			}
			InventoryManager.Instance.junkOwned.Remove (associatedItem);
		}
		if (associatedItem.itemType == ItemType.costume) {
			InventoryCanvas.Instance.allItemSlots.Remove (this);
			InventoryManager.Instance.costumesOwned.Remove (associatedItem);
			remove = true;
		}
		if (associatedItem.itemType == ItemType.equipment) {
			InventoryCanvas.Instance.allItemSlots.Remove (this);
			InventoryManager.Instance.equipmentOwned.Remove (associatedItem);
			remove = true;
		}

		if (remove)
			DestroyMe ();
		// GameManager.Instance.IncSouls(_cost);
		//ToolTipCanvas.Instance.ToggleMe(false);
		//AudioManager.Instance.PlayClip("sellitems", true, false);

	}

	public void IncrementItemSlot (int amountInc)
    //Send Negative or Positive
	{
		m_qty += amountInc;
		qtyText.text = m_qty.ToString ();
	}

	public bool CanEquip ()
	{
		bool b = true;
		if (associatedItem.requiredLevel > GameManager.Instance.baseEntityStat.xpLevel) {
			//Debug.Log("Level Too Low!");
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("You Must", "Level Up");
			puc.SetRejectIcon ();
			puc.SetTextColor ("white");
			nextObj.SetActive (true);
			b = false;
		}
		return b;
	}

	public void Equip ()
	{
		//alllow a background equip only in the void so we can sset first weapon
		//in main game if we allow this, it lets chat tab index enter equip weird stuff
		if (!InventoryCanvas.Instance.equipmentOpen & !GameManager.Instance.VoidObject.activeSelf)
			return;
		//REQUIREMENTS
		if (!CanEquip ())
			return;

		if (associatedItem.itemType == ItemType.weapon) {
			if (GameManager.Instance.equippedWeapon != null) {
				InventoryCanvas.Instance.equipWeapon.Unequip ();
			}
			GameManager.Instance.equippedWeapon = associatedItem;//set the item
			if (GameManager.Instance.currentScene == CustomSceneType.Battle) {
				BattleManager.Instance.myAdventurer.equippedWeapon = associatedItem;
			}
			if (GameManager.Instance.currentScene == CustomSceneType.Town) {
				TownManager.Instance.myAtn.equippedWeapon = associatedItem;
			}

			InventoryCanvas.Instance.equipWeapon.SetItem (associatedItem, 1);
			AudioManager.Instance.PlayClip ("equipweapon", true, false);
			GameManager.Instance.weapEquipped = true;
			InventoryCanvas.Instance.allItemSlots.Remove (this);
			InventoryManager.Instance.weaponsOwned.Remove (associatedItem);
		}
		if (associatedItem.itemType == ItemType.costume) {
			if (associatedItem.costumeType == CostumeType.back) {
				if (GameManager.Instance.equippedCostumeBack != null) {
					InventoryCanvas.Instance.costumeSlotBack.Unequip ();
				}
				GameManager.Instance.equippedCostumeBack = associatedItem;//set the item
				if (GameManager.Instance.currentScene == CustomSceneType.Battle) {
					BattleManager.Instance.myAdventurer.equippedCostumeBack = associatedItem;
				}
				if (GameManager.Instance.currentScene == CustomSceneType.Town) {
					TownManager.Instance.myAtn.equippedCostumeBack = associatedItem;
				}
				InventoryCanvas.Instance.costumeSlotBack.SetItem (associatedItem, 1);
				AudioManager.Instance.PlayClip ("equiparmor", true, false);
				GameManager.Instance.backEquipped = true;
			}
			if (associatedItem.costumeType == CostumeType.body) {
				if (GameManager.Instance.equippedCostumeBody != null) {
					InventoryCanvas.Instance.costumeSlotBody.Unequip ();
				}
				GameManager.Instance.equippedCostumeBody = associatedItem;//set the item
				if (GameManager.Instance.currentScene == CustomSceneType.Battle) {
					BattleManager.Instance.myAdventurer.equippedCostumeBody = associatedItem;
				}
				if (GameManager.Instance.currentScene == CustomSceneType.Town) {
					TownManager.Instance.myAtn.equippedCostumeBody = associatedItem;
				}
				InventoryCanvas.Instance.costumeSlotBody.SetItem (associatedItem, 1);
				AudioManager.Instance.PlayClip ("equiparmor", true, false);
				GameManager.Instance.bodyEquipped = true;
			}
			if (associatedItem.costumeType == CostumeType.head) {
				if (GameManager.Instance.equippedCostumeHead != null) {
					InventoryCanvas.Instance.costumeSlotHead.Unequip ();
				}
				GameManager.Instance.equippedCostumeHead = associatedItem;//set the item
				AudioManager.Instance.PlayClip ("equiparmor", true, false);
				GameManager.Instance.headEquipped = true;
				if (GameManager.Instance.currentScene == CustomSceneType.Battle) {
					BattleManager.Instance.myAdventurer.equippedCostumeHead = associatedItem;
				}
				if (GameManager.Instance.currentScene == CustomSceneType.Town) {
					TownManager.Instance.myAtn.equippedCostumeHead = associatedItem;
				}
				InventoryCanvas.Instance.costumeSlotHead.SetItem (associatedItem, 1);

			}
			InventoryCanvas.Instance.allItemSlots.Remove (this);
			InventoryManager.Instance.costumesOwned.Remove (associatedItem);
		}
		if (associatedItem.itemType == ItemType.equipment) {

			if (associatedItem.equipmentType == EquipmentType.artifact) {
				if (GameManager.Instance.equippedEquipmentArtifact != null) {
					InventoryCanvas.Instance.equipSlotArtifact.Unequip ();
				}
				GameManager.Instance.equippedEquipmentArtifact = associatedItem;//set the item
				GameManager.Instance.artifactEquipped = true;
				AudioManager.Instance.PlayClip ("equiparmor", true, false);
				InventoryCanvas.Instance.equipSlotArtifact.SetItem (associatedItem, 1);
			}
			if (associatedItem.equipmentType == EquipmentType.necklace) {
				if (GameManager.Instance.equippedEquipmentNecklace != null) {
					InventoryCanvas.Instance.equipSlotNecklace.Unequip ();
				}
				GameManager.Instance.equippedEquipmentNecklace = associatedItem;//set the item
				GameManager.Instance.necklaceEquipped = true;
				AudioManager.Instance.PlayClip ("equiparmor", true, false);
				InventoryCanvas.Instance.equipSlotNecklace.SetItem (associatedItem, 1);
			}
			if (associatedItem.equipmentType == EquipmentType.ring) {
				if (GameManager.Instance.equippedEquipmentRing != null) {
					InventoryCanvas.Instance.equipSlotRing.Unequip ();
				}
				GameManager.Instance.equippedEquipmentRing = associatedItem;//set the item
				GameManager.Instance.ringEquipped = true;
				AudioManager.Instance.PlayClip ("equiparmor", true, false);
				InventoryCanvas.Instance.equipSlotRing.SetItem (associatedItem, 1);
			}
			InventoryCanvas.Instance.allItemSlots.Remove (this);
			InventoryManager.Instance.equipmentOwned.Remove (associatedItem);
		}

		//Gather the right photonView
		PhotonView _photonView = new PhotonView ();
		if (GameManager.Instance.currentScene == CustomSceneType.Battle) {
			_photonView = BattleManager.Instance.myAdventurer.photonView;
		}
		if (GameManager.Instance.currentScene == CustomSceneType.Town) {
			_photonView = TownManager.Instance.myAtn.photonView;
		}

		GameManager.Instance.ChangeEquipment (associatedItem, _photonView);
		GameManager.Instance.CalculateStats ();
		RemoveSlot ();
		ToolTipCanvas.Instance.ToggleMe (false);
	}

	public void DestroyMe ()
	{
		Destroy (gameObject);
	}

	public void Unequip ()
	{
		Unequip (true); //silent is the default sound
	}

	public void Unequip (bool silent)
	{
		//Debug.Log ("Unequip:" + associatedItem);
		if (associatedItem == null)
			return;
		switch (associatedItem.itemType) {
		case ItemType.weapon:
			if (!silent)
				AudioManager.Instance.PlayClip ("equipweapon", true, false);

                // Debug.Log("UnequipWeapon");
			iconImage.sprite = InventoryCanvas.Instance.origWeap;
			InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
			InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
			titleText.enabled = true;
			associatedItem = null;
			GameManager.Instance.equippedWeapon = null;
			GameManager.Instance.weapEquipped = false;
			break;
		case ItemType.equipment:
			if (!silent)
				AudioManager.Instance.PlayClip ("equiparmor", true, false);

			if (associatedItem.equipmentType == EquipmentType.artifact) {
				iconImage.sprite = InventoryCanvas.Instance.origArtifact;
				InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
				InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
				titleText.enabled = true;
				associatedItem = null;
				GameManager.Instance.equippedEquipmentArtifact = null;
				GameManager.Instance.artifactEquipped = false;
				break;
			}
			if (associatedItem.equipmentType == EquipmentType.necklace) {
				iconImage.sprite = InventoryCanvas.Instance.origNecklace;
				InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
				InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
				titleText.enabled = true;
				associatedItem = null;
				GameManager.Instance.equippedEquipmentNecklace = null;
				GameManager.Instance.necklaceEquipped = false;
				break;
			}
			if (associatedItem.equipmentType == EquipmentType.ring) {
				iconImage.sprite = InventoryCanvas.Instance.origArtifact;
				InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
				InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
				titleText.enabled = true;
				associatedItem = null;
				GameManager.Instance.equippedEquipmentRing = null;
				GameManager.Instance.ringEquipped = false;
			}
			break;
		case ItemType.costume:
			if (!silent)
				AudioManager.Instance.PlayClip ("equiparmor", true, false);

			if (associatedItem.costumeType == CostumeType.back) {
				iconImage.sprite = InventoryCanvas.Instance.origBackCostume;
				InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
				InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
				titleText.enabled = true;
				associatedItem = null;
				GameManager.Instance.equippedCostumeBack = null;
				GameManager.Instance.backEquipped = false;
				break;
			}
			if (associatedItem.costumeType == CostumeType.body) {
			//	Debug.Log ("**** Unequip Costume");
				iconImage.sprite = InventoryCanvas.Instance.origBodyCostume;
				InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
				InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
				titleText.enabled = true;
				associatedItem = null;
				GameManager.Instance.equippedCostumeBody = null;
				GameManager.Instance.bodyEquipped = false;
				break;
			}
			if (associatedItem.costumeType == CostumeType.head) {
				iconImage.sprite = InventoryCanvas.Instance.origHeadCostume;
				InventoryManager.Instance.AddItem (associatedItem.itemType, associatedItem.ID);
				InventoryCanvas.Instance.AddItemSlot (associatedItem, 1);
				titleText.enabled = true;
				associatedItem = null;
				GameManager.Instance.equippedCostumeHead = null;
				GameManager.Instance.headEquipped = false;
				break;
			}
			break;
		}
		//ResetEquipItemSlot();
		GameManager.Instance.SetAllEquipment (TownManager.Instance.myAdventurerTownNetwork.photonView);
		GameManager.Instance.CalculateStats ();
	}
}

//[System.Serializable]
//public enum EquipmentSlotType
//{
//    weapon,
//    head,
//    body,
//    back,
//    ring,
//    necklace,
//    artifact

//}