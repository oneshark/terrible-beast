﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolTipCanvas : MonoBehaviour {
    private static ToolTipCanvas _instance;
    public static ToolTipCanvas Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<ToolTipCanvas>();
            return _instance;
        }
    }
    CanvasGroup cg;
    bool tooltipOpen = false;

    void Awake()
    {
        cg = GetComponent<CanvasGroup>();
    }

   public void ToggleMe(bool b)
    {
        if (b == tooltipOpen) return;
        if (b)
        {
            cg.alpha = 1;
        }else
        {
            cg.alpha = 0;
        }
        tooltipOpen = b;
    }

}
