﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TradeCanvasSelector : MonoBehaviour
{
	public CanvasGroup cgTradeOptions, cgWantsToTradeWithYou, cgPlayerOffersTrade, cgStillThinking;
	public CanvasTools ct;

	private static TradeCanvasSelector _instance;

	public static TradeCanvasSelector Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<TradeCanvasSelector> ();
			return _instance;
		}
	}

	void Awake ()
	{
		ct = GetComponent<CanvasTools> ();
		GameManager.Instance.InputEnabled = false;
	}

	public void ClickTrade ()
	{
		AudioManager.Instance.PlayClip ("click", true, false);
		gameObject.SetActive (false);

	}

	public void SetPanel (string s)
	{
		cgTradeOptions.alpha = 0;
		cgTradeOptions.blocksRaycasts = false;
		cgTradeOptions.interactable = false;

		cgWantsToTradeWithYou.alpha = 0;
		cgWantsToTradeWithYou.blocksRaycasts = false;
		cgWantsToTradeWithYou.interactable = false;

		cgPlayerOffersTrade.alpha = 0;
		cgPlayerOffersTrade.blocksRaycasts = false;
		cgPlayerOffersTrade.interactable = false;

		cgStillThinking.alpha = 0;
		cgStillThinking.blocksRaycasts = false;
		cgStillThinking.interactable = false;


		switch (s) {
		case "TradeOptions":
			cgTradeOptions.alpha = 1;
			cgTradeOptions.blocksRaycasts = true;
			cgTradeOptions.interactable = true;
			break;
		case "WantsToTradeWithYou":
			cgWantsToTradeWithYou.alpha = 1;
			cgWantsToTradeWithYou.blocksRaycasts = true;
			cgWantsToTradeWithYou.interactable = true;
			break;
		case "PlayerOffersTrade":
			cgPlayerOffersTrade.alpha = 1;
			cgPlayerOffersTrade.blocksRaycasts = true;
			cgPlayerOffersTrade.interactable = true;
			break;
		case "StillThinking":
			cgStillThinking.alpha = 1;
			cgStillThinking.blocksRaycasts = true;
			cgStillThinking.interactable = true;
			break;
		}
	}
}
