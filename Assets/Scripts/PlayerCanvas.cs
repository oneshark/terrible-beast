﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerCanvas : MonoBehaviour
{
    //public GameObject startInvasionButton;
    public SoulFlameUI sfUI;
    public Text waveNumberText;
    public Animator waveNumberAnim;

    public Text playerLevelText, playerHPText;
    public Text soulAmountText;
    public Image playerHPFill;
    public Image playerXPFill;
    public Text expText;
    public Animator expAnim;
    private static PlayerCanvas _instance;
    public static PlayerCanvas Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<PlayerCanvas>();
            return _instance;
        }
    }


    public void SetHP(float hp, float hpMax)
    {
        //Debug.Log("SetHP:" + hp + "/" + hpMax);
        playerHPText.text = (int)hp + " / " + (int)hpMax;
        playerHPFill.fillAmount = (int)hp / hpMax;
        AdventurerNetwork myAdventurer = BattleManager.Instance.myAdventurer;
        // Debug.Log("MyAdventurerHPSetting:" + myAdventurer.playerPosition); this
        object[] clientArgs = new object[] { myAdventurer.photonView.viewID, myAdventurer.playerPosition, myAdventurer.currentEntityStat.hp, GameManager.Instance.currentEntityStat.hpMax, GameManager.Instance.baseEntityStat.xp, GameManager.Instance.baseEntityStat.xpMax, GameManager.Instance.baseEntityStat.xpLevel, GameManager.Instance.PlayerName };
        BattleCanvas.Instance.photonView.RPC("SetPlayerInfoMaster", PhotonTargets.MasterClient, clientArgs);
    }

    public void SetXP(float xp, float xpMax)
    {
        playerXPFill.fillAmount = xp / xpMax;
        AdventurerNetwork myAdventurer = BattleManager.Instance.myAdventurer;
        // Debug.Log("MyAdventurerHPSetting:" + myAdventurer.playerPosition); this
        object[] clientArgs = new object[] { myAdventurer.photonView.viewID, myAdventurer.playerPosition, myAdventurer.currentEntityStat.hp, myAdventurer.currentEntityStat.hpMax, GameManager.Instance.baseEntityStat.xp, GameManager.Instance.baseEntityStat.xpMax, GameManager.Instance.baseEntityStat.xpLevel, GameManager.Instance.PlayerName };
        BattleCanvas.Instance.photonView.RPC("SetPlayerInfoMaster", PhotonTargets.MasterClient, clientArgs);
    }

    public void CallSFEat()
    {
        sfUI.Eat();
    }

    public void CallExp(int val)
    {
        expText.text = "Exp +" + val;
        expAnim.CrossFade("Exp", 0);
        //Debug.Log("Called Exp!");
    }

    public void CallWave(int _wave)
    {
        //  Debug.Log("CallWave");
        waveNumberAnim.CrossFade("Wave", 0);
        waveNumberText.text = _wave + "";
    }

    CanvasGroup cg;
    bool canvasOpen = false;
    public void ToggleMe(bool b)
    {
        if (cg == null) cg = GetComponent<CanvasGroup>();
        if (b == canvasOpen) return;
        if (b)
        {
            cg.alpha = 1;
            cg.interactable = true;
            cg.blocksRaycasts = true;
        }
        else
        {
            cg.alpha = 0;
            cg.interactable = false;
            cg.blocksRaycasts = false;
        }
        canvasOpen = b;
    }

}
