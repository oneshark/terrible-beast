﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvas : MonoBehaviour
{
	public ActivateObj[] ObjTurnOn;
	public ActivateObj[] ObjTurnOff;
	public GameObject FadeToBlack;
	public Animator m_BlackFader;
	// Use this for initialization
	void Start ()
	{
		Invoke ("ReadyToClick", 1);
	}

	public void BlackFade(){
	m_BlackFader.CrossFade("FadeToBlack",0);
	}

	bool readyToClick = false;

	void ReadyToClick ()
	{
		readyToClick = true;
	}

	public void TryClick ()
	{
		if (readyToClick) {
				Clicked ();

		}
	}
	void Clicked ()
	{
	Invoke("ActivateObjects",.2f);
	}

	void ActivateObjects ()
	{
		for (int inc = 0; inc < ObjTurnOn.Length; inc++) {
			if (ObjTurnOn [inc].delay > 0) {
				GameManager.Instance.CallDelayedActivate (ObjTurnOn [inc].obj, ObjTurnOn [inc].delay, true);
			} else {
				ObjTurnOn [inc].obj.SetActive (true);
			}
		}
		for (int inc = 0; inc < ObjTurnOff.Length; inc++) {
			ObjTurnOff [inc].obj.SetActive (false);
		}
	}



	public void OpenWebsite (string site)
	{
		//	Input.GetMouseButtonDown (0) = false;
		AudioManager.Instance.PlayClip ("click", true, false);
		Application.OpenURL (site);
	}
}

[System.Serializable]
public class ActivateObj
{
	public GameObject obj;
	public float delay = 0;
}