﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RaycastTool : MonoBehaviour {

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        { // UI elements getting the hit/hover
            RaycastHit hit = new RaycastHit();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
                print(hit.collider.transform.gameObject.name);
            Debug.Log("Point is over UI:" + hit.collider.transform.gameObject.name);
        }


    }
}
