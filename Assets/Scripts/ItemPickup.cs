﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ItemPickup : MonoBehaviour
{
	public Color ordinary, unique, rare, veryrare, elite, special;
	public SpriteRenderer icon, cube;
	public ItemType itemType;
	public ItemGrade itemGrade;
    
	public int id;
	public string itemName;
	public bool isExpired = false;
	//Expired means it's picked up. Trying to avoid duplicates
	public Rigidbody2D rb;
	public bool isActive;
	//not active until hit ground
//	public PhotonView photonView;
	public Collider2D col;
	public int layerIndex = 1;

	private void Awake ()
	{
		col.enabled = true;
	}

	void Start ()
	{
	//	photonView = GetComponent<PhotonView> ();

		rb = GetComponent<Rigidbody2D> ();
		//gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		//Physics.IgnoreLayerCollision(0,1,true); //PLAYER IS ON THIS ONE
		Physics.IgnoreLayerCollision (0, 2, true);
		Physics.IgnoreLayerCollision (0, 3, true);
		Physics.IgnoreLayerCollision (0, 4, true);
		Physics.IgnoreLayerCollision (0, 5, true);
		Physics.IgnoreLayerCollision (0, 6, true);
		Physics.IgnoreLayerCollision (0, 7, true);
		//Physics.IgnoreLayerCollision(0,8,true); //FLOOR
		Physics.IgnoreLayerCollision (0, 9, true);
		Physics.IgnoreLayerCollision (0, 10, true);


	}
	public AdventurerNetwork myAtn;

	void OnTriggerEnter2D(Collider2D other) {
		//BattleManager.Instance.SpawnWave (wave);
myAtn=other.GetComponent<AdventurerNetwork>();
		if (myAtn == BattleManager.Instance.myAdventurer) {
			//Debug.Log ("Triggered the item pickup");
			currentTarget = myAtn.gameObject;
			isMoving = true;
		}
	}

	GameObject currentTarget=null;
	bool isMoving=false;
	void Update(){
		//	Raycasts ();
		if (rb.velocity.y <= -11)
			Land ();
		if (isMoving){
			
			MoveToTarget ();
	}
	}

	void MoveToTarget(){
		transform.position = Vector3.MoveTowards (transform.position, currentTarget.transform.position, 10*Time.deltaTime);
		CheckArrived ();
	}

	void CheckArrived(){
		if (Vector2.Distance (transform.position, currentTarget.transform.position) < .5f) {
			PickItUp ();
		}
	}

	public Collider2D myCollider;
	void Raycasts()
	{
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), Vector2.zero);
		//RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		if (hit.collider ==myCollider) {
			PickItUp ();
		} 
	}

	[PunRPC]
	public void SetItem (ItemType _itemType, int _id)
	{
		id = _id;

		itemType = _itemType;
		if (itemType == ItemType.junk) {
			Item _item = InventoryManager.Instance.junkDb [id];
			icon.sprite = _item.icon;
			itemName = _item.name;
			SetColor (_item);
		}
		if (itemType == ItemType.costume) {
			Item _item = InventoryManager.Instance.costumesDb [id];
			icon.sprite = _item.icon;
			itemName = _item.name;
			SetColor (_item);
		}
		if (itemType == ItemType.weapon) {
			Item _item = InventoryManager.Instance.weaponsDb [id];
			icon.sprite = _item.icon;
			itemName = _item.name;
			SetColor (_item);
		}
		if (itemType == ItemType.equipment) {
			Item _item = InventoryManager.Instance.equipmentDb [id];
			icon.sprite = _item.icon;
			itemName = _item.name;
			SetColor (_item);
		}
		//  Debug.Log("How far");
	}

	void SetColor (Item _item)
	{
		//     Debug.Log("******** SetColor");
		if (_item.itemGrade == ItemGrade.ordinary) {
			cube.color = ordinary;
		}
		if (_item.itemGrade == ItemGrade.rare) {
			cube.color = rare;
		}
		if (_item.itemGrade == ItemGrade.special) {
			cube.color = special;
		}
		if (_item.itemGrade == ItemGrade.unique) {
			cube.color = unique;
		}
		if (_item.itemGrade == ItemGrade.veryrare) {
			cube.color = veryrare;
		}
	}

//	void OnCollisionEnter (Collision collisionInfo)
//	{
//		isActive = true;
//	}



	void Land(){
		isActive = true;
		rb.gravityScale = 0;
		rb.velocity = Vector2.zero;
	}

	public LayerMask mask;

	void PickItUp ()
	{
		if (isExpired)
			return;
		if (!isActive)
			return;
		//  gameObject.SetActive(false);
		//call to all to destroy it
		//if (photonView == null)
		//	photonView = GetComponent<PhotonView> ();
		CollectAndNotify ();

	}

   
	public void CollectAndNotify ()
	{
		if (!isCollectCool)
			return; // prevent duplicate pickups
		//Make IsCollect cool early

		if (GameManager.Instance.currentScene == CustomSceneType.Battle) {//CANT GET STUFF YOUR DEAD!
			if (BattleManager.Instance.myAdventurer.charState == CharacterState.dead)
				return;
		}

		isCollectCool = false;

		if (PhotonNetwork.playerList.Length > 1)
			DropManager.Instance.CallItemPickupCooldown ();

		PlaySoundByGrade ();
		//if (photonView == null)
		//	photonView = GetComponent<PhotonView> ();

		InventoryManager.Instance.AddItem (itemType, id);
		Item _item = InventoryManager.Instance.GetItem (itemType, id);
		string colorName = "white";

		if (_item.itemGrade == ItemGrade.elite)
			colorName = "yellow";
		if (_item.itemGrade == ItemGrade.special)
			colorName = "yellow";
		if (_item.itemGrade == ItemGrade.unique)
			colorName = "06C355FF";
		if (_item.itemGrade == ItemGrade.rare)
			colorName = "0061FFFF";
		if (_item.itemGrade == ItemGrade.veryrare)
			colorName = "F100FFFF";
		//override all
		colorName = "green";
		string msg = GameManager.Instance.PlayerName + " got a " + " [" + itemName + "]";

		if (_item.itemType != ItemType.junk)
		BattleManager.Instance.myAdventurer.photonView.RPC("CallChatLog",PhotonTargets.All,msg);
			//photonView.RPC ("CallChatLog", PhotonTargets.All, msg);
		InventoryCanvas.Instance.AddItemSlot (_item, 1);

		HideItem();
		//photonView.RPC ("HideItem", PhotonTargets.All);
	}

	bool isCollectCool = true;

	IEnumerator CollectCooldown ()
	{
		isCollectCool = false;
		yield return new WaitForSeconds (1);
		col.enabled = false;//get it out of the way for other BC

		yield return new WaitForSeconds (4);
		isCollectCool = true;
	}


	void PlaySoundByGrade ()
	{
		Item _item = null;
		if (itemType == ItemType.junk) {
			_item = InventoryManager.Instance.junkDb.Where (x => x.ID == id).FirstOrDefault ();
		}
		if (itemType == ItemType.costume) {
			_item = InventoryManager.Instance.costumesDb.Where (x => x.ID == id).FirstOrDefault ();
		}
		if (itemType == ItemType.weapon) {
			_item = InventoryManager.Instance.weaponsDb.Where (x => x.ID == id).FirstOrDefault ();
		}
		if (itemType == ItemType.equipment) {
			_item = InventoryManager.Instance.equipmentDb.Where (x => x.ID == id).FirstOrDefault ();
		}

		if (_item.itemGrade == ItemGrade.rare || _item.itemGrade == ItemGrade.veryrare || _item.itemGrade == ItemGrade.elite || _item.itemGrade == ItemGrade.unique) {
			AudioManager.Instance.PlayClip ("rareitem", true, false);
		} else {
			AudioManager.Instance.PlayClip ("ordinaryitempickup", true, false);
		}
	}



	void OnDisable ()
	{
		StopAllCoroutines ();
	}

	[PunRPC]
	public void HideItem ()
	{
		StartCoroutine (CollectCooldown ());
		//Debug.Log("ViewID of Bone:" + photonView.viewID);
		Animator anim = GetComponent<Animator> ();
		anim.CrossFade ("ItemPickedUp", 0);
		Invoke ("DelayedHideItem", 3);
	}

	public void DelayedHideItem ()
	{
		isMoving = false;
		StopAllCoroutines ();
		gameObject.SetActive (false);
	}


	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			
		} else {


		}
	}

}
