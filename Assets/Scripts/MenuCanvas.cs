﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuCanvas : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	private static MenuCanvas _instance;

	public static MenuCanvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<MenuCanvas> ();
			return _instance;
		}
	}

	public bool menuCanvasOpen = true;
	public CanvasGroup cg;
	public Text onlineCounter;
	public Text versionNumber;

	void Awake ()
	{
		cg = GetComponent<CanvasGroup> ();
		versionNumber.text="V "+LoginGUI.Instance.VERSION_NUMBER;
	}

	void Update ()
	{

		onlineCounter.text = PhotonNetwork.countOfPlayers.ToString ();
	//	Debug.Log ("PhotonNetwork:" + PhotonNetwork.playerList.Length);
	}

//	public override void OnLobbyStatisticsUpdate ()
//	{
//		Debug.Log ("count:" + PhotonNetwork.countOfPlayers.ToString () + " Players Online");
//	}

	public void OnPointerClick (PointerEventData eventData)
	{
		AudioManager.Instance.PlayClip ("click", true, false);

	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		AudioManager.Instance.PlayClip ("hover", true, false);
	}

	public void OnPointerExit (PointerEventData eventData)
	{
		ToolTipCanvas.Instance.ToggleMe (false);
	}

	bool m_isDrop = false;

	public void InventoryClicked ()
	{
		InventoryCanvas.Instance.ToggleEquipmentSlots (true);
		InventoryCanvas.Instance.ToggleEquipmentStats (true);
		InventoryCanvas.Instance.ToggleEquipment (true);
		Debug.Log ("InventoryClicked m_drop:" + m_isDrop);
		if (m_isDrop == true) {//only call this if you need to
			EventManager.Instance.CallInventoryStyleChange (false);
		}
		m_isDrop = false;
		ToggleMe (false);
	}

	public void InventoryTradeClicked ()
	{
		InventoryCanvas.Instance.ToggleEquipmentSlots (true);
		InventoryCanvas.Instance.ToggleEquipmentStats (true);
		InventoryCanvas.Instance.ToggleEquipment (true, true);
		if (m_isDrop == false) {//only call this if you need to
			EventManager.Instance.CallInventoryStyleChange (true);
		}
		m_isDrop = true;
		ToggleMe (false);
	}

	public void SettingsClicked ()
	{
		InventoryCanvas.Instance.ToggleEquipmentSlots (false);
		InventoryCanvas.Instance.ToggleEquipmentStats (false);
		InventoryCanvas.Instance.ToggleEquipment (false);
		SettingsCanvas.Instance.ToggleMe (true, false);
		ToggleMe (false);
	}

	public void SkillTreeClicked ()
	{
		InventoryCanvas.Instance.ToggleEquipmentSlots (false);
		InventoryCanvas.Instance.ToggleEquipmentStats (false);
		InventoryCanvas.Instance.ToggleEquipment (false);
		SkillTreeCanvas.Instance.ct.ToggleMe (true);

		SettingsCanvas.Instance.ToggleMe (false, false);
		ToggleMe (false);
	}

	public void CrownMallClicked ()
	{
		InventoryCanvas.Instance.ToggleEquipmentSlots (false);
		InventoryCanvas.Instance.ToggleEquipmentStats (false);
		InventoryCanvas.Instance.ToggleEquipment (false);
		SettingsCanvas.Instance.ToggleMe (false, false);
		ToggleMe (false);
		CrownMallManager.Instance.ToggleMe (true);
		AudioManager.Instance.PlayClip ("click", true, false);

	}


	public void ToggleMe(){
		ToggleMe (!menuCanvasOpen);
	}

	public void ToggleMe (bool b)
        //show/hide menu
	{
		//JUST TO KEEP MENU DOWN DURING VOID
		if (GameManager.Instance.VoidObject.activeSelf)
			return;


		if (b) {
			cg.alpha = 1;
		} else {
			cg.alpha = 0;
		}

		menuCanvasOpen = b;
		cg.blocksRaycasts = b;
		cg.interactable = b;
	}
}
