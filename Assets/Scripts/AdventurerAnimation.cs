﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AdventurerAnimation : AnimationController
{
    AdventurerNetwork adventurerNetwork;
    public string currentAnimation;
    public GameObject particleLeftHand, particleRightHand;

    new void Start()
    {
        adventurerNetwork = GetComponentInParent<AdventurerNetwork>();
        base.Start();
    }

    public void SyncAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        if (base.skeletonAnimation != null)
        {

            if (animation == base.skeletonAnimation.AnimationName)
            {
                //Debug.Log("Same Animation return;");
                return;
            }

            //NOW CHECK FOR DOUBLE ATTACKS BUT DIFF NUMBERS
            if (base.skeletonAnimation.AnimationName != null)
            {
                if (animation.Contains("Attack") & base.skeletonAnimation.AnimationName.Contains("Attack"))
                {
                    //Debug.Log("Two Attacks Same Animation - return");
                    return;
                }
            }
        }
        base.SetAnimation(animation, isLoop, isOnComplete);
    }

    bool syncAnimTownCool = true;

    public void SyncAnimationTown(string animation, bool isLoop, bool isOnComplete)
    {
        if (syncAnimTownCool = false)
            return;
        //  if (animation == currentAnimation) return;//no need to try setting repeated animation of same kind+
        if (base.skeletonAnimation != null)
        {
            if (base.skeletonAnimation.AnimationName != null)
            {
                if (animation == base.skeletonAnimation.AnimationName)
                {
                    //Debug.Log("Same Animation return;");
                    return;
                }
            }
        }
        base.SetAnimationTown(animation, isLoop, isOnComplete);
    }

    IEnumerator SyncAnimTownCooldown()
    {
        syncAnimTownCool = false;
        yield return new WaitForSeconds(.5f);
        syncAnimTownCool = true;
    }

    bool LockeForever = false;

    public override void SetAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        if (LockeForever) return;
        //use this when we need to know what animation we are in certainly
        string realAnim = base.skeletonAnimation.AnimationName;

        //struggling with jilted attacks
        if (realAnim!=null)
        {
            if (realAnim.Contains("Attack"))
            {
                if (animation.Contains("Idle") & !isOnComplete) return;
            }
        }

        if (adventurerNetwork != null)
        {
            if (!animation.Contains("Roll"))
                adventurerNetwork.battleMover.isRolling = false;
        }

        //NOTHING FIXES DEATH
        if (skeletonAnimation == null) return;
        if (skeletonAnimation.AnimationName == null)
            return;
        if (skeletonAnimation.AnimationName.Contains("Death"))
        {
            return;
        }
        if (currentAnimation.Contains("Death"))
        {
            //HARD CODE TO STOP DEATH OVERRIDE
            return;
        }

        if (adventurerNetwork == null)
            adventurerNetwork = GetComponentInParent<AdventurerNetwork>();

        //If this is not death animation call, if the character is dead - only idle is allowed to revive him
        if (!animation.Contains("Death"))
        {
            if (adventurerNetwork.charState == CharacterState.dead)
                return; //IF ITS DEATH DONT ALLOW CHANGE ANIMATION
        }else
        {
            LockeForever = true;//if we are calling death anim - lock it forever
        }

        if (animation.Contains("Hit"))
        {        //NEED TO PREVENT OVERRIDING ATK

            AttackGraphic();

            if (!currentAnimation.Contains("Idle"))
            {
                Debug.Log("----------TRIED TO INTERRUPT ADVENTURER ANIM WITH A HIT");
                return;//Only can play hit whne you're in Idle!
            }
        }

        //call the same animation for all others
        object[] args = new object[] { animation, isLoop, isOnComplete };
        //Debug.Log("CallSyncAnimation:"+animation);
        adventurerNetwork.photonView.RPC("CallSyncAnimation", PhotonTargets.Others, args);
        if (!adventurerNetwork.photonView.isMine)
            return;
        //Debug.Log("Set Animation:" + animation);

        if (currentAnimation.Contains("Attack"))
        {
            //Debug.Log("4.1 Tanimation is stuck cuz it thinks its in attack but its not"); 
            if (!realAnim.Contains("Attack"))
            {
                string animString = adventurerNetwork.stance + "-Idle";
                Debug.Log("##### Specail Block of anim attack fired");
                currentAnimation = animString;
            }
            if (!isOnComplete)
            { //if it's not an Oncomplete animation
                if (!animation.Contains("Death"))
                    return; //if its not an oncomplete and not a death - its not allowed
            }
            adventurerNetwork.StartAttackCooldown();
        }

        if (base.skeletonAnimation != null)
        {
            if (animation == base.skeletonAnimation.AnimationName)
            {
                //    Debug.Log("Same Animation return;");
                return;
            }
        }

        //CREATE AUDIO FOOTSTEPS
        if (animation.Contains("Idle"))
        {
            //   Debug.Log("Stop Run Clip");

            AudioManager.Instance.StopClip("townrun");
        }
        if (animation.Contains("Run"))
        {
            //  Debug.Log("Start Run Clip");

            AudioManager.Instance.PlayClip("townrun", true, true);
        }

        base.SetAnimation(animation, isLoop, isOnComplete);
        currentAnimation = animation;

        //HARD CODE A PARTICLE EFFECT FOR BOW 2
        if (animation == "Bow-Attack2")
        {
            Invoke("CallLateJumpFeathers", .5f);
        }

        //LOCK death
        if (animation.Contains("Death"))
        {
            adventurerNetwork.enabled = false;
            this.enabled = false;
        }
    }

    void CallLateJumpFeathers()
    {
        adventurerNetwork.photonView.RPC("CallJumpFeathers", PhotonTargets.All, adventurerNetwork.photonView.viewID, transform.position);
    }

    public override void ForceAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        //NOTHING OVERRIDES DEATH
        if (skeletonAnimation.AnimationName.Contains("Death"))
        {
            return;
        }
        if (currentAnimation.Contains("Death"))
        {
            return;
        }
        if (LockeForever)
        {
            return;
        }

        //ONLY RULE IS DONT OVERRIDE SAME ANIMATION
        if (base.skeletonAnimation != null)
        {
            if (animation == base.skeletonAnimation.AnimationName)
            {
                Debug.Log("Same Animation return;");
                return;
            }
        }

        base.SetAnimation(animation, isLoop, isOnComplete);
        currentAnimation = animation;
    }


    AdventurerTownNetwork atn;

    public override void SetAnimationTown(string animation, bool isLoop, bool isOnComplete)
    {
        if (atn == null)
            atn = GetComponentInParent<AdventurerTownNetwork>();

        object[] args = new object[] { animation, isLoop, isOnComplete };
        atn.photonView.RPC("CallSyncAnimation", PhotonTargets.Others, args);
        if (!atn.photonView.isMine)
            return;

        //Debug.Log("Set Animation:" + animation);

        if (base.skeletonAnimation != null)
        {
            if (base.skeletonAnimation.AnimationName != null)
            {
                if (animation == base.skeletonAnimation.AnimationName)
                {
                    //Debug.Log("Same Animation return;");
                    return;
                }
                else
                {
                    if (animation.Contains("Idle"))
                    {
                        //   Debug.Log("Stop Run Clip");

                        AudioManager.Instance.StopClip("townrun");
                    }
                    if (animation.Contains("Run"))
                    {
                        //  Debug.Log("Start Run Clip");

                        AudioManager.Instance.PlayClip("townrun", true, true);
                    }

                }
            }
        }

        base.SetAnimationTown(animation, isLoop, isOnComplete);
        currentAnimation = animation;
    }

    public void AttackGraphic()
    {
        GameObject obj = ObjectPool.Instance.NextObject("RedHit");
        Vector3 newPos = adventurerNetwork.transform.position;
        newPos.z -= 1;
        obj.transform.position = newPos;
        obj.SetActive(true);

    }

    public override void AnimationComplete(int trackIndex, string animationName)
    {
        //print("Adventurer animation complete:" + animationName);.
		if (GameManager.Instance.currentScene == CustomSceneType.Battle){
			if (!adventurerNetwork.photonView.isMine)
				return;
		}

        if (animationName.Contains("Roll"))
        {
			if (GameManager.Instance.currentScene ==CustomSceneType.Battle)
            adventurerNetwork.battleMover.isRolling = false;
			if (GameManager.Instance.currentScene == CustomSceneType.Town) {
				SimpleMover sm = GetComponentInParent<SimpleMover>();
				sm.isRolling = false;
				return;
			}
        }

        string animString = adventurerNetwork.stance + "-Idle";
        switch (animationName)
        {
            case "Onehander-Death":
                animString = "";
                break;
        }

        if (animString != "")
        {
            SetAnimation(animString, true, true);
        }
    }

    public override void AnimationOnHit(string animation)
    {
        // if (!adventurerNetwork.photonView.isMine) return;
        if (!adventurerNetwork.photonView.isMine)
            return;

        if (animation.Contains("Attack"))
        {
			if (animation.Contains("Cannon")){
				adventurerNetwork.CallPushBack (adventurerNetwork.cannonKnockback);
			}
				
            //    Debug.Log("************** ON HIT ATTACk FROM ADVENTURER");
            AudioManager.Instance.PlayAttackSound(GameManager.Instance.equippedWeapon.weaponSound);


            if (adventurerNetwork.lastHit)
            {
                EnemyNetwork en = adventurerNetwork.currentTarget;

                if (en == null)        //TRYING TO PICK A NEW ENEMY
                {
                    int _side = 1;
                    
                    if (adventurerNetwork.lastEnemyX < transform.position.x)
                        _side = 0;

                    en = BattleManager.Instance.GetRandomEnemyBySide(_side);

                    if (en == null) return;
                }
                en.photonView.RPC("Hit", PhotonTargets.All, en.photonView.viewID);
                float damage = Random.Range(adventurerNetwork.currentEntityStat.atkMin, adventurerNetwork.currentEntityStat.atkMax);
                bool isCrit = adventurerNetwork.RollForCritical();

                if (isCrit & en.enemyType == EnemyType.boss)
                {
                    CameraEffects ce = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraEffects>();
                    ce.CallCameraShake(1f);
                }

                if (isCrit)
                {
                    //PickNewTarget ();
                    int chanceAggro = Random.Range(0, 4);
                    if (chanceAggro == 1)
                    {
                        en.currentTarget = adventurerNetwork;
                    }
                }

                //CALCULATING JUICED ADJUSTMENT
                if (adventurerNetwork.isJuiced)
                {
                    float juicedDamage = damage * adventurerNetwork.juicedPercent;
                    damage = damage + juicedDamage;
                }

                //Debug.Log("Adventurer RollForCrit:" + isCrit);
                if (isCrit)
                    damage = damage * adventurerNetwork.currentEntityStat.critPower;

                //CALCULATING WEAKPOINTS
                damage = damage * adventurerNetwork.lastDamageAdjustmentPercent;

                damage = damage - (Random.Range(en.currentEntityStat.defMin, en.currentEntityStat.defMax));
                if (damage <= 0)
                    damage = 1;

                bool isSilent = false;
                object[] args = new object[] {
                    damage,
                    adventurerNetwork.currentTarget.photonView.viewID,
                    isSilent,
                    isCrit,
                    adventurerNetwork.photonView.viewID,
                    adventurerNetwork.lastDamageAdjustmentPercent
                };
                adventurerNetwork.currentTarget.photonView.RPC("TakeDamageMaster", PhotonTargets.MasterClient, args);
				if (GameManager.Instance.equippedWeapon.stat.knockbackPower>0)adventurerNetwork.currentTarget.photonView.RPC("CallPushBack",PhotonTargets.MasterClient, GameManager.Instance.equippedWeapon.stat.knockbackPower,adventurerNetwork.facing);

                AudioManager.Instance.PlayClip(GameManager.Instance.equippedWeapon.weaponSound);


                //if the stance has AOE - address it here
                StanceInfo stanceInfo = new StanceInfo();
                stanceInfo = stanceInfo.GetStanceInfo(adventurerNetwork.stance);

                if (stanceInfo.AOEbySide)
                {//We are an AOE by side so we have to damage enemies other than the one which was clicked
                 //Get the right side to send
                    int side = 0;//ASSUME LEFT 
                    if (adventurerNetwork.currentTarget.transform.position.x > transform.position.x)
                    {
                        side = 1;//OVERRIDE RIGHT
                    }
					adventurerNetwork.photonView.RPC("CallAOEBySideMaster", PhotonTargets.MasterClient, adventurerNetwork.currentTarget.photonView.viewID, side, stanceInfo.AOENumberEnemies, stanceInfo.AOEPerc, adventurerNetwork.equippedWeapon,GameManager.Instance.equippedWeapon.stat.knockbackPower);
                }
            }
        }
    }

    public void SetParticles(Item _item)
    {
        bool isLeftVisi = true;
        bool isRightVisi = true;

        if (_item == null)
        {
            isLeftVisi = false;
            isRightVisi = false;
        }
        else
        {
            //Debug.Log ("Trying to set it to:" + _item.weaponParticles);
            if (_item.weaponParticles == "")
            {
                isLeftVisi = false;
                isRightVisi = false;
            }
        }

        //Turn off all the onehanders
        if (_item.stance == Stance.Onehander)
        {
            isRightVisi = false;
        }
        if (_item.stance == Stance.Bow)
        {
            isRightVisi = false;
        }
        if (_item.stance == Stance.Cannon)
        {
            isRightVisi = false;
        }
        if (_item.stance == Stance.Rod)
        {
            isLeftVisi = false;
        }

        // Debug.Log("VISIBILITY:" + isVisi);


        particleLeftHand.SetActive(isLeftVisi);
        particleRightHand.SetActive(isRightVisi);

        //try to set correct color
        if (isLeftVisi)
        {
            WeaponParticleSelector wps = particleLeftHand.GetComponent<WeaponParticleSelector>();
            wps.ToggleParticleByColor(_item.weaponParticles);
        }
        if (isRightVisi)
        {
            WeaponParticleSelector wps = particleRightHand.GetComponent<WeaponParticleSelector>();
            wps.ToggleParticleByColor(_item.weaponParticles);
        }

    }

    public void SetSkin(string skin)
    {
        // Debug.Log("SetSkin:"+skin);
        SkeletonAnimation skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();
        if (skin == "")
        {
            //    Debug.Log("SetSkin = ''");
            skin = "Default";
        }

        try
        {
            skeletonAnimation.skeleton.SetSkin(skin);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex);
        }
    }

    public void SetAttachment(string slotName, string slotName2, string attachmentName, string attachmentName2)
    {
        if (GameManager.Instance.currentScene == CustomSceneType.Battle)
        {
            Debug.Log("AdventurerID:" + adventurerNetwork.photonView.viewID + " SlotName:" + slotName + " attachmentName:" + attachmentName);
            Debug.Log("AdventurerID:" + adventurerNetwork.photonView.viewID + " SlotName:" + slotName2 + " attachmentName:" + attachmentName2);
        }
        SkeletonAnimation skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();

        //slot 1
        try
        {
            skeletonAnimation.skeleton.SetAttachment(slotName, attachmentName);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex);
        }

        //slot 2
        if (slotName2 == "NOSLOT")
            return;
        try
        {
            skeletonAnimation.skeleton.SetAttachment(slotName2, attachmentName2);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex);
        }
    }


}
