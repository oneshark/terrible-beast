using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
	public GameObject[] wayPoints;
	public PhotonView m_PhotonView;

	public float moveSpeed = 1.5f;
	public float waitTime = 6;
	float arrivalDistance = 1;

	public GameObject m_waypoint;
	public bool isWaiting = true;
	public bool goingUp=true;
	public bool goingDown=false;



	void Awake ()
	{
		m_PhotonView = GetComponent<PhotonView> ();
	}

	public int elevatorDirection = 1;


	[PunRPC]
	public void ReverseElevator(){
		if (!isWaiting)
			return;
		if (elevatorDirection == 0) {
			CallElevatorUp ();
			elevatorDirection = 1;
		} else {
			CallElevatorDown ();
			elevatorDirection = 0;
		}
		isWaiting = false;
	}

	void CallElevatorDown ()
	{
		m_waypoint = wayPoints [0];
		isWaiting = false;
	}

 void CallElevatorUp ()
	{
		m_waypoint = wayPoints [1];
		isWaiting = false;
	}


	void Update ()
	{
		if (!PhotonNetwork.isMasterClient)
			return;
		if (!isWaiting & m_waypoint!=null)
			Lerp ();
	}

	//IF WE ARE NOT WAITING, DO THIS
	void Lerp ()
	{
		if (Vector3.Distance (transform.position, m_waypoint.transform.position) < arrivalDistance) {
			Finished ();
			//MoveToTarget ();
		} else { 
			Vector3 targetPos = m_waypoint.transform.position;
			transform.position = Vector3.MoveTowards (transform.position, targetPos, Time.deltaTime * moveSpeed);
		}
	}


	void Finished ()
	{
		isWaiting = true;
	}
}
