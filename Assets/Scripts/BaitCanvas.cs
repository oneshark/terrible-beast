﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaitCanvas : MonoBehaviour
{

	#region singleton

	private static BaitCanvas _instance;

	public static BaitCanvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<BaitCanvas> ();
			return _instance;
		}
	}
	//Singleton complete

	#endregion

	#region CanvasSetup

	public CanvasTools ct;

	void Awake ()
	{
		ct = GetComponent<CanvasTools> ();
	}

	#endregion


	public void Close ()
	{
		AudioManager.Instance.PlayClip ("click",true,false);

		MenuCanvas.Instance.ToggleMe (true);
		GameManager.Instance.InputEnabled = true;
		ct.CallFadeCanvas (0);
	}
}
