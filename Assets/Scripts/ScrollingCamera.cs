﻿using UnityEngine;
using System.Collections;

public class ScrollingCamera : MonoBehaviour
{
    public float minXLocation, origMinX;
    public float maxXLocation, origMaxX;
    public float minYLocation, origMinY;
    public float maxYLocation, origMaxY;

    [Header("Distances")]
    public float xDist, yDist;
    public float permanentZLocation;

    public float speed = .25f;

    void Awake()
    {
        InitOriginalPlacementVars();
    }

    void InitOriginalPlacementVars()
    {
        origMinX = minXLocation;
        origMaxX = maxXLocation;
        origMinY = minYLocation;
        origMaxY = maxYLocation;
    }

    public void ResetOriginalPlacementVars()
    {
        minXLocation = origMinX;
        maxXLocation = origMaxX;
        minYLocation = origMinY;
        maxYLocation = origMaxY;
    }

    public void SetPlacementVars(Vector3 pos)
    {
        minXLocation = pos.x-xDist;
        maxXLocation = pos.x+xDist;
        minYLocation = pos.y-yDist;
        maxYLocation = pos.y+yDist;
    }

    void Update()
    {
        float mousePositionX = Input.mousePosition.x / Screen.width;
        float mousePositionY = Input.mousePosition.y / Screen.width;

        if (mousePositionX < 0)
            mousePositionX = 0;
        else if (mousePositionX > 1)
            mousePositionX = 1;

        if (mousePositionY < 0)
            mousePositionY = 0;
        else if (mousePositionY > 1)
            mousePositionY = 1;

        float differenceX = this.maxXLocation - this.minXLocation;
        float differenceY = this.maxYLocation - this.minYLocation;

        Vector3 position = this.transform.position;
        position.x = minXLocation + differenceX * mousePositionX;
        position.y = minYLocation + differenceY * mousePositionY;

        //extended this to use the z value
        if (transform.position.z > permanentZLocation + .2f) position.z = position.z - .7f;
        if (transform.position.z < permanentZLocation - .2f) position.z = position.z + .7f;

        transform.position = Vector3.Lerp(transform.position, position, speed);
        //this.transform.position = position;   
    }

    public void callSlowdown(float _speed, float _duration)
    {
        StartCoroutine(slowdown(_speed, _duration));
    }

    IEnumerator slowdown(float _speed, float _duration)
    {
        float originalSpeed = speed;
        speed = _speed;
        yield return new WaitForSeconds(_duration);
        speed = originalSpeed;
    }

}
