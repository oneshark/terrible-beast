﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FirstTimeCanvas : MonoBehaviour
{
	public InputField nameInput;
	public Face selectedFace;



	void Start ()
	{
		nameInput.text = "";
	}


	public void SetSelectedFace (int id)
	{
		selectedFace = InventoryManager.Instance.GetFace (id);
	}
	public string choiceWeapon="sword";
	public void TrySubmit (string _weaponChoice)
	{
		choiceWeapon=_weaponChoice;
		if (nameInput.text == "") {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Please choose a", "Name");
			puc.SetRejectIcon ();
			puc.SetTextColor ("blue");
			nextObj.SetActive (true);
			return;
		}

		DoesPlayerNameExist (Submit);
	}

	public void Submit ()
	{
		
		if (choiceWeapon == "sword") {
			Item newItem = InventoryManager.Instance.weaponsDb.Where (x => x.ID == 57).FirstOrDefault ();
			InventoryManager.Instance.AddItem (ItemType.weapon, 57);
			InventoryCanvas.Instance.AddItemSlot (newItem, 1);
			InventoryCanvas.Instance.EquipLastAddedItem ();

			//   GameManager.Instance.ChangeEquipment(newItem, TownManager.Instance.myAtn.photonView);

		}
		if (choiceWeapon == "bow") {
			Item newItem = InventoryManager.Instance.weaponsDb.Where (x => x.ID == 45).FirstOrDefault ();
			InventoryManager.Instance.AddItem (ItemType.weapon, 45);
			InventoryCanvas.Instance.AddItemSlot (newItem, 1);
			InventoryCanvas.Instance.EquipLastAddedItem ();
			//  GameManager.Instance.ChangeEquipment(newItem, TownManager.Instance.myAtn.photonView);
		}
		GameManager.Instance.PlayerName = nameInput.text;
		PhotonView _photonView = TownManager.Instance.myAtn.photonView;
		GameManager.Instance.ChangeFace (selectedFace, _photonView);
		if (GameManager.Instance.currentScene == CustomSceneType.Town) {
			TownManager.Instance.myAdventurerTownNetwork.CallBroadcastName ();
		}

		EventManager.Instance.CallInvasionLocks ();
		GameManager.Instance.InputEnabled = true;
		gameObject.SetActive (false);
	}


	public void DoesPlayerNameExist (System.Action followingMethod)
	{
		Debug.Log ("DOES USER EXIST");

		Dictionary<string, string> formPost = new Dictionary<string, string> ();
		formPost.Add ("playerName", nameInput.text);
		StartCoroutine (HandleWWWGetDoesPlayerNameExist ("http://tod.dx.am/Main/DoesPlayerNameExist.php", formPost, followingMethod));
	}

	bool playerExists = true;

	IEnumerator HandleWWWGetDoesPlayerNameExist (string url, Dictionary<string, string> post, System.Action followingMethod)
	{
		Debug.Log ("HANDLE DOES PlayerName EXIST");

		playerExists = true;//this tells us if this user is in the db
		WWWForm form = new WWWForm ();
		foreach (KeyValuePair<String, String> post_arg in post) {
			form.AddField (post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW (url, form);
		yield return www;

		//GameObject registeredItemCanvas = (GameObject)Instantiate (Resources.Load ("ItemRegisteredCanvas"));
		Debug.Log ("GetUserExist - www:" + www.text);
		if (www.text == "0") {
			playerExists = false;
		} else if (www.text == "1") {
			playerExists = true;
		} 


		if (playerExists) {
			Debug.Log ("Mercenary name already taken.");
			PopPlayerNameTaken ();
		} else {
			if (followingMethod != null)
				followingMethod ();
		}
		//   JsonUtility.FromJson(www.text);
	}


	public void PopPlayerNameTaken ()
	{
	//put a reject audio here
		GameObject newObj = (GameObject)Instantiate (Resources.Load ("PopUpCanvas"), new Vector3 (0, 0, 0), Quaternion.identity);
		PopUpCanvas puc = newObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("Mercenary Name", "Taken!");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
	}

	public void SetFaceButtons ()
	{
		GameObject[] obj = GameObject.FindGameObjectsWithTag ("FirstTimeFaceButton");
		foreach (GameObject nextObj in obj) {
			
			nextObj.GetComponent<FirstTimeFaceButton> ().SetFace ();
		}
	}
}
