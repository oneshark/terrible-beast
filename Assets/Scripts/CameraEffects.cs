﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class CameraEffects : MonoBehaviour
{
	public Grayscale grayScale;
	public BlurOptimized blur;
	public Animator anim;
	public SmoothFollow smoothFollow;
	public SmoothFollowRandomizer smoothFollowRandomizer;
	public ScrollingCamera scrollingCamera;
	bool isShaking = false;
	public GameObject featherSplosion;

	public void ActivateDeactivate (string sceneName)
	{
		if (sceneName == "Town") {
			Follow ();
		}
		if (sceneName == "Battle") {
			Scroll ();
		}
	}

	public void BustFeatheryLoad ()
	{
		featherSplosion.SetActive (true);
	}

	public void Scroll ()
	{
		CancelInvoke ("Follow");//if we are waiting on it, cancel
		smoothFollow.enabled = false;
		scrollingCamera.enabled = true;
	}

	public void ScrollLocal (Vector3 pos)
	{
		CancelInvoke ("Follow");//if we are waiting on it, cancel
		smoothFollow.enabled = false;
		scrollingCamera.enabled = true;
		scrollingCamera.SetPlacementVars (pos);
	}

	public bool isZoomed = false;

	public void SetScrollValues (float permZ)
	{
		scrollingCamera.permanentZLocation = permZ;
	}

	public void InvokeFollow (float f)
    //Just like it reads
	{
		Invoke ("Follow", f);
	}

	public void Follow ()
	{
		smoothFollow.enabled = true;
		scrollingCamera.enabled = false;
		scrollingCamera.ResetOriginalPlacementVars ();
	}

	public void ToggleGrayscale (bool b)
	{
		grayScale.enabled = b;
	}

	public void CallCameraShake (float duration)
	{
		if (isShaking)
			return;
		StartCoroutine (cameraShake (duration, .2f));
	}


	IEnumerator cameraShake (float duration, float magnitude)
	{

		isShaking = true;
		Transform mainCam = Camera.main.transform;

		float elapsed = 0.0f;

		while (elapsed < duration) {
			elapsed += Time.deltaTime;

			float percentComplete = elapsed / duration;
			float damper = 1.0f - Mathf.Clamp (4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

			// map value to [-1, 1] 
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			x *= magnitude * damper;
			y *= magnitude * damper;

			mainCam.position = new Vector3 (mainCam.position.x + x, mainCam.position.y + y, mainCam.position.z);

			yield return null;
			isShaking = false;
		}
	}

	public void ToggleBlur (bool b)
	{
		blur.enabled = b;
		if (b) {
			anim.CrossFade ("Blur", 0);
		} else {
			anim.CrossFade ("Empty", 0);
		}
	}

}
