﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponParticleSelector : MonoBehaviour
{
	public GameObject greenParticles;
	public GameObject yellowParticles;
	public GameObject torchParticles;
	public GameObject purpleParticles;

	void Awake(){
		HideAll ();
	}

	public void ToggleParticleByColor (string color)
	{
		HideAll ();
		if (color == "green") {
			greenParticles.SetActive (true);
		}
		if (color == "yellow") {
			yellowParticles.SetActive (true);
		}
		if (color == "torch") {
			torchParticles.SetActive (true);
		}
		if (color == "purple") {
			purpleParticles.SetActive (true);
		}
	}

	public void HideAll ()
	{
		greenParticles.SetActive (false);
		yellowParticles.SetActive (false);
		torchParticles.SetActive (false);
		purpleParticles.SetActive (false);


	}
}
