﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FishingRewardIcon : MonoBehaviour
{

	public Text itemName;
	public Image icon;

	public void SetItem (Item _item)
	{

		icon.sprite =_item.icon;
		itemName.text = _item.name;
	}
}
