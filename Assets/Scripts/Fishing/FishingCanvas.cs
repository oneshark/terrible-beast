﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingCanvas : MonoBehaviour
{
	public GameObject Waiting, Biting, PullOut, PullFish;


	#region singleton

	private static FishingCanvas _instance;

	public static FishingCanvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<FishingCanvas> ();
			return _instance;
		}
	}
	//Singleton complete

	#endregion

	#region CanvasSetup

	public CanvasTools ct;

	void Awake ()
	{
		ct = GetComponent<CanvasTools> ();
	}

	#endregion

	//
	//
	//	void OnGUI ()
	//	{
	//		if (GUI.Button (new Rect (350, 0, 120, 30), "Idle")) {
	//			Wait ();
	//		}
	//		if (GUI.Button (new Rect (350, 50, 120, 30), "Bite")) {
	//			Bite ();
	//		}
	//		if (GUI.Button (new Rect (350, 100, 120, 30), "Pull")) {
	//			Pull ();
	//		}
	//	}

	bool hookedFish = false;

	public void StartTimer ()
	{
	canClick=true;
		CancelInvoke ();
		hookedFish = false;
		//Debug.Log ("*************************Start the timer up to 30s and start Idle");
		float waitTime = Random.Range (5, 20);
		Wait ();
		Invoke ("StartBiting", waitTime);
	}

	void StartBiting ()
	{
		Bite ();

		Invoke ("Reset", 2);

	}

	void Reset ()
	{
		if (hookedFish)
			return;
		canClick = true;
		StartTimer ();
	}

	bool isBiting = false;

	void Bite ()
	{
		if (hookedFish)
			return;
		isBiting = true;
		Biting.SetActive (true);
		Waiting.SetActive (false);
		PullOut.SetActive (false);
		PullFish.SetActive (false);
	}

	void Wait ()
	{
		if (hookedFish)
			return;
		isBiting = false;
		Biting.SetActive (false);
		Waiting.SetActive (true);
		PullOut.SetActive (false);
		PullFish.SetActive (false);
	}

	void Pull ()
	{
		Biting.SetActive (false);
		Waiting.SetActive (false);
		PullOut.SetActive (true);
		PullFish.SetActive (false);
	}

	void DoPullFish ()
	{
		AudioManager.Instance.PlayClip ("fishcatch", true, false);

		hookedFish = true;
		Biting.SetActive (false);
		Waiting.SetActive (false);
		PullOut.SetActive (false);
		PullFish.SetActive (true);
	}

	bool canClick = true;

	public void Click ()
	{
		if (!canClick)
			return;
		canClick = false;
		if (isBiting) {
			DoPullFish ();
			Invoke ("StartFishingPart2", 1);
		} else {
			Pull ();

			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("You Were Too", "Quick");
			puc.SetWormIcon ();
			puc.SetTextColor ("white");
			nextObj.SetActive (true);
			AudioManager.Instance.PlayClip ("fishfail", true, false);
			Invoke ("Reset", 3);
		}
	}

	public void StartFishingPart2 ()
	{
		hookedFish = true;
		GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
		PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("You Hooked a", "Fish!");
		puc.SetFishIcon ();
		puc.SetTextColor ("white");
		nextObj.SetActive (true);
//start fadecanvas FishingCanvasPart2
		Invoke ("CallFade", 2.75f);
		Invoke ("StartFishingPart2Final", 3);
	}

	void CallFade ()
	{
		ct.CallFadeCanvas (0);
	}

	void StartFishingPart2Final ()
	{
		FishingPart2Canvas.Instance.ct.CallFadeCanvas (1);
		FishingPart2Canvas.Instance.StartMoving ();
	}

	public void Close ()
	{
		MenuCanvas.Instance.ToggleMe (true);
		GameManager.Instance.InputEnabled = true;
		GameManager.Instance.SetAllEquipment (TownManager.Instance.myAdventurerTownNetwork.photonView);
		AudioManager.Instance.PlayClip ("click",true,false);

		AudioManager.Instance.StopClip ("fishingambience");

		ct.CallFadeCanvas (0);
	}
}
