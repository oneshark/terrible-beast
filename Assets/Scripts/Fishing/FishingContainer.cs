﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingContainer : MonoBehaviour
{
	public int minLevel, maxLevel;
	public int cost;
	public bool isLocked = false;
	public GameObject lockedObj;

	void Awake ()
	{
		Invoke ("ToggleLocked", 4);
	}

	void OnEnable ()
	{
		EventManager.OnLoadComplete += OnLoadComplete;
		EventManager.OnTownStart += ToggleLocked;

	}

	void OnDisable ()
	{
		EventManager.OnLoadComplete -= OnLoadComplete;
		EventManager.OnTownStart -= ToggleLocked;

	}

	void OnLoadComplete ()
	{
		Invoke ("ToggleLocked", 1);
	}

	public void ToggleLocked ()
	{
		if (GameManager.Instance.baseEntityStat.xpLevel < minLevel) {

			lockedObj.SetActive (true);
			isLocked = true;
		} else {
			lockedObj.SetActive (false);
			isLocked = false;
		}
	}

	public void Clicked ()
	{
		TryBuyBait ();

	}

	void TryBuyBait ()
	{
		if (GameManager.Instance.souls < cost) {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Not Enough", "Souls");
			puc.SetRejectIcon ();
			puc.SetTextColor ("blue");
			nextObj.SetActive (true);
			AudioManager.Instance.PlayClip ("rejection", true, false);

			return;
		}
		BuyBait ();

	}

	void BuyBait ()
	{
		AudioManager.Instance.PlayClip ("click", true, false);
		AudioManager.Instance.PlayClip ("fishingambience", true, true);

		GameManager.Instance.IncSouls (-cost);
		//Debug.Log ("Clicked on fishing section");
		FishingPart2Canvas.Instance.SetBeadSpeed (minLevel);//could send min or max - wouldnt matter
		FishingCanvas.Instance.ct.CallFadeCanvas (1);
		FishingCanvas.Instance.StartTimer ();
		BaitCanvas.Instance.ct.CallFadeCanvas (0);
	}




}
