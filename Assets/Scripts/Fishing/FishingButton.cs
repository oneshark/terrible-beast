﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class FishingButton : MonoBehaviour
{
	public SpriteRenderer sr;
	public Collider myCollider;
	public LayerMask mask;

	void Awake ()
	{
		sr = GetComponent<SpriteRenderer> ();
		myCollider = GetComponent<Collider> ();
		clickCooled = true;
		// Invoke("CheckUnlocked", 4);
	}

	void Update ()
	{
		sr.enabled = GameManager.Instance.InputEnabled;
		Raycasts ();
	}

	void Raycasts ()
	{
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (ray, out hit, 100, mask)) {
			ClickLogic (hit.collider);
		}
	}

	void ClickLogic (Collider col)
	{
		//	Debug.Log ("RAYCAST HIT LOBBY!");
		if (SettingsCanvas.Instance.canvasOpen)
			return;//cant click on it when inventory open!
		if (!GameManager.Instance.InputEnabled)
			return;
		if (InventoryCanvas.Instance.equipmentOpen)
			return;//cant click on it when inventory open!
		if (Input.GetMouseButton (0)) {
			if (col == myCollider) {
				OnClick ();
			}
		}
	}

	public void OnClick ()
	{
		AudioManager.Instance.PlayClip ("click", true, false);
		MenuCanvas.Instance.ToggleMe (false);
		BaitCanvas.Instance.ct.CallFadeCanvas (1);
		GameManager.Instance.InputEnabled = false;
		Debug.Log ("Fishing Idle");
		TownManager.Instance.myAtn.adventurerAnimation.SetAnimationTown ("Fishing-Idle", true, false);

		//UNITY ANALYITICS
		Analytics.CustomEvent ("FISHING_STARTED", new Dictionary<string, object> {
			{ "NAME", GameManager.Instance.PlayerName },
			{ "LVL", GameManager.Instance.baseEntityStat.xpLevel }
		});

		//	TownManager.Instance.myAtn.adventurerAnimation.SetAnimation ("Fishing", false, false);

	}

	public bool clickCooled = true;

	IEnumerator OnClickCooldown ()
	{
		clickCooled = false;
		yield return new WaitForSeconds (.25f);
		clickCooled = true;
	}
}
