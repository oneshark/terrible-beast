﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnemyMovement : MonoBehaviour
{
    public bool enterFromLeft = false;
    public bool enterFromRight = false;
    public EnemyNetwork enemyNetwork;
    public EnemyAnimation enemyAnimation;
    public Vector3 m_target;
    //general movement
    public bool isHome = true;
    public float walkSpeed;

    public GameObject graphics;

    void Start()
    {
        enemyNetwork = GetComponentInParent<EnemyNetwork>();
    }

    void FixedUpdate()
    {
        //EVERYTHING THAT HAPPENS TO NONMASTER ALL HAPPENS HERE
        //EX: HP UPDATES, ANIMATION SYNCHING

        if (!PhotonNetwork.isMasterClient) return;
        //EVERYTHING BELOW HERE HAPPENS ON THE MASTER
        //MOVEMENT + DECISIONS HAPPEN HERE
        if (!isHome)
        {
            if (Vector3.Distance(transform.position, m_target) > .1f)
            {
                Entrance();
            }
            else
            {
                enemyAnimation.SetAnimation("Idle", true, false);
                isHome = true;
            }
        }
    }



   

    public void Entrance()
    {
        Debug.Log("Entrance running");
        Vector3 currentWalk = new Vector3(0, 0, 0);
        if (enterFromLeft) currentWalk=new Vector3(walkSpeed, 0, 0); ;
        if (enterFromRight) currentWalk= new Vector3(walkSpeed*-1,0,0);
        //apply the motion
        transform.position += currentWalk;
        
    }
}

