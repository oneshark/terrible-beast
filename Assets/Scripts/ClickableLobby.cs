﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableLobby : MonoBehaviour
{
    public string mapName;
    public string requiredMapName;
    public Collider myCollider;
    public GameObject lockedPanel, unlockedPanel;
    public bool unlocked = false;
    public bool forceUnlocked = false;
    public LayerMask mask;

    void Awake()
    {

        myCollider = GetComponent<Collider>();
        clickCooled = true;
        // Invoke("CheckUnlocked", 4);
    }

    void OnLoadComplete()
    {
        Invoke("CheckUnlocked", 2);
    }

    void CheckUnlocked()
    {
        //Debug.Log ("CHECK UNLOCKED HAPPENED");
        if (GameManager.Instance.completedThings.Contains(requiredMapName))
        {
            unlocked = true;
        }
        else
        {
            unlocked = false;
        }
        if (forceUnlocked)
            unlocked = true;
        lockedPanel.SetActive(!unlocked);
        unlockedPanel.SetActive(unlocked);
    }

    void OnEnable()
    {
        EventManager.OnInvasionFinished += OnInvasionFinished;
        EventManager.OnInvasionUnlocks += OnInvasionUnlocks;
        EventManager.OnInvasionLocks += CheckUnlocked;

        EventManager.OnLoadComplete += OnLoadComplete;
    }

    void OnDisable()
    {
        EventManager.OnInvasionFinished -= OnInvasionFinished;
        EventManager.OnInvasionUnlocks -= OnInvasionUnlocks;
        EventManager.OnInvasionLocks -= CheckUnlocked;

        EventManager.OnLoadComplete -= OnLoadComplete;
    }

    public void OnInvasionFinished()
    {
        CheckUnlocked();
    }

    public void OnInvasionUnlocks()
    {
        forceUnlocked = true;
        CheckUnlocked();
    }

    //	public void OnInvasionLocks()
    //    {
    //        forceUnlocked = true;
    //        CheckUnlocked();
    //    }


    void Update()
    {
        Raycasts();
    }

    //Check for hovering over enemy
    void Raycasts()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //USE VOID CAM for this
        if (GameManager.Instance.VoidObject.activeSelf)
        {
            //The Void is open so get the other camera for raycast
            Camera voidCam = GameManager.Instance.smoothFollowVoid.GetComponent<Camera>();
            ray = voidCam.ScreenPointToRay(Input.mousePosition);
        }

        if (Physics.Raycast(ray, out hit, 100, mask))
        {
            ClickLogic(hit.collider);
        }


    }

    void ClickLogic(Collider col)
    {
        //	Debug.Log ("RAYCAST HIT LOBBY!");
        if (MapSelectCanvas.Instance.isOpen)
            return;
        if (SettingsCanvas.Instance.canvasOpen)
            return;//cant click on it when inventory open!
        if (!GameManager.Instance.InputEnabled)
            return;
        if (InventoryCanvas.Instance.equipmentOpen)
            return;//cant click on it when inventory open!
        if (Input.GetMouseButton(0))
        {
            if (col == myCollider)
            {
                OnClick();
                AdventurerTownNetwork atn = TownManager.Instance.myAdventurerTownNetwork;
                atn.adventurerAnimation.SetAnimationTown(atn.stance + "-Idle", true, false);
            }
        }
    }

    [Header("Which set of maps to show")]
    public int mapIndex = 0;
    public void OnClick()
    {

        if (!unlocked)
            return;
        if (!clickCooled)
            return;
        StartCoroutine(OnClickCooldown());

        if (mapName == "QuickStart")
        {
            MapSelectCanvas.Instance.MapTileClicked("QuickStart");
        }
        else
        {
            MapSelectCanvas.Instance.ToggleMe(true);
            MapSelectCanvas.Instance.SetPanel(mapIndex);
            EventManager.Instance.CallMapRefreshed();
        }
    }

    //	public void OnClick ()
    //	{
    //
    //		if (!unlocked)
    //			return;
    //		if (!clickCooled)
    //			return;
    //		StartCoroutine (OnClickCooldown ());
    //		LoadingManager.Instance.CloseAllPanels ();
    //		LoadingManager.Instance.CallFadeCanvas (1);
    //		LoadingManager.Instance.StartLobbyTimeout ();
    //
    //
    //		// LoadingManager.Instance.CallDelayedClose(2);
    //		//LoadingManager.Instance.CallCloseWithCallback(GameManager.Instance.CallLobby, 2);
    //
    //		GameManager.Instance.selectedMap = mapName;
    //		if (mapName == "QuickStart") {
    //			LobbyCanvas.Instance.quickJoinTry = 0;
    //			PhotonNetwork.LeaveRoom ();
    //			return;
    //		}
    //
    //		GameManager.Instance.CallLobby ();
    //	}

    public bool clickCooled = true;

    IEnumerator OnClickCooldown()
    {
        clickCooled = false;
        yield return new WaitForSeconds(.25f);

        clickCooled = true;
    }
}