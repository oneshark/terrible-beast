﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CanvasTools : MonoBehaviour
{
	CanvasGroup cg;
    public List<GameObject> worldSpaceObjectsToToggle = new List<GameObject>();
	public List<System.Action> OnOpenMethods = new List<System.Action>();
	public List<System.Action> OnCloseMethods = new List<System.Action>();

	void Awake ()
	{
		if (cg == null)
			cg = GetComponent<CanvasGroup> ();
	}

	public bool isOpen = false;
	public void ToggleMe (bool b)
	{
		if (b == isOpen)
			return;
		if (b) {
			cg.alpha = 1;
			cg.interactable = true;
			cg.blocksRaycasts = true;

            //toggle worldspace objects
            if (worldSpaceObjectsToToggle.Count > 0)
            {
                for (int inc = 0; inc < worldSpaceObjectsToToggle.Count; inc++)
                {
                    worldSpaceObjectsToToggle[inc].SetActive(true);
                }
            }

			//run open actions
			ProcessOpenActions();

		} else {
			cg.alpha = 0;
			cg.interactable = false;
			cg.blocksRaycasts = false;

            //toggle worldspace objects
            if (worldSpaceObjectsToToggle.Count > 0)
            {
                for (int inc = 0; inc < worldSpaceObjectsToToggle.Count; inc++)
                {
                    worldSpaceObjectsToToggle[inc].SetActive(false);
                }
            }

			//run close actions
			ProcessCloseActions();

        }

		isOpen = b;
	}

    //Non-Canvas elements to toggle
    public void AddWorldSpaceObject(GameObject obj)
    {
        worldSpaceObjectsToToggle.Add(obj);
    }

	public void AddOpenAction(System.Action sa){
		OnOpenMethods.Add (sa);
	}

	void ProcessOpenActions(){
		foreach (System.Action sa in OnOpenMethods) {
			sa ();
		}
	}

	public void AddCloseAction(System.Action sa){
		OnCloseMethods.Add (sa);
	}

	void ProcessCloseActions(){
		foreach (System.Action sa in OnCloseMethods) {
			sa ();
		}
	}

	public void CallFadeCanvas (float _desiredAlpha)
	{

		StartCoroutine (FadeCanvasGroup (_desiredAlpha));
	}

	bool fadingIn = false;
	bool fadingOut = false;

	IEnumerator FadeCanvasGroup (float _newAlpha)
	{

		if (_newAlpha == 0) {
			fadingIn = false;
			fadingOut = true;
		
		} else {
			fadingIn = true;
			fadingOut = false;
		}

		float _currentAlpha = cg.alpha;

		//Fading out
		if (_newAlpha < _currentAlpha) {
			for (float alpha = _currentAlpha; alpha >= _newAlpha; alpha -= .1f) {
				yield return new WaitForSeconds (0);
				cg.alpha = alpha;
			}
		}

		//Fading in
		if (_newAlpha > _currentAlpha) {
			for (float alpha = _currentAlpha; alpha <= _newAlpha; alpha += .1f) {
				yield return new WaitForSeconds (0);
				cg.alpha = alpha;
			}
		}

		cg.alpha = _newAlpha;

		if (cg.alpha == 1) {
			cg.interactable = true;
			cg.blocksRaycasts = true;
			isOpen = true;
		} else if (cg.alpha == 0) {
			cg.interactable = false;
			cg.blocksRaycasts = false;
			isOpen = false;
		}
	}



}
