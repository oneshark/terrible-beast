﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour
{
    //SET, BLEND, RECEIVE EVENTS - FROM ANIMATION
    public SkeletonAnimation skeletonAnimation;
    
    public void Start()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.state.Event += OnEvent;
           }

    public void SetLoop(bool b)
    {
        skeletonAnimation.loop = b;
    }

    public int getMaxAttacks()
    {
        int count = 0;
        //string[] animations = new string[skeletonAnimation.skeleton.Data.Animations.Count + 1];
        Spine.ExposedList<Spine.Animation> anim = skeletonAnimation.skeleton.data.Animations;
        foreach (Spine.Animation s in anim)
        {
            if (s.name.Contains("attack")|| s.name.Contains("Attack")) count++;
        }
        return count+1;
    }
    
    public int GetMaxDeaths()
    {
        int count = 0;
        Spine.ExposedList<Spine.Animation> anim = skeletonAnimation.skeleton.data.Animations;
        foreach (Spine.Animation s in anim)
        {
            if (s.name.Contains("Death")) count++;
        }
        return count;
    }

    public bool hasThisAnimation(string _desiredAnimation)
    {
        if (skeletonAnimation==null) skeletonAnimation = GetComponent<SkeletonAnimation>();

        bool b = false;
        //string[] animations = new string[skeletonAnimation.skeleton.Data.Animations.Count + 1];
        Spine.ExposedList<Spine.Animation> anim = skeletonAnimation.skeleton.data.Animations;
        foreach (Spine.Animation s in anim)
        {
            if (s.name == _desiredAnimation) return true;
        }
        return false;
    }

    void OnEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
       //print("OnEvent:"+e.Data.name+"  for Animation:" + skeletonAnimation.state.GetCurrent(trackIndex).animation.name +" TrackIndex:"+ trackIndex);
        if (e.Data.name == "OnComplete")
        {
            string animationName = skeletonAnimation.state.GetCurrent(trackIndex).animation.name;
            AnimationComplete(trackIndex, animationName);
        }
        if (e.Data.name == "OnHit")
        {
            string animationName = skeletonAnimation.state.GetCurrent(trackIndex).animation.name;
            AnimationOnHit(animationName);
        }
        if (e.Data.name == "OnSkill")
        {
            string animationName = skeletonAnimation.state.GetCurrent(trackIndex).animation.name;
            AnimationOnSkill(animationName);
        }
    }

	public virtual void SetAnimationOnTrack(int trackIndex,string animation,  bool isLoop){
		if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();
		skeletonAnimation.state.SetAnimation(trackIndex, animation,isLoop);

	}

    public virtual void SetAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        // print("animation:" + animation.ToString());
        if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();

        skeletonAnimation.loop = isLoop;
        skeletonAnimation.state.SetAnimation(0, animation, isLoop);
        SetAnimationOccurred(animation);
    }

    public virtual void ForceAnimation(string animation, bool isLoop, bool isOnComplete)
    {
        // print("animation:" + animation.ToString());
        if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();

        skeletonAnimation.loop = isLoop;
        skeletonAnimation.state.SetAnimation(0, animation, isLoop);
        SetAnimationOccurred(animation);
    }


	public virtual void ForceAnimation(string animation, bool isLoop, bool isOnComplete, bool ignoreRules)
	{
		// print("animation:" + animation.ToString());
		if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();

		skeletonAnimation.loop = isLoop;
		skeletonAnimation.state.SetAnimation(0, animation, isLoop);
		SetAnimationOccurred(animation);
	}

    public virtual void SetAnimationTown(string animation, bool isLoop, bool isOnComplete)
    {
        // print("animation:" + animation.ToString());
        if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();

        skeletonAnimation.loop = isLoop;
        skeletonAnimation.state.SetAnimation(0, animation, isLoop);
        SetAnimationOccurred(animation);
    }

    ////ATTACKS HAPPEN HERE
    //public virtual void SetAnimationTrackOne(string animation, bool isLoop)
    //{
    //    // print("animation:" + animation.ToString());
    //    if (skeletonAnimation == null) skeletonAnimation = GetComponent<SkeletonAnimation>();

    //    skeletonAnimation.loop = isLoop;
    //    skeletonAnimation.state.SetAnimation(1, animation, isLoop);
    //    SetAnimationOccurred(animation);
    //}

    public virtual void SetAnimationOccurred(string animation)
    {
    }
    public virtual void AnimationComplete(int trackIndex, string animationName)
    {
    }
    public virtual void AnimationOnHit(string _animation)
    {
    }
    public virtual void AnimationOnGunHit(string _animation)
    {
    }
    public virtual void AnimationOnSkill(string _animation)
    {
    }


}
