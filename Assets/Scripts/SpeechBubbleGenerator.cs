﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechBubbleGenerator : MonoBehaviour
{
	[Header ("Set Parent to follow, messages and then timers")]
	GameObject currentSpeechBubble;
	public ChatBubbleCanvas cbc;
	public float timeBetweenMessages;
	public float timeShown;
	public string[] messages;
	public int facing = 1;
//right=1
	// Use this for initialization

	void OnEnable ()
	{
		//   CreateSpeechBubble();
		StartCoroutine (CreateSpeechBubbleTimer ());
		if (cbc == null)
			cbc = GetComponentInChildren<ChatBubbleCanvas> ();
	}

	void OnDisable ()
	{
		cbc.CloseBubble ();

		StopAllCoroutines ();
	}

	IEnumerator CreateSpeechBubbleTimer ()
	{
		float timeRandomized = timeBetweenMessages + Random.Range (-5, 5);
		if (timeRandomized < 0)
			timeRandomized = 5;
		yield return new WaitForSeconds (timeRandomized);
		CreateSpeechBubble ();
       
		yield return new WaitForSeconds (timeShown);
		StartCoroutine (CreateSpeechBubbleTimer ());
		StartCoroutine (HideBubble ());
	}

	public void CreateSpeechBubble ()
	{
		string nextMessage = messages [Random.Range (0, messages.Length)];
		Debug.Log("Asked for Message:"+nextMessage);
		if (nextMessage=="")return;
		cbc.OpenBubble (nextMessage);
	}

	IEnumerator HideBubble ()
	{
		yield return new WaitForSeconds (timeShown);
		cbc.CloseBubble ();
	}


	public void SetFacing (int i)
	{
		bool isFlip = false;
		if (facing != i)
			isFlip = true;
		if (isFlip) {
			facing = i;
			transform.Rotate (Vector3.up * 180);

		}
	}

}
