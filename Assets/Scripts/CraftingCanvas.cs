﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CraftingCanvas : MonoBehaviour
{
	public CanvasGroup cg;
	public List<CraftingShop> craftingShops = new List<CraftingShop> ();
	[Header ("Here you decide what items are required to craft different items")]
	public List<CraftingMaterials> craftingMaterialsList = new List<CraftingMaterials> ();

	private static CraftingCanvas _instance;

	public static CraftingCanvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<CraftingCanvas> ();
			return _instance;
		}
	}

	public Text soulText;
	public GameObject enviro, enviro2;
	public GameObject onehanderText, twohanderText, dualWieldText, rodText, cannonText, bowText, ringText, necklaceText, costumeText, junkText;
	public CanvasGroup cgOnehander, cgTwohander, cgDualWield, cgRod, cgCannon, cgBow, cgRing, cgNecklace, cgCostume, cgJunk;

	public GameObject cgOnehanderParent, cgTwohanderParent, cgDualWieldParent, cgRodParent, cgCannonParent, cgBowParent, cgRingParent, cgNecklaceParent, cgCostumeParent, cgJunkParent;
	string selectedPanel;

	void Awake ()
	{
		cg = GetComponent<CanvasGroup> ();
		SetPanel ("dualwield");
	}

	public void ButtonClicked (string s)
	{
		AudioManager.Instance.PlayClip ("click", true, false);
		AudioManager.Instance.photonView.RPC ("StopLoop", PhotonTargets.All);

		SetPanel (s);
	}

	public void SetPanel (string s)
	{
		//set selected
		selectedPanel = s;

		//blue tab
		ChooseTab (s);

		//slots
		ChooseSlotPanel (s);
	}

	public void ChooseTab (string s)
    //Turn on the correct tab - turn off others
	{
		onehanderText.SetActive (false);
		twohanderText.SetActive (false);
		dualWieldText.SetActive (false);
		cannonText.SetActive (false);
		rodText.SetActive (false);
		bowText.SetActive (false);
		ringText.SetActive (false);
		necklaceText.SetActive (false);
		costumeText.SetActive (false);
		junkText.SetActive (false);


		switch (s) {
		case "onehander":
			onehanderText.SetActive (true);
			break;
		case "twohander":
			twohanderText.SetActive (true);
			break;
		case "dualwield":
			dualWieldText.SetActive (true);
			break;
		case "rod":
			rodText.SetActive (true);
			break;
		case "bow":
			bowText.SetActive (true);
			break;
		case "cannon":
			cannonText.SetActive (true);
			break;
		case "costume":
			costumeText.SetActive (true);
			break;
		case "ring":
			ringText.SetActive (true);
			break;
		case "necklace":
			necklaceText.SetActive (true);
			break;
		case "junk":
			junkText.SetActive (true);
			break;
		}
	}


	public void ChooseSlotPanel (string s)
    //turn on correct panel - turn off others
	{
		cgOnehander.alpha = 0;
		cgOnehander.blocksRaycasts = false;
		cgOnehander.interactable = false;

		cgBow.alpha = 0;
		cgBow.blocksRaycasts = false;
		cgBow.interactable = false;

		cgCostume.alpha = 0;
		cgCostume.blocksRaycasts = false;
		cgCostume.interactable = false;

		cgDualWield.alpha = 0;
		cgDualWield.blocksRaycasts = false;
		cgDualWield.interactable = false;

		cgRod.alpha = 0;
		cgRod.blocksRaycasts = false;
		cgRod.interactable = false;

		cgCannon.alpha = 0;
		cgCannon.blocksRaycasts = false;
		cgCannon.interactable = false;

		cgRing.alpha = 0;
		cgRing.blocksRaycasts = false;
		cgRing.interactable = false;

		cgNecklace.alpha = 0;
		cgNecklace.blocksRaycasts = false;
		cgNecklace.interactable = false;

		cgTwohander.alpha = 0;
		cgTwohander.blocksRaycasts = false;
		cgTwohander.interactable = false;

		cgJunk.alpha = 0;
		cgJunk.blocksRaycasts = false;
		cgJunk.interactable = false;


		//SET THEM TO THE SIDE PERFECTLY

//		
//		rt = cgTwohanderParent.GetComponent<RectTransform> ();
//		rt.localPosition = new Vector3 (rt.localPosition.x, -5000, rt.localPosition.z);

		//Debug.Log("s=" + s);
		RectTransform rt;

		switch (s) {
		case "onehander":
			cgOnehander.alpha = 1;
			cgOnehander.blocksRaycasts = true;
			cgOnehander.interactable = true;
			rt = cgOnehanderParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "twohander":
			cgTwohander.alpha = 1;
			cgTwohander.blocksRaycasts = true;
			cgTwohander.interactable = true;
			rt = cgTwohanderParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "dualwield":
			cgDualWield.alpha = 1;
			cgDualWield.blocksRaycasts = true;
			cgDualWield.interactable = true;
			rt = cgDualWieldParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "rod":
			cgRod.alpha = 1;
			cgRod.blocksRaycasts = true;
			cgRod.interactable = true;
			rt = cgRodParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "bow":
			cgBow.alpha = 1;
			cgBow.blocksRaycasts = true;
			cgBow.interactable = true;
			rt = cgBowParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "cannon":
			cgCannon.alpha = 1;
			cgCannon.blocksRaycasts = true;
			cgCannon.interactable = true;
			rt = cgCannonParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "ring":
			cgRing.alpha = 1;
			cgRing.blocksRaycasts = true;
			cgRing.interactable = true;
			rt = cgRingParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "necklace":
			cgNecklace.alpha = 1;
			cgNecklace.blocksRaycasts = true;
			cgNecklace.interactable = true;
			rt = cgNecklaceParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "costume":
			cgCostume.alpha = 1;
			cgCostume.blocksRaycasts = true;
			cgCostume.interactable = true;
			rt = cgCostumeParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		case "junk":
			cgJunk.alpha = 1;
			cgJunk.blocksRaycasts = true;
			cgJunk.interactable = true;
			rt = cgJunkParent.transform.parent.GetComponent<RectTransform> ();
			rt.localPosition = new Vector3 (5000, rt.localPosition.y, rt.localPosition.z);
			break;
		}
	}

	public void Close ()
	{
		_GameSaveLoad.Instance.Save(false);

		if (craftingCanvasOpen)
			FadeMeOut ();
		//	MenuCanvas.Instance.ToggleMe (true);
	}

	public void ClearCraftItems ()
	{
		foreach (Transform child in cgOnehanderParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgTwohanderParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgDualWieldParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgBowParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgCannonParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgNecklaceParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgRingParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgRodParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgCostumeParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
		foreach (Transform child in cgJunkParent.gameObject.transform) {
			child.gameObject.SetActive (false);
		}
	}

	public string currentShop_NPCName;

	public void LoadCraftItems (string _npcName)
	{
		currentShop_NPCName = _npcName;
		CraftingShop myCraftingShop = craftingShops.Where (x => x.npcName == _npcName).FirstOrDefault ();
		InventoryManager inventoryManager = InventoryManager.Instance;

		//Onehanders
		for (int inc = 0; inc < myCraftingShop.onehanders.Count; inc++) {
			Item nextItem = inventoryManager.weaponsDb.Where (x => x.ID == myCraftingShop.onehanders [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgOnehanderParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			Debug.Log ("One hander crafting:" + nextItem);
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//DualWield
		for (int inc = 0; inc < myCraftingShop.dualwields.Count; inc++) {
			Item nextItem = inventoryManager.weaponsDb.Where (x => x.ID == myCraftingShop.dualwields [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgDualWieldParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Twohander
		for (int inc = 0; inc < myCraftingShop.twohanders.Count; inc++) {
			Item nextItem = inventoryManager.weaponsDb.Where (x => x.ID == myCraftingShop.twohanders [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgTwohanderParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Cannon
		for (int inc = 0; inc < myCraftingShop.cannons.Count; inc++) {
			Item nextItem = inventoryManager.weaponsDb.Where (x => x.ID == myCraftingShop.cannons [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgCannonParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Bow
		for (int inc = 0; inc < myCraftingShop.bows.Count; inc++) {
			Item nextItem = inventoryManager.weaponsDb.Where (x => x.ID == myCraftingShop.bows [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgBowParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Rod
		for (int inc = 0; inc < myCraftingShop.rods.Count; inc++) {
			Item nextItem = inventoryManager.weaponsDb.Where (x => x.ID == myCraftingShop.rods [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgRodParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Costumes - Body
		for (int inc = 0; inc < myCraftingShop.bodycostumes.Count; inc++) {
			Item nextItem = inventoryManager.costumesDb.Where (x => x.ID == myCraftingShop.bodycostumes [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgCostumeParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Costumes - back
		for (int inc = 0; inc < myCraftingShop.backcostumes.Count; inc++) {
			Item nextItem = inventoryManager.costumesDb.Where (x => x.ID == myCraftingShop.backcostumes [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgCostumeParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Equip - Ring
		for (int inc = 0; inc < myCraftingShop.ringequipment.Count; inc++) {
			Item nextItem = inventoryManager.equipmentDb.Where (x => x.ID == myCraftingShop.ringequipment [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgRingParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Equip - Necklace
		for (int inc = 0; inc < myCraftingShop.necklaceequipment.Count; inc++) {
			Item nextItem = inventoryManager.equipmentDb.Where (x => x.ID == myCraftingShop.necklaceequipment [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgNecklaceParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}

		//Equip - Junk
		for (int inc = 0; inc < myCraftingShop.junk.Count; inc++) {
			Item nextItem = inventoryManager.junkDb.Where (x => x.ID == myCraftingShop.junk [inc]).FirstOrDefault ();
			GameObject newItem = ((GameObject)Instantiate (Resources.Load ("UI/CraftingItemSlot")));
			newItem.transform.parent = cgJunkParent.transform;
			ItemSlot itemSlot = newItem.GetComponentInChildren<ItemSlot> ();
			itemSlot.SetItem (nextItem, 1);
			itemSlot.purchasable = false;
		}
	}


	#region Fade In

	public void FadeMeIn ()
	{
		GameManager.Instance.InputEnabled = false;
		Debug.Log("FADEMEIN");
		ToggleOtherMenus (false);
		Invoke ("CallLoadCraftingItems", 1);
		//in delayed fade in - it opens the panel
		LoadingManager.Instance.CloseAllPanels ();
		LoadingManager.Instance.CallFadeCanvas (1);
		Invoke ("DelayedCraftIcon", 1.5f);//shows crafting icon briefly
		Invoke ("DelayedFadeMeIn", 2.5f);
		

		LoadingManager.Instance.CallDelayedClose (2.5f);//close this at same time and allowing fade in
	}

	void CallLoadCraftingItems ()
	{
		//want this to start only after we are black
		ClearCraftItems ();
		LoadCraftItems (NPCManager.Instance.currentNPC);
	}

	void DelayedCraftIcon ()
	{
		LoadingManager.Instance.OpenPanel ("Craft");
	}

	void DelayedFadeMeIn ()
	{
		ToggleMe (true);
	}


	#endregion

	#region Fade Out

	public void FadeMeOut ()
	{
		Invoke ("DelayedFadeMeOut", 1.75f);
		LoadingManager.Instance.CallFadeCanvas (1);//close this at same time and allowing fade in

	}

	void DelayedFadeMeOut ()
	{
		ToggleMe (false);
		GameManager.Instance.InputEnabled = true;
		Invoke ("DelayedFinalClose", 1.2f);
	}

	void DelayedFinalClose ()
	{
		LoadingManager.Instance.CallFadeCanvas (0);//close this at same time and allowing fade in
		ToggleOtherMenus (true);
	}

	#endregion

	void ToggleFalse ()
	{
		ToggleMe (false);
	}

	public bool craftingCanvasOpen = false;

	public void ToggleMe (bool b)
	{
		if (b == craftingCanvasOpen)
			return;
		if (b) {
			//Delay, ClipIndex
			GameManager.Instance.CallPlayTutorialBlock(1,2,true);
			cg.alpha = 1;
			cg.interactable = true;
			cg.blocksRaycasts = true;
			if(currentShop_NPCName=="Valder")enviro.SetActive (true);
			if(currentShop_NPCName=="James Mason")enviro2.SetActive (true);
			ToggleOtherMenus (false);
			ToggleStationaryCamera (true);
		} else {
			cg.alpha = 0;
			cg.interactable = false;
			cg.blocksRaycasts = false;
			if(currentShop_NPCName=="Valder")enviro.SetActive (false);
			if(currentShop_NPCName=="James Mason")enviro2.SetActive (false);
			ToggleStationaryCamera (false);

		}
		craftingCanvasOpen = b;
	}

	Vector3 originalToolTipPlacement = new Vector3 ();

	public void ToggleOtherMenus (bool b)
    //Menus which must open/close in accordance with this one
	{
		//change the tooltip placement

		//if (!b) {
		//    originalToolTipPlacement = ToolTip.Instance.offset;
		//    ToolTip.Instance.offset += new Vector3(0, 150, 0);
		//}
		//else
		//{
		//    ToolTip.Instance.offset = originalToolTipPlacement;
		//}
		TownCanvas.Instance.ToggleMe (b);
		MenuCanvas.Instance.ToggleMe (b);
		Debug.Log ("ToggleOtherMenus:" + b);
	}

	public void ToggleStationaryCamera (bool b)
	{
		if (b) {
			GameManager.Instance.cameraEffects.SetScrollValues (-7);
			GameManager.Instance.cameraEffects.ScrollLocal (TownManager.Instance.myAtn.transform.position);
			GameManager.Instance.cameraEffects.isZoomed = true;
		} else {
			if (GameManager.Instance.cameraEffects.isZoomed) {
				GameManager.Instance.cameraEffects.SetScrollValues (-10);
				GameManager.Instance.cameraEffects.isZoomed = false;
			}
			GameManager.Instance.cameraEffects.InvokeFollow (.7f);
		}
	}


}



[System.Serializable]
public class CraftingShop
{
	public string npcName;
	public List<int> onehanders = new List<int> ();
	public List<int> bows = new List<int> ();
	public List<int> dualwields = new List<int> ();
	public List<int> twohanders = new List<int> ();
	public List<int> cannons = new List<int> ();
	public List<int> rods = new List<int> ();
	public List<int> knuckles = new List<int> ();

	public List<int> bodycostumes = new List<int> ();
	public List<int> backcostumes = new List<int> ();
	public List<int> headcostumes = new List<int> ();

	public List<int> ringequipment = new List<int> ();
	public List<int> necklaceequipment = new List<int> ();
	public List<int> junk = new List<int> ();

}

[System.Serializable]
public class CraftingMaterials
{
	[Header ("What type of item are we crafting")]
	public ItemType itemType;
	[Header ("What is the Id of the item we are crafting in the database")]
	public int itemId;
	[Header ("Enter all the junk items required to craft it. Enter multiple times if more than one required")]
	public List<int> requiredJunkItems = new List<int> ();
	public int requiredSouls;

}