﻿using UnityEngine;
using System.Collections;

public class VolumeSetter : MonoBehaviour
{
    AudioSource[] audioSrc;
    public bool isSFX, isBG;
    void Start()
    {
        audioSrc = GetComponents<AudioSource>();
        setVolumes();
    }

    void OnEnable()
    {
        EventManager.OnAudioLevelChanged += OnAudioLevelChanged;
    }

    void OnDisable()
    {
        EventManager.OnAudioLevelChanged -= OnAudioLevelChanged;
    }

    void setVolumes()
    {
        if (isSFX)
        {
            foreach (AudioSource _audio in audioSrc)
            {
                _audio.volume = AudioManager.Instance.getSFXVolume();
            }
        }
        if (isBG)
        {
            foreach (AudioSource _audio in audioSrc)
            {
                _audio.volume = AudioManager.Instance.getBGVolume();
               // Debug.Log("Test");
            }
        }
    }

    public void OnAudioLevelChanged()
    {
       // Debug.Log("Audio Level Chagned, fix all volumes");
        setVolumes();
    }
}
