﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameObjectToggler : MonoBehaviour {

    [Header("0=unlock, 1=lock, 2=empty")]
    public GameObject[] obj;

    public void TurnOn(int i)
    {
        for (int inc = 0; inc < obj.Length; inc++)
        {
            //if index matches object, turn it active, otherwise turn it inactive
            Debug.Log("Toggle:" + inc + " " + (inc == 1));
            obj[inc].SetActive(inc == i);
        }

    }
}

