﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Component_GUI : MonoBehaviour
{
public Behaviour[] behaviours;

	void OnGUI ()
	{
		int i = 0;
		GUI.Label (new Rect (0,0, 420, 30), gameObject.name);
		foreach (Behaviour b in behaviours) {
			if (GUI.Button (new Rect (0, 30 + (i * 30), 200, 30), "Toggle:" + b.name)) {
				b.enabled = !b.enabled;
			}
			i++;
		}
	}
}