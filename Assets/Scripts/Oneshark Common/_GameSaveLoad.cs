﻿
using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

public class _GameSaveLoad : MonoBehaviour
{
	public bool allowSave=true;
	public bool isInit;
	//is this initialized?

	#region singleton

	private static _GameSaveLoad _instance;

	public static _GameSaveLoad Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<_GameSaveLoad> ();
			return _instance;
		}
	}
	//Singleton complete

	#endregion

	public SaveData saveData;
	string _FileLocation, _FileName;


	public void StartLoad ()
	{
		_FileLocation = Application.persistentDataPath;
		_FileName = "Reference254.txt";
//		if (File.Exists (_FileLocation + "\\" + _FileName)) {
//			Debug.Log ("File exists, so load it");
//			Load ();
//		} else {
//			GameManager.Instance.DelayedNewToMorningWood (0);
//		}

		if (LoginGUI.Instance.isNewUser) {
			GameManager.Instance.DelayedNewToMorningWood (0);
		} else {
			Load ();
		}
	}

	void Awake ()
	{
		//Invoke ("IntermittentSave", 35);
	}

	//	void IntermittentSave ()
	//	{
	//		Save ();
	//		Invoke ("IntermittentSave", 60);
	//	}

	public void Save (bool includeMilestone)
	{
		if (LoginGUI.Instance.isGuestLogin)
			return;

		if (GameManager.Instance.PlayerName == "Guest" || GameManager.Instance.PlayerName == "") {
			Debug.Log ("Trying to save an empty file, STOP!");
			return;
		}

		SaveData saveData = new SaveData ();

		if (saveData == null) {
			Debug.Log ("Trying to save NULL file, STOP!");
			return;
		}
			
		saveData.souls = (int)GameManager.Instance.souls;
		saveData.crowns = (int)GameManager.Instance.crowns;
		if (saveData.souls == null) {
			Debug.Log ("Trying to save NULL souls File, STOP!");
			return;
		}

		//STORE EQUIPPED STUFF
		if (GameManager.Instance.equippedWeapon != null)
			saveData.weaponEquipped = GameManager.Instance.equippedWeapon.ID;
		if (GameManager.Instance.ringEquipped)
			saveData.equipmentEquipped.Add (GameManager.Instance.equippedEquipmentRing.ID);
		if (GameManager.Instance.artifactEquipped)
			saveData.equipmentEquipped.Add (GameManager.Instance.equippedEquipmentArtifact.ID);
		if (GameManager.Instance.necklaceEquipped)
			saveData.equipmentEquipped.Add (GameManager.Instance.equippedEquipmentNecklace.ID);
		if (GameManager.Instance.backEquipped)
			saveData.costumesEquipped.Add (GameManager.Instance.equippedCostumeBack.ID);
		if (GameManager.Instance.bodyEquipped)
			saveData.costumesEquipped.Add (GameManager.Instance.equippedCostumeBody.ID);
		if (GameManager.Instance.headEquipped)
			saveData.costumesEquipped.Add (GameManager.Instance.equippedCostumeHead.ID);
		saveData.faceEquipped = GameManager.Instance.equippedFace;

		//Store Inventory Lists
		for (int inc = 0; inc < InventoryManager.Instance.junkOwned.Count; inc++) {
			saveData.junkOwned.Add (InventoryManager.Instance.junkOwned [inc].ID);
		}

		for (int inc = 0; inc < InventoryManager.Instance.weaponsOwned.Count; inc++) {
			saveData.weaponsOwned.Add (InventoryManager.Instance.weaponsOwned [inc].ID);
		}

		for (int inc = 0; inc < InventoryManager.Instance.costumesOwned.Count; inc++) {
			saveData.costumesOwned.Add (InventoryManager.Instance.costumesOwned [inc].ID);
		}

		for (int inc = 0; inc < InventoryManager.Instance.equipmentOwned.Count; inc++) {
			saveData.equipmentOwned.Add (InventoryManager.Instance.equipmentOwned [inc].ID);
		}

		//STATS
		//  saveData.baseStat = saveData.baseStat.MirrorStat(GameManager.Instance.baseEntityStat);
		//get current level, xp from currentstats tho
		saveData.baseStat.hpMax = GameManager.Instance.baseEntityStat.hpMax;
		saveData.baseStat.xp = GameManager.Instance.baseEntityStat.xp;
		saveData.baseStat.xpMax = GameManager.Instance.baseEntityStat.xpMax;
		saveData.baseStat.xpLevel = GameManager.Instance.baseEntityStat.xpLevel;
		saveData.PlayerName = GameManager.Instance.PlayerName;
		saveData.completedTHings = GameManager.Instance.completedThings;
		saveData.soundLevel = AudioManager.Instance.soundLevel;
		saveData.musicLevel = AudioManager.Instance.musicLevel;

		//                    Stats baseStat = baseEntityStat.MirrorStat(baseEntityStat); //deep copy to keep them untied from each other

		//  string key = "PLAYERDATA";
		string jsonSaveData = JsonUtility.ToJson (saveData);
		string encrypted = jsonSaveData;
			//StringUtil.EncryptString (jsonSaveData, "Ozark");

		//Debug.Log ("SaveData enc:" + encrypted);
		bool isGlobal = false;

//		try
//		{
//			string decrypt = StringUtil.DecryptString (encrypted, "Ozark");
//		}
//		catch (Exception e){
//			Debug.Log ("Save Decrypt() Exception:" + e);
//			allowSave = false;
//		}

		//Put it into whatever file we using
		if (allowSave)
		LoginGUI.Instance.PostSave (includeMilestone, encrypted, jsonSaveData);
		//Debug.Log ("save data:" + encrypted);
		//File.WriteAllText (_FileLocation + "\\" + _FileName, encrypted);

		//GameJolt.API.DataStore.Set(key, value, isGlobal, (bool success) =>
		//{
		//    Debug.Log("PlayerData was successful:" + success);
		//});

	}

	public void Load ()
	{
		Debug.Log ("Loading...");
		saveData = new SaveData ();

		//string loadedData = File.ReadAllText (_FileLocation + "\\" + _FileName).ToString ();
		string loadedData = LoginGUI.Instance.saveString;
		if (!loadedData.Contains ("isRanged")) {
			//Debug.Log ("loadedData encrypted:" + loadedData);
			string decrypt=loadedData;
			//NEW STUFF
//			try
//			{
//				decrypt = StringUtil.DecryptString (loadedData, "Ozark");
//			}
//			catch (Exception e){
//				Debug.Log ("Load() Exception:" + e);
//
//				allowSave = false;
//			}


			saveData = JsonUtility.FromJson<SaveData> (decrypt);
			//Debug.Log ("during load weaponcount:" + saveData.weaponsOwned.Count);
		} else {
			saveData = JsonUtility.FromJson<SaveData> (loadedData);

		}
		//  if (GameManager.Instance==null)Debug.Log("GAMEMANAGER ISS NULL - DAMN");
		EventManager.Instance.CallLoadComplete ();
	}

	public void DeleteSaveFile ()
	{
		if (File.Exists (_FileLocation + "\\" + _FileName)) {
			File.Delete (_FileLocation + "\\" + _FileName);
		}
		//reset original audios
		AudioManager.Instance.soundLevel = AudioManager.Instance.origSoundLevel;
		AudioManager.Instance.musicLevel = AudioManager.Instance.origMusicLevel;
		AudioManager.Instance.setLevels ();

		GameManager.Instance.baseEntityStat = GameManager.Instance.baseEntityStat.MirrorStat (GameManager.Instance.originalBaseStats);
		GameManager.Instance.currentEntityStat = GameManager.Instance.currentEntityStat.MirrorStat (GameManager.Instance.originalBaseStats);
		GameManager.Instance.PlayerName = "";
		GameManager.Instance.completedThings.Clear ();
		if (saveData == null) {
			saveData = new SaveData ();
		}
		saveData.PlayerName = "";
		if (GameManager.Instance.currentScene == CustomSceneType.Town) {
			TownManager.Instance.myAdventurerTownNetwork.CallBroadcastName ();
		}
		GameManager.Instance.SetDefaultWeapon ();
		GameManager.Instance.SetDefaultBody (); ///MAYBE CAN REMOVE THIS - BECAUSE WE ALREADY UNQUIP BELOW

		InventoryCanvas.Instance.costumeSlotHead.Unequip ();
		InventoryCanvas.Instance.costumeSlotBack.Unequip ();
		InventoryCanvas.Instance.costumeSlotBody.Unequip ();

		GameManager.Instance.DelayedNewToMorningWood (5);
		InventoryManager.Instance.ClearInventory ();
		EventManager.Instance.CallInvasionFinished ();//call this so it will lock all the doors



	}

	void OnApplicationQuit ()
	{
		//Debug.Log("Doesnt work");
		//Save ();
	}
}

//[System.Serializable]
public class SaveData
{
	public Stats baseStat = GameManager.Instance.baseEntityStat;

	//inventory
	public List<int> junkOwned = new List<int> ();
	public List<int> costumesOwned = new List<int> ();
	public List<int> weaponsOwned = new List<int> ();
	public List<int> equipmentOwned = new List<int> ();
	public List<string> completedTHings = new List<string> ();
	//equipped inventory
	public List<int> costumesEquipped = new List<int> ();
	public List<int> equipmentEquipped = new List<int> ();
	public int weaponEquipped;
	public int faceEquipped;
	public int souls;
	public int crowns;
    
	public string PlayerName;

	//audio
	public float soundLevel, musicLevel;
}



//IEnumerator HandleWWWLoad(string url)
//{
//    Debug.Log("url:" + url);
//    WWW www = new WWW(url);
//    yield return www;
//    //  int.TryParse(www.text, out pesosInbox);
//    // pesosValue.text = pesosInbox.ToString();
//    Debug.Log("www.text=" + www.text);
//    //string JsonImport = www.text;
//  // List<InboxPesos> parsedInboxPesos = ItemAuctionManager.Instance.ParseInboxPesosFromJson(JsonImport);
//    //EventManager.Instance.CallInboxPesosLoaded();
//}
//#endregion

//#region GET SERVER MESSAGE
//public void LoadPlayerData()
//{
////   StartCoroutine(HandleWWWLoadData("http://terriblebeast.azurewebsites.net/servercheck.php"));
//    StartCoroutine(HandleWWWLoadData("http://terriblebeast.azurewebsites.net/GetPlayerData.php"));
//}

//IEnumerator HandleWWWLoadData(string url)
//{
//    WWW www = new WWW(url);
//    yield return www;
//    Debug.Log("Load Complete - Server response:" + www.text);
//    EventManager.Instance.CallLoadComplete();
//}
//#endregion