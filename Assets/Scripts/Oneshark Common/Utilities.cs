﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class Utilities
{

    public static string FirstCharToUpper(string input)
    {
        if (String.IsNullOrEmpty(input))
            throw new ArgumentException("ARGH!");
        return input.First().ToString().ToUpper() + input.Substring(1);
    }

    public static T DeepClone<T>(T obj)
    //example         List<Wave> newWaves = Utilities.DeepClone(currentWaveset.waves);

    {
        using (var ms = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, obj);
            ms.Position = 0;

            return (T)formatter.Deserialize(ms);
        }
    }


    public static int getRandomElement(int[] array, int searchForThisValue)
    {
        List<int> possibleChoices = new List<int>();
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == searchForThisValue) possibleChoices.Add(i);
        }
        return possibleChoices[UnityEngine.Random.Range(0, possibleChoices.Count)]; //pick one at random and return it;
    }

   

    public static GameObject[] shuffle(GameObject[] charArray)
    {
        GameObject[] shuffledArray = new GameObject[charArray.Length];
        int rndNo;

        System.Random rnd = new System.Random();
        for (int i = charArray.Length; i >= 1; i--)
        {
            rndNo = rnd.Next(1, i + 1) - 1;
            shuffledArray[i - 1] = charArray[rndNo];
            charArray[rndNo] = charArray[i - 1];
        }
        return shuffledArray;
    }

    public static string FormatCurrency(int raw)
    {
        string formatted = string.Format("{0:n0}", raw);
        formatted = formatted.Replace(",", "'");
        return formatted;
    }

    public static string getMonthByNumber(int i)
    {
        switch (i)
        {
            case 1:
                return "Jan";
                break;
            case 2:
                return "Feb";
                break;
            case 3:
                return "Mar";
                break;
            case 4:
                return "Apr";
                break;
            case 5:
                return "May";
                break;
            case 6:
                return "Jun";
                break;
            case 7:
                return "Jul";
                break;
            case 8:
                return "Aug";
                break;
            case 9:
                return "Sept";
                break;
            case 10:
                return "Oct";
                break;
            case 11:
                return "Nov";
                break;
            case 12:
                return "Dec";
                break;
        }
        return "Mystery";
    }

    public static string getDayByNumber(int i)
    {
        switch (i)
        {
            case 1:
                return "Sun";
                break;
            case 2:
                return "Mon";
                break;
            case 3:
                return "Tues";
                break;
            case 4:
                return "Wed";
                break;
            case 5:
                return "Thurs";
                break;
            case 6:
                return "Fri";
                break;
            case 7:
                return "Sat";
                break;
        }
        return "Mystery";
    }

    public static bool IsDivisble(int x, int n)
    {
        return (x % n) == 0;
    }


}
