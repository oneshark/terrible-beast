﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAnimationWorkbench : MonoBehaviour {

    // Use this for initialization
    public float yOffset = 0;
    public SkeletonAnimation skeletonAnimation;
    public List<string> skins = new List<string>();
    public List<string> animations = new List<string>();

    void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        
     //   foreach(SpineSkin ss in skeletonAnimation.skeleton.SetSkin)
    }
    void OnGUI()
    {
        yOffset = 0;
        foreach (string skinName in skins)
        {
            if (GUI.Button(new Rect(0, 0 + yOffset, 120, 30), skinName))
            {
                SetSkin(skinName);
            }
            yOffset += 30;
        }

        yOffset = 0;
        foreach (string animName in animations)
        {
            if (GUI.Button(new Rect(120, 0 + yOffset, 120, 30), animName))
            {
                SetAnimation(animName,true);
            }
            yOffset += 30;
        }

    }

    public void SetSkin(string s)
    {
        try
        {
            skeletonAnimation.skeleton.SetSkin(s);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex);
        }
    }

    public void SetAnimation(string s, bool isLoop)
    {
        skeletonAnimation.state.SetAnimation(0, s, isLoop);
    }
}
