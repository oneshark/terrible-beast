﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class AdventurerTownNetwork : MonoBehaviour
{
	public Rigidbody rb;

	public PhotonView photonView;
	public Stance stance;
	public ChatBubbleCanvas cbc;

	public Text nametag;
	public int playerID;
	public Text nametagLvl;

	public GameObject cameraTarget;
	public MeshRenderer mr;
	public AdventurerAnimation adventurerAnimation;
	public GameObject graphics;
	private bool appliedInitialUpdate;

	[Header ("Equipped Stuff")]
	public Item equippedCostumeHead;
	public Item equippedCostumeBody, equippedCostumeBack, equippedWeapon, equippedEquipmentRing, equippedEquipmentNecklace, equippedEquipmentArtifact;

	public void Initialize ()
	{
		Invoke ("LateInitialize", .1f);
		rb= GetComponent<Rigidbody> ();

	}

	//This Temporary workaround during testing should be removed on 10.1.17 before 0.4.0 release
	public void LateInitialize ()
	{
		GameManager.Instance.SetAllEquipment (photonView);
		CallBroadcastName ();
		//DisableGravityRemotePlayers ();

	}

	void Update ()
	{
	//If by some case character falls off the map, respawn them
//		if (transform.position.y < -1000)
//			FellOff ();

		if (isLerping)
			LerpMe ();
	}



	[PunRPC]
	public void ToggleColliders(int viewId, bool b){
		if (photonView.viewID != viewId) {
			return;
		}
		CapsuleCollider cc = graphics.GetComponent<CapsuleCollider> ();
		cc.enabled = b;
		Rigidbody rb= GetComponent<Rigidbody> ();
		rb.useGravity = b;
	}

	public bool isFlying = false;
	[PunRPC]
	public void ToggleFlying(int viewId, bool b){
		if (photonView.viewID != viewId) {
			return;
		}
		//CapsuleCollider cc = graphics.GetComponent<CapsuleCollider> ();
		//cc.enabled = b;
		Rigidbody rb= GetComponent<Rigidbody> ();
		if (b) {
			rb.drag = 45;
		} else {
			rb.drag = 0;
		}
			//rb.useGravity = !b;
		isFlying = b;
	}

	public bool isLerping=false;
	public void StartLerp(Vector3 _targ, bool isPoof){
		Debug.Log ("StartLerp Received");
		if (isLerping) {
			Debug.Log ("isLerping=true, return;");
			return;
		}
		if (isPoof) {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PoofEffect");
			nextObj.transform.position = graphics.transform.position + new Vector3 (0, 2, -1);
			nextObj.SetActive (true);
		}
		m_waypoint = _targ;
		isLerping = true;

	//	photonView.RPC ("ToggleColliders", PhotonTargets.All, photonView.viewID,false);
ToggleColliders(photonView.viewID,false);


//		Rigidbody rb= GetComponent<Rigidbody> ();
//		rb.useGravity = false;


	}

	Vector3 m_waypoint=new Vector3();
	float moveSpeed=9;

	void LerpMe ()
	{
		if (Vector3.Distance (transform.position, m_waypoint) < 1f) {
			isLerping = false;
//			Rigidbody rb= GetComponent<Rigidbody> ();
//			rb.useGravity = true;
//			CapsuleCollider cc = graphics.GetComponent<CapsuleCollider> ();
//			cc.enabled = true;
//			Debug.Log ("Reached Destination");
		//	photonView.RPC ("ToggleColliders", PhotonTargets.All, photonView.viewID,true);
			ToggleColliders(photonView.viewID,true);

			//MoveToTarget ();
		} else { 
			Vector3 targetPos = m_waypoint;
			transform.position = Vector3.MoveTowards (transform.position, targetPos, Time.deltaTime * moveSpeed);
			Debug.Log ("MoveTowards");
		}
	}


	/// <summary>
	/// This will call a sync animation across all clients
	/// </summary>
	/// <param name="args">Arguments.</param>
	[PunRPC]
	public void CallSyncAnimation (params object[] args)
	{
		string animationName = (string)args [0];
		bool isLoop = (bool)args [1];
		bool isOnComplete = (bool)args [2];
		adventurerAnimation.SyncAnimationTown (animationName, isLoop, isOnComplete); //can share this method with town and battle
	}

	#region Facing Methods
	/// <summary>
	/// This will call a sync the facing left or right across all clients
	/// </summary>
	/// <param name="_shouldFace">Should face.</param>
	[PunRPC]
	public void CallSyncFacing (int _shouldFace)
	{
		SyncFacing (_shouldFace);
	}

	public int facing = 1;

	public void SetFacing (int shouldFace)
	{
		bool isFlip = false;
		if (facing != shouldFace)
			isFlip = true;

		//FLIP THE GRAPHICS
		if (isFlip) {
			facing = shouldFace;
			graphics.transform.Rotate (Vector3.up * 180);
			photonView.RPC ("CallSyncFacing", PhotonTargets.Others, facing);
		}
	}

	public void SyncFacing (int shouldFace)
	{
		bool isFlip = false;
		if (facing != shouldFace)
			isFlip = true;

		//FLIP THE GRAPHICS
		if (isFlip) {
			facing = shouldFace;
			graphics.transform.Rotate (Vector3.up * 180);
		}
	}
	#endregion Facing Methods

	#region Player Name Methods
	public void CallBroadcastName ()
	{
		photonView.RPC ("BroadcastNametag", PhotonTargets.All, photonView.viewID, GameManager.Instance.PlayerName, GameManager.Instance.baseEntityStat.xpLevel.ToString ());
	}

	[PunRPC]
	public void BroadcastNametag (int _viewID, string _name, string _lvl)
	{
		if (photonView.viewID != _viewID)
			return;

		nametag.text = _name;
		nametagLvl.text = "Lvl " + _lvl;
	}
	#endregion Player Name Methods

	[PunRPC]
	public void WantsToTradeWithYou (int _theirViewID, int _myViewID)
	{
		if (_myViewID != TownManager.Instance.myAtn.photonView.viewID)
			return;

			Debug.Log("*** Hello, someone wants to trade with you now");
	}

	bool fellOffIsCool = true;

	void FellOff ()
	{
		if (!fellOffIsCool)
			return;
//		StartCoroutine ("FellOffCooldown");
//		GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
//		PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
//		puc.SetPopup ("You do not", "know how to fly.");
//		puc.SetRejectIcon ();
//		puc.SetTextColor ("white");
//		nextObj.SetActive (true);
		Vector3 playerSpawnPoint = GameObject.FindGameObjectWithTag (GameManager.Instance.NextTownSpawn).transform.position;

		transform.position = playerSpawnPoint;
		Debug.Log ("FallOff");
		Camera.main.transform.position=new Vector3(Camera.main.transform.position.x, transform.position.y, Camera.main.transform.position.z);
		//photonView.RPC ("CallChatLog", PhotonTargets.All,"["+ GameManager.Instance.PlayerName+"]" + " met the void...");
	}

	IEnumerator FellOffCooldown ()
	{
		fellOffIsCool = false;
		yield return new WaitForSeconds (2);
		fellOffIsCool = true;
	}

	void CloseChatBubble ()
	{
		cbc.CloseBubble ();
	}

	[PunRPC]
	public void CallChatBubble (params object[] args)
	{
		int _viewID = (int)args [0];
		string msg = (string)args [1];
		if (photonView.viewID != _viewID)
			return;
		//
		cbc.OpenBubble (msg);
		CancelInvoke ("CloseChatBubble");
		Invoke ("CloseChatBubble", 3);
		//now - remove the local one and call it for all clients
	}

	[PunRPC]
	public void CallChatLog (string msg)
	{
		Chat.AddMessageToCanvas (msg);
	}


	[PunRPC]
	public void CallSetSkin (int viewId, string skin)
	{
		if (photonView.viewID != viewId)
			return;
		if (skin == "") {
			//Debug.Log("Default is white tunic");
			adventurerAnimation.SetSkin ("White Tunic");
		}
		adventurerAnimation.SetSkin (skin);
	}

	[PunRPC]
	public void CallSetAttachment (int viewID, string slotName, string slotName2, string attachemntName, string attachmentName2)
	{
		if (photonView.viewID != viewID)
			return;
		adventurerAnimation.SetAttachment (slotName, slotName2, attachemntName, attachmentName2);
	}

	/// <summary>
	/// This important method broadcasts what equipment you are wearing to all clients! Or else you won't be in sync
	/// </summary>
	public void BroadcastGearMaster ()
	{
		//Debug.Log("WARNING - POSSIBLE BUG HERE: Start with Default no matter what");
		//GameManager.Instance.SetDefaultBody();
		//GameManager.Instance.SetDefaultWeapon();

		equippedWeapon = GameManager.Instance.equippedWeapon;
		equippedCostumeBack = GameManager.Instance.equippedCostumeBack;
		equippedCostumeBody = GameManager.Instance.equippedCostumeBody;
		equippedCostumeHead = GameManager.Instance.equippedCostumeHead;

		photonView.RPC ("CallSetAttachment", PhotonTargets.Others, photonView.viewID, equippedWeapon.slotName, equippedWeapon.slotName2, equippedWeapon.atachmentName, equippedWeapon.attachmentName2);
		object[] clientArgs = new object[] { photonView.viewID, (int)equippedWeapon.stance };
		photonView.RPC ("SetStanceClient", PhotonTargets.All, clientArgs);
		photonView.RPC ("SetParticlesAll", PhotonTargets.All, equippedWeapon.ID, photonView.viewID);

		photonView.RPC ("CallSetSkin", PhotonTargets.Others, photonView.viewID, equippedCostumeBody.skin);
		photonView.RPC ("CallSetAttachment", PhotonTargets.Others, photonView.viewID, equippedCostumeBack.slotName, equippedCostumeBack.slotName2, equippedCostumeBack.atachmentName, equippedCostumeBack.attachmentName2);
		photonView.RPC ("CallSetAttachment", PhotonTargets.Others, photonView.viewID, equippedCostumeHead.slotName, equippedCostumeHead.slotName2, equippedCostumeHead.atachmentName, equippedCostumeHead.attachmentName2);
	}


	[PunRPC]
	public void SetParticlesAll (int _weapId, int viewId)
	{
		if (photonView.viewID != viewId)
			return;//its not right player
		Debug.Log ("Set Particles for all players on this character. weapID:" + _weapId + "  viewID:" + viewId);
		Item weaponFromDB = InventoryManager.Instance.GetItem (ItemType.weapon, _weapId);
		//Debug.Log ("Weapon from database:" + weaponFromDB.weaponParticles);
		adventurerAnimation.SetParticles (weaponFromDB);
	}

	[PunRPC]
	public void SetStanceClient (params object[] args)
	{

		int viewID = (int)args [0];
		int stanceIndex = (int)args [1];
		//Debug.Log ("^^^^^^^^^^^^^^^^^^^^    SetStanceClient:" + stanceIndex);
		if (photonView.viewID != viewID)
			return;//its not right player

		switch (stanceIndex) {
		case 0:
			stance = Stance.Onehander;
			break;
		case 1:
			stance = Stance.Bow;
			break;
		case 2:
			stance = Stance.Dualwield;
			break;
		case 3:
			stance = Stance.Twohander;
			break;
		case 4:
			stance = Stance.Cannon;
			break;
		case 5:
			stance = Stance.Rod;
			break;
		case 6:
			stance = Stance.Bareknuckle;
			break;

		}
		StanceInfo stanceInfo = new StanceInfo ();
		stanceInfo = stanceInfo.GetStanceInfo (stance);

		GameManager.Instance.currentEntityStat.isRanged = stanceInfo.isRanged;
		//string animationString = stance + "-Idle";
		//adventurerAnimation.SetAnimation(animationString, true, false);
	}

	public void OnPhotonPlayerConnected (PhotonPlayer newPlayer)
	{
		if (photonView.isMine) {
			CallBroadcastName ();
			BroadcastGearMaster ();
		} else {
			//DisableGravityRemotePlayers ();
		}
	}

	//UNUSED!
//	void DisableGravityRemotePlayers(){
//		GameObject[] advRemain = GameObject.FindGameObjectsWithTag ("Adventurer");
//		List<AdventurerNetwork> advNetworkRemain = new List<AdventurerNetwork> ();
//		foreach (GameObject obj in advRemain) {
//			AdventurerTownNetwork _nextAtn = obj.GetComponent<AdventurerTownNetwork> ();
//			if (this != _nextAtn) {//if it is not ours!
//				Rigidbody _nextRB = _nextAtn.GetComponent<Rigidbody> ();
//				_nextRB.useGravity = false;
//			}
//		}
//
//	}

	public void OnPhotonPlayerDisconnected (PhotonPlayer otherPlayer)
	{
		//  Debug.Log("otherPlayer" + otherPlayer);
		PhotonNetwork.DestroyPlayerObjects (otherPlayer);
	}

//	void OnLeftRoom ()
//	{
//		// photonView.RPC("RemovePlayer", PhotonTargets.All);
//	}

//	void OnDisconnectedFromServer ()
//	{
//	}


}
