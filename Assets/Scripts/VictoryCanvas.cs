﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryCanvas : MonoBehaviour {
    bool leaveRoomCooled = true;
    public GameObject exitBUtton;

    private void Awake()
    {
    exitBUtton.SetActive(true);
        leaveRoomCooled = true;
    }

    void OnEnable(){
		exitBUtton.SetActive(true);
		leaveRoomCooled = true;

    }

    public void ExitButton()
    {
        LeaveRoom(false);

		//MILESTONES 1-LevelUP 2-FinishedMap
		//_GameSaveLoad.Instance.Save (true);

        Deactivate();

        LoadingManager.Instance.OpenPanel("Town");
        LoadingManager.Instance.CallFadeCanvas(1);

	
    }

    void Deactivate()
    {
		exitBUtton.SetActive(false);

        Invoke("DelayedDeactivate", 3f);
    }

    void DelayedDeactivate()
    {
        gameObject.SetActive(false);
    }


    public void LeaveRoom(bool playChickenSound)
    {
        if (!leaveRoomCooled)
        {
            return;
        }
        GameManager.Instance.NextTownSpawn = "PlayerSpawnLobby";
        if (playChickenSound)
        {
            AudioManager.Instance.PlayClip("leave", true, false);
        }
        Invoke("LeaveRoomDelayed", 2.1f);

    }

    public void LeaveRoomDelayed()
    {
        PhotonNetwork.LeaveRoom();
        GameManager.Instance.joinRandomTown = true;
        //gotta do this before ActivateDeactivate();
        BattleManager.Instance.mapWhenAllHopeIsLost1.SetActive(false);
		BattleManager.Instance.mapWhenAllHopeIsLost2.SetActive(false);
		BattleManager.Instance.mapWhenAllHopeIsLost3.SetActive(false);
		BattleManager.Instance.mapWhenAllHopeIsLost4.SetActive(false);
        BattleManager.Instance.mapRuins1.SetActive(false);
        BattleManager.Instance.mapRuins2.SetActive(false);
        BattleManager.Instance.mapRuins3.SetActive(false);
		BattleManager.Instance.mapRuins4.SetActive(false);

        GameManager.Instance.ActivateDeactivate("Town");
    }
}
