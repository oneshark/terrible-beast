﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsCanvas : MonoBehaviour
{
    public CanvasGroup cg;
    public Animator anim;
    public Slider soundSlider, musicSlider;
    private static SettingsCanvas _instance;
    public static SettingsCanvas Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<SettingsCanvas>();
            return _instance;
        }
    }

    private void Awake()
    {
        {
            cg = GetComponent<CanvasGroup>();
        }
    }
    public void Delete()
    {
        ToggleAreYouSure(true);
    }

    public void SetSoundLevel()
    {
        AudioManager.Instance.soundLevel = soundSlider.value;
        AudioManager.Instance.setLevels();
    }


    public void SetMusicLevel()
    {
        AudioManager.Instance.musicLevel = musicSlider.value;
        AudioManager.Instance.setLevels();
    }

    public void Close()
    {
        Debug.Log("Close");
        ToggleMe(false,false);
    }

    //public void CallSettingsIn()
    //{
    //    if (InventoryCanvas.Instance.equipmentOpen) InventoryCanvas.Instance.ToggleEquipment(false);
    //    if (ShopManager.Instance.shopCanvasOpen) ShopManager.Instance.ToggleMe(false);
    //    ToggleMe(true);
    //}


    public bool canvasOpen = false;
    public void ToggleMe(bool b, bool skipMenuCanvas)
    {
        if (b == canvasOpen) return;
        if (b)
        {
            GameManager.Instance.InputEnabled = false;
            AudioManager.Instance.PlayClip("inventoryslide", true, false);
            TownCanvas.Instance.ToggleMe(false);
            anim.CrossFade("SettingsIn", 0);
            cg.interactable = true;
            cg.blocksRaycasts = true;
            soundSlider.value = AudioManager.Instance.soundLevel;
            musicSlider.value = AudioManager.Instance.musicLevel;
        }
        else
        {
            if (!InventoryCanvas.Instance.equipmentOpen) GameManager.Instance.InputEnabled = true;
            AudioManager.Instance.PlayClip("inventoryslide", true, false);
            anim.CrossFade("SettingsOut", 0);
            cg.interactable = false;
            cg.blocksRaycasts = false;
            if (!skipMenuCanvas)
            {
                MenuCanvas.Instance.ToggleMe(true);
                TownCanvas.Instance.ToggleMe(true);
            }
        }
        canvasOpen = b;
    }

    public void YesImSure()
    {
        AudioManager.Instance.PlayClip("click", true, false);
        _GameSaveLoad.Instance.DeleteSaveFile();

        ToggleAreYouSure(false);
        ToggleMe(false,true);
        Invoke("DelayedTurnMenusBackOn", 4);
    }

    public void DelayedTurnMenusBackOn()
    {
        TownCanvas.Instance.ToggleMe(true);
        MenuCanvas.Instance.ToggleMe(true);
    }

    public void NoImNotSure()
    {
        AudioManager.Instance.PlayClip("click", true, false);

        ToggleAreYouSure(false);
    }

    public GameObject areYouSureObj;
    public void ToggleAreYouSure(bool b)
    {
        areYouSureObj.SetActive(b);
    }

    public void ExitGame()
    {
    _GameSaveLoad.Instance.Save(false);
    LoadingManager.Instance.battlePanel.SetActive(false);
    LoadingManager.Instance.craftPanel.SetActive(false);
    LoadingManager.Instance.mainLoadPanel.SetActive(false);
		LoadingManager.Instance.CallFadeCanvas(1);
		ToggleMe(false,true);
    Invoke("LetsQuit",2);
    }

    void LetsQuit(){
    Debug.Log("QUIT");
		Application.Quit();

    }
}
