﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomButton : MonoBehaviour
{
	public Text playerCountTxt;
	public Text roomNameTxt;
	public Text waveTxt;
	public Text InvasionNameTxt;
	public Text JoinableTxt;
	public Text pingTxt;
	int playerSpace = 0;
	bool clickable = true;
	public GameObject lockedImage;
	RoomInfo m_ri;

	public void SetRoomButton (RoomInfo ri)
	{
		clickable = true;
		m_ri = ri;
		playerCountTxt.text = ri.playerCount + "/" + ri.maxPlayers;
		roomNameTxt.text = ri.name;
		int m_wave = (int)ri.customProperties ["Wave"];
		waveTxt.text = "Wave:" + m_wave;
		if (m_wave >= 11) {
			//some kind of graphic lock
			clickable = false;
			lockedImage.SetActive (true);
		} else {
			lockedImage.SetActive (false);
		}

		if ((bool)ri.customProperties ["Locked"] == true) {
			lockedImage.SetActive (true);
			clickable=false;
		}
	
		InvasionNameTxt.text = ri.customProperties ["Invasion"].ToString ();
		JoinableTxt.text = ri.customProperties ["Joinable"].ToString ();
		pingTxt.text = "Ping " + ri.customProperties ["Ping"].ToString ();
		retries = 0;
	}

	void SilentClicked ()
	{
		Clicked (true);
	}

	public int retryMax;
	public int retries = 0;

	public void Clicked (bool dontPlaySound)
	{
	//If the room is locked do this stuff
		if (!clickable) {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Wave > 10", "Room Locked");
			puc.SetRejectIcon ();
			puc.SetTextColor ("white");
			nextObj.SetActive (true);
			AudioManager.Instance.PlayClip ("rejection", true, false);
			return;
		}

		if (!dontPlaySound)
			AudioManager.Instance.PlayClip ("click", true, false);

		LobbyCanvas.Instance.ToggleMe (false);

		if (m_ri.playerCount >= m_ri.maxPlayers) {
            Debug.Log("Maxed Players - cant join it");
            retries = retryMax;//finish this
            return;
		}

		if (m_ri.customProperties ["Type"].ToString () == "Battle_Final") {
			GameManager.Instance.ActivateDeactivate ("Battle");
		}

		if (!FinalJoinableCheck (m_ri.name)) {
			Debug.Log ("!finaljoincheck retries:" + retries);
			retries++;
			if (retries > retryMax) {
				//give up and return
				GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
				PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
				puc.SetPopup ("You could not", "Join Room");
				puc.SetRejectIcon ();
				puc.SetTextColor ("white");
				nextObj.SetActive (true);
				AudioManager.Instance.PlayClip ("rejection", true, false);

				LobbyCanvas.Instance.RefreshRooms ();
				GameManager.Instance.ActivateDeactivate ("Clear");
				//LobbyCanvas.Instance.ToggleMe (true);
				LobbyCanvas.Instance.ToggleMe (false);

				LobbyCanvas.Instance.LeaveLobby();
				CancelInvoke ();
				return;
			} else {
                //keep trying
                Invoke("SilentClicked", 2);

                return;
			}
		}

		//Final Join Success  
		string mapChosen = m_ri.customProperties ["Invasion"].ToString ();
		BattleManager.Instance.mapStarted = mapChosen;
		JoinBattle ();
	}

	void JoinBattle ()
	{
		Debug.Log ("JoinBattle() Success");
		LoadingManager.Instance.OpenPanel ("Battle");
		LoadingManager.Instance.CallFadeCanvas (1);

		BattleManager.Instance.battleJoinSuccess = false;//reset the flag
		PhotonNetwork.JoinOrCreateRoom (m_ri.name, null, null);
	}

	

	bool FinalJoinableCheck (string s)
	{
		foreach (RoomInfo game in PhotonNetwork.GetRoomList()) {
			ExitGames.Client.Photon.Hashtable h = game.customProperties;
			if (game.name == s) {
                Debug.Log("Final Joinable Check isJoinable:" + (bool)game.customProperties["Joinable"]);

                return (bool)game.customProperties ["Joinable"];
			}
		}
        //NEVER GONNA JOIN CUZ NO ROOM
        retries = retryMax+1;
		return false;
	}

	void OnPhotonJoinFailed ()
	{
		Debug.Log ("Caught! OnPhotonJoinFailed");
	}

	void DelayedJoin ()
	{
	}
}
