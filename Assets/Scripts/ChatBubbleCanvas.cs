﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBubbleCanvas : MonoBehaviour
{

	public Text message;
	public Animator anim;
	//adventurer networks are not required
	public AdventurerNetwork adventurerNetwork;
	public AdventurerTownNetwork adventurerTownNetwork;
	public GameObject[] objectsToToggle;

	[PunRPC]
	public void CallOpenBubble (params object[] args)
	{
		int _viewID = (int)args [0];
		if (adventurerNetwork != null) {
			if (adventurerNetwork.photonView.viewID != _viewID)
				return;
		}
		if (adventurerTownNetwork != null) {
			if (adventurerTownNetwork.photonView.viewID != _viewID)
				return;
		}

		OpenBubble ("Test Bubble");
	}

//	void Update ()
//	{
//		if (isOpen & message.text == "")
//			CloseBubble ();
//	}

	void ToggleOtherObjects (bool b)
	{
		if (objectsToToggle.Length == 0)
			return;
		if (objectsToToggle.Length == null)
			return;

		foreach (GameObject obj in objectsToToggle) {
			obj.SetActive (b);
		}
	}

	bool isOpen = false;

	public void OpenBubble (string txt)
	{
		isOpen = true;
		message.text = "";
		StartCoroutine ("DelayText", txt);
		anim.CrossFade ("OpenBubble", 0);
		ToggleOtherObjects (false);
	}

	IEnumerator DelayText (string txt)
	{
		yield return new WaitForSeconds (.2f);
		message.text = txt;
	}

	public void CloseBubble ()
	{
		message.text = "";
		anim.CrossFade ("CloseBubble", 0);
		ToggleOtherObjects (true);
		isOpen = false;
	}
}
