﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRollRandom : MonoBehaviour
{

	// Update is called once per frame
	void Update ()
	{
		int roll = Random.Range (4, 10) * (Random.value < 0.5 ? -1 : 1);
		Debug.Log ("Roll:" + roll);
	}

}
