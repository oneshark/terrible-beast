﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCController : AnimationController
{
	public bool horizontalOnly = false;
	public float specialIdleMaxTimeInterval = 30;
	public float specialIdleMaxPlayTime = 10;
	public string currentAnimation="idle";
	bool targetReached;
	public int facing = 1;
	public float waitTime;
	public List<GameObject> waypoints = new List<GameObject> ();
	float interpVelocity;
	public bool followOnly;
	public float followDistance;
	public float currentDistance;
	public GameObject target;
	public Vector3 offset;
	Vector3 targetPos;
	public float speed = 0.1f;

	public bool allowMovement = true;

	new void Start ()
	{
		skeletonAnimation = GetComponentInChildren<SkeletonAnimation> ();
		//hardode ranalong to horizontal only
		horizontalOnly = true;
		if (horizontalOnly) {
			//set all waypoints to the same y value
         
			foreach (GameObject waypoint in waypoints) {
				waypoint.transform.position = new Vector3 (waypoint.transform.position.x, this.gameObject.transform.position.y + .5f, this.gameObject.transform.position.z);
			}
            
		}
		Physics2D.IgnoreLayerCollision (gameObject.layer, gameObject.layer, true);

		base.Start ();
		if (allowMovement) {
			if (followOnly) {
				targetReached = false;
			} else {
				StartCoroutine (setTarget (0));
			}
		}

	}



	#region Special Idle Animation

	void Awake ()
	{
		//coroutines would die on scene change so we do this
		StartCoroutine (specialIdleAnimations ());
	}

	IEnumerator specialIdleAnimations ()
	{
		float timer = Random.Range ((specialIdleMaxTimeInterval / 3), specialIdleMaxTimeInterval);
		yield return new WaitForSeconds (timer);
		if (currentAnimation != "idle") {
			//print("Not idling cant play special");
			StartCoroutine (specialIdleAnimations ());
		} else {
			if (hasThisAnimation ("idle0")) {

				if (currentAnimation != "idle0") {
					SetAnimation ("idle0", true);
					StartCoroutine (specialIdleAnimationEnd ());
				}
			}
		}
	}

	IEnumerator specialIdleAnimationEnd ()
	{

		float timer = Random.Range ((specialIdleMaxPlayTime / 3), specialIdleMaxPlayTime);
		yield return new WaitForSeconds (timer);
		if (currentAnimation == "idle0") {
			SetAnimation ("idle", true);
		}
		StartCoroutine (specialIdleAnimations ());
	}

	#endregion

	IEnumerator setTarget (float startDelay)
	{
		//print("set target");
		yield return new WaitForSeconds (startDelay);
		targetReached = false;
		target = waypoints [Random.Range (0, waypoints.Count)];
	}

	IEnumerator setTargetFollowOnly (float startDelay)
	{
		//print("set target");
		yield return new WaitForSeconds (startDelay);
		targetReached = false;
       
	}

	void FixedUpdate ()
	{
		if (!allowMovement)
			return;
		if (target & !targetReached) {
			currentDistance = Vector3.Distance (transform.position, target.transform.position);

			if (currentDistance > followDistance) {
				setFacing ();
				Vector3 posNoZ = transform.position;
				posNoZ.z = target.transform.position.z;

				Vector3 targetDirection = (target.transform.position - posNoZ);

				interpVelocity = 1;// targetDirection.magnitude * 5f;

				targetPos = transform.position + (targetDirection.normalized * 5 * Time.deltaTime);

				transform.position = Vector3.Lerp (transform.position, targetPos + offset, speed);
				if (currentAnimation != "run")
					SetAnimation ("run", true);

				float distanceToFollow = 2;
				if (followOnly)
					distanceToFollow = followDistance;
				if (Vector3.Distance (transform.position, target.transform.position) < distanceToFollow) {
					targetReached = true;
					if (currentAnimation != "idle")
						SetAnimation ("idle", true);
					//print("reached dest");
					if (followOnly) {
						StartCoroutine (setTargetFollowOnly (Random.Range (2, 4)));
					} else {
						StartCoroutine (setTarget (Random.Range (2, 4)));
					}
				}

			}

		}

      
	}

	void setFacing ()
	{
		if (target.transform.position.x < transform.position.x & facing != 0)
			setFacing (0);
		if (target.transform.position.x > transform.position.x & facing != 1)
			setFacing (1);
	}

	public void setFacing (int i)
	{
		bool isFlip = false;
		if (facing != i)
			isFlip = true;
		if (isFlip) {
			facing = i;
			transform.Rotate (Vector3.up * 180);

		}
	}

	void SetAnimation (string animation, bool isLoop)
	{
		//time saver
		SetAnimation (animation, isLoop, false);
	}

	public override void SetAnimation (string animation, bool isLoop, bool isOnComplete)
	{
		base.SetAnimation (animation, isLoop, false);
		currentAnimation = animation;
	}

}
