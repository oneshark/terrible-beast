﻿using UnityEngine;
using System.Collections;

public class AdventurerMovement : MonoBehaviour
{
    //network
    private bool isRemotePlayer = true;

    //general movement
    public bool isMoving = false;
    public float walkSpeedHorizontal;
    public int facing = 1;
    public AdventurerAnimation adventurerAnimation;
    public AdventurerNetwork adventurerNetwork;
    public GameObject graphics;
    public float jumpHeight = 10;
    //  AdventurerAnimation advAnimation;

    void Awake()
    {
        adventurerNetwork = GetComponent<AdventurerNetwork>();
        adventurerAnimation = GetComponentInChildren<AdventurerAnimation>();

        // advAnimation = GetComponent<AdventurerAnimation>();
    }

    void FixedUpdate()
    {
        Raycasts();
        KeyDowns();
    }

    void KeyDowns()
    {
        //if (Input.GetKeyDown("space"))
        //{
        //    if (adventurerNetwork.isGrounded) Jump();
        //}
    }

    void Raycasts()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log("Someone Clicked on:" + hit.collider.gameObject);
                if (hit.collider.gameObject.GetComponentInChildren<ClickableObject>())
                {
                    hit.collider.gameObject.GetComponentInChildren<ClickableObject>().OnClick();
                }
                else
                {
                    //No Enemy Clicked - just a WIFF 
                    //  adventurerNetwork.Attack(false,0,null);
                }
            }
        }
    }

    /// <summary>
    /// call the animation script to tell if if we are idle,run,hit, etc.
    /// </summary>
    public void SetIsRemotePlayer(bool val)
    {
        isRemotePlayer = val;
    }

    
    public void SetFacing(int shouldFace)
    {
        //// RAYCAST TO SEE WHICH SIDE THE MOUSE IS ON
        // //int shouldFace = 0;
        // Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // RaycastHit hit;
        // if (Physics.Raycast(ray, out hit))
        // {
        //     if (transform.position.x > hit.point.x)
        //     {
        //         shouldFace = 0;
        //     }
        //     else
        //     {
        //         shouldFace = 1;
        //     }
        // }

        bool isFlip = false;
        if (facing != shouldFace) isFlip = true;

        //FLIP THE GRAPHICS
        if (isFlip)
        {
            facing = shouldFace;
            graphics.transform.Rotate(Vector3.up * 180);
        }
    }

    public void SyncFacing(int shouldFace)
    {
        bool isFlip = false;
        if (facing != shouldFace) isFlip = true;

        //FLIP THE GRAPHICS
        if (isFlip)
        {
            facing = shouldFace;
            graphics.transform.Rotate(Vector3.up * 180);
        }
    }
}
