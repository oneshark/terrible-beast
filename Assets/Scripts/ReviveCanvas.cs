﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviveCanvas : MonoBehaviour {
    public Text percentText;
    public Image filledImage;
    public PhotonView photonView;
    public AdventurerNetwork adventurerNetwork;
    public Collider myCollider;

    public CanvasGroup cg;
    bool fadingIn = false;
    bool fadingOut = false;
    bool cgVisible;
    bool isReviveCool;

    public float max,fill,inc;

    void Update()
    {
        Raycasts();
    }

    void Raycasts()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100))
        {
            //HoverLogic(hit.collider);
            ClickLogic(hit.collider);
        }
    }

    bool clickLogicCool = true;
    void ClickLogic(Collider col)
    {
        //if we are the player allow no clicking to the revive module
        if (adventurerNetwork.playerID == BattleManager.Instance.myAdventurer.playerID) return;
        //if we are dead - dont let clicks through
        if (BattleManager.Instance.myAdventurer.charState == CharacterState.dead) return;

        //if our attack cooldown not ready dont let it get clicked
        if (!BattleManager.Instance.myAdventurer.isCool())
        {
         //   Debug.Log("##### You tried to click revive but not cool");
            return;
        }

        if (Input.GetMouseButton(0))
        {
            if (col == myCollider)
            {
                if (!clickLogicCool) return;
                //Debug.Log("ClickLogic");

                photonView.RPC("Clicked", PhotonTargets.All, photonView.viewID, GameManager.Instance.currentEntityStat.atkMax, adventurerNetwork.currentEntityStat.hpMax);

                BattleManager.Instance.myAdventurer.StartAttackCooldown();

                StartCoroutine("ClickLogicCool");
            }
                    }
    }

   IEnumerator ClickLogicCool()
    {
        clickLogicCool = false;
        yield return new WaitForSeconds(.01f);
        clickLogicCool = true;
    }

    [PunRPC]
    public void Clicked(int viewId,float inc, float max)
    {
        if (photonView.viewID != viewId) return;
        fill =fill+ inc;
        filledImage.fillAmount = fill / max;
        float formattedPerc = ((fill / max) * 100);
        int roundedPerc = (int)formattedPerc;
        if (roundedPerc > 99)
        {
            //Debug.Log("CALL THE REVIVE!!!");
            roundedPerc = (int)max;//
            photonView.RPC("CallCool", PhotonTargets.All); //start cooldown so nobody else can trigger revive
            AudioManager.Instance.PlayClip("revive", true, false);

            adventurerNetwork.photonView.RPC("Revive", PhotonTargets.All);

        }
        if (roundedPerc > 99) roundedPerc = 100;//max is 100 dont let it go over
        percentText.text =roundedPerc  + "%";
        
    }

    [PunRPC]
    public void CallCool()
    {
        Cool(2);
    }

    IEnumerator Cool(float timer)
    {
        isReviveCool = false;
        yield return new WaitForSeconds(timer);
        isReviveCool = true;
    }

    public void CallFadeCanvas(float _desiredAlpha)
    {
        StartCoroutine(FadeCanvasGroup(_desiredAlpha));
    }

    IEnumerator FadeCanvasGroup(float _newAlpha)
    {

        if (_newAlpha == 0)
        {
            fadingIn = false;
            fadingOut = true;
        }
        else
        {
            fadingIn = true;
            fadingOut = false;
        }


        float _currentAlpha = cg.alpha;

        //Fading out
        if (_newAlpha < _currentAlpha)
        {
            for (float alpha = _currentAlpha; alpha >= _newAlpha; alpha -= .1f)
            {
                yield return new WaitForSeconds(0);
                cg.alpha = alpha;
            }
        }

        //Fading in
        if (_newAlpha > _currentAlpha)
        {
            for (float alpha = _currentAlpha; alpha <= _newAlpha; alpha += .1f)
            {
                yield return new WaitForSeconds(0);
                cg.alpha = alpha;
            }
        }
        cg.alpha = _newAlpha;
        if (cg.alpha == 1)
        {
            cgVisible = true;
        }
        else
        {
            cgVisible = false;
        }
    }
}
