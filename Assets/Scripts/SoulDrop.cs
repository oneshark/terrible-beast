﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulDrop : MonoBehaviour
{
    public CanvasGroup cg;
    public SoulSize soulSize;
    public float value;
    public Rigidbody2D rb;
    public float magnetSpeed = 6;
    //USE THIS TO MAKE IT MORE POWERFUL LATER
    public float magnetPower = 3;
    bool isSeek;//seek mouse position
    Vector3 targetPos;
    public bool isActive;//not active until hit ground
    Vector2 flyAwayTarget = new Vector2(0, 0);
    Vector3 originalPos = new Vector3(0, 0, 0);
    bool isFlyingAway = false;
    float currentScale = 1;
    public float originalScale;
    public float expireTime = 5;
    public float expireTimeRemain;
    public bool isExpired = false;
	public LayerMask mask;

    void Start ()
	{
		GameObject playerSpawn0 = GameObject.FindGameObjectWithTag ("PlayerSpawn0");


        rb = GetComponent<Rigidbody2D>();
        Physics.IgnoreLayerCollision(0, 0, true);
    }

	GameObject currentTarget=null;

	bool isMoving=false;
    void Update()
    {
		expireTimeRemain -= Time.deltaTime;

        if (expireTimeRemain < 0) //No Time Left start the expire shrink
        {
            Expired();
        }

        if (isExpired)
        {
            currentScale -= .08f;
            transform.localScale = new Vector3(currentScale, currentScale, currentScale);
            if (currentScale < 0)
            {
                currentScale = 0;
                TurnOffAfterExpire();  //expire shrink done, turn it off and set it back to normal state for next use from obj pool
            }
        }
        if (isFlyingAway)
        {
            currentScale -= .08f;
            if (currentScale < 0) currentScale = 0;
            transform.localScale = new Vector3(currentScale, currentScale, currentScale);

            transform.position = Vector3.MoveTowards(transform.position, flyAwayTarget, 20f * Time.deltaTime);//.05f
            if (Vector3.Distance(transform.position, flyAwayTarget) < .1f)
            {
                Arrived();
            }
        }

		if (rb.velocity.y <= -11)
			Land ();
    }

 AdventurerNetwork myAtn;
	void OnTriggerEnter2D(Collider2D other) {
		//BattleManager.Instance.SpawnWave (wave);
		myAtn=other.GetComponent<AdventurerNetwork>();
		if (myAtn == BattleManager.Instance.myAdventurer) {
			flyAwayTarget = myAtn.gameObject.transform.position;

			PickItUp ();
		}
	}

	public void CallLand(){
		Invoke ("Land", Random.Range(.1f,.3f));
	}

	void Land(){
		rb.gravityScale = 0;
		rb.velocity = Vector2.zero;
	}

	public Collider2D myCollider;

    void PickItUp()
    {
		//Debug.Log ("Pickup called");
        if (isExpired) return;
		if (GameManager.Instance.currentScene == CustomSceneType.Battle) {//CANT GET STUFF YOUR DEAD!
			if (BattleManager.Instance.myAdventurer.charState == CharacterState.dead)
				return;
		}

        GameManager.Instance.IncSouls((int)value);
		Debug.Log ("=============FlyAway");
        FlyAway();
    }

    float startTime;
    void FlyAway()
    {
        AudioManager.Instance.PlayClip("soulpickup", true,false);
        isActive = false;
        originalPos = transform.position;
        isFlyingAway = true;
        float startTime = Time.time;
        currentScale = originalScale;
    }

    void Arrived()
    {
        isExpired = false;
        isFlyingAway = false;
        gameObject.SetActive(false);
        transform.localScale = new Vector3(originalScale, originalScale, originalScale);
    }

    void Expired()
    {
        if (isExpired) return;
        isExpired = true;
        currentScale = originalScale;
    }

    void TurnOffAfterExpire()
    {
   // Debug.Log("SOUL EXPIRED");
        isExpired = false;
        isFlyingAway = false;
        transform.localScale = new Vector3(originalScale, originalScale, originalScale);
        gameObject.SetActive(false);
    }

}
