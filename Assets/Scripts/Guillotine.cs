﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guillotine : MonoBehaviour {
    SkeletonAnimation sa = new SkeletonAnimation();
    Animator anim;
    public PhotonView photonView;
    void Awake()
    {
        anim = GetComponent<Animator>();
        sa = GetComponentInChildren<SkeletonAnimation>();
        photonView = GetComponent<PhotonView>();
    }

    public void CallSetAnimation(string animationName, bool loop)
    {
        photonView.RPC("SetAnimation", PhotonTargets.All, animationName, loop);
    }

    [PunRPC]
    public void SetAnimation(string animationName, bool loop)
    {
        sa.state.SetAnimation(0, animationName, loop);
    }

    public void CallToggleMe(bool b)
    {
        photonView.RPC("ToggleMe", PhotonTargets.All, b);
    }

    [PunRPC]
    public void ToggleMe(bool b)
    {
        if (b)
        {
            anim.CrossFade("GuillotineIn", 0);
        }
        else
        {
            anim.CrossFade("GuillotineOut", 0);
            Invoke("Deactivate",2);
			GameManager.Instance.waitingOnGuillotine = false;
			//Debug.Log ("Toggle GUILLOTINE false");
			//Delay, ClipIndex
			GameManager.Instance.CallPlayTutorialBlock(1.5f,4,true);
        }
    }

  public void Deactivate()
    {
        gameObject.SetActive(false);
	
    }

    public void ForceOff()
    {
        anim.CrossFade("GuillotineOff", 0);
    }

}
