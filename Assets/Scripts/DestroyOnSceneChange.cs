﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnSceneChange : MonoBehaviour
{
public bool justDisable=true;
	void OnEnable ()
	{
		EventManager.OnSceneChange += DestroyMe;
	}

	void OnDisable ()
	{
		EventManager.OnLoadComplete -= DestroyMe;
	}

	void DestroyMe ()
	{
		if (justDisable) {
		gameObject.SetActive(false);
		} else {
			Destroy (gameObject);
		}
	}
}
