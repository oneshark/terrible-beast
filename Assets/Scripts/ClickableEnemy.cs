﻿using UnityEngine;
using System.Collections;

public class ClickableEnemy : MonoBehaviour
{
    AdventurerNetwork adventurerNetwork;
    public EnemyNetwork enemyNetwork;
    public PhotonView photonView;
    public BoxCollider2D myCollider;
    [Header ("Use this for weakspots 1=normal, 1.25 equal 25% more damage")]
    public float damageAdjustment = 1;
	public LayerMask layerMask;

    public void OnClick()
    {
      if (!clickCool) return;
        StartCoroutine(ClickCool());       
        // Debug.Log("Clicked on Enemy w PhotonViewID:" + photonView.viewID);
        adventurerNetwork = BattleManager.Instance.myAdventurer;
        if (!CanClick())
        {
            return;
        }
        if (!adventurerNetwork.isCool())
        {
            return;
        }
        if (adventurerNetwork.charState == CharacterState.dead) return;

        if (enemyNetwork == null)
        {
            return;
        }
        float enX = enemyNetwork.transform.position.x;
        adventurerNetwork.Attack(true, photonView.viewID, enemyNetwork,damageAdjustment,enX);
    }



    bool clickCool = true;
    IEnumerator ClickCool()
    {
        clickCool = false;
        yield return new WaitForSeconds(.1f);
        clickCool = true;
    }

    void FixedUpdate()
    {
        Raycasts();
    }


    public bool CanClick()
    {
        bool _canClick = true;

		AdventurerNetwork atn =BattleManager.Instance.myAdventurer;
		if (!CheckInRange(enemyNetwork.rootPosition.transform.position,atn.transform.position,GameManager.Instance.currentEntityStat.range)){
			//Debug.Log ("OUT OF RANGE");
			_canClick = false;
            GameObject mouse = GameObject.FindGameObjectWithTag("MouseFillAndPop");
            MouseFillAndPop mfp = mouse.GetComponent<MouseFillAndPop>();
            mfp.PopUpX();

        }
			
		if (enemyNetwork.baseEntityStat.isRanged & !adventurerNetwork.currentEntityStat.isRanged){
			_canClick=false;
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("You need ranged", "Weaponry!");
			puc.SetRejectIcon ();
			puc.SetTextColor ("white");
			nextObj.SetActive (true);
		//	Debug.Log("Dont forget a pop up about not allowing clicks cause you need ranged!");
		}

        if (enemyNetwork.isDying)
        {
            _canClick = false;
        }
        //Debug.Log("******* isHome:" + enemyNetwork.isHome+"  currentAnimation:"+ enemyNetwork.enemyAnimation.currentAnimation + " currentAnimation:"+enemyNetwork.enemyAnimation.skeletonAnimation.AnimationName);
        //Debug.Log("CanClick:" + _canClick);
        return _canClick;
    }

	bool CheckInRange(Vector3 oneObj, Vector3 twoObj, float _rangeAllowed){
		float xDist = oneObj.x - twoObj.x;
		//Debug.Log ("X Dist:" + xDist + " RangeAllowed:"+_rangeAllowed);
		if (xDist < 0)
			xDist *= -1;
		if (xDist > _rangeAllowed) {
			return false;
		}
		
		float yDist = oneObj.y - twoObj.y;
		//Debug.Log ("Y Dist:" + yDist + " RangeAllowed:"+_rangeAllowed);
		if (yDist < 0)
			yDist *= -1;
		//y Distance should be limited or it looks unequal
		yDist *= 1.75f;
		if (yDist > _rangeAllowed) {
			return false;
		}
		return true;
	}

    //Check for hovering over enemy
    void Raycasts()
    {
		if (GameManager.Instance.waitingOnGuillotine)
			return;
        if (adventurerNetwork == null) adventurerNetwork = BattleManager.Instance.myAdventurer;
        if (adventurerNetwork == null) {
            //Debug.Log("ClickableEnemy: AdventurerNEtwork Null");
            return;//tried to get ref but still null
            }


		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), Vector2.zero,99,layerMask);


		//RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		if (hit.collider != null) {
			HoverLogic2D (hit.collider);
			ClickLogic2D (hit.collider);
			//Debug.Log ("ClickableeEnemy Target: " + hit.collider.gameObject.transform.position);
		} else {
			//Debug.Log ("CLickable Enemy Target Null");
		}
		
    }
    void ClickLogic2D(Collider2D col)
    {
        if (Input.GetMouseButton(0))
        {
            if (col == myCollider)
            {
                //normal damage
               // Debug.Log("ClickLogic, get mousebutton");
                OnClick();

            }
        }

    }

    void HoverLogic2D(Collider2D col)
    {
        if (enemyNetwork.isDying) return;//we dont want this turning on if they r dying, cuz its annoying
        //if its a boss cause the LIFEBAR to stay on
        if (enemyNetwork.enemyType == EnemyType.boss)
        {
            if (enemyNetwork.lifebarCanvas.cg.alpha == 0)//if its turned off - try to turn it on
            {
                enemyNetwork.lifebarCanvas.CallFadeCanvas(1);
            }
            return;
        }


        if (col == myCollider)//WE know we r hovering on the collider
        {
            if (enemyNetwork.lifebarCanvas.cg.alpha == 0)//if its turned off - try to turn it on
            {
                enemyNetwork.lifebarCanvas.CallFadeCanvas(1);
            }
        }
        else//we are not hover on the collider
        {
			try{
            if (enemyNetwork.lifebarCanvas.cg.alpha != 0) enemyNetwork.lifebarCanvas.CallFadeCanvas(0);
			}catch{
				Debug.Log ("Error caught 172 of Clickable Enemy");
			}
        }
    }

}
