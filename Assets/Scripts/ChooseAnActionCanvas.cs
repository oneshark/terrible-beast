﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseAnActionCanvas : MonoBehaviour
{
	public AdventurerTownNetwork m_ChosenATN;

	//on right click we should set who the player is right away
	public void SetPlayer (AdventurerTownNetwork _atn)
	{
		m_ChosenATN = _atn;

	}

	public void Trade ()
	{
		GameManager.Instance.InputEnabled = true;
		AudioManager.Instance.PlayClip ("click", true, false);
		TradeCanvasSelector.Instance.SetPanel ("StillThinking");

		int _myViewId = TownManager.Instance.myAtn.photonView.viewID;
		int _otherViewId = m_ChosenATN.photonView.viewID;

		m_ChosenATN.photonView.RPC("WantsToTradeWithYou", PhotonTargets.Others, _myViewId,_otherViewId);
		gameObject.SetActive (false);


	}
}
