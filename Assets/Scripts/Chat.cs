using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// This simple chat example showcases the use of RPC targets and targetting certain players via RPCs.
/// </summary>
public class Chat : Photon.MonoBehaviour
{

    private static Chat _instance;
    public static Chat Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<Chat>();
            return _instance;
        }

    }
    public InputField chatInputCustom, chatInputDecoy;
    public static Chat SP;
    public List<string> messages = new List<string>();

    private int chatHeight = (int)140;
    private Vector2 scrollPos = Vector2.zero;
    private float lastUnfocusTime = 0;

    void Awake()
    {
        SP = this;
    }

    public void Submit()
    //WHEN PRESS ENTER ON A TEXT IT SENDS WITH THIS METHOD
    {
        Invoke("SetDecoy", .1f);
        SendChat(PhotonTargets.All);
    }

    void SetDecoy()
    {
        chatInputCustom.interactable = false;
        if (chatInputDecoy == null) return;
        chatInputDecoy.Select();
    }


    void Update()
    {
        //if (EventSystem.current.currentSelectedGameObject!=null) print(EventSystem.current.currentSelectedGameObject.ToString());

        if (Input.GetKeyDown(KeyCode.Return))
        {
            //Debug.Log("Enter Key");
            if (!chatInputCustom.isFocused)
            {
                chatInputCustom.enabled = true;
                chatInputCustom.interactable = true;
                GameManager.Instance.ToggleInputEnabled(true);
                chatInputCustom.Select();
            }

        }

    }

    public static void AddMessage(string text)
    {
        SP.messages.Add(text);
        if (SP.messages.Count > 15)
            SP.messages.RemoveAt(0);
    }

    public void ClearChatBox()
    {
        GameObject chatParent = GameObject.FindGameObjectWithTag("ChatBoxParent");
        if (chatParent == null)
        {
            Debug.Log("No chat parent");
            return;
        }
        foreach (Transform child in chatParent.transform)
        {
            // do whatever you want with child transform object here
            if (child.gameObject.activeSelf)
            {
               // child.gameObject.SetActive(false);
				Destroy(child.gameObject);

            }
        }
    }

    public static void AddMessageToCanvas(string text)
    {

        GameObject chatParent = GameObject.FindGameObjectWithTag("ChatBoxParent");
        int charsCount = 0;
        foreach (char c in text)
        {
            if (char.IsLetter(c))
            {
                charsCount++;
            }
        }
        string sizeBox = "UI/ChatMessageShort";
        if (charsCount > 53)//50 is two chars off
        {
            sizeBox = "UI/ChatMessage";
        }

        GameObject chatBox = ((GameObject)Instantiate(Resources.Load(sizeBox)));
        ChatMessageObject cmo = chatBox.GetComponent<ChatMessageObject>();
        cmo.SetText(text);

		if (chatParent!=null)
        chatBox.transform.SetParent(chatParent.transform,false);

        //chatBox.transform.parent = chatParent.transform;

		//Debug.Log(" chatParent.transform.childCount:"+chatParent.transform.childCount);
        //enforce limit
		if (chatParent==null)return;

		if (chatParent.transform.childCount > 27)
        {
            foreach (Transform child in chatParent.transform)
            {
           // Debug.Log("CHAT: child.transform:"+child.gameObject.name);
                // do whatever you want with child transform object here
                if (child.gameObject.activeSelf)
                {
                Text txt = child.GetComponent<Text>();
                 //   child.gameObject.SetActive(false);
                    Destroy(child.gameObject);
                    break;
                }
            }
        }

        //check number of transforms under parent
        //remove top one if >> limit

    }




    [PunRPC]
    void SendChatMessage(string text, string senderName)
    {
        AddMessage(senderName + ": " + text);
        AddMessageToCanvas("<color=yellow>" + senderName + ":</color> " + text);
    }

    void SendChat(PhotonTargets target)
    {
        PhotonView adventPhotonView = null;

        if (GameManager.Instance.currentScene == CustomSceneType.Battle & BattleManager.Instance != null)
        {
            adventPhotonView = BattleManager.Instance.myAdventurer.photonView;
        }

        if (GameManager.Instance.currentScene == CustomSceneType.Town & TownManager.Instance != null)
        {
            adventPhotonView = TownManager.Instance.myAdventurerTownNetwork.photonView;
        }

        object[] masterArgs = new object[] { adventPhotonView.viewID, chatInputCustom.text };

        if (chatInputCustom.text != "")
        {
            adventPhotonView.RPC("CallChatBubble", PhotonTargets.All, masterArgs);

            photonView.RPC("SendChatMessage", target, chatInputCustom.text, GameManager.Instance.PlayerName);
            chatInputCustom.text = "";
        }
    }

    public void SendChat(PhotonPlayer target)
    {
        if (chatInputCustom.text != "")
        {
            chatInputCustom.text = "[PM] " + chatInputCustom.text;
            photonView.RPC("SendChatMessage", target, chatInputCustom.text, GameManager.Instance.PlayerName);
            chatInputCustom.text = "";
        }
    }

    void OnLeftRoom()
    {
        this.enabled = false;
    }

    void OnJoinedRoom()
    {
        this.enabled = true;
    }
    void OnCreatedRoom()
    {
        this.enabled = true;
    }
}
