﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameOverCanvas : MonoBehaviour
{
    public Text waveNumber;
    public Text rankPrev, rankNext, soulsAcquiredText, highScoreText;
    public int rankCurrent;
    public int soulsAcq = 0;
    public int highScore = 0;

    public void SetHighestLevelReached(int highestWaveReached)
    {
        waveNumber.text = highestWaveReached.ToString();
    }

    public void Exit()
    {
        if (!exitCooled) return;
        StartCoroutine(ExitCooldown());

       // GameManager.Instance.SetBlackFaderTrigger("ClearToBlack", 0);
        Invoke("ExitDelayed", 0);
    }

    private void Awake()
    {
        exitCooled = true;
    }
    bool exitCooled = true;
    IEnumerator ExitCooldown()
    {
        exitCooled = false;
        yield return new WaitForSeconds(.8f);
        exitCooled = true;

    }

    public void ExitDelayed()
    {
        GameManager.Instance.NextTownSpawn = "PlayerSpawnLobby";
        LoadingManager.Instance.OpenPanel("Town");
        LoadingManager.Instance.CallFadeCanvas(1);

        BattleCanvas.Instance.LeaveRoom(false);
        gameObject.SetActive(false);
       
    }
}
