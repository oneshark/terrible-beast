﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollowRandomizer : MonoBehaviour
{
    public float timer = 0;
    public float secondsBetweenPositionChange = 5;
    public Vector3 originalOffset;
    public Vector3 customOffset, previousOffset, nextOffset;
    public float moveSpeed = .01f;
    public float xChangeMax = 1, yChangeMax = 1, zChangeMax = 1;

    SmoothFollow smoothFollow;

    void Awake()
    {
        smoothFollow = GetComponent<SmoothFollow>();
        originalOffset = deepCopyVector(smoothFollow.offset);
    }

    void Start()
    {
        previousOffset = deepCopyVector(originalOffset);
        nextOffset = deepCopyVector(originalOffset);
        ChangePosition();
    }

    public Vector3 deepCopyVector(Vector3 orig)
    {
        Vector3 newOne = new Vector3(orig.x, orig.y, orig.z);
        return newOne;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > secondsBetweenPositionChange)
        {
            ChangePosition();
        }
        else
        {
            LerpToPosition();//while timer is going, this position is changing
        }
    }

    void LerpToPosition()
    {
        
        customOffset = Vector3.Lerp(previousOffset, nextOffset, Time.deltaTime * moveSpeed);
        //Debug.Log("CustomOffset was:" + customOffset);
        smoothFollow.offset = deepCopyVector(customOffset);
    }

    void ChangePosition()
    {
        //Debug.Log("ChangePosition():"+xChangeMax);
        float xPos = Random.Range(-xChangeMax, xChangeMax);
        float yPos = Random.Range(-yChangeMax, yChangeMax);
        float zPos = Random.Range(-zChangeMax, zChangeMax);
        previousOffset = deepCopyVector(nextOffset);
        
      //  if (TownManager.Instance.myAtn.)
        nextOffset = new Vector3(originalOffset.x + xPos, originalOffset.y + yPos, originalOffset.z+zPos);
    //    Debug.Log("NextOffset.z:" + nextOffset.z);
        timer = 0;
    }
}
