﻿using UnityEngine;
using System.Collections;

public class ClickableObject : MonoBehaviour
{
    void Start() { 
        gameObject.tag = "ClickableObject";
    }

    public virtual void OnClick()
    {
        print("Click Object!");
    }

    public virtual void OnCollect()
    {
        //print("Collect Object!");
    }


}
