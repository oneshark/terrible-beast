﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyCanvas : MonoBehaviour
{
	private static LobbyCanvas _instance;

	public static LobbyCanvas Instance {
		get {
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<LobbyCanvas> ();
			return _instance;
		}
	}

	public GameObject roomParent;
	public InputField roomInput;
	public InputField passwordInput;
//optional
	CanvasGroup cg;
	public GameObject goButton;
	public GameObject closeButton;

	void Awake ()
	{
		cg = GetComponent<CanvasGroup> ();
	}

	public void Close ()
	{

	}

	public void Go ()
	{
		//  Debug.Log("Enter the Room");
		AudioManager.Instance.PlayClip ("click", true, false);
		LoadingManager.Instance.OpenPanel ("Battle");
		LoadingManager.Instance.CallFadeCanvas (1);
		if (connectedToMaster) {
			//SET PARAM FOR ROOM INFO
			ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable ();
			h ["Wave"] = 1;
			h ["Type"] = "Battle_Final";
			h ["Invasion"] = GameManager.Instance.selectedMap;
			h ["Joinable"] = false;
			h ["Ping"] = PhotonNetwork.GetPing ();
			h ["Locked"] = false;

			//  h["LastJoin"] = 30;


			// PhotonNetwork.room.SetCustomProperties(h);
			string[] customPropertiesForLobby = new string[6];
			customPropertiesForLobby [0] = "Wave";
			customPropertiesForLobby [1] = "Type";
			customPropertiesForLobby [2] = "Invasion";
			customPropertiesForLobby [3] = "Joinable";
			customPropertiesForLobby [4] = "Ping";
			customPropertiesForLobby [5] = "Locked";


			// customPropertiesForLobby[0] = "Wave";
			string roomName = roomInput.text;
			if (roomName == "")
				roomName = "Invasion_" + Random.Range (0, 9999);

			GameManager.Instance.ActivateDeactivate ("Battle"); //this object will be deactivated after this.

			PhotonNetwork.CreateRoom (roomName, new RoomOptions () {
				MaxPlayers = 4,
				CustomRoomProperties = h,
				CustomRoomPropertiesForLobby = customPropertiesForLobby
			}, TypedLobby.Default);
			ToggleMe (false);
		}
	}

	public bool connectedToMaster = false;

	public void OnConnectedToMaster ()
	{
		connectedToMaster = true;
		// this method gets called by PUN, if "Auto Join Lobby" is off.
		// this demo needs to join the lobby, to show available rooms!
		PhotonNetwork.JoinLobby ();  // this joins the "default" lobby
	}

	public void OnJoinedLobby ()
	{
		//Debug.Log ("**Joined Lobby and trying to join:" + GameManager.Instance.selectedMap);

		if (GameManager.Instance.selectedMap == "QuickStart") {
			QuickJoin ();
			return;
		}

		ToggleGoClose (true);
		RefreshRooms ();
	}

	void OnJoinedRoom ()
	{
		Debug.Log ("OnJoinedRoom()");
		//  Invoke("DelayFadeIn", 4);
	}

	public void ToggleGoClose (bool b)
	{
		goButton.SetActive (b);
		closeButton.SetActive (b);

	}

	public void RefreshRooms ()
	{
		ClearRoomButtons ();
		//Debug.Log("Length of rooms:"+PhotonNetwork.GetRoomList().Length);
		Debug.Log ("PhotonNetwork.RoomList:" + PhotonNetwork.GetRoomList ().Length);

		foreach (RoomInfo game in PhotonNetwork.GetRoomList()) {
			ExitGames.Client.Photon.Hashtable h = game.customProperties;
			if (game.customProperties ["Type"].ToString () != "Battle_Final") {
				// Debug.Log("Didn't make a button, this is a town room");
			} else {
				CreateRoomButton (game);
			}
		}
	}

	public int quickJoinMaxTries = 5;
	public int quickJoinTry = 0;

	public void QuickJoin ()
	{
		quickJoinTry++;
		//Debug.Log("QUICK JOIN CALLED tries:" + quickJoinTry);

		GameManager.Instance.NextTownSpawn = "PlayerSpawnLobby";

		bool foundRoom = false;

		Debug.Log ("PhotonNetwork.RoomList.Length:" + PhotonNetwork.GetRoomList ().Length);
		foreach (RoomInfo game in PhotonNetwork.GetRoomList()) {
			Debug.Log ("NEXT ROOM NAME:" + game.name);

			ExitGames.Client.Photon.Hashtable h = game.customProperties;

			if (game.customProperties ["Type"].ToString () != "Battle_Final") {
				continue;
			}

			Debug.Log("QuickJoin block 2");
			
			//Debug.Log ("QuickJoin Room Game:" + game.customProperties ["Type"].ToString () + game.customProperties ["Invasion"].ToString ());
			string _invasionName = game.customProperties ["Invasion"].ToString ();

		
			if (_invasionName != "WhenAllHopeIsLost") { //Is this When All Hope Is Lost
				if (!GameManager.Instance.completedThings.Contains (_invasionName)) {
					continue;
				}
			}
			
			Debug.Log("QuickJoin block 3");

			foundRoom = true;
			GameManager.Instance.ActivateDeactivate ("Battle");
			BattleManager.Instance.mapStarted = game.customProperties ["Invasion"].ToString ();
			GameManager.Instance.selectedMap = game.customProperties ["Invasion"].ToString ();
			BattleManager.Instance.battleJoinSuccess = false;//reset the flag
			PhotonNetwork.JoinOrCreateRoom (game.name, null, null);
			CancelInvoke ();
			return;
		}

		if (!foundRoom) {
			Debug.Log ("Did not find room");
			if (quickJoinTry >= quickJoinMaxTries) {
				GiveUpQuickJoin ();
			} else {
				Invoke ("QuickJoin", 1);
			}
		}
	}

	void OnPhotonJoinRoomFailed ()
	{
		GiveUpQuickJoin ();
	}

	//join a random town and stop trying to loop for battles
	public void GiveUpQuickJoin ()
	{
		Debug.Log ("GIVE UP QUICK JOIN!");
        if (!giveUpCool)
        {
            Debug.Log("Give Up Isnt Cool!");
            return;
        }
        Debug.Log("Give Up Is Cool!");
		StartCoroutine (GiveUpCool ());
		GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
		PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
		puc.SetPopup ("There were no suitable", "Rooms Open");
		puc.SetRejectIcon ();
		puc.SetTextColor ("white");
		nextObj.SetActive (true);
		AudioManager.Instance.PlayClip ("rejection", true, false);
		GameManager.Instance.JoinRandomTown ();
		GameManager.Instance.InputEnabled = true;
	}

	bool giveUpCool = true;

	IEnumerator GiveUpCool ()
	{
		giveUpCool = false;
		yield return new WaitForSeconds (.1f);
		giveUpCool = true;
	}

	public void CreateRoomButton (RoomInfo ri)
	{
		string invasionMapName = ri.customProperties ["Invasion"].ToString ();
		if (invasionMapName != GameManager.Instance.selectedMap)
			return;//only start the maps that are for this one

		GameObject obj = ObjectPool.Instance.NextObject ("RoomButton");
		obj.transform.position = new Vector3 (0, 0, 0);
		obj.transform.parent = roomParent.transform;
		obj.SetActive (true);
		RoomButton roomButton = obj.GetComponent<RoomButton> ();
		roomButton.SetRoomButton (ri);
	}

	public void ClearRoomButtons ()
	{
		foreach (Transform child in roomParent.transform) {
			child.gameObject.SetActive (false);
		}
	}

	public void LeaveLobby ()
	{
		if (!leaveRoomCooled)
			return;
		StartCoroutine (LeaveRoomCooldown ());
		AudioManager.Instance.PlayClip ("click", true, false);

		ToggleMe (false);
		Invoke ("LeaveRoomDelayed", 0);
	}

	bool leaveRoomCooled = true;

	IEnumerator LeaveRoomCooldown ()
	{
		leaveRoomCooled = false;
		yield return new WaitForSeconds (3);
		leaveRoomCooled = true;
	}

	public void LeaveRoomDelayed ()
	{
		GameManager.Instance.ActivateDeactivate ("Clear");
		GameManager.Instance.NextTownSpawn = "PlayerSpawnLobby";
		PhotonNetwork.LeaveRoom ();
		GameManager.Instance.ActivateDeactivate ("Town");
		GameManager.Instance.JoinRandomTown ();
	}



	public bool lobbyCanvasOpen = false;

	public void ToggleMe (bool b)
	{
		if (lobbyCanvasOpen == b)
			return;
		if (b) {
			leaveRoomCooled = true;
			cg.alpha = 1;
			cg.interactable = true;
			cg.blocksRaycasts = true;
			StartCoroutine (Refresher ());
		} else {
			cg.alpha = 0;
			cg.interactable = false;
			cg.blocksRaycasts = false;
			StopAllCoroutines ();
		}
		lobbyCanvasOpen = b;
	}

	IEnumerator Refresher ()
	{
		yield return new WaitForSeconds (2);
		PhotonNetwork.JoinLobby ();

		RefreshRooms ();

		StartCoroutine (Refresher ());
	}

}
