using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class AdventurerNetwork : Photon.MonoBehaviour
{
	public bool isInitialized = false;
	public int playerID = 0;
	public CharacterState charState = CharacterState.normal;
	MouseFillAndPop mfp;
	public GameObject graphics;
	public int facing = 1;
	public float cannonKnockback = 1500;
	public int playerPosition = 0;
	//which standing position they are in
	public EnemyNetwork currentTarget;
	public bool lastHit;
	public Stance stance;
	public Stats baseEntityStat;
	[Header ("-NO NEED TO SET CURRENT STATS THIS TIME-")]
	public Stats currentEntityStat;
	AdventurerCamera adventurerCamera;
	public AdventurerAnimation adventurerAnimation;
	//LifebarCanvas lifebarCanvas;
	private bool appliedInitialUpdate;
	public Rigidbody2D rb;
	public ChatBubbleCanvas cbc;
	public MeshRenderer mr;
	public ReviveCanvas rc;
	[Header ("Used for Juiced Up")]
	public float originalScale, bigScale;
	[Header ("Equipped Stuff")]
	public Item equippedCostumeHead;
	public Item equippedCostumeBody, equippedCostumeBack, equippedWeapon, equippedEquipmentRing, equippedEquipmentNecklace, equippedEquipmentArtifact;

	public bool isJuiced = false;
	public float juicedPercent = 1;

	public bool isShielded = false;
	public float shieldDuration = 10;
	public GameObject shieldGraphic;
	public float lastEnemyX;
	public BattleMover battleMover;
	public GameObject footLocation;

	void Awake(){
		battleMover = GetComponentInChildren<BattleMover> ();
	}

	void Start ()
	{
		adventurerCamera = GetComponent<AdventurerCamera> ();
		rb = GetComponent<Rigidbody2D> ();

		if (photonView.isMine) {
			StartAttackCooldown ();
		} else {
			adventurerCamera.enabled = false;
		}
		gameObject.name = gameObject.name + photonView.viewID;
	}

	public bool isCool ()
	{
		return mfp.isCool;
	}

	public void Initialize ()
	{
		currentEntityStat.hp = GameManager.Instance.currentEntityStat.hp;
		GameManager.Instance.IncSouls (0);

		Invoke ("LateInitialize", .1f);
	}

	public void LateInitialize ()
	{
		if (charState != CharacterState.chained) {
			adventurerAnimation.SetAnimation (stance + "-Idle", true, false);
		}
		if (PhotonNetwork.player.ID == playerID) {
			GameManager.Instance.SetAllEquipment (photonView);

			CalculateStats (true);
			CallMatchStats ();
			isInitialized = true;

			CallHeal (9999999, false, true);
			//CRUDE HACK TO COVER FOR MISSING WEAPON
			Invoke ("LateEquipmentCleanup", 1.5f);
		}

	}

	public void LateEquipmentCleanup ()
	{
		GameManager.Instance.ChangeEquipment (GameManager.Instance.equippedWeapon, BattleManager.Instance.myAdventurer.photonView);
	}

	[PunRPC]
	public void CallHealEffect (int viewID, int val, float x, float y)
	{
		if (photonView.viewID != viewID)
			return;
		BattleTextManager.Instance.CreateText (BattleTextType.green, new Vector3 (x, y, 0), val);

		GameObject nextObj = ObjectPool.Instance.NextObject ("HealEffect");
		nextObj.transform.position = new Vector3 (x, y + 1, 0);
		nextObj.SetActive (true);

	}

	[PunRPC]
	public void CallRemoveJuicedEffect (int viewID, float x, float y)
	{
		if (photonView.viewID != viewID)
			return;
		GameObject nextObj = ObjectPool.Instance.NextObject ("PoofEffect");
		nextObj.transform.position = new Vector3 (x, y, 0);
		nextObj.SetActive (true);
		photonView.RPC ("CallScaleOriginal", PhotonTargets.All);
		isJuiced = false;
	}

	[PunRPC]
	public void CallJumpFeathers (int viewID, Vector3 _pos)
	{
		if (photonView.viewID != viewID)
			return;
		GameObject feathers = ObjectPool.Instance.NextObject ("JumpFeathers");
		feathers.transform.position = _pos;
//		feathers.transform.localScale = new Vector3 (scale, scale, scale);
		feathers.SetActive (true);
	}

	[PunRPC]
	public void CallPoof (int viewID, Vector3 _pos)
	{
		if (photonView.viewID != viewID)
			return;
		GameObject feathers = ObjectPool.Instance.NextObject ("PoofEffect");
		feathers.transform.position = _pos;
		feathers.SetActive (true);
	}



	[PunRPC]
	public void CallRemoveShieldEffect (int viewID, float x, float y)
	{
		if (photonView.viewID != viewID)
			return;
		GameObject nextObj = ObjectPool.Instance.NextObject ("PoofEffect");
		nextObj.transform.position = new Vector3 (x, y, 0);
		nextObj.SetActive (true);
		shieldGraphic.SetActive (false);
		isShielded = false;
	}

	[PunRPC]
	public void CallScaleOriginal ()
	{
		CameraEffects ce = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraEffects> ();
		ce.CallCameraShake (.6f);
		Invoke ("ScaleOriginal", .5f);
	}

	void ScaleOriginal ()
	{
		graphics.transform.localScale = new Vector3 (originalScale, originalScale, originalScale);
	}


	[PunRPC]
	public void CallJuicedEffect (int viewID, float x, float y)
	{
		if (photonView.viewID != viewID)
			return;

		isJuiced = true;
		GameObject nextObj = ObjectPool.Instance.NextObject ("PoofEffect");
		nextObj.transform.position = new Vector3 (x, y, 0);
		nextObj.SetActive (true);

		photonView.RPC ("CallScaleBig", PhotonTargets.All);
	}



	[PunRPC]
	public void CallShieldEffect (int viewID, float x, float y)
	{
		if (photonView.viewID != viewID)
			return;
		isShielded = true;
		GameObject nextObj = ObjectPool.Instance.NextObject ("PoofEffect");
		nextObj.transform.position = new Vector3 (x, y, 0);
		shieldGraphic.SetActive (true);
		nextObj.SetActive (true);
	}

	[PunRPC]
	public void CallScaleBig ()
	{
		CameraEffects ce = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraEffects> ();
		ce.CallCameraShake (.6f);
		Invoke ("ScaleBig", .5f);
	}

	void ScaleBig ()
	{
		graphics.transform.localScale = new Vector3 (bigScale, bigScale, bigScale);
	}


	public void CallHeal (int val, bool isRegen, bool isSilent) //call heal on host+clients
	{
		//basic method that Heals (use for regen) 
		photonView.RPC ("Heal", PhotonTargets.All, photonView.viewID, val, isRegen, isSilent);
		Invoke ("CallSyncHP", .25f); //synch hp after its called on everyone
	}

	[PunRPC]
	public void Heal (int _viewID, int val, bool isRegen, bool isSilent)
	{
		if (photonView.viewID != _viewID)
			return;
		if (isRegen) {
			val = (int)(currentEntityStat.hpMax * currentEntityStat.regen);
			Debug.Log ("REGEN AMOUNT:" + (currentEntityStat.hpMax * currentEntityStat.regen));
		}

		currentEntityStat.hp += val;
		if (currentEntityStat.hp > currentEntityStat.hpMax)
			currentEntityStat.hp = currentEntityStat.hpMax;

		if (!isSilent) {
			//call pop up text
		}
	}

	public void CallMatchStats ()
	{
		Stats ces = currentEntityStat;
		photonView.RPC ("MatchStats", PhotonTargets.Others, ces.atkMin, ces.atkMax, ces.crit, ces.critPower, ces.defMin, ces.defMax, ces.regen, ces.hp, ces.hpMax);
		Invoke ("CallSyncHP", .25f);
	}

	[PunRPC]
	public void MatchStats (float atkMin, float atkMax, float crit, float critPower, float defMin, float defMax, float regen, float hp, float hpMax)
	{
		currentEntityStat.atkMin = atkMin;
		currentEntityStat.atkMax = atkMax;
		currentEntityStat.crit = crit;
		currentEntityStat.critPower = critPower;
		currentEntityStat.defMin = defMin;
		currentEntityStat.defMax = defMax;
		currentEntityStat.regen = regen;
		currentEntityStat.hp = hp;
		currentEntityStat.hpMax = hpMax;
		isInitialized = true;
	}

	[PunRPC]
	public void CallIncXP (int _viewID, int val)
	{
		if (photonView.viewID != _viewID)
			return;

		//we only want to call mirror stats on the right adventurer
		//Debug.Log("Whoah got exp");
		if (!photonView.isMine) {
			return;
		}
		IncXP (_viewID, val);
	}



	void IncXP (int _viewID, int val)
	{
		GameManager.Instance.baseEntityStat.xp += val;
		//trying to match up

		if (GameManager.Instance.baseEntityStat.xp >= GameManager.Instance.baseEntityStat.xpMax) {
			if (GameManager.Instance.baseEntityStat.xpLevel > LevelUpManager.Instance.LevelCap) {
				GameManager.Instance.baseEntityStat.xp = GameManager.Instance.baseEntityStat.xpMax;
				return;
			}

			GameManager.Instance.baseEntityStat.xp = 0;
			GameManager.Instance.baseEntityStat.xpLevel++;


			PlayerCanvas.Instance.playerLevelText.text = GameManager.Instance.baseEntityStat.xpLevel.ToString ();

			int myID = BattleManager.Instance.myAdventurer.photonView.viewID;

			if (PhotonNetwork.player.ID == playerID) {
				LevelUpManager.Instance.CallLevelUp ();

				GameManager.Instance.baseEntityStat.hpMax += 10;
				//CRUDE HACK TO THE HP MAX
				baseEntityStat.hpMax += 10;
				currentEntityStat.hpMax += 10;

				float healAmount = currentEntityStat.hpMax * 1f;
				CallHeal ((int)healAmount, false, false);

				GameManager.Instance.CalculateStats ();

			}

			LevelEntry nextLev = LevelUpManager.Instance.levelConfig [(int)GameManager.Instance.baseEntityStat.xpLevel];
			GameManager.Instance.baseEntityStat.xpMax = nextLev.xpRequired;

		}
		PlayerCanvas.Instance.CallExp (val);
		PlayerCanvas.Instance.SetXP (GameManager.Instance.baseEntityStat.xp, GameManager.Instance.baseEntityStat.xpMax);
	}

	[PunRPC]
	public void Hit (int _viewID)
	{
		if (photonView.viewID != _viewID)
			return;//only do it on the right character

		string hitString = stance + "-Hit";
		adventurerAnimation.SetAnimation (hitString, false, false);
	}

	[PunRPC]
	public void CallChatBubble (params object[] args)
	{
		int _viewID = (int)args [0];
		string msg = (string)args [1];
		if (photonView.viewID != _viewID)
			return;
		//we only want to call mirror stats on the right adventurer
		//  Debug.Log("Call Chat from viewID:" + photonView.viewID);
		cbc.OpenBubble (msg);
		CancelInvoke ("CloseChatBubble");
		Invoke ("CloseChatBubble", 3);
		//now - remove the local one and call it for all clients
	}

	[PunRPC]
	public void CallPopUpText (params object[] args)
	{
		float val = (float)args [0];
		bool isCrit = (bool)args [3];
		if (val == 0) {
			// Debug.Log("Whoah empty damage texts!!!!");
			return;
		}
		if (isCrit) {
			BattleTextManager.Instance.CreateText (BattleTextType.enemycrit, transform.position, val);
		} else {
			BattleTextManager.Instance.CreateText (BattleTextType.red, transform.position, val);
		}
	}

	void CloseChatBubble ()
	{
		cbc.CloseBubble ();
	}

	[PunRPC]
	public void CallSetSkin (int viewId, string skin)
	{
		if (photonView.viewID != viewId)
			return;

		adventurerAnimation.SetSkin (skin);
	}

	[PunRPC]
	public void CallSetAttachment (int viewID, string slotName, string slotName2, string attachemntName, string attachmentName2)
	{
		if (photonView.viewID != viewID)
			return;
        
		adventurerAnimation.SetAttachment (slotName, slotName2, attachemntName, attachmentName2);

	}


	public void OnPhotonPlayerConnected (PhotonPlayer newPlayer)
	{
		if (photonView.isMine) {
			Invoke ("BroadcastGearMaster", .2f);// BroadcastGearMaster();

			object[] args = new object[] {
				adventurerAnimation.currentAnimation,
				adventurerAnimation.skeletonAnimation.loop,
				false
			};
			photonView.RPC ("CallSyncAnimation", PhotonTargets.Others, args);

		}
	}

	public void BroadcastGearMaster ()
	{
		//add scale
		photonView.RPC ("SetScaleAll", PhotonTargets.Others, photonView.viewID,transform.localScale.x);
		//  Debug.Log("BroadcastGear");

		equippedWeapon = GameManager.Instance.equippedWeapon;
		equippedCostumeBack = GameManager.Instance.equippedCostumeBack;
		equippedCostumeBody = GameManager.Instance.equippedCostumeBody;
		equippedCostumeHead = GameManager.Instance.equippedCostumeHead;

		object[] clientArgs = new object[] { photonView.viewID, (int)GameManager.Instance.equippedWeapon.stance };
		photonView.RPC ("SetStanceClient", PhotonTargets.All, clientArgs);
		photonView.RPC ("SetParticlesAll", PhotonTargets.All, equippedWeapon.ID, photonView.viewID);
		//face
		Face newFace = InventoryManager.Instance.GetFace (GameManager.Instance.equippedFace);
		photonView.RPC ("CallSetAttachment", PhotonTargets.All, photonView.viewID, "h", "NOSLOT", newFace.attachmentName, "");

		// Debug.Log("&&&&& BroadCastGearMaster CallSetAttachment");
		photonView.RPC ("CallSetAttachment", PhotonTargets.Others, photonView.viewID, equippedWeapon.slotName, equippedWeapon.slotName2, equippedWeapon.atachmentName, equippedWeapon.attachmentName2);
		photonView.RPC ("CallSetSkin", PhotonTargets.Others, photonView.viewID, equippedCostumeBody.skin);
		photonView.RPC ("CallSetAttachment", PhotonTargets.Others, photonView.viewID, equippedCostumeBack.slotName, equippedCostumeBack.slotName2, equippedCostumeBack.atachmentName, equippedCostumeBack.attachmentName2);

		//HEAD BLOCK NEEDS TO CHECK FOR EMPTY SO IT DOESNT START WITH A CROWN OR ANYTHING WEIRD
		if (equippedCostumeHead == null) {
			photonView.RPC ("CallSetAttachment", PhotonTargets.All, photonView.viewID, "Hat", "NOSLOT", "Weapons/Empty", "");
		} else if (equippedCostumeHead.name == "") {
			photonView.RPC ("CallSetAttachment", PhotonTargets.All, photonView.viewID, "Hat", "NOSLOT", "Weapons/Empty", "");
		} else {
			photonView.RPC ("CallSetAttachment", PhotonTargets.Others, photonView.viewID, equippedCostumeHead.slotName, equippedCostumeHead.slotName2, equippedCostumeHead.atachmentName, equippedCostumeHead.attachmentName2);
		}

	}

	[PunRPC] public void SetScaleAll (int viewId, float preferredScale)
	{
		if (photonView.viewID != viewId)
			transform.localScale = new Vector3 (preferredScale, preferredScale, preferredScale);
	}

	[PunRPC]
	public void SetParticlesAll (int _weapId, int viewId)
	{
		if (photonView.viewID != viewId)
			return;//its not right player
		Debug.Log ("Set Particles for all players on this guy");
		Item weaponFromDB = InventoryManager.Instance.GetItem (ItemType.weapon, _weapId);
		adventurerAnimation.SetParticles (weaponFromDB);
	}


	[PunRPC]
	public void SetStanceClient (params object[] args)
	{
		int viewID = (int)args [0];
		int stanceIndex = (int)args [1];
		if (photonView.viewID != viewID)
			return;//its not right player

		switch (stanceIndex) {
		case 0:
			stance = Stance.Onehander;
			break;
		case 1:
			stance = Stance.Bow;
			break;
		case 2:
			stance = Stance.Dualwield;
			break;
		case 3:
			stance = Stance.Twohander;
			break;
		case 4:
			stance = Stance.Cannon;
			break;
		case 5:
			stance = Stance.Rod;
			break;
		case 6:
			stance = Stance.Bareknuckle;
			break;

		}
		StanceInfo stanceInfo = new StanceInfo ();
		stanceInfo = stanceInfo.GetStanceInfo (stance);

		currentEntityStat.isRanged = stanceInfo.isRanged;
		string animationString = stance + "-Idle";
		adventurerAnimation.SetAnimation (animationString, true, false);
	}

	private Vector3 correctPlayerPos = Vector3.zero;
	//We lerp towards this

	void Update ()
	{
		if (!photonView.isMine) {
			//Update remote player (smooth this, this looks good, at the cost of some accuracy)
			transform.position = Vector3.Lerp (transform.position, correctPlayerPos, Time.deltaTime * 5);
            //transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        }
	}

   

    public void CallGray (bool b)
	{
		CameraEffects ce = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraEffects> ();
		ce.ToggleGrayscale (b);
	}

	public void ChangeState (CharacterState newState)
	{

		switch (newState) {
		case CharacterState.chained:
			break;
		case CharacterState.dead:
			break;
		case CharacterState.normal:
			isCallDeathCool = true;
			break;
		}
		charState = newState;

		// Debug.Log("Change State:" + charState);
	}

	[PunRPC]
	public void CallStartAttackCooldown (int _viewID)
	{
		if (photonView.viewID != _viewID)
			return;
		StartAttackCooldown ();
	}

	public void StartAttackCooldown ()
	{
		if (mfp == null)
			mfp = GameObject.FindGameObjectWithTag ("MouseFillAndPop").GetComponent<MouseFillAndPop> ();
		mfp.StartCooldown (currentEntityStat.atkCooldown);
	}

	public float lastDamageAdjustmentPercent = 1;

	public void Attack (bool isHit, int _viewID, EnemyNetwork enemyNetwork, float adjustmentPercent, float enemyX)
	{

		lastEnemyX = enemyX;
		if (enemyNetwork == null) {
			//Debug.Log ("*** TRIED TO ATTACK AND ENEMY DIED BEFORE WE COULD!");
			return;
		}
		lastDamageAdjustmentPercent = adjustmentPercent;
		lastHit = isHit;
		currentTarget = enemyNetwork;
		int sideEnemyIsOn = 0;
		//FACE LEFT B4 ATTACK
		if (enemyX < transform.position.x) {
			SetFacing (0);
			sideEnemyIsOn = 0;
			//call for clients too
			object[] args = new object[] { 0 };
			photonView.RPC ("CallSyncFacing", PhotonTargets.Others, args);
		}
		//FACE RIGHT B4 ATTACK
		if (enemyX > transform.position.x) {
			SetFacing (1);
			sideEnemyIsOn = 1;
			//call for clients too
			object[] args = new object[] { 1 };
			photonView.RPC ("CallSyncFacing", PhotonTargets.Others, args);
		}
		//	Debug.Log ("AdventurerNetwork Step 1");
		//build attack string + randomize
		int attackIndex = UnityEngine.Random.Range (1, 4);
		if (stance == Stance.Bow) {
			attackIndex = UnityEngine.Random.Range (1, 4);
			Debug.Log ("attackINdex:" + attackIndex);
			//give attack 1 a better chance than attack 2 which is special looking
			if (attackIndex == 3)
				attackIndex = 1;
			

		}
		string attackString = stance + "-Attack" + attackIndex;
		adventurerAnimation.SetAnimation (attackString, false, false);
		//  Debug.Log("************** CALLED ATTACk FROM ADVENTURER");

	}

	[PunRPC]
	public void CallPushBack(float amount){
		float adjustment = UnityEngine.Random.Range (0, 100);
		if (facing == 0)
		{
			rb.AddForce(Vector2.right * (amount+adjustment), ForceMode2D.Force);
		}
		else
		{
			rb.AddForce(Vector2.left * (amount+adjustment), ForceMode2D.Force);
		}
	}

	[PunRPC]
	public void CallAOEBySideMaster (int excludePhotonId, int side, int numberEnemies, float percentDamage, float pushBack)
	{
		//photonView.RPC("CallAOEBySideMaster", PhotonTargets.MasterClient, side, 3, .5f);

		//Shuffled OrderBy code
		List<EnemyNetwork> allEnemies = BattleManager.Instance.GetAllEnemies ().OrderBy (a => Guid.NewGuid ()).ToList ();

		List<EnemyNetwork> enemiesChosen = new List<EnemyNetwork> ();
		// Debug.Log("ExcludeID:" + excludePhotonId);
		for (int inc = 0; inc < allEnemies.Count; inc++) {

			EnemyNetwork nextEn = allEnemies [inc];

			//Add them if they are on correct side
			//EXCLUDE THE MAIN TARGET
			if (nextEn.photonView.viewID == excludePhotonId)
				continue;
			//   Debug.Log("Enemy Photon ID:" + nextEn.photonView.viewID);

			if (nextEn.gameObject.transform.position.x < transform.position.x & side == 0) {
				if (enemiesChosen.Count < numberEnemies)
					enemiesChosen.Add (nextEn);
			}

			if (nextEn.gameObject.transform.position.x > transform.position.x & side == 1) {
				if (enemiesChosen.Count < numberEnemies)
					enemiesChosen.Add (nextEn);
			}
		}

		//make a list of them which is random = count of numberEnemies
		//apply Damage using percent

		foreach (EnemyNetwork en in enemiesChosen) {
			float damage = UnityEngine.Random.Range (currentEntityStat.atkMin, currentEntityStat.atkMax);
			damage = damage - (UnityEngine.Random.Range (en.currentEntityStat.defMin, en.currentEntityStat.defMax));
			bool isSilent = false;
			bool isCrit = false;
			object[] args = new object[] { damage, en.photonView.viewID, isSilent, isCrit, photonView.viewID };
			en.photonView.RPC ("TakeDamageMaster", PhotonTargets.MasterClient, args);
			en.photonView.RPC("CallPushBack",PhotonTargets.MasterClient, GameManager.Instance.equippedWeapon.stat.knockbackPower,facing);
		}
	}

	[PunRPC]
	public void Revive ()
	{
		ChangeState (CharacterState.normal);
		int percHP = (int)(currentEntityStat.hpMax / 3);
		photonView.RPC ("SetHPMaster", PhotonTargets.All, photonView.viewID, percHP, (int)currentEntityStat.hpMax);
		//CallHeal(halfHP, false, true);
		photonView.RPC ("RemoveReviveCanvas", PhotonTargets.All);
		adventurerAnimation.SetAnimation (stance + "-Idle", true, false);
		Debug.Log ("PhotonView.IsMine" + photonView.isMine);
		BattleCanvas.Instance.ToggleYouAreDead (false);

	}

	public bool RollForCritical ()
	{
		bool isCrit = false;
		int rand = UnityEngine.Random.Range (0, 100);
		if (rand <= currentEntityStat.crit) {
			isCrit = true;
		}
		return isCrit;
	}


	public void SetFacing (int shouldFace)
	{
		bool isFlip = false;
		if (facing != shouldFace)
			isFlip = true;

		//FLIP THE GRAPHICS
		if (isFlip) {
			facing = shouldFace;
			graphics.transform.Rotate (Vector3.up * 180);
			photonView.RPC ("CallSyncFacing", PhotonTargets.Others, facing);

		}
	}

	public void SyncFacing (int shouldFace)
	{
		bool isFlip = false;
		if (facing != shouldFace)
			isFlip = true;

		//FLIP THE GRAPHICS
		if (isFlip) {
			facing = shouldFace;
			graphics.transform.Rotate (Vector3.up * 180);
		}
	}

	[PunRPC]
	public void CallSyncAnimation (params object[] args)
	{
		string animationName = (string)args [0];
		bool isLoop = (bool)args [1];
		bool isOnComplete = (bool)args [2];
		adventurerAnimation.SyncAnimation (animationName, isLoop, isOnComplete);
	}

	[PunRPC]
	public void CallSyncFacing (int _shouldFace)
	{
		SyncFacing (_shouldFace);
	}

	public void CallSyncHP ()
	{
		photonView.RPC ("SyncHPMaster", PhotonTargets.All, photonView.viewID);
	}

	[PunRPC]
	public void SyncHPMaster (int _viewID)
	{
		if (!PhotonNetwork.isMasterClient)
			return;
		object[] newArgs = new object[] { _viewID, (int)currentEntityStat.hp, (int)currentEntityStat.hpMax };

		photonView.RPC ("SyncHP", PhotonTargets.All, newArgs);
		//  Debug.Log("SyncHPMaster Finish:"+ (int)currentEntityStat.hp);

	}

	[PunRPC]
	public void SetHPMaster (int _viewID, int hpAmount, int hpMax)
	{
		if (!PhotonNetwork.isMasterClient)
			return;
		object[] newArgs = new object[] { _viewID, hpAmount, hpMax };
		photonView.RPC ("SyncHP", PhotonTargets.All, newArgs);
	}

	[PunRPC]
	public void SyncHP (int _viewID, int currHP, int currHPMax)
	{
		if (_viewID != photonView.viewID)
			return;
		if (charState == CharacterState.dead)
			return;
		//First we sync hp stats with the master 
		if (!PhotonNetwork.isMasterClient) {
			currentEntityStat.hp = currHP;//set this to the masters hp
			currentEntityStat.hpMax = currHPMax;//set this to the masters hpMax
		}

		if (photonView.isMine) {
			//Debug.Log("SyncHP SetHP");
			PlayerCanvas.Instance.SetHP (currentEntityStat.hp, GameManager.Instance.currentEntityStat.hpMax);
		}
	}


	[PunRPC]
	public void TakeDamageMaster (params object[] args)
	{
		//  Debug.Log("Take Damage Master");
		bool isSilent = (bool)args [2];
		if (!PhotonNetwork.isMasterClient)
			return;
		args [0] = Mathf.Round ((float)args [0]);//round damage
		if (isShielded)
			args [0] = (float)1;
		object[] newArgs = new object[] {
			(float)args [0],
			args [1],
			(int)currentEntityStat.hp,
			(int)GameManager.Instance.currentEntityStat.hpMax,
			args [3],
			args[4]
		};
		photonView.RPC ("TakeDamage", PhotonTargets.All, newArgs);
		if (!isSilent)
			photonView.RPC ("CallPopUpText", PhotonTargets.All, args);
	}

	[PunRPC] //ALSO CAN USE THIS TO SET HEALTH ;)
    public void TakeDamage (params object[] args)
	{
		
		
		//Wrong Enemy? Then quit method
		if ((int)args [1] != photonView.viewID)
			return;

		//Debug.Log("Args:" + args);
		float damage = (float)args [0];
		int viewID = (int)args [1];
		int hp = (int)args [2];
		int hpMax = (int)args [3];
		bool overrideRoll = (bool)args [4];
		if (!overrideRoll){
		if (battleMover.isRolling)
			return;//CANT HURT A ROLLER!
	}
		if (charState == CharacterState.dead)
			return;
	
		////TRYING TO REMOVE THIS AND SEE WHAT HAPPENS
		//First we sync hp stats with the master 
		if (!PhotonNetwork.isMasterClient) {
			//currentEntityStat.hp = hp;//set this to the masters hp
			//currentEntityStat.hpMax = hpMax;//set this to the masters hpMax
		}

		//Calculate damage
		damage = damage - UnityEngine.Random.Range (currentEntityStat.defMin, currentEntityStat.defMax);
		if (damage < 0)
			damage = 0;

		//Apply Damage
		currentEntityStat.hp -= damage;

		//Only master can destroy them
		if (PhotonNetwork.isMasterClient) {
			if (currentEntityStat.hp <= 0) {
				currentEntityStat.hp = 0;
				//PLAYER DEATH
				if (charState != CharacterState.dead)
					photonView.RPC ("CallDeath", PhotonTargets.All);

			}
		}
		if (photonView.isMine) {
			if (BattleManager.Instance.myAdventurer == null)
				BattleManager.Instance.myAdventurer = this;
			//Debug.Log("TakeDamage SetHP");
			PlayerCanvas.Instance.SetHP (currentEntityStat.hp, currentEntityStat.hpMax);
		}
	}

	public float RollForDef ()
	{
		return UnityEngine.Random.Range (currentEntityStat.defMin, currentEntityStat.defMax);
	}

	[PunRPC]
	public void BreakChains ()
	{
		adventurerAnimation.currentAnimation = stance + "-Idle";
		ChangeState (CharacterState.normal);
		adventurerAnimation.SetAnimation (stance + "-Idle", true, false);
	}

	[PunRPC]
	public void AddChains (int viewID)
	{
		if (photonView.viewID != viewID)
			return;
		ChangeState (CharacterState.chained);
		adventurerAnimation.SetAnimation ("Chained-Idle", true, false);
	}

	public bool isCallDeathCool = true;

	[PunRPC] //ALSO CAN USE THIS TO SET HEALTH ;)
    public void CallDeath ()
	{
		// photonView.RPC("AddReviveCanvas", PhotonTargets.Others);
		if (!isCallDeathCool)
			return;
		isCallDeathCool = false;
		ChangeState (CharacterState.dead);
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

		//charState = CharacterState.dead;
		GameObject ghost = ObjectPool.Instance.NextObject ("Ghost");
		ghost.transform.position = transform.position;
		ghost.SetActive (true);
		//Debug.Log("&&&&&&&&&&&&&&& CALL DEATH");
		string animString = stance + "-Death";

		if (PhotonNetwork.isMasterClient) {
			adventurerAnimation.SetAnimation (animString, false, false);
		}
		if (photonView.isMine) {//Call the local death effects here
			BattleCanvas.Instance.ToggleYouAreDead (true);
		}

		BattleManager.Instance.CheckAdventurersRemain ();
	}

	[PunRPC]
	public void AddReviveCanvas ()
	{//added to all others - not the actual player
		if (rc.gameObject.activeSelf)
			return;//this is already active - dont call all of this again
		rc.gameObject.SetActive (true);
		rc.filledImage.fillAmount = 0;
		rc.fill = 0;
		rc.percentText.text = "0%";
	}

	[PunRPC]
	public void RemoveReviveCanvas ()
	{
		if (!rc.gameObject.activeSelf)
			return;
		rc.gameObject.SetActive (false);
		rc.filledImage.fillAmount = 0;
		rc.fill = 0;
		rc.percentText.text = "0%";

	}

	[PunRPC]
	public void CallChatLog (string msg)
	{
		Debug.Log (msg);
		Chat.AddMessageToCanvas (msg);
	}

	[PunRPC]
	public void DropItemLocal (ItemType _itemType, int _id, Vector3 dropLocation)
	{
		
		GameObject obj = (GameObject)Instantiate (Resources.Load ("ItemPickup"), dropLocation, Quaternion.identity);

		dropLocation.z = 0;
		obj.transform.position = dropLocation+new Vector3(0,1,0);
		obj.SetActive (true);
		ItemPickup ip = obj.GetComponent<ItemPickup> ();
		ip.isActive = false;
		ip.isExpired = false;

		//X offset
		float xOffset = 0;
		if (BattleManager.Instance.mapStarted == "MisadventuresInTheRiverOfGod")
			xOffset = 3;
		//add force and fling it
		float xForce = UnityEngine.Random.Range (6 + xOffset, 6 - xOffset) * (UnityEngine.Random.value < 0.5 ? -1 : 1);

		ip.rb.AddForce (new Vector2 (xForce, UnityEngine.Random.Range(2,5f)), ForceMode2D.Impulse);
		ip.SetItem (_itemType, _id);
		//ip.photonView.RPC ("SetItem", PhotonTargets.All, _itemType, _id);
	}



	public void CalculateStats (bool isFirstTime)
	{
		//GRAB FROM GAME MANAGER
		//	Debug.Log("1 GM HPMax:"+GameManager.Instance.currentEntityStat.hpMax);
		Stats truebaseStat = baseEntityStat.MirrorStat (GameManager.Instance.baseEntityStat); //hpmax stays true - so needing this

		GameManager.Instance.CalculateStats ();//REFRESHH BEFORE SNAPPINGG A COPY OF IT
		Stats baseStat = baseEntityStat.MirrorStat (GameManager.Instance.currentEntityStat); //deep copy to keep them untied from each other
		//	Debug.Log("1.5 GM HPMax:"+GameManager.Instance.currentEntityStat.hpMax);

		PlayerCanvas.Instance.playerLevelText.text = GameManager.Instance.baseEntityStat.xpLevel.ToString ();

        //We may not need some of this = OLD LEGACY UPGRADES CODE
		//Debug.Log("=================================RAN================");
		currentEntityStat.atkMin = baseStat.atkMin;
		currentEntityStat.atkMax = baseStat.atkMax;
		currentEntityStat.atkCooldown = baseStat.atkCooldown;
		currentEntityStat.range = baseStat.range;

		float calcCrit = baseStat.crit;
		currentEntityStat.crit = baseStat.crit + calcCrit;
		currentEntityStat.critPower = baseStat.critPower;

		float upgradesHPAmount = truebaseStat.hpMax ;

		currentEntityStat.hpMax = baseStat.hpMax + upgradesHPAmount;

		//match it
		GameManager.Instance.currentEntityStat.hpMax = currentEntityStat.hpMax;
        currentEntityStat.defMin = baseStat.defMin;
        currentEntityStat.defMax = baseStat.defMax;
        currentEntityStat.regen = baseStat.regen;
	}


	public Stats AddStats (Stats main, Stats toAdd)
	{

		main.atkMin += toAdd.atkMin;
		main.atkMax += toAdd.atkMax;

		main.crit += toAdd.crit;
		main.critPower += toAdd.critPower;
		main.hpMax += (int)(toAdd.hpMax);
		main.defMin += toAdd.defMin;
		main.defMax += toAdd.defMax;
		main.atkCooldown += toAdd.atkCooldown;

		main.regen += toAdd.regen;
		return main;
	}





	#region NETWORK RELATED

	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			//We own this player: send the others our data
			// stream.SendNext((int)controllerScript._characterState);
			stream.SendNext (transform.position);
			// stream.SendNext(transform.rotation);
			stream.SendNext (GetComponent<Rigidbody2D> ().velocity);
			stream.SendNext (playerPosition);
			stream.SendNext (battleMover.isRolling);

		} else {
			//Network player, receive data
			//controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();
			correctPlayerPos = (Vector3)stream.ReceiveNext ();
			//  correctPlayerRot = (Quaternion)stream.ReceiveNext();
			GetComponent<Rigidbody2D> ().velocity = (Vector2)stream.ReceiveNext ();

			if (!appliedInitialUpdate) {
				appliedInitialUpdate = true;
				transform.position = correctPlayerPos;
				// transform.rotation = correctPlayerRot;
				GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
			}
			playerPosition = (int)stream.ReceiveNext ();
			battleMover.isRolling = (bool)stream.ReceiveNext ();

		}
	}

	void OnPhotonInstantiate (PhotonMessageInfo info)
	{
		//We know there should be instantiation data..get our bools from this PhotonView!
		object[] objs = photonView.instantiationData; //The instantiate data..
		bool[] mybools = (bool[])objs [0];   //Our bools!
	}

	void OnLeftRoom ()
	{
		// photonView.RPC("RemovePlayer", PhotonTargets.All);
	}

	public void OnPhotonPlayerDisconnected (PhotonPlayer otherPlayer)
	{
		//  Debug.Log("otherPlayer" + otherPlayer);
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.RemoveRPCs (otherPlayer);
			PhotonNetwork.DestroyPlayerObjects (otherPlayer);
		}
	}

	void OnDisconnectedFromServer ()
	{
	}

	#endregion

}

public enum CharacterState
{
	normal,
	dead,
	chained
}