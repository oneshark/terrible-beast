﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableAdventurer : MonoBehaviour
{
	public LayerMask mask;

	public Collider myCollider;

	void Update ()
	{
		if (Input.GetMouseButton (1)) {
		Debug.Log("Right Click");
			Raycasts ();
		}
	}

	//Check for hovering over enemy
	void Raycasts ()
	{

		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (ray, out hit, 100, mask)) {
			Debug.Log("Ray Hit"+hit.collider.gameObject.name);

			ClickLogic (hit.collider);
		}
	}

	void ClickLogic (Collider col)
	{
		if (col == myCollider) {
			//normal damage

			OnClick ();
		}

	}

	public void OnClick ()
	{
		//Debug.Log("Clicked on the Lobby Button");

		Debug.Log ("You right clicked an adventurer -ClickableAdventurer Script");
		return;
		GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/TradeCanvas")));
		//GameManager.Instance.InputEnabled = false;
		InventoryCanvas.Instance.ToggleEquipment(true);
		TradeCanvasSelector tcs = newItem.GetComponent<TradeCanvasSelector> ();
		tcs.SetPanel ("TradeOptions");
	//	newItem.transform.parent = cgBody.gameObject.transform;
		// Spawn a ChooseAnActionCanvas
		//position above player
	}
}
