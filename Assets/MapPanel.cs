﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MapPanel : MonoBehaviour
{
   public  string prereqMap;
    public Button button;

    void OnEnable()
    {
        EventManager.OnMapRefreshed += Refresh;
    }

    void OnDisable()
    {
        EventManager.OnMapRefreshed -= Refresh;
    }

   void Refresh()
    {
        GameObjectToggler got = GetComponent<GameObjectToggler>();
        button = GetComponent<Button>();
        if (GameManager.Instance.completedThings.Contains(prereqMap) || prereqMap=="None"){
            //its unlocked
            got.TurnOn(0);
            button.interactable = true;
        }else
        {
            got.TurnOn(1);
            button.interactable = false;
        }
    }

}
