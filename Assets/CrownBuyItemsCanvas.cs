﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CrownBuyItemsCanvas : MonoBehaviour {
	CanvasTools ct;

	public Text crownsText, crownsTotalText;
	public GameObject shoppingCartParent;
	public NPCShop crownShopItem;
	public CanvasGroup cgJunk, cgBody, cgHead, cgBack;
	public GameObject tabJunk,tabBody,tabHead,tabBack;
	//public ItemSlot headSlot, bodySlot, backSlot, junkSlot;

	public GameObject graphics;
	public Mannequin mannequin;

	private static CrownBuyItemsCanvas _instance;
	public static CrownBuyItemsCanvas Instance
	{
		get
		{
			//If _instance is null then we find it from the scene 
			if (_instance == null)
				_instance = FindObjectOfType<CrownBuyItemsCanvas>();
			return _instance;
		}
	}

	void Awake(){
		ct = GetComponent<CanvasTools> ();
			}

	public void EquipOnMannequin(Item _item){
		if (_item.itemType == ItemType.weapon) {
			mannequin.SetWeapon (_item.ID);
		} else {
			//must be costume
			mannequin.SetCostume (_item.ID);
		}
	}

	public void SetCrownsText(){
		crownsText.text = GameManager.Instance.crowns.ToString ();
	}

//	public void SetItemSlot(Item _item){
//		if (_item.itemType == ItemType.junk) {
//			junkSlot.gameObject.SetActive (true);
//			junkSlot.SetItem (_item, 1);
//		}
//		if (_item.itemType == ItemType.costume) {
//			if (_item.costumeType == CostumeType.head) {
//				headSlot.gameObject.SetActive (true);
//
//				headSlot.SetItem (_item,1);
//			} else if (_item.costumeType == CostumeType.body) {
//				bodySlot.gameObject.SetActive (true);
//
//				bodySlot.SetItem (_item,1);
//			} else if (_item.costumeType == CostumeType.back) {
//				backSlot.gameObject.SetActive (true);
//
//				backSlot.SetItem (_item,1);
//			}
//		}
//	}
	RectTransform rt;
	public void ChooseSlotPanel(string tab)
	//turn on correct panel - turn off others
	{
		//Debug.Log("Clicked on Slot Panel:" + tab);
		tabJunk.SetActive(false);
		tabBody.SetActive(false);
		tabBack.SetActive(false);
		tabHead.SetActive(false);

		cgJunk.alpha = 0;
		cgJunk.blocksRaycasts = false;
		cgJunk.interactable = false;
		rt = cgJunk.transform.parent.GetComponent<RectTransform> ();
		rt.localPosition = new Vector3 (rt.localPosition.x, rt.localPosition.y-1000, rt.localPosition.z);

		cgBody.alpha = 0;
		cgBody.blocksRaycasts = false;
		cgBody.interactable = false;
		rt = cgBody.transform.parent.GetComponent<RectTransform> ();
		rt.localPosition = new Vector3 (rt.localPosition.x, rt.localPosition.y-1000, rt.localPosition.z);


		cgBack.alpha = 0;
		cgBack.blocksRaycasts = false;
		cgBack.interactable = false;
		rt = cgBack.transform.parent.GetComponent<RectTransform> ();
		rt.localPosition = new Vector3 (rt.localPosition.x, rt.localPosition.y-1000, rt.localPosition.z);


		cgHead.alpha = 0;
		cgHead.blocksRaycasts = false;
		cgHead.interactable = false;
		rt = cgHead.transform.parent.GetComponent<RectTransform> ();
		rt.localPosition = new Vector3 (rt.localPosition.x, rt.localPosition.y-1000, rt.localPosition.z);


		switch (tab)
		{
		case "Junk":
			cgJunk.alpha = 1;
			cgJunk.blocksRaycasts = true;
			cgJunk.interactable = true;
			tabJunk.SetActive(true);
			break;
		case "Body":
			cgBody.alpha = 1;
			cgBody.blocksRaycasts = true;
			cgBody.interactable = true;
			tabBody.SetActive(true);
			break;
		case "Back":
			cgBack.alpha = 1;
			cgBack.blocksRaycasts = true;
			cgBack.interactable = true;
			tabBack.SetActive(true);
			break;
		case "Head":
			cgHead.alpha = 1;
			cgHead.blocksRaycasts = true;
			cgHead.interactable = true;
			tabHead.SetActive(true);
			break;
		}
	}

	public void Close(){
		AudioManager.Instance.PlayClip ("click", true, false);

		ToggleMe (false);
	}

	void ClearCartItems(){
		foreach (Transform t in shoppingCartParent.gameObject.transform) {
			t.gameObject.SetActive (false);
		}
		crownsTotalText.text = "0";

	}

	public List <Item> shoppingCartItems = new List<Item> ();
	public void RefreshCartItems(){
		ClearCartItems ();
		for (int inc=0;inc<shoppingCartItems.Count;inc++)
		{
			AddCartItem (shoppingCartItems[inc]);
		}
	}

	void InitMannequin(){
		//Debug.Log ("Init Mannequin");
		Debug.Log (GameManager.Instance.equippedWeapon.slotName + GameManager.Instance.equippedWeapon.slotName2);//+myAtn.equippedWeapon.atachmentName, myAtn.equippedWeapon.attachmentName2);
		GameManager gm = GameManager.Instance;
		//mannequin.SetAttachment (gm.equippedWeapon.slotName, gm.equippedWeapon.slotName2, gm.equippedWeapon.atachmentName, gm.equippedWeapon.attachmentName2);
		mannequin.SetAttachment (gm.equippedCostumeHead.slotName, gm.equippedCostumeHead.slotName2, gm.equippedCostumeHead.atachmentName, gm.equippedCostumeHead.attachmentName2);
		mannequin.SetAttachment (gm.equippedCostumeBack.slotName,gm.equippedCostumeBack.slotName2, gm.equippedCostumeBack.atachmentName, gm.equippedCostumeBack.attachmentName2);
		mannequin.SetSkin (gm.equippedCostumeBody.skin);

	}

	public void AddCartItem(Item _item){
	//	Debug.Log ("ADD CART ITEM");
		GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/CartItemSection")));
		newItem.transform.parent = shoppingCartParent.transform;
		newItem.transform.localScale = new Vector3 (1, 1, 1);
		newItem.GetComponent<CartItemSection> ().SetItem (_item);
		crownsTotalText.text = CalcCrownTotal ().ToString ();
	}

	public int CalcCrownTotal(){
		int newTotal = 0;
		foreach (Item _item in shoppingCartItems) {
			newTotal += _item.crownCost;
		}
		return newTotal;
	}

//	public void AddCartItemList(Item _item){
//		shoppingCartItems.Add (_item);
//	}

	public void ClearShopItems()
	{
		foreach (Transform child in cgBody.gameObject.transform)
		{
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in cgBack.gameObject.transform)
		{
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in cgJunk.gameObject.transform)
		{
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in cgHead.gameObject.transform)
		{
			child.gameObject.SetActive(false);
		}
	
	}

	public void LoadShopItems()
	{
		NPCShop myNPCShop = crownShopItem;
		InventoryManager inventoryManager = InventoryManager.Instance;

		//Costumes - Body
		for (int inc = 0; inc < myNPCShop.bodycostumes.Count; inc++)
		{
			Item nextItem = inventoryManager.GetItem (ItemType.costume, myNPCShop.bodycostumes [inc]);
			GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/CrownItemSlot")));
			newItem.transform.parent = cgBody.gameObject.transform;
			ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
			itemSlot.equip.SetActive(false);
			itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
			itemSlot.SetItem(nextItem, 1);
			itemSlot.purchasable = false;
			newItem.transform.localScale=new Vector3(1,1,1);
		}

		//Costumes - Back
		for (int inc = 0; inc < myNPCShop.backcostumes.Count; inc++)
		{
			Item nextItem = inventoryManager.GetItem (ItemType.costume, myNPCShop.backcostumes [inc]);
			GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/CrownItemSlot")));
			newItem.transform.parent = cgBack.gameObject.transform;
			ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
			itemSlot.equip.SetActive(false);
			itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
			itemSlot.SetItem(nextItem, 1);
			itemSlot.purchasable = false;
			newItem.transform.localScale=new Vector3(1,1,1);
		}

		//Costumes - Head
		for (int inc = 0; inc < myNPCShop.headcostumes.Count; inc++)
		{
			Item nextItem = inventoryManager.GetItem (ItemType.costume, myNPCShop.headcostumes [inc]);

			GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/CrownItemSlot")));
			newItem.transform.parent = cgHead.gameObject.transform;
			ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();

			itemSlot.equip.SetActive(false);
			itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
			itemSlot.SetItem(nextItem, 1);
			itemSlot.purchasable = false;
			newItem.transform.localScale=new Vector3(1,1,1);
		}

		Debug.Log ("junk");

		//Junk
		for (int inc = 0; inc < myNPCShop.junk.Count; inc++)
		{
			Item nextItem = inventoryManager.GetItem (ItemType.junk, myNPCShop.junk [inc]);
			GameObject newItem = ((GameObject)Instantiate(Resources.Load("UI/CrownItemSlot")));
			newItem.transform.parent = cgJunk.gameObject.transform;
			ItemSlot itemSlot = newItem.GetComponent<ItemSlot>();
			itemSlot.equip.SetActive(false);
			itemSlot.sell.SetActive(false);
			itemSlot.drop.SetActive(false);
			itemSlot.SetItem(nextItem, 1);
			itemSlot.purchasable = false;
			newItem.transform.localScale=new Vector3(1,1,1);
		}

	}

	public void BuyAll(){
		if (GameManager.Instance.crowns < CalcCrownTotal ()) {
			GameObject nextObj = ObjectPool.Instance.NextObject ("PopUpCanvas");
			PopUpCanvas puc = nextObj.GetComponent<PopUpCanvas> ();
			puc.SetPopup ("Not Enough", "Souls");
			puc.SetSoulIcon ();
			puc.SetTextColor ("blue");
			nextObj.SetActive (true);
			return;
		}


		for (int inc = 0; inc < shoppingCartItems.Count; inc++) {
			Item nextItem = shoppingCartItems [inc];
			InventoryManager.Instance.AddItem (nextItem.itemType, nextItem.ID);
			InventoryCanvas.Instance.AddItemSlot (nextItem, 1);
		}

		GameManager.Instance.crowns -= CalcCrownTotal ();
		SetCrownsText ();

		AudioManager.Instance.PlayClip ("invasion_unlock", true, false);
		GameObject feathers = ObjectPool.Instance.NextObject ("CrownFeathers");
		feathers.transform.position = mannequin.transform.position;
			feathers.SetActive (true);

		shoppingCartItems.Clear ();
		ClearCartItems ();


	}





	public bool isOpen=false;
	public void ToggleMe(bool b)
	{
		if (b == isOpen) return;
		if (b)
		{
			GameManager.Instance.InputEnabled = false;

			shoppingCartItems.Clear ();
			ClearCartItems ();
			ClearShopItems();
			LoadShopItems();
			SetCrownsText ();
			ct.CallFadeCanvas (1);
			graphics.SetActive (true);
			Face newFace = InventoryManager.Instance.GetFace (GameManager.Instance.equippedFace);
			mannequin.SetAttachment("h", "NOSLOT", newFace.attachmentName, "");
			InitMannequin ();
			ChooseSlotPanel ("Body");
			NPCManager.Instance.ToggleNPCCanvas(false);
			TownCanvas.Instance.ToggleSoulText(false);
			TownCanvas.Instance.ToggleMe (false);
			MenuCanvas.Instance.ToggleMe (false);
		}
		else
		{
			ct.CallFadeCanvas (0);
			GameManager.Instance.InputEnabled = true;

			graphics.SetActive (false);
			TownCanvas.Instance.ToggleSoulText(true);
			TownCanvas.Instance.ToggleMe (true);
			MenuCanvas.Instance.ToggleMe (true);
		}
		isOpen = b;
	}

}
