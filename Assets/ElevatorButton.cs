﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using System;

public class ElevatorButton : MonoBehaviour
{
	public SpriteRenderer sr;
	public Collider myCollider;
	public LayerMask mask;

	void Awake ()
	{
		sr = GetComponent<SpriteRenderer> ();
		myCollider = GetComponent<Collider> ();
		clickCooled = true;
		// Invoke("CheckUnlocked", 4);
	}

	void Update ()
	{
	//	sr.enabled = GameManager.Instance.InputEnabled;
		Raycasts ();
	}

	void Raycasts ()
	{
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (ray, out hit, 100, mask)) {
			ClickLogic (hit.collider);
		}
	}

	void ClickLogic (Collider col)
	{
		//	Debug.Log ("RAYCAST HIT LOBBY!");
		if (SettingsCanvas.Instance.canvasOpen)
			return;//cant click on it when inventory open!
		if (!GameManager.Instance.InputEnabled)
			return;
		if (InventoryCanvas.Instance.equipmentOpen)
			return;//cant click on it when inventory open!
		if (Input.GetMouseButton (0)) {
			if (col == myCollider) {
				OnClick ();
			}
		}
	}

	public void OnClick ()
	{
		AudioManager.Instance.PlayClip ("ding", true, false);
		AudioManager.Instance.photonView.RPC ("StopLoop", PhotonTargets.All);
		//StopLooping ();

		OnClickFinal ();	
	}

	public Transporter transporter;
	void OnClickFinal(){
		bool isInPosition = true;

		if (Vector3.Distance (TownManager.Instance.myAtn.transform.position, transporter.transform.position) > transporter.radius) {
			isInPosition = false;
		}

		if (isInPosition) {
			TownManager.Instance.myAtn.StartLerp (transporter.targetLocation.transform.position, true);
			Invoke ("DelayedTubeSound", .1f);

		}
		//Debug.Log ("Was in position:"+isInPosition);
	}

	void DelayedTubeSound(){
		AudioManager.Instance.PlayClip ("tube", true, false);

	}

	public bool clickCooled = true;

	IEnumerator OnClickCooldown ()
	{
		clickCooled = false;
		yield return new WaitForSeconds (.25f);
		clickCooled = true;
	}

	void OnEnable ()
	{
		EventManager.OnLoadComplete += OnLoadComplete;
		EventManager.OnElevatorKeyChange += OnElevatorKeyChange;

	}

	void OnDisable ()
	{
		EventManager.OnLoadComplete -= OnLoadComplete;
		EventManager.OnElevatorKeyChange -= OnElevatorKeyChange;

	}

	public bool isLocked = false;

	void OnLoadComplete ()
	{
		CheckForKey ();
		SetLocked ();
	}


	void OnElevatorKeyChange ()
	{
		CheckForKey ();
		SetLocked ();
	}

	void CheckForKey ()
	{
		if (InventoryManager.Instance.GetQTY (ItemType.junk, 16) > 0) {
			isLocked = false;
		} else {
			isLocked = true;
		}
		SetLocked ();
	}

	void SetLocked ()
	{
		sr.enabled = !isLocked;
		myCollider.enabled = !isLocked;
	}
}
