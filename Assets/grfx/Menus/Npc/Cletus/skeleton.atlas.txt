
skeleton.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Bag
  rotate: false
  xy: 2, 293
  size: 381, 217
  orig: 381, 217
  offset: 0, 0
  index: -1
a1
  rotate: false
  xy: 274, 110
  size: 87, 101
  orig: 87, 101
  offset: 0, 0
  index: -1
a2
  rotate: true
  xy: 2, 20
  size: 77, 115
  orig: 77, 115
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 385, 344
  size: 149, 166
  orig: 149, 166
  offset: 0, 0
  index: -1
ear
  rotate: false
  xy: 363, 262
  size: 19, 29
  orig: 19, 29
  offset: 0, 0
  index: -1
eyebrow1
  rotate: true
  xy: 522, 322
  size: 20, 11
  orig: 20, 11
  offset: 0, 0
  index: -1
eyebrow2
  rotate: false
  xy: 288, 15
  size: 13, 11
  orig: 13, 11
  offset: 0, 0
  index: -1
feather
  rotate: false
  xy: 956, 446
  size: 65, 64
  orig: 65, 64
  offset: 0, 0
  index: -1
fo1
  rotate: false
  xy: 779, 359
  size: 69, 62
  orig: 69, 62
  offset: 0, 0
  index: -1
fo2
  rotate: false
  xy: 536, 336
  size: 100, 50
  orig: 100, 50
  offset: 0, 0
  index: -1
fo3
  rotate: false
  xy: 638, 335
  size: 100, 50
  orig: 100, 50
  offset: 0, 0
  index: -1
h
  rotate: true
  xy: 756, 423
  size: 87, 91
  orig: 87, 91
  offset: 0, 0
  index: -1
hair
  rotate: true
  xy: 219, 2
  size: 7, 9
  orig: 7, 9
  offset: 0, 0
  index: -1
hair1
  rotate: true
  xy: 979, 412
  size: 32, 43
  orig: 32, 43
  offset: 0, 0
  index: -1
hair2
  rotate: true
  xy: 924, 386
  size: 56, 53
  orig: 56, 53
  offset: 0, 0
  index: -1
hand1
  rotate: false
  xy: 849, 444
  size: 105, 66
  orig: 105, 66
  offset: 0, 0
  index: -1
hand2
  rotate: false
  xy: 219, 11
  size: 67, 91
  orig: 67, 91
  offset: 0, 0
  index: -1
hat1
  rotate: false
  xy: 850, 386
  size: 72, 56
  orig: 72, 56
  offset: 0, 0
  index: -1
hat2
  rotate: false
  xy: 2, 2
  size: 75, 16
  orig: 75, 16
  offset: 0, 0
  index: -1
hat3
  rotate: false
  xy: 651, 387
  size: 126, 34
  orig: 126, 34
  offset: 0, 0
  index: -1
l1
  rotate: false
  xy: 536, 388
  size: 113, 122
  orig: 113, 122
  offset: 0, 0
  index: -1
l2
  rotate: false
  xy: 2, 99
  size: 148, 192
  orig: 148, 192
  offset: 0, 0
  index: -1
l3
  rotate: true
  xy: 152, 104
  size: 107, 120
  orig: 107, 120
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 850, 361
  size: 35, 23
  orig: 35, 23
  offset: 0, 0
  index: -1
neck
  rotate: true
  xy: 288, 28
  size: 80, 64
  orig: 80, 64
  offset: 0, 0
  index: -1
obj
  rotate: true
  xy: 152, 2
  size: 100, 65
  orig: 100, 65
  offset: 0, 0
  index: -1
obj2
  rotate: true
  xy: 385, 294
  size: 48, 135
  orig: 48, 135
  offset: 0, 0
  index: -1
pants
  rotate: false
  xy: 651, 423
  size: 103, 87
  orig: 103, 87
  offset: 0, 0
  index: -1
t1
  rotate: false
  xy: 79, 2
  size: 43, 16
  orig: 43, 16
  offset: 0, 0
  index: -1
t2
  rotate: true
  xy: 740, 344
  size: 41, 21
  orig: 41, 21
  offset: 0, 0
  index: -1
weapon1/Wooden Sword
  rotate: true
  xy: 152, 213
  size: 78, 209
  orig: 78, 209
  offset: 0, 0
  index: -1
