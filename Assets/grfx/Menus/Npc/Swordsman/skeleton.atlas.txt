
skeleton.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
a1
  rotate: false
  xy: 595, 417
  size: 194, 88
  orig: 194, 88
  offset: 0, 0
  index: -1
a2
  rotate: true
  xy: 160, 7
  size: 120, 69
  orig: 120, 69
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 2, 252
  size: 222, 253
  orig: 222, 253
  offset: 0, 0
  index: -1
fo2
  rotate: false
  xy: 932, 483
  size: 48, 22
  orig: 48, 22
  offset: 0, 0
  index: -1
h
  rotate: false
  xy: 160, 129
  size: 111, 114
  orig: 111, 114
  offset: 0, 0
  index: -1
h1
  rotate: false
  xy: 443, 231
  size: 146, 167
  orig: 146, 167
  offset: 0, 0
  index: -1
h2
  rotate: true
  xy: 791, 429
  size: 76, 139
  orig: 76, 139
  offset: 0, 0
  index: -1
l1
  rotate: false
  xy: 2, 2
  size: 156, 248
  orig: 156, 248
  offset: 0, 0
  index: -1
l2
  rotate: true
  xy: 226, 245
  size: 153, 215
  orig: 153, 215
  offset: 0, 0
  index: -1
w
  rotate: true
  xy: 226, 400
  size: 105, 367
  orig: 105, 367
  offset: 0, 0
  index: -1
