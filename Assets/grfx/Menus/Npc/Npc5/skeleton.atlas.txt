
skeleton.png
size: 745,206
format: RGBA8888
filter: Linear,Linear
repeat: none
b
  rotate: true
  xy: 297, 6
  size: 95, 182
  orig: 95, 182
  offset: 0, 0
  index: -1
fo1
  rotate: false
  xy: 665, 20
  size: 72, 40
  orig: 72, 40
  offset: 0, 0
  index: -1
fo2
  rotate: true
  xy: 665, 62
  size: 53, 60
  orig: 53, 60
  offset: 0, 0
  index: -1
h
  rotate: false
  xy: 671, 117
  size: 72, 87
  orig: 72, 87
  offset: 0, 0
  index: -1
h1
  rotate: true
  xy: 471, 133
  size: 71, 198
  orig: 71, 198
  offset: 0, 0
  index: -1
h2
  rotate: false
  xy: 481, 25
  size: 99, 106
  orig: 99, 106
  offset: 0, 0
  index: -1
l1
  rotate: true
  xy: 230, 103
  size: 101, 239
  orig: 101, 239
  offset: 0, 0
  index: -1
l2
  rotate: true
  xy: 2, 74
  size: 130, 226
  orig: 130, 226
  offset: 0, 0
  index: -1
obj
  rotate: true
  xy: 582, 20
  size: 111, 81
  orig: 111, 81
  offset: 0, 0
  index: -1
w
  rotate: false
  xy: 2, 2
  size: 293, 70
  orig: 293, 70
  offset: 0, 0
  index: -1
