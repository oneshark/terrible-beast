
skeleton.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Fo1
  rotate: false
  xy: 906, 172
  size: 96, 66
  orig: 96, 66
  offset: 0, 0
  index: -1
Fo2
  rotate: false
  xy: 793, 185
  size: 111, 53
  orig: 111, 53
  offset: 0, 0
  index: -1
L1
  rotate: true
  xy: 431, 121
  size: 117, 234
  orig: 117, 234
  offset: 0, 0
  index: -1
L2
  rotate: true
  xy: 188, 2
  size: 111, 235
  orig: 111, 235
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 2, 2
  size: 184, 236
  orig: 184, 236
  offset: 0, 0
  index: -1
h
  rotate: true
  xy: 667, 136
  size: 102, 124
  orig: 102, 124
  offset: 0, 0
  index: -1
h1
  rotate: true
  xy: 188, 115
  size: 123, 241
  orig: 123, 241
  offset: 0, 0
  index: -1
