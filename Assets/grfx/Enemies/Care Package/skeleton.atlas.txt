
skeleton.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
chest
  rotate: false
  xy: 2, 65
  size: 260, 178
  orig: 260, 178
  offset: 0, 0
  index: -1
chest2
  rotate: true
  xy: 264, 23
  size: 220, 176
  orig: 220, 176
  offset: 0, 0
  index: -1
chest3
  rotate: false
  xy: 603, 15
  size: 148, 121
  orig: 148, 121
  offset: 0, 0
  index: -1
leg1
  rotate: false
  xy: 603, 138
  size: 313, 105
  orig: 313, 105
  offset: 0, 0
  index: -1
leg2
  rotate: false
  xy: 442, 2
  size: 159, 241
  orig: 159, 241
  offset: 0, 0
  index: -1
leg3
  rotate: true
  xy: 918, 30
  size: 213, 86
  orig: 213, 86
  offset: 0, 0
  index: -1
