
skeleton.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
b
  rotate: false
  xy: 2, 386
  size: 632, 634
  orig: 632, 634
  offset: 0, 0
  index: -1
brains
  rotate: false
  xy: 636, 480
  size: 109, 98
  orig: 109, 98
  offset: 0, 0
  index: -1
eye1
  rotate: true
  xy: 539, 253
  size: 131, 91
  orig: 131, 91
  offset: 0, 0
  index: -1
eye2
  rotate: false
  xy: 747, 700
  size: 97, 108
  orig: 97, 108
  offset: 0, 0
  index: -1
light
  rotate: false
  xy: 861, 908
  size: 111, 112
  orig: 111, 112
  offset: 0, 0
  index: -1
m
  rotate: true
  xy: 636, 580
  size: 228, 109
  orig: 228, 109
  offset: 0, 0
  index: -1
m2
  rotate: false
  xy: 2, 176
  size: 321, 208
  orig: 321, 208
  offset: 0, 0
  index: -1
nerve1
  rotate: false
  xy: 2, 2
  size: 251, 172
  orig: 251, 172
  offset: 0, 0
  index: -1
nerve2
  rotate: true
  xy: 325, 192
  size: 192, 212
  orig: 192, 212
  offset: 0, 0
  index: -1
obj
  rotate: false
  xy: 636, 810
  size: 223, 210
  orig: 223, 210
  offset: 0, 0
  index: -1
