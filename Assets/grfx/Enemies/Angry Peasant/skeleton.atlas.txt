
skeleton.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
a2
  rotate: false
  xy: 745, 333
  size: 83, 56
  orig: 83, 56
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 552, 324
  size: 191, 181
  orig: 191, 181
  offset: 0, 0
  index: -1
bg
  rotate: true
  xy: 2, 26
  size: 172, 348
  orig: 172, 348
  offset: 0, 0
  index: -1
f1
  rotate: true
  xy: 956, 443
  size: 62, 66
  orig: 62, 66
  offset: 0, 0
  index: -1
f2
  rotate: false
  xy: 352, 2
  size: 72, 56
  orig: 72, 56
  offset: 0, 0
  index: -1
fo1
  rotate: false
  xy: 426, 15
  size: 70, 43
  orig: 70, 43
  offset: 0, 0
  index: -1
fo2
  rotate: false
  xy: 859, 380
  size: 77, 44
  orig: 77, 44
  offset: 0, 0
  index: -1
h
  rotate: false
  xy: 745, 391
  size: 112, 114
  orig: 112, 114
  offset: 0, 0
  index: -1
h2
  rotate: false
  xy: 859, 426
  size: 95, 79
  orig: 95, 79
  offset: 0, 0
  index: -1
l1
  rotate: true
  xy: 552, 226
  size: 96, 170
  orig: 96, 170
  offset: 0, 0
  index: -1
l2
  rotate: true
  xy: 352, 60
  size: 138, 180
  orig: 138, 180
  offset: 0, 0
  index: -1
w
  rotate: false
  xy: 2, 200
  size: 548, 305
  orig: 548, 305
  offset: 0, 0
  index: -1
