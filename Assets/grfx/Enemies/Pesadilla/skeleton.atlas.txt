
skeleton.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Lezard Barnabas_arm1
  rotate: false
  xy: 429, 78
  size: 80, 61
  orig: 80, 61
  offset: 0, 0
  index: -1
Lezard Barnabas_arm2
  rotate: false
  xy: 243, 85
  size: 89, 71
  orig: 89, 71
  offset: 0, 0
  index: -1
Lezard Barnabas_body
  rotate: false
  xy: 118, 345
  size: 179, 165
  orig: 179, 165
  offset: 0, 0
  index: -1
Lezard Barnabas_bottle
  rotate: false
  xy: 166, 24
  size: 74, 61
  orig: 74, 61
  offset: 0, 0
  index: -1
Lezard Barnabas_finger1
  rotate: true
  xy: 152, 2
  size: 20, 32
  orig: 20, 32
  offset: 0, 0
  index: -1
Lezard Barnabas_finger2
  rotate: false
  xy: 756, 416
  size: 26, 47
  orig: 26, 47
  offset: 0, 0
  index: -1
Lezard Barnabas_foot1
  rotate: false
  xy: 678, 290
  size: 48, 46
  orig: 48, 46
  offset: 0, 0
  index: -1
Lezard Barnabas_foot2
  rotate: false
  xy: 511, 99
  size: 53, 40
  orig: 53, 40
  offset: 0, 0
  index: -1
Lezard Barnabas_hand1
  rotate: true
  xy: 242, 24
  size: 59, 76
  orig: 59, 76
  offset: 0, 0
  index: -1
Lezard Barnabas_hand2
  rotate: false
  xy: 680, 212
  size: 59, 76
  orig: 59, 76
  offset: 0, 0
  index: -1
Lezard Barnabas_head
  rotate: false
  xy: 299, 373
  size: 205, 137
  orig: 205, 137
  offset: 0, 0
  index: -1
Lezard Barnabas_leg1
  rotate: false
  xy: 166, 87
  size: 75, 102
  orig: 75, 102
  offset: 0, 0
  index: -1
Lezard Barnabas_leg2
  rotate: false
  xy: 571, 107
  size: 95, 80
  orig: 95, 80
  offset: 0, 0
  index: -1
Lezard Barnabas_leg3
  rotate: true
  xy: 334, 100
  size: 56, 93
  orig: 56, 93
  offset: 0, 0
  index: -1
Lezard Barnabas_leg4
  rotate: true
  xy: 68, 2
  size: 60, 82
  orig: 60, 82
  offset: 0, 0
  index: -1
Lezard Barnabas_light
  rotate: true
  xy: 243, 164
  size: 25, 36
  orig: 25, 36
  offset: 0, 0
  index: -1
Lezard Barnabas_obj1
  rotate: false
  xy: 590, 189
  size: 88, 96
  orig: 88, 96
  offset: 0, 0
  index: -1
Lezard Barnabas_obj2
  rotate: false
  xy: 68, 64
  size: 96, 125
  orig: 96, 125
  offset: 0, 0
  index: -1
Lezard Barnabas_pelvis
  rotate: false
  xy: 290, 158
  size: 149, 136
  orig: 149, 136
  offset: 0, 0
  index: -1
Lezard Barnabas_tail
  rotate: false
  xy: 118, 191
  size: 170, 152
  orig: 170, 152
  offset: 0, 0
  index: -1
Weapons/Commoner Rod
  rotate: true
  xy: 492, 287
  size: 49, 184
  orig: 49, 184
  offset: 0, 0
  index: -1
Weapons/Defender Cane
  rotate: true
  xy: 506, 338
  size: 69, 231
  orig: 69, 231
  offset: 0, 0
  index: -1
Weapons/Fighting Stick
  rotate: true
  xy: 797, 437
  size: 73, 214
  orig: 73, 214
  offset: 0, 0
  index: -1
Weapons/Heavy Town Scepter
  rotate: true
  xy: 506, 409
  size: 54, 248
  orig: 54, 248
  offset: 0, 0
  index: -1
Weapons/Iron Wand
  rotate: false
  xy: 51, 23
  size: 15, 157
  orig: 15, 157
  offset: 0, 0
  index: -1
Weapons/Military Scepter
  rotate: false
  xy: 2, 182
  size: 55, 328
  orig: 55, 328
  offset: 0, 0
  index: -1
Weapons/Short Rod
  rotate: true
  xy: 299, 296
  size: 75, 191
  orig: 75, 191
  offset: 0, 0
  index: -1
Weapons/Traveller's Rod
  rotate: true
  xy: 506, 465
  size: 45, 289
  orig: 45, 289
  offset: 0, 0
  index: -1
Weapons/Wild Rod
  rotate: false
  xy: 59, 201
  size: 57, 309
  orig: 57, 309
  offset: 0, 0
  index: -1
Weapons/Wooden Rod
  rotate: false
  xy: 2, 2
  size: 47, 178
  orig: 47, 178
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 441, 200
  size: 147, 85
  orig: 147, 85
  offset: 0, 0
  index: -1
tongue
  rotate: false
  xy: 441, 141
  size: 128, 57
  orig: 128, 57
  offset: 0, 0
  index: -1
