
skeleton.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Eye1
  rotate: true
  xy: 134, 12
  size: 29, 28
  orig: 29, 28
  offset: 0, 0
  index: -1
Eye2
  rotate: true
  xy: 302, 60
  size: 21, 22
  orig: 21, 22
  offset: 0, 0
  index: -1
W1
  rotate: true
  xy: 206, 131
  size: 123, 187
  orig: 123, 187
  offset: 0, 0
  index: -1
a1
  rotate: true
  xy: 379, 99
  size: 29, 90
  orig: 29, 90
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 2, 90
  size: 202, 164
  orig: 202, 164
  offset: 0, 0
  index: -1
eyelight
  rotate: false
  xy: 204, 63
  size: 56, 18
  orig: 56, 18
  offset: 0, 0
  index: -1
f1
  rotate: true
  xy: 328, 86
  size: 43, 49
  orig: 43, 49
  offset: 0, 0
  index: -1
f2
  rotate: true
  xy: 94, 2
  size: 39, 38
  orig: 39, 38
  offset: 0, 0
  index: -1
fo1
  rotate: false
  xy: 151, 53
  size: 51, 35
  orig: 51, 35
  offset: 0, 0
  index: -1
fo2
  rotate: false
  xy: 471, 96
  size: 35, 32
  orig: 35, 32
  offset: 0, 0
  index: -1
h
  rotate: false
  xy: 395, 130
  size: 112, 124
  orig: 112, 124
  offset: 0, 0
  index: -1
h1
  rotate: true
  xy: 2, 2
  size: 39, 90
  orig: 39, 90
  offset: 0, 0
  index: -1
l1
  rotate: true
  xy: 206, 83
  size: 46, 120
  orig: 46, 120
  offset: 0, 0
  index: -1
l2
  rotate: true
  xy: 2, 43
  size: 45, 147
  orig: 45, 147
  offset: 0, 0
  index: -1
obj2
  rotate: true
  xy: 262, 64
  size: 17, 38
  orig: 17, 38
  offset: 0, 0
  index: -1
