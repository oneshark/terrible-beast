
skeleton.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Eye
  rotate: false
  xy: 2, 2
  size: 25, 25
  orig: 25, 25
  offset: 0, 0
  index: -1
Slash
  rotate: false
  xy: 2, 201
  size: 263, 307
  orig: 263, 307
  offset: 0, 0
  index: -1
a1
  rotate: false
  xy: 854, 751
  size: 100, 71
  orig: 100, 71
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 380, 676
  size: 224, 239
  orig: 224, 239
  offset: 0, 0
  index: -1
cloack
  rotate: false
  xy: 2, 510
  size: 376, 512
  orig: 376, 512
  offset: 0, 0
  index: -1
cloack arm
  rotate: false
  xy: 267, 214
  size: 100, 294
  orig: 100, 294
  offset: 0, 0
  index: -1
f1
  rotate: false
  xy: 868, 645
  size: 89, 48
  orig: 89, 48
  offset: 0, 0
  index: -1
f2
  rotate: false
  xy: 868, 695
  size: 86, 54
  orig: 86, 54
  offset: 0, 0
  index: -1
finger
  rotate: false
  xy: 479, 222
  size: 67, 48
  orig: 67, 48
  offset: 0, 0
  index: -1
fo1
  rotate: true
  xy: 945, 867
  size: 48, 35
  orig: 48, 35
  offset: 0, 0
  index: -1
fo2
  rotate: true
  xy: 837, 539
  size: 99, 41
  orig: 99, 41
  offset: 0, 0
  index: -1
h
  rotate: false
  xy: 2, 29
  size: 249, 170
  orig: 249, 170
  offset: 0, 0
  index: -1
h1
  rotate: false
  xy: 369, 211
  size: 108, 59
  orig: 108, 59
  offset: 0, 0
  index: -1
h2
  rotate: false
  xy: 598, 515
  size: 237, 123
  orig: 237, 123
  offset: 0, 0
  index: -1
l1
  rotate: true
  xy: 380, 525
  size: 149, 216
  orig: 149, 216
  offset: 0, 0
  index: -1
l2
  rotate: true
  xy: 380, 365
  size: 158, 181
  orig: 158, 181
  offset: 0, 0
  index: -1
l3
  rotate: true
  xy: 369, 272
  size: 91, 180
  orig: 91, 180
  offset: 0, 0
  index: -1
obj
  rotate: false
  xy: 606, 750
  size: 246, 165
  orig: 246, 165
  offset: 0, 0
  index: -1
obj2
  rotate: false
  xy: 854, 824
  size: 89, 91
  orig: 89, 91
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 606, 640
  size: 260, 108
  orig: 260, 108
  offset: 0, 0
  index: -1
w
  rotate: false
  xy: 380, 917
  size: 606, 105
  orig: 606, 105
  offset: 0, 0
  index: -1
