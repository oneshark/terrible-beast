
skeleton.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
a2
  rotate: false
  xy: 470, 2
  size: 243, 236
  orig: 243, 236
  offset: 0, 0
  index: -1
b
  rotate: false
  xy: 2, 428
  size: 555, 593
  orig: 555, 593
  offset: 0, 0
  index: -1
f2
  rotate: false
  xy: 715, 9
  size: 208, 229
  orig: 208, 229
  offset: 0, 0
  index: -1
foot1
  rotate: false
  xy: 470, 376
  size: 86, 50
  orig: 86, 50
  offset: 0, 0
  index: -1
foot2
  rotate: true
  xy: 902, 865
  size: 156, 70
  orig: 156, 70
  offset: 0, 0
  index: -1
h1
  rotate: true
  xy: 559, 240
  size: 312, 350
  orig: 312, 350
  offset: 0, 0
  index: -1
l1
  rotate: true
  xy: 2, 48
  size: 378, 466
  orig: 378, 466
  offset: 0, 0
  index: -1
l2
  rotate: false
  xy: 559, 554
  size: 341, 467
  orig: 341, 467
  offset: 0, 0
  index: -1

skeleton2.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
f1
  rotate: false
  xy: 2, 157
  size: 473, 243
  orig: 473, 243
  offset: 0, 0
  index: -1
h
  rotate: false
  xy: 2, 2
  size: 200, 153
  orig: 200, 153
  offset: 0, 0
  index: -1
h2
  rotate: true
  xy: 477, 98
  size: 302, 337
  orig: 302, 337
  offset: 0, 0
  index: -1
