
skeleton.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Bush1
  rotate: false
  xy: 2, 2
  size: 758, 741
  orig: 758, 741
  offset: 0, 0
  index: -1

skeleton2.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Bush2
  rotate: false
  xy: 452, 144
  size: 477, 486
  orig: 477, 486
  offset: 0, 0
  index: -1
Bush3
  rotate: false
  xy: 2, 2
  size: 448, 628
  orig: 448, 628
  offset: 0, 0
  index: -1
