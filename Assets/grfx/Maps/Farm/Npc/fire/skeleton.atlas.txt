
skeleton.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
dag_0000_Layer-10
  rotate: false
  xy: 2, 293
  size: 186, 292
  orig: 186, 292
  offset: 0, 0
  index: -1
dag_0001_Layer-9
  rotate: false
  xy: 190, 322
  size: 180, 287
  orig: 180, 287
  offset: 0, 0
  index: -1
dag_0002_Layer-8
  rotate: false
  xy: 2, 587
  size: 163, 324
  orig: 163, 324
  offset: 0, 0
  index: -1
dag_0003_Layer-7
  rotate: true
  xy: 351, 741
  size: 170, 311
  orig: 170, 311
  offset: 0, 0
  index: -1
dag_0004_Layer-6
  rotate: true
  xy: 664, 745
  size: 166, 303
  orig: 166, 303
  offset: 0, 0
  index: -1
dag_0005_Layer-5
  rotate: false
  xy: 167, 611
  size: 182, 300
  orig: 182, 300
  offset: 0, 0
  index: -1
dag_0006_Layer-4
  rotate: false
  xy: 2, 2
  size: 179, 289
  orig: 179, 289
  offset: 0, 0
  index: -1
dag_0007_Layer-3
  rotate: false
  xy: 372, 457
  size: 186, 282
  orig: 186, 282
  offset: 0, 0
  index: -1
dag_0008_Layer-2
  rotate: false
  xy: 560, 468
  size: 176, 271
  orig: 176, 271
  offset: 0, 0
  index: -1
dag_0009_Layer-1
  rotate: false
  xy: 738, 482
  size: 182, 261
  orig: 182, 261
  offset: 0, 0
  index: -1
light
  rotate: false
  xy: 372, 343
  size: 111, 112
  orig: 111, 112
  offset: 0, 0
  index: -1
