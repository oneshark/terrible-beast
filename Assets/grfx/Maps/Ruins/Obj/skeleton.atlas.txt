
skeleton.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
_0000_Layer-17
  rotate: false
  xy: 2, 590
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0001_Layer-16
  rotate: false
  xy: 2, 220
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0002_Layer-15
  rotate: false
  xy: 111, 590
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0003_Layer-14
  rotate: false
  xy: 111, 220
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0004_Layer-11
  rotate: false
  xy: 220, 590
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0005_Layer-10
  rotate: false
  xy: 220, 220
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0006_Layer-9
  rotate: false
  xy: 329, 590
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0007_Layer-8
  rotate: false
  xy: 329, 220
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0008_Layer-7
  rotate: true
  xy: 2, 111
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0009_Layer-6
  rotate: false
  xy: 438, 590
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0010_Layer-5
  rotate: true
  xy: 2, 2
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0011_Layer-4
  rotate: false
  xy: 438, 220
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0012_Layer-3
  rotate: false
  xy: 547, 590
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0013_Layer-12
  rotate: false
  xy: 547, 220
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0014_Layer-2
  rotate: false
  xy: 656, 590
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
_0015_Layer-1
  rotate: false
  xy: 656, 220
  size: 107, 368
  orig: 107, 368
  offset: 0, 0
  index: -1
