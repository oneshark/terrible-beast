﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardSpeechAnimationController : AnimationController {
	public AudioClip[] audioClips;
	//0=welcome to morning wood
	//1=beware boss is coming

	AudioSource audioSource;

	void Start () {
		base.Start ();

	}

	public void InitAudioClip(int i, bool includeGraphics){
		if (GameManager.Instance.currentScene==CustomSceneType.Town)
		TownCanvas.Instance.ToggleMe (false);
		//MenuCanvas.Instance.ToggleMe (false);
		audioSource = GetComponent<AudioSource> ();

		transform.parent = Camera.main.transform;

		if (i == 5) {
			GameObject voidCam = GameObject.FindGameObjectWithTag ("Void Camera");
			if (voidCam!=null)transform.parent=voidCam.transform;
		}
			transform.localPosition = new Vector3 (-3, -4.2f, 6);

		transform.localScale = new Vector3 (.5f, .5f, .5f);
		audioSource.clip = audioClips [i];
		Invoke ("StartSpeech", .9f);
		if (includeGraphics) {
			Invoke ("StartSpeechAnim", 1.01f);
		} else {
			MeshRenderer mr = GetComponent<MeshRenderer> ();
			mr.enabled = false;
		}

	}

	void Update () {
		if (!audioSource.isPlaying & isTalking)
			StopSpeech ();
	}

	bool isTalking=false;

	void StartSpeechAnim(){
		base.SetAnimationOnTrack (0,"Idle", true);

		base.SetAnimationOnTrack (1,"Talk", true);
	}

	void StartSpeech(){
		isTalking = true;
	audioSource.Play ();
		currentAnimation = "Talk";
	}

	string currentAnimation="";
	void StopSpeech(){
		if (currentAnimation == "Exit")
			return;
		currentAnimation = "Exit";
		base.SetAnimationOnTrack (1,"Exit", false);
		base.SetAnimationOnTrack (0,"Exit", false);

		//base.SetAnimation ("Exit", false, false);
		Invoke ("Deactivate", 8);
	}

	void Deactivate(){
		if (GameManager.Instance.currentScene==CustomSceneType.Town)
					TownCanvas.Instance.ToggleMe (true);
		//MenuCanvas.Instance.ToggleMe (true);

		Destroy (gameObject);
	}
}
