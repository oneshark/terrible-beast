﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorBlockOut : MonoBehaviour
{
	public GameObject lockBlock;

	void OnEnable ()
	{
		EventManager.OnLoadComplete += OnLoadComplete;
		EventManager.OnElevatorKeyChange += OnElevatorKeyChange;

	}

	void OnDisable ()
	{
		EventManager.OnLoadComplete -= OnLoadComplete;
		EventManager.OnElevatorKeyChange -= OnElevatorKeyChange;

	}

	public bool isLocked = false;

	void OnLoadComplete ()
	{
		CheckForKey ();
		SetLocked ();
	}


	void OnElevatorKeyChange ()
	{
		CheckForKey ();
		SetLocked ();
	}

	void CheckForKey ()
	{
		if (InventoryManager.Instance.GetQTY (ItemType.junk, 16) > 0) {
			isLocked = false;
		} else {
			isLocked = true;
		}
		SetLocked ();
	}

	void SetLocked ()
	{
		lockBlock.SetActive (isLocked);
	}
}
