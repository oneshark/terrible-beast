﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushForwardTrigger : MonoBehaviour {

	public Vector3 moveAmount = new Vector3 (.1f, -.1f,0);

	public void OnTriggerStay2D(Collider2D other){
		if (other.tag == "Adventurer") {
			myAdv= other.GetComponent<AdventurerNetwork> ();
			if (myAdv.photonView.isMine)isPushing = true;
		}
	}
	public void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Adventurer") {
			myAdv= other.GetComponent<AdventurerNetwork> ();
			if (myAdv.photonView.isMine)
				isPushing = false;
		}
	}

	bool isPushing=false;
	void FixedUpdate(){
		if(isPushing) PushPlayer ();
	}

	AdventurerNetwork myAdv;
	void PushPlayer(){
		myAdv.transform.position += moveAmount;
	}


}
