﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.UI;

public class SkillTreeToolTip : MonoBehaviour
{
    public Skill associatedSkill;
    public Text skillTitle, skillHeading, skillDesc, skillCooldownSeconds, skilLCost;

    void OnEnable()
    {
        // EventManager.OnInventoryStyleChange += OnInventoryStyleChange;
    }

    void OnDisable()
    {
        // EventManager.OnInventoryStyleChange -= OnInventoryStyleChange;
    }

    public void SetSkill(int skillId)
    {
        //set our text and graphics
        associatedSkill = SkillTreeCanvas.Instance.skillDatabase.Where(x => x.skillId == skillId).FirstOrDefault();
        skillTitle.text = associatedSkill.skillName;
        skillHeading.text = associatedSkill.skillHeader;
        skillDesc.text = associatedSkill.skillDesc;
        skillCooldownSeconds.text = "Cooldown: " + associatedSkill.stat.cooldownSeconds + " sec";
        skilLCost.text = "Cost: " + associatedSkill.stat.cost + "pts";
    }
    CanvasGroup cg;
    public void ToggleMe(bool b)
    {
        if (cg == null) cg = GetComponent<CanvasGroup>();

        float alpha = 0;
        if (b)
        {
            alpha = 1;
        }
        else
        {
            //its already 0 alpha
        }

        cg.alpha = alpha;

    }



}
