﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO
//includeANimation
//isHealingArea - DONE
//usesBounds
public class AreaDamage : Photon.MonoBehaviour
{
	[Header ("AI options")]
	public float hitRange=3;
	public bool includesAnimation=true;
	public GameObject rootCollider;

	public bool isHealingArea;
    //public bool isExplosive;
    public bool hasParticles;
    ParticleSystem attackParticles;

	[Header("Bounds method means it creates a hit box")]
	public bool usesBounds;
	public GameObject boundsUpperLeft;
	public GameObject boundsLowerRight;

	public AdventurerNetwork currentTarget;

	public EnemyType enemyType = EnemyType.normal;
	[Header ("Use this for heal too!")]
	public float atkCooldown=2;
	PhotonView photonView;

	private bool appliedInitialUpdate;
	public float attackTimer = 99;

	public GameObject rootPosition;
	public string enemyAttackSound;

	SkeletonUtility su;
	public SkeletonAnimation skeletonAnimation;

	private void Awake ()
	{
		su = GetComponent<SkeletonUtility> ();
        if (hasParticles) attackParticles = GetComponentInChildren<ParticleSystem>();
	}
		
	void Start ()
	{
		if (includesAnimation) {
			skeletonAnimation = GetComponentInChildren<SkeletonAnimation> ();
			skeletonAnimation.state.Event += OnEvent;
		}
		photonView = GetComponent<PhotonView> ();
		//try disabling this one
	}

	//WAY POINT IS FOR WHEN NOT PURSUING ENEMY, set a waypoitn and go to it
	public Vector3 wayPoint= new Vector2(0,0);

	private void FixedUpdate ()
	{
	//	if (!photonView.isMine)
		//	return;

		attackTimer -= Time.deltaTime;
		//  Debug.Log("attack timer:" + attackTimer);
			//SetAnimation ("Idle", true, false);

		if (attackTimer <= 0) {
			if (isHealingArea) {
				Debug.Log ("IsHealinAreaHeal");
				Heal ();
			} else {
				TryAttack (false);
			}
			}

		//Reset trial attack / heal
		if (attackTimer <= 0) {
			attackTimer =atkCooldown;// * Random.Range(-currentEntityStat.atkCooldown, currentEntityStat.atkCooldown);
		}
	}

		public void TryAttack (bool force)
	{
		
		if (currentTarget==null)currentTarget=BattleManager.Instance.myAdventurer;
			Attack (force);
	}

	void Heal(){
		if (currentTarget==null)currentTarget=BattleManager.Instance.myAdventurer;
		//Attack (force);
		if (CanReach ()) {
		//	Debug.Log ("CAN REACH AND GONNA HEAL");
			float healAmount = currentTarget.currentEntityStat.hpMax * atkPerc;
			currentTarget.CallHeal ((int)healAmount, false, false);
			currentTarget.photonView.RPC ("CallHealEffect", PhotonTargets.All, currentTarget.photonView.viewID, (int)healAmount, currentTarget.transform.position.x, currentTarget.transform.position.y);
            AudioManager.Instance.PlayClip("heal", true, false);
		}
	}

	public void Attack (bool force)
	{
		//if we dont use animation, we need to process our damage manually
		if (!includesAnimation) {
			ProcessDamage ();
			return;
		}

		SetAnimation ("Attack1", false, false);

	}

	public void Initialize ()
	{
		if (photonView == null)
			photonView = GetComponent<PhotonView> ();
			gameObject.name = gameObject.name + photonView.viewID;
			return;//they dont need any of the rest of this crap

		attackTimer=atkCooldown;

		//Vector3 startingPosition = new Vector3 (0, 0, 0);
		//GameObject playerSpawn0 = GameObject.FindGameObjectWithTag ("PlayerSpawn0");
	}


	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		//if (!photonView.isMine) return;
		if (stream.isWriting) {
			//We own this player: send the others our data
			// stream.SendNext((int)controllerScript._characterState);

			float _atkCooldown = atkCooldown;
			stream.SendNext (_atkCooldown);


		} else {
			//Network player, receive data
			//controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();

			atkCooldown = (float)stream.ReceiveNext ();

			if (!appliedInitialUpdate) {
				appliedInitialUpdate = true;
			}
		}
	}

	private Vector3 correctPlayerPos = Vector3.zero;
	//We lerp towards this
	private Quaternion correctPlayerRot = Quaternion.identity;
	//We lerp towards this

	[PunRPC]
	public void CallSyncAnimation (params object[] args)
	{
		bool isOnComplete;

		string animationName = (string)args [0];
		bool isLoop = (bool)args [1];
		if (args.Length > 2) {//if the sync animation missing the onComplete we set it automatically
			isOnComplete = (bool)args [2];
		} else {
			isOnComplete = false;
		}
		SyncAnimation (animationName, isLoop, isOnComplete);
	}

	[Header("0 to 100")]
	public float critChance=10;
	public bool RollForCritical ()
	{
		bool isCrit = false;
		int rand = Random.Range (0, 100);
		if (rand <= critChance) {
			isCrit = true;
		}
		//Debug.Log("Enemy Attack is Critical:" + isCrit);
		return isCrit;
	}

	[PunRPC]
	public void AttackGraphic ()
	{
		GameObject obj = ObjectPool.Instance.NextObject ("YellowHit");
		Vector3 newPos = rootPosition.transform.position;
		newPos.z -= 1;
		obj.transform.position = newPos;
		obj.SetActive (true);
	}

	public void InvokeCallSyncStatMaster (float delay)
	{
		Invoke ("CallSyncStatMaster", delay);
	}

	void CallSyncStatMaster ()
	{
		photonView.RPC ("SyncStatMaster", PhotonTargets.MasterClient, photonView.viewID);

	}


	[PunRPC]
	public void TurnOffCollider ()
	{
		ClickableEnemy ce = GetComponentInChildren<ClickableEnemy> ();
		ce.myCollider.enabled = false;
	}

	public void TurnOnCollider ()
	{
		ClickableEnemy ce = GetComponentInChildren<ClickableEnemy> ();
		ce.myCollider.enabled = true;
	}

	public void OnPhotonPlayerDisconnected ()
	{
		attackTimer = atkCooldown;
	}

	/// <summary>
	/// ANIMATION STUFF
	public string currentAnimation;
	public int maxAttacks;

	public void SyncAnimation(string animation, bool isLoop, bool isOnComplete)
	{
		///END OF TEST BLOCK
		if (animation == currentAnimation) return;//no need to try setting repeated animation of same kind
		SetAnimation(animation, isLoop, isOnComplete);
	}

	public  void SetAnimation(string animation, bool isLoop, bool isOnComplete)
	{
		//call the same animation for all others
		object[] args = new object[] { animation, isLoop };
		if (photonView==null)photonView = GetComponent<PhotonView> ();

		//photonView.RPC("CallSyncAnimation", PhotonTargets.Others, args);
	//	if (!photonView.isMine) return;
		//Debug.Log("Set Animation:" + animation);

		//if (animation == currentAnimation) return;//already playing it on this track
		skeletonAnimation.loop = isLoop;
		skeletonAnimation.state.SetAnimation(0, animation, isLoop);
		currentAnimation = animation;
	}

	void OnEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
	{
		//print("OnEvent:"+e.Data.name+"  for Animation:" + skeletonAnimation.state.GetCurrent(trackIndex).animation.name +" TrackIndex:"+ trackIndex);
		if (e.Data.name == "OnComplete")
		{
			string animationName = skeletonAnimation.state.GetCurrent(trackIndex).animation.name;
			AnimationComplete(trackIndex, animationName);
		}

		if (e.Data.name == "OnHit")
		{
			string animationName = skeletonAnimation.state.GetCurrent(trackIndex).animation.name;
			if (animationName == "Attack1") {
				ProcessDamage ();
			}
            if (hasParticles)
            {
                attackParticles.Play();
            }
		}
	}

	void ProcessDamage(){
		//hit the local?
		if(!currentTarget)return;
	
		if (CanReach()) {
			//Debug.Log ("Spikes hurt player");
			CameraEffects ce = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraEffects> ();
			ce.CallCameraShake (.6f);
			ProcessDamage (currentTarget);
		}
	}

	bool CanReach(){
		//USES RANGE
		if (currentTarget==null)currentTarget=BattleManager.Instance.myAdventurer;

		if (!usesBounds) {
			
			if (Vector3.Distance (currentTarget.footLocation.transform.position, rootCollider.transform.position) <= hitRange) {
				return true;
			}else{
				return false;
				}
		} 

		//USES BOUNDS
			if (currentTarget.footLocation.transform.position.x >= boundsUpperLeft.transform.position.x
			& currentTarget.footLocation.transform.position.x <= boundsLowerRight.transform.position.x
			& currentTarget.footLocation.transform.position.y <= boundsUpperLeft.transform.position.y
			& currentTarget.footLocation.transform.position.y >= boundsLowerRight.transform.position.y) {
				//we are in bounds
			} else {
				//Debug.Log ("We are outside the bounds");
				return false;
			}

		return true;
		}



	public float atkPerc;
	void ProcessDamage(AdventurerNetwork playerTargetted){
float damage = currentTarget.currentEntityStat.hpMax*atkPerc;

		bool isCrit = false;
		object[] args = new object[] { damage, currentTarget.photonView.viewID, false, isCrit,false };
		playerTargetted.photonView.RPC("TakeDamageMaster", PhotonTargets.MasterClient, args);
		AudioManager.Instance.PlayClip(enemyAttackSound);
	}

	public  void AnimationComplete(int trackIndex, string animationName)
	{
		//print("Enemy animation complete:" + animationName);
		if (!photonView.isMine) return;

		switch (animationName)
		{
		case "Attack1":
			SetAnimation("Idle", true, true);
			break;
		}
	}


}

public enum EnemyType
{
	normal,
	boss
}

