﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour {
	PhotonView m_PhotonView;

	void Awake(){
		m_PhotonView = GetComponent<PhotonView> ();
	}

	void OnEnable ()
	{
		EventManager.OnGuillotineDropped += Reset;
        Debug.Log("ENABLED BARRIERS");
        GetComponent<BoxCollider2D>().enabled = true; //it should always start with BC on
        GetComponent<ParticleSystem>().Play();

    }

    void OnDisable ()
	{
		EventManager.OnGuillotineDropped -= Reset;
	}

	void Reset(){
		GetComponent<BoxCollider2D>().enabled=true;
		GetComponent<ParticleSystem> ().Play ();

	}


[PunRPC]
	public void RemoveBarrier(int viewID){
		if (viewID != m_PhotonView.viewID)
			return;
		
		GetComponent<BoxCollider2D>().enabled=false;
		GetComponent<ParticleSystem> ().Stop ();

       // CameraEffects ce = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraEffects>();
       // ce.CallCameraShake(.3f);
        AudioManager.Instance.PlayClip("boss", true, false);

        //remove the barrier gracefully for all
    }
}
