﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTreeCanvas : MonoBehaviour
{
    public List<Skill> skillDatabase = new List<Skill>();
    public GameObject enviroGraphics;
    public Text skillPoints;
    public Text permSkillPoints;
    public SkillTreeToolTip skillTreeToolTip;

    #region singleton

    private static SkillTreeCanvas _instance;

    public static SkillTreeCanvas Instance
    {
        get
        {
            //If _instance is null then we find it from the scene 
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<SkillTreeCanvas>();
            return _instance;
        }
    }
    //Singleton complete

    #endregion

    #region CanvasSetup

    public CanvasTools ct;

    void Awake()
    {
        ct = GetComponent<CanvasTools>();

        //Any Custom World Space Objects? Add them here
        ct.AddWorldSpaceObject(TownManager.Instance.skillTreeEnviroCuzItsTurnedOff);
        ct.AddOpenAction(EventManager.Instance.CallSkillTreeRefreshed);
        ct.AddCloseAction(MenuCanvas.Instance.ToggleMe);
    }

    #endregion

    void OnEnable()
    {
        EventManager.OnSkillTreeRefreshed += SetSkillPoints;
    }

    void OnDisable()
    {
        EventManager.OnSkillTreeRefreshed += SetSkillPoints;
    }

    void SetSkillPoints()
    {
        skillPoints.text = "Skill Points: " + GameManager.Instance.skillPoints;
        permSkillPoints.text = "Permanent Skill Points: " + GameManager.Instance.permSkillPoints;

    }

    public void CloseButton()
    {
        ct.ToggleMe(false);
    }

    #region Manager Code
    public void AddSkill(Skill skillToAdd)
    {
        //you have unlocked a skill via the tree
        //GameManager.Instance.skillsOwned.Add (newSkill);
    }

    public void CallSkill(SkillType _st, int _sid)
    {
        if (_st == SkillType.Active)
        {
            //Get the skill power from gamemanager probably
            //AudioManager.Instance.PlayClip ("heal", false, false);
            //adv.photonView.RPC ("CallJuicedEffect", PhotonTargets.All, adv.photonView.viewID, adv.transform.position.x, adv.transform.position.y);
            //StartCoroutine (RemoveSkill (adv, 10, _st));
        }
        if (_st == SkillType.Ordinary_Passive)
        {
        }
        if (_st == SkillType.Special_Passive)
        {
        }
        if (_st == SkillType.Unique_Passive)
        {
        }
        if (_st == SkillType.Upgrade_Active)
        {
        }
    }

    IEnumerator RemoveSkill(AdventurerNetwork adv, float duration, SkillType st)
    {
        yield return new WaitForSeconds(duration);
        //adv.photonView.RPC ("CallRemoveJuicedEffect", PhotonTargets.All, adv.photonView.viewID, adv.transform.position.x, adv.transform.position.y);
    }

    #endregion
}


[System.Serializable]
public class Skill
{
    public int skillId;
    public string skillName;
    public string skillHeader;
    public string skillDesc;
    public SkillType skillType;
    //public float skillLevel; //maybe we dont need this. just keep one record of active skills at all times
    public SkillStats stat;
}

[System.Serializable]
public class SkillStats
{
    public float atkPerc;
    public float hpPerc;
    public float defPerc;
    public float critPerc;
    public float cooldownSeconds;
    public float cost;
}


[System.Serializable]
public enum SkillType
{
    Ordinary_Passive,
    Unique_Passive,
    Special_Passive,
    Upgrade_Active,
    Active
}