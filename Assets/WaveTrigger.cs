﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveTrigger : MonoBehaviour
{
    public List<Wave> waveset;
    public PhotonView m_PhotonView;
    public bool includesAnimation;
    public string animationAudio;
	public GameObject bossTrapBarrier;
	public GameObject bossSpawnPos;

    void Awake()
    {
        m_PhotonView = GetComponent<PhotonView>();
    }

    void OnEnable()
    {
        ResetTrigger();
        EventManager.OnGuillotineDropped += ResetTrigger;
		EventManager.OnBossDefeated += ClearBossBarrier;

    }

    void OnDisable()
    {
        EventManager.OnGuillotineDropped -= ResetTrigger;
		EventManager.OnBossDefeated -= ClearBossBarrier;

    }

    void ResetTrigger()
    {
        Debug.Log("RESET THE WAAVES!");
        ClearBossBarrier();
        GetComponent<BoxCollider2D>().enabled = true;
        isTriggered = false;
    }


    //on trigger enter, send to the battlemanager to start wave!
    void OnTriggerEnter2D(Collider2D other)
    {
        if (BattleManager.Instance.waveSetActivated) return;//PREVENT MULTIPLE TRIGGERING
        if (!GameManager.Instance.InputEnabled) return;//prevent accidental entry trigger of waves
        //BattleManager.Instance.SpawnWave (wave);
        if (other.tag == "Adventurer")
        {
            Debug.Log("##### adventurer:" + other.gameObject.name);
            Debug.Log("####### Activated WaveTrigger:" + gameObject.transform.parent.name);
            m_PhotonView.RPC("DisableTrigger", PhotonTargets.All, m_PhotonView.viewID);
            m_PhotonView.RPC("CallTriggered", PhotonTargets.MasterClient, m_PhotonView.viewID);
			//Check Waveset for boss
			foreach (Wave w in waveset) {
				if (w.isBoss) {
					m_PhotonView.RPC ("SetBossBarrier", PhotonTargets.All);
					m_PhotonView.RPC ("SetBossSpawn", PhotonTargets.Others);
				}
			}
        }
    }


    [PunRPC]
    public void DisableTrigger(int viewId)
    {
        if (viewId != m_PhotonView.viewID)
            return;

        GetComponent<BoxCollider2D>().enabled = false;
    }

    public SkeletonAnimation skeletonAnimation;
    bool isTriggered = false;

    [PunRPC]
    public void CallTriggered(int viewId)
    {
        if (viewId != m_PhotonView.viewID)
            return;

        if (isTriggered)
            return;

        isTriggered = true;

        if (includesAnimation)
        {
            m_PhotonView.RPC("CallAnimationAndSound", PhotonTargets.All, m_PhotonView.viewID);
        }

        BattleManager.Instance.StartWaveset(waveset);


    }

	[PunRPC]
	public void SetBossBarrier(){
        if (!bossTrapBarrier) return;
		bossTrapBarrier.SetActive (true);
	}

	[PunRPC]
	public void SetBossSpawn(){
		Invoke ("DelayedSetBossSpawn", 1);
	}
	void DelayedSetBossSpawn(){
        if (bossSpawnPos == null)
        {
            bossSpawnPos = new GameObject();
            bossSpawnPos.transform.position = transform.position;
        }
		BattleManager.Instance.myAdventurer.transform.position = bossSpawnPos.transform.position;

	}


	void ClearBossBarrier(){
		foreach (Wave w in waveset) {
			if (w.isBoss) {
				if(bossTrapBarrier) bossTrapBarrier.SetActive (false);
			}
		}
	}


    [PunRPC]
    public void CallAnimationAndSound(int viewId)
    {
        if (m_PhotonView.viewID != viewId) return;
        //now call button press and a sound with it

        skeletonAnimation.state.SetAnimation(0, "Death", false);
        AudioManager.Instance.PlayClip(animationAudio, true, false);
    }

}
